

truncate data_billing_full;
truncate temp_log_data_unoerp;

SET @FECHA_INF=DATE("2020-11-01");
SET @FECHA_SUP=DATE("2020-11-07");


/*-------------------5. FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL IMPALA SIN PRESENCIA ICA -------------------*/
INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee, ' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    sum(rfs.qta_coffee) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL DE IMPALA SIN PRESENCIA ICA" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*SUM(rfs.qta_coffee) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
INNER JOIN remittances_caffee_has_fumigation_services AS rfs ON fs.id = rfs.fumigation_services_id
INNER JOIN remittances_caffee AS rm ON rfs.remittances_caffee_id = rm.id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join items_unoee pds on pds.codigo='0000023'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000023','001')
WHERE (fs.departament_id=10) AND fs.completed=1 AND fs.ica=0 AND rm.client_id !=1
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
GROUP BY fs.id
ORDER BY cts.exporter_code ASC
); 



/*-------------------6. HORAS DE ELEVADOR SERVICIO FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL IMPALA SIN PRESENCIA ICA -------------------*/
INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee, ' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    fs.hour_elevator AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "HORAS DE ELEVADOR SERVICIO DE FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL DE IMPALA SIN PRESENCIA ICA" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*fs.hour_elevator AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
INNER JOIN remittances_caffee_has_fumigation_services AS rfs ON fs.id = rfs.fumigation_services_id
INNER JOIN remittances_caffee AS rm ON rfs.remittances_caffee_id = rm.id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join items_unoee pds on pds.codigo='0000006'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000006','001')
WHERE (fs.departament_id=10) AND fs.completed=1 AND fs.ica=0 AND fs.hour_elevator !=0 AND rm.client_id !=1
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
GROUP BY fs.id
ORDER BY cts.exporter_code ASC
); 



/*-------------------7. FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL IMPALA CON PRESENCIA ICA -------------------*/
INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee, ' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    sum(rfs.qta_coffee) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL DE IMPALA CON PRESENCIA ICA" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*SUM(rfs.qta_coffee) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
INNER JOIN remittances_caffee_has_fumigation_services AS rfs ON fs.id = rfs.fumigation_services_id
INNER JOIN remittances_caffee AS rm ON rfs.remittances_caffee_id = rm.id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join items_unoee pds on pds.codigo='0000092'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000092','001')
WHERE (fs.departament_id=10) AND fs.completed=1 AND fs.ica=1 AND rm.client_id !=1
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
GROUP BY fs.id
ORDER BY cts.exporter_code ASC
); 

/*-------------------2.RECOBRO FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL IMPALA CON PRESENCIA ICA HASTA 100 TON-------------------*/
INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee, ' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "RECOBRO FUMIGACION ICA PARTICULARES HASTA 100 TON" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
INNER JOIN remittances_caffee_has_fumigation_services AS rfs ON fs.id = rfs.fumigation_services_id
INNER JOIN remittances_caffee AS rm ON rfs.remittances_caffee_id = rm.id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join items_unoee pds on pds.codigo='0000091'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000091','001')
WHERE (fs.departament_id=10) AND fs.completed=1 AND fs.ica=1 AND rm.client_id !=1
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  ) AND (fs.qta_bags*70/1000) <= 100
GROUP BY fs.id
ORDER BY cts.exporter_code ASC
); 


/*-------------------1. HORAS ELEVADOR PARA FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL IMPALA CON PRESENCIA ICA HASTA 100 TON-------------------*/
INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee, ' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    fs.hour_elevator AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "HORAS ELEVADOR DE FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL DE IMPALA CON PRESENCIA ICA" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*fs.hour_elevator AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
INNER JOIN remittances_caffee_has_fumigation_services AS rfs ON fs.id = rfs.fumigation_services_id
INNER JOIN remittances_caffee AS rm ON rfs.remittances_caffee_id = rm.id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join items_unoee pds on pds.codigo='0000006'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000006','001')
WHERE (fs.departament_id=10) AND fs.completed=1 AND fs.ica=1 AND fs.hour_elevator !=0 AND rm.client_id !=1
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  ) AND (fs.qta_bags*70/1000) <= 100
GROUP BY fs.id
ORDER BY cts.exporter_code ASC
);


/*-------------------3. RECOBRO DE FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL IMPALA CON PRESENCIA ICA DE 100 HASTA 200 TON-------------------*/
INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee, ' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "RECOBRO FUMIGACION ICA DESDE 100 HASTA 200 TON" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
INNER JOIN remittances_caffee_has_fumigation_services AS rfs ON fs.id = rfs.fumigation_services_id
INNER JOIN remittances_caffee AS rm ON rfs.remittances_caffee_id = rm.id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join items_unoee pds on pds.codigo='0000301'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000301','001')
WHERE (fs.departament_id=10) AND fs.completed=1 AND fs.ica=1 AND rm.client_id !=1
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  ) AND ( (fs.qta_bags*70/1000) between 100 and 200)
GROUP BY fs.id
ORDER BY cts.exporter_code ASC
);



/*-------------------4. HORAS ELEVADOR PARA FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL IMPALA CON PRESENCIA ICA DE 100 HASTA 200 TON-------------------*/
INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee, ' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    fs.hour_elevator AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "HORAS ELEVADOR DE FUMIGACION ICA PARTICULAR DESDE 100 HASTA 200 TON" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*hour_elevator AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
INNER JOIN remittances_caffee_has_fumigation_services AS rfs ON fs.id = rfs.fumigation_services_id
INNER JOIN remittances_caffee AS rm ON rfs.remittances_caffee_id = rm.id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join items_unoee pds on pds.codigo='0000006'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000006','001')
WHERE (fs.departament_id=10) AND fs.completed=1 AND fs.ica=1 AND fs.hour_elevator !=0 AND rm.client_id !=1
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  ) AND ( (fs.qta_bags*70/1000) between 100 and 200)
GROUP BY fs.id
ORDER BY cts.exporter_code ASC
);






/* --FNC--*/


/*-------------------5. FUMIGACION DE CAFE PARA FNC EN TERMINAL IMPALA SIN PRESENCIA ICA -------------------*/
INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee, ' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    sum(rfs.qta_coffee) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA FNC EN TERMINAL DE IMPALA SIN PRESENCIA ICA" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*SUM(rfs.qta_coffee) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
INNER JOIN remittances_caffee_has_fumigation_services AS rfs ON fs.id = rfs.fumigation_services_id
INNER JOIN remittances_caffee AS rm ON rfs.remittances_caffee_id = rm.id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join items_unoee pds on pds.codigo='0000023'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000023','007')
WHERE (fs.departament_id=10) AND fs.completed=1 AND fs.ica=0 AND rm.client_id=1
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
GROUP BY fs.id
ORDER BY cts.exporter_code ASC
); 



/*-------------------6. HORAS DE ELEVADOR SERVICIO FUMIGACION DE CAFE PARA FNC EN TERMINAL IMPALA SIN PRESENCIA ICA -------------------*/
INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee, ' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    fs.hour_elevator AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "HORAS DE ELEVADOR SERVICIO DE FUMIGACION DE CAFE PARA FNC EN TERMINAL DE IMPALA SIN PRESENCIA ICA" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*fs.hour_elevator AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
INNER JOIN remittances_caffee_has_fumigation_services AS rfs ON fs.id = rfs.fumigation_services_id
INNER JOIN remittances_caffee AS rm ON rfs.remittances_caffee_id = rm.id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join items_unoee pds on pds.codigo='0000006'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000006','007')
WHERE (fs.departament_id=10) AND fs.completed=1 AND fs.ica=0 AND fs.hour_elevator !=0 AND rm.client_id=1
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
GROUP BY fs.id
ORDER BY cts.exporter_code ASC
); 



/*-------------------7. FUMIGACION DE CAFE PARA FNC EN TERMINAL IMPALA CON PRESENCIA ICA -------------------*/
INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee, ' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    sum(rfs.qta_coffee) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA FNC EN TERMINAL DE IMPALA CON PRESENCIA ICA" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*SUM(rfs.qta_coffee) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
INNER JOIN remittances_caffee_has_fumigation_services AS rfs ON fs.id = rfs.fumigation_services_id
INNER JOIN remittances_caffee AS rm ON rfs.remittances_caffee_id = rm.id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join items_unoee pds on pds.codigo='0000092'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000092','007')
WHERE (fs.departament_id=10) AND fs.completed=1 AND fs.ica=1 AND rm.client_id=1
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
GROUP BY fs.id
ORDER BY cts.exporter_code ASC
); 

/*-------------------2. RECOBRO FUMIGACION DE CAFE PARA FNC EN TERMINAL IMPALA CON PRESENCIA ICA HASTA 100 TON-------------------*/
INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee, ' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "RECOBRO FUMIGACION ICA FNC HASTA 100 TON" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
INNER JOIN remittances_caffee_has_fumigation_services AS rfs ON fs.id = rfs.fumigation_services_id
INNER JOIN remittances_caffee AS rm ON rfs.remittances_caffee_id = rm.id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join items_unoee pds on pds.codigo='0000091'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000091','007')
WHERE (fs.departament_id=10) AND fs.completed=1 AND fs.ica=1 AND rm.client_id=1
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  ) AND (fs.qta_bags*70/1000) <= 100
GROUP BY fs.id
ORDER BY cts.exporter_code ASC
); 


/*-------------------1. HORAS ELEVADOR PARA FUMIGACION DE CAFE PARA FNC EN TERMINAL IMPALA CON PRESENCIA ICA HASTA 100 TON-------------------*/
INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee, ' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    fs.hour_elevator AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "HORAS ELEVADOR DE FUMIGACION DE CAFE PARA FNC EN TERMINAL DE IMPALA CON PRESENCIA ICA" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*fs.hour_elevator AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
INNER JOIN remittances_caffee_has_fumigation_services AS rfs ON fs.id = rfs.fumigation_services_id
INNER JOIN remittances_caffee AS rm ON rfs.remittances_caffee_id = rm.id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join items_unoee pds on pds.codigo='0000006'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000006','007')
WHERE (fs.departament_id=10) AND fs.completed=1 AND fs.ica=1 AND fs.hour_elevator !=0 AND rm.client_id=1
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  ) AND (fs.qta_bags*70/1000) <= 100
GROUP BY fs.id
ORDER BY cts.exporter_code ASC
);


/*-------------------3. RECOBRO DE FUMIGACION DE CAFE PARA FNC EN TERMINAL IMPALA CON PRESENCIA ICA DE 100 HASTA 200 TON-------------------*/
INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee, ' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "RECOBRO FUMIGACION ICA FNC DESDE 100 HASTA 200 TON" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
INNER JOIN remittances_caffee_has_fumigation_services AS rfs ON fs.id = rfs.fumigation_services_id
INNER JOIN remittances_caffee AS rm ON rfs.remittances_caffee_id = rm.id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join items_unoee pds on pds.codigo='0000301'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000301','007')
WHERE (fs.departament_id=10) AND fs.completed=1 AND fs.ica=1 AND rm.client_id=1
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  ) AND ( (fs.qta_bags*70/1000) between 100 and 200)
GROUP BY fs.id
ORDER BY cts.exporter_code ASC
);

 

/*-------------------4. HORAS ELEVADOR PARA FUMIGACION DE CAFE PARA FNC EN TERMINAL IMPALA CON PRESENCIA ICA DE 100 HASTA 200 TON-------------------*/
INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee, ' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    fs.hour_elevator AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "HORAS ELEVADOR DE FUMIGACION ICA FNC DESDE 100 HASTA 200 TON" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*hour_elevator AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
INNER JOIN remittances_caffee_has_fumigation_services AS rfs ON fs.id = rfs.fumigation_services_id
INNER JOIN remittances_caffee AS rm ON rfs.remittances_caffee_id = rm.id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join items_unoee pds on pds.codigo='0000006'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000006','007')
WHERE (fs.departament_id=10) AND fs.completed=1 AND fs.ica=1 AND rm.client_id=1
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  ) AND ( (fs.qta_bags*70/1000) between 100 and 200)
GROUP BY fs.id
ORDER BY cts.exporter_code ASC
);


