
/*------------------- SET @DIAS_DE_FACTURACION=5; -------------------*/

/*------------------- SET @FECHA_INF=DATE(DATE_SUB(NOW(), INTERVAL @DIAS_DE_FACTURACION DAY)); -------------------*/
/*------------------- SET @FECHA_SUP=DATE(DATE_SUB(NOW(), INTERVAL 1 DAY)); -------------------*/
SET @FECHA_INF=DATE("2020-12-01");
SET @FECHA_SUP=DATE("2020-12-08");
 


/*------------------- EMBALAJE IMPALA CONTENEDORES DE 20  -------------------*/
/*------------------- EMBALAJE IMPALA CONTENEDORES TIPO SACOS DE 20 CON PAPEL KRAFT -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    vp.expotador_code AS CODIGO_EXPORTADOR,
    vp.exportador AS NOMBRE_EXPORTADOR,
    vp.lotes AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.packaging_type AS TIPO_SACOS,
    vp.packaging_mode AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.packaging_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.oie AS OIE, 
    vp.bic_ctn AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    vp.tipo_papel AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40, 
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.estado_embalaje AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    NULL AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "EMBALAJE DE CONTENEDORES EN TERMINAL IMPALA TIPO SACOS DE LONGITUD 20 PAPEL TIPO KRAFT" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging AS vp 
INNER JOIN clients AS cts ON cts.exporter_code = vp.expotador_code
inner join items_unoee pds on pds.codigo='0000160'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000160','001')
WHERE   (  DATE(vp.packaging_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
        AND vp.jetty = 'IMP'  AND vp.packaging_mode = 'SACOS' 
        AND (vp.state_packaging_id=3 or vp.state_packaging_id=9 or vp.state_packaging_id=10) 
        AND (vp.iso_ctn between 19 and 30 ) 
        AND vp.tipo_papel = "KRAFT"
ORDER BY vp.expotador_code ASC
); 


/*------------------- EMBALAJE IMPALA CONTENEDORES TIPO SACOS DE 20 CON PAPEL CARTON -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    vp.expotador_code AS CODIGO_EXPORTADOR,
    vp.exportador AS NOMBRE_EXPORTADOR,
    vp.lotes AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.packaging_type AS TIPO_SACOS,
    vp.packaging_mode AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.packaging_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.oie AS OIE, 
    vp.bic_ctn AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    vp.tipo_papel AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.estado_embalaje AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    NULL AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "EMBALAJE DE CONTENEDORES EN TERMINAL IMPALA TIPO SACOS DE LONGITUD 20 PAPEL TIPO CARTON" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging AS vp 
INNER JOIN clients AS cts ON cts.exporter_code = vp.expotador_code
inner join items_unoee pds on pds.codigo='0000162'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000162','001')
WHERE   (  DATE(vp.packaging_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
        AND vp.jetty = 'IMP'  AND vp.packaging_mode = 'SACOS' 
        AND (vp.state_packaging_id=3 or vp.state_packaging_id=9 or vp.state_packaging_id=10) 
        AND (vp.iso_ctn between 19 and 30 ) 
        AND vp.tipo_papel = "CARTON"
ORDER BY vp.expotador_code ASC
); 


/*------------------- EMBALAJE IMPALA CONTENEDORES TIPO GRANEL DE 20 TIPO KRAFT  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    vp.expotador_code AS CODIGO_EXPORTADOR,
    vp.exportador AS NOMBRE_EXPORTADOR,
    vp.lotes AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.packaging_type AS TIPO_SACOS,
    vp.packaging_mode AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.packaging_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.oie AS OIE,
    vp.bic_ctn AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    vp.tipo_papel AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.estado_embalaje AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    NULL AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "EMBALAJE DE CONTENEDORES EN TERMINAL IMPALA TIPO GRANEL DE LONGITUD 20 TIPO PAPEL KRAFT" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging AS vp 
INNER JOIN clients AS cts ON cts.exporter_code = vp.expotador_code
inner join items_unoee pds on pds.codigo='0000168'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000168','001')
WHERE   (  DATE(vp.packaging_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
        AND vp.jetty = 'IMP'  AND vp.packaging_mode = 'granel' 
        AND (vp.state_packaging_id=3 or vp.state_packaging_id=9 or vp.state_packaging_id=10) 
        AND (vp.iso_ctn between 19 and 30 ) 
        AND vp.tipo_papel = "KRAFT"
ORDER BY vp.expotador_code ASC
); 


/*------------------- EMBALAJE IMPALA CONTENEDORES TIPO GRANEL DE 20 TIPO CARTON  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    vp.expotador_code AS CODIGO_EXPORTADOR,
    vp.exportador AS NOMBRE_EXPORTADOR,
    vp.lotes AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.packaging_type AS TIPO_SACOS,
    vp.packaging_mode AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.packaging_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.oie AS OIE,
    vp.bic_ctn AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    vp.tipo_papel AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.estado_embalaje AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    NULL AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "EMBALAJE DE CONTENEDORES EN TERMINAL IMPALA TIPO GRANEL DE LONGITUD 20 TIPO PAPEL CARTON" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging AS vp 
INNER JOIN clients AS cts ON cts.exporter_code = vp.expotador_code
inner join items_unoee pds on pds.codigo='0000169'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000169','001')
WHERE   (  DATE(vp.packaging_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
        AND vp.jetty = 'IMP'  AND vp.packaging_mode = 'granel' 
        AND (vp.state_packaging_id=3 or vp.state_packaging_id=9 or vp.state_packaging_id=10) 
        AND (vp.iso_ctn between 19 and 30 ) 
        AND vp.tipo_papel = "CARTON"
ORDER BY vp.expotador_code ASC
); 


/*------------------- EMBALAJE IMPALA CONTENEDORES TIPO COMBINADOS SACOS - CAJAS DE 20 TIPO PAPEL KRAFT -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    vp.expotador_code AS CODIGO_EXPORTADOR,
    vp.exportador AS NOMBRE_EXPORTADOR,
    vp.lotes AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.packaging_type AS TIPO_SACOS,
    vp.packaging_mode AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.packaging_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.oie AS OIE,
    vp.bic_ctn AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    vp.tipo_papel AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.estado_embalaje AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    NULL AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "EMBALAJE DE CONTENEDORES EN TERMINAL IMPALA TIPO COMBINADO(SACOS - CAJAS) DE LONGITUD 20 TIPO PAPEL KRAFT" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging AS vp 
INNER JOIN clients AS cts ON cts.exporter_code = vp.expotador_code
inner join items_unoee pds on pds.codigo='0000291'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000291','001')
WHERE   (  DATE(vp.packaging_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
        AND vp.jetty = 'IMP'  AND vp.packaging_mode = 'COMBINADO(SACOS - CAJAS)' 
        AND (vp.state_packaging_id=3 or vp.state_packaging_id=9 or vp.state_packaging_id=10) 
        AND (vp.iso_ctn between 19 and 30 ) 
        AND vp.tipo_papel = "KRAFT"
ORDER BY vp.expotador_code ASC
); 

/*------------------- EMBALAJE IMPALA CONTENEDORES TIPO COMBINADOS SACOS - CAJAS DE 20 TIPO PAPEL CARTON -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    vp.expotador_code AS CODIGO_EXPORTADOR,
    vp.exportador AS NOMBRE_EXPORTADOR,
    vp.lotes AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.packaging_type AS TIPO_SACOS,
    vp.packaging_mode AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.packaging_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.oie AS OIE,
    vp.bic_ctn AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    vp.tipo_papel AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.estado_embalaje AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    NULL AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "EMBALAJE DE CONTENEDORES EN TERMINAL IMPALA TIPO COMBINADO(SACOS - CAJAS) DE LONGITUD 20 TIPO PAPEL CARTON" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging AS vp 
INNER JOIN clients AS cts ON cts.exporter_code = vp.expotador_code
inner join items_unoee pds on pds.codigo='0000292'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000292','001')
WHERE   (  DATE(vp.packaging_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
        AND vp.jetty = 'IMP'  AND vp.packaging_mode = 'COMBINADO(SACOS - CAJAS)' 
        AND (vp.state_packaging_id=3 or vp.state_packaging_id=9 or vp.state_packaging_id=10) 
        AND (vp.iso_ctn between 19 and 30 ) 
        AND vp.tipo_papel = "CARTON"
ORDER BY vp.expotador_code ASC
); 

/*------------------- EMBALAJE IMPALA CONTENEDORES TIPO CONBINADOS SACOS - BIG BGAS DE 20 TIPO PAPEL KRAFT -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    vp.expotador_code AS CODIGO_EXPORTADOR,
    vp.exportador AS NOMBRE_EXPORTADOR,
    vp.lotes AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.packaging_type AS TIPO_SACOS,
    vp.packaging_mode AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.packaging_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.oie AS OIE,
    vp.bic_ctn AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    vp.tipo_papel AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.estado_embalaje AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    NULL AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "EMBALAJE DE CONTENEDORES EN TERMINAL IMPALA TIPO COMBINADO(SACOS - BIG BAGS) DE LONGITUD 20 TIPO PAPEL KRAFT" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1  AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging AS vp 
INNER JOIN clients AS cts ON cts.exporter_code = vp.expotador_code
inner join items_unoee pds on pds.codigo='0000291'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000291','001')
WHERE   (  DATE(vp.packaging_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
        AND vp.jetty = 'IMP'  AND vp.packaging_mode = 'COMBINADO(SACOS - BIG BAGS)' 
        AND (vp.state_packaging_id=3 or vp.state_packaging_id=9 or vp.state_packaging_id=10) 
        AND (vp.iso_ctn between 19 and 30 ) 
        AND vp.tipo_papel = "KRAFT"
ORDER BY vp.expotador_code ASC
); 

/*------------------- EMBALAJE IMPALA CONTENEDORES TIPO CONBINADOS SACOS - BIG BGAS DE 20 TIPO PAPEL CARTON -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    vp.expotador_code AS CODIGO_EXPORTADOR,
    vp.exportador AS NOMBRE_EXPORTADOR,
    vp.lotes AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.packaging_type AS TIPO_SACOS,
    vp.packaging_mode AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.packaging_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.oie AS OIE,
    vp.bic_ctn AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    vp.tipo_papel AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.estado_embalaje AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    NULL AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "EMBALAJE DE CONTENEDORES EN TERMINAL IMPALA TIPO COMBINADO(SACOS - BIG BAGS) DE LONGITUD 20 TIPO PAPEL CARTON" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1  AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging AS vp 
INNER JOIN clients AS cts ON cts.exporter_code = vp.expotador_code
inner join items_unoee pds on pds.codigo='0000292'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000292','001')
WHERE   (  DATE(vp.packaging_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
        AND vp.jetty = 'IMP'  AND vp.packaging_mode = 'COMBINADO(SACOS - BIG BAGS)' 
        AND (vp.state_packaging_id=3 or vp.state_packaging_id=9 or vp.state_packaging_id=10) 
        AND (vp.iso_ctn between 19 and 30 ) 
        AND vp.tipo_papel = "CARTON"
ORDER BY vp.expotador_code ASC
); 



/*------------------- EMBALAJE IMPALA CONTENEDORES TIPO CAJAS DE 20  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    vp.expotador_code AS CODIGO_EXPORTADOR,
    vp.exportador AS NOMBRE_EXPORTADOR,
    vp.lotes AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.packaging_type AS TIPO_SACOS,
    vp.packaging_mode AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.packaging_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.oie AS OIE,
    vp.bic_ctn AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    vp.tipo_papel AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.estado_embalaje AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    NULL AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "EMBALAJE DE CONTENEDORES EN TERMINAL IMPALA TIPO CAJAS DE LONGITUD 20" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging AS vp 
INNER JOIN clients AS cts ON cts.exporter_code = vp.expotador_code
inner join items_unoee pds on pds.codigo='0000041'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000041','001')
WHERE   (  DATE(vp.packaging_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
        AND vp.jetty = 'IMP'  AND vp.packaging_mode = 'Cajas' AND (vp.state_packaging_id=3 or vp.state_packaging_id=9 or vp.state_packaging_id=10) AND (vp.iso_ctn between 19 and 30 ) 
ORDER BY vp.expotador_code ASC
);  

/*------------------- EMBALAJE IMPALA CONTENEDORES TIPO CACAO DE 20  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    vp.expotador_code AS CODIGO_EXPORTADOR,
    vp.exportador AS NOMBRE_EXPORTADOR,
    vp.lotes AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.packaging_type AS TIPO_SACOS,
    vp.packaging_mode AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.packaging_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.oie AS OIE,
    vp.bic_ctn AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    vp.tipo_papel AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.estado_embalaje AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    NULL AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "EMBALAJE DE CONTENEDORES EN TERMINAL IMPALA TIPO CACAO DE LONGITUD 20" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging AS vp 
INNER JOIN clients AS cts ON cts.exporter_code = vp.expotador_code
inner join items_unoee pds on pds.codigo='0000050'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000050','001')
WHERE   (  DATE(vp.packaging_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
        AND vp.jetty = 'IMP'  AND vp.packaging_mode = 'CACAO' AND (vp.state_packaging_id=3 or vp.state_packaging_id=9 or vp.state_packaging_id=10) AND (vp.iso_ctn between 19 and 30 ) 
ORDER BY vp.expotador_code ASC
); 

/*------------------- EMBALAJE IMPALA CONTENEDORES TIPO BIG BAGS DE 20  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    vp.expotador_code AS CODIGO_EXPORTADOR,
    vp.exportador AS NOMBRE_EXPORTADOR,
    vp.lotes AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.packaging_type AS TIPO_SACOS,
    vp.packaging_mode AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.packaging_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.oie AS OIE,
    vp.bic_ctn AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    vp.tipo_papel AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.estado_embalaje AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    NULL AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "EMBALAJE DE CONTENEDORES EN TERMINAL IMPALA TIPO BIG BAGS DE LONGITUD 20" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging AS vp 
INNER JOIN clients AS cts ON cts.exporter_code = vp.expotador_code
inner join items_unoee pds on pds.codigo='0000172'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000172','001')
WHERE   (  DATE(vp.packaging_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
        AND vp.jetty = 'IMP'  AND vp.packaging_mode LIKE 'BIG BAG%' AND (vp.state_packaging_id=3 or vp.state_packaging_id=9 or vp.state_packaging_id=10) AND (vp.iso_ctn between 19 and 30 ) 
ORDER BY vp.expotador_code ASC
); 


/*------------------- EMBALAJE IMPALA CONTENEDORES TIPO REFRIGERADOS  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    vp.expotador_code AS CODIGO_EXPORTADOR,
    vp.exportador AS NOMBRE_EXPORTADOR,
    vp.lotes AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.packaging_type AS TIPO_SACOS,
    vp.packaging_mode AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.packaging_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.oie AS OIE, 
    vp.bic_ctn AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    vp.tipo_papel AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.estado_embalaje AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    NULL AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "EMBALAJE DE CONTENEDORES EN TERMINAL IMPALA TIPO REFRIGERADO" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging AS vp 
INNER JOIN clients AS cts ON cts.exporter_code = vp.expotador_code
inner join items_unoee pds on pds.codigo='0000286'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000286','001')
WHERE   (  DATE(vp.packaging_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
        AND vp.jetty = 'IMP'  AND (vp.state_packaging_id=3 or vp.state_packaging_id=9 or vp.state_packaging_id=10) 
        AND vp.tipo_papel = "REF-CTN"
ORDER BY vp.expotador_code ASC
); 


/*------------------- EMBALAJE IMPALA CONTENEDORES DE 40  -------------------*/
/*------------------- EMBALAJE IMPALA CONTENEDORES TIPO SACOS DE 40 TIPO PAPEL KRAFT  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    vp.expotador_code AS CODIGO_EXPORTADOR,
    vp.exportador AS NOMBRE_EXPORTADOR,
    vp.lotes AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.packaging_type AS TIPO_SACOS,
    vp.packaging_mode AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.packaging_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.oie AS OIE,
    vp.bic_ctn AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    vp.tipo_papel AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.estado_embalaje AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    NULL AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "EMBALAJE DE CONTENEDORES EN TERMINAL IMPALA TIPO SACOS DE LONGITUD 40 TIPO PAPEL KRAFT" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging AS vp 
INNER JOIN clients AS cts ON cts.exporter_code = vp.expotador_code
inner join items_unoee pds on pds.codigo='0000161'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000161','001')
WHERE   (  DATE(vp.packaging_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
        AND vp.jetty = 'IMP'  AND vp.packaging_mode = 'SACOS' 
        AND (vp.state_packaging_id=3 or vp.state_packaging_id=9 or vp.state_packaging_id=10) 
        AND (vp.iso_ctn between 39 and 50 ) 
        AND vp.tipo_papel = "KRAFT"
ORDER BY vp.expotador_code ASC
); 

/*------------------- EMBALAJE IMPALA CONTENEDORES TIPO SACOS DE 40 TIPO PAPEL CARTON  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    vp.expotador_code AS CODIGO_EXPORTADOR,
    vp.exportador AS NOMBRE_EXPORTADOR,
    vp.lotes AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.packaging_type AS TIPO_SACOS,
    vp.packaging_mode AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.packaging_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.oie AS OIE,
    vp.bic_ctn AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    vp.tipo_papel AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.estado_embalaje AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    NULL AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "EMBALAJE DE CONTENEDORES EN TERMINAL IMPALA TIPO SACOS DE LONGITUD 40 TIPO PAPEL CARTON" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging AS vp 
INNER JOIN clients AS cts ON cts.exporter_code = vp.expotador_code
inner join items_unoee pds on pds.codigo='0000163'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000163','001')
WHERE   (  DATE(vp.packaging_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
        AND vp.jetty = 'IMP'  AND vp.packaging_mode = 'SACOS' 
        AND (vp.state_packaging_id=3 or vp.state_packaging_id=9 or vp.state_packaging_id=10) 
        AND (vp.iso_ctn between 39 and 50 ) 
        AND vp.tipo_papel = "CARTON"
ORDER BY vp.expotador_code ASC
); 


/*------------------- EMBALAJE IMPALA CONTENEDORES TIPO COMBINADOS SACOS - CAJAS DE 40 TIPO PAPEL KRAFT -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    vp.expotador_code AS CODIGO_EXPORTADOR,
    vp.exportador AS NOMBRE_EXPORTADOR,
    vp.lotes AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.packaging_type AS TIPO_SACOS,
    vp.packaging_mode AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.packaging_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.oie AS OIE,
    vp.bic_ctn AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    vp.tipo_papel AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.estado_embalaje AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    NULL AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "EMBALAJE DE CONTENEDORES EN TERMINAL IMPALA TIPO COMBINADO(SACOS - CAJAS) DE LONGITUD 40 TIPO PAPEL KRAFT" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging AS vp 
INNER JOIN clients AS cts ON cts.exporter_code = vp.expotador_code
inner join items_unoee pds on pds.codigo='0000291'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000291','001')
WHERE   (  DATE(vp.packaging_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
        AND vp.jetty = 'IMP'  AND vp.packaging_mode = 'COMBINADO(SACOS - CAJAS)' 
        AND (vp.state_packaging_id=3 or vp.state_packaging_id=9 or vp.state_packaging_id=10) 
        AND (vp.iso_ctn between 39 and 50 )
        AND vp.tipo_papel = "KRAFT"
ORDER BY vp.expotador_code ASC
); 


/*------------------- EMBALAJE IMPALA CONTENEDORES TIPO COMBINADOS SACOS - CAJAS DE 40 TIPO PAPEL CARTON -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    vp.expotador_code AS CODIGO_EXPORTADOR,
    vp.exportador AS NOMBRE_EXPORTADOR,
    vp.lotes AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.packaging_type AS TIPO_SACOS,
    vp.packaging_mode AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.packaging_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.oie AS OIE,
    vp.bic_ctn AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    vp.tipo_papel AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.estado_embalaje AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    NULL AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "EMBALAJE DE CONTENEDORES EN TERMINAL IMPALA TIPO COMBINADO(SACOS - CAJAS) DE LONGITUD 40 TIPO PAPEL CARTON" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging AS vp 
INNER JOIN clients AS cts ON cts.exporter_code = vp.expotador_code
inner join items_unoee pds on pds.codigo='0000292'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000292','001')
WHERE   (  DATE(vp.packaging_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
        AND vp.jetty = 'IMP'  AND vp.packaging_mode = 'COMBINADO(SACOS - CAJAS)' 
        AND (vp.state_packaging_id=3 or vp.state_packaging_id=9 or vp.state_packaging_id=10) 
        AND (vp.iso_ctn between 39 and 50 )
        AND vp.tipo_papel = "CARTON"
ORDER BY vp.expotador_code ASC
); 



/*------------------- EMBALAJE IMPALA CONTENEDORES TIPO CONBINADOS SACOS - BIG BGAS DE 40 TIPO PAPEL KRAFT -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    vp.expotador_code AS CODIGO_EXPORTADOR,
    vp.exportador AS NOMBRE_EXPORTADOR,
    vp.lotes AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.packaging_type AS TIPO_SACOS,
    vp.packaging_mode AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.packaging_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.oie AS OIE,
    vp.bic_ctn AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    vp.tipo_papel AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.estado_embalaje AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    NULL AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "EMBALAJE DE CONTENEDORES EN TERMINAL IMPALA TIPO COMBINADO(SACOS - BIG BAGS) DE LONGITUD 40 TIPO PAPEL KRAFT" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging AS vp 
INNER JOIN clients AS cts ON cts.exporter_code = vp.expotador_code
inner join items_unoee pds on pds.codigo='0000291'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000291','001')
WHERE   (  DATE(vp.packaging_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
        AND vp.jetty = 'IMP'  AND vp.packaging_mode = 'COMBINADO(SACOS - BIG BAGS)' 
        AND (vp.state_packaging_id=3 or vp.state_packaging_id=9 or vp.state_packaging_id=10) 
        AND (vp.iso_ctn between 39 and 50 )
        AND vp.tipo_papel = "KRAFT"
ORDER BY vp.expotador_code ASC
); 

/*------------------- EMBALAJE IMPALA CONTENEDORES TIPO CONBINADOS SACOS - BIG BGAS DE 40 TIPO PAPEL CARTON -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    vp.expotador_code AS CODIGO_EXPORTADOR,
    vp.exportador AS NOMBRE_EXPORTADOR,
    vp.lotes AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.packaging_type AS TIPO_SACOS,
    vp.packaging_mode AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.packaging_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.oie AS OIE,
    vp.bic_ctn AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    vp.tipo_papel AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.estado_embalaje AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    NULL AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "EMBALAJE DE CONTENEDORES EN TERMINAL IMPALA TIPO COMBINADO(SACOS - BIG BAGS) DE LONGITUD 40 TIPO PAPEL CARTON" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging AS vp 
INNER JOIN clients AS cts ON cts.exporter_code = vp.expotador_code
inner join items_unoee pds on pds.codigo='0000292'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000292','001')
WHERE   (  DATE(vp.packaging_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
        AND vp.jetty = 'IMP'  AND vp.packaging_mode = 'COMBINADO(SACOS - BIG BAGS)' 
        AND (vp.state_packaging_id=3 or vp.state_packaging_id=9 or vp.state_packaging_id=10) 
        AND (vp.iso_ctn between 39 and 50 )
        AND vp.tipo_papel = "CARTON"
ORDER BY vp.expotador_code ASC
); 


/*------------------- EMBALAJE IMPALA CONTENEDORES TIPO CAJAS DE 40  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    vp.expotador_code AS CODIGO_EXPORTADOR,
    vp.exportador AS NOMBRE_EXPORTADOR,
    vp.lotes AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.packaging_type AS TIPO_SACOS,
    vp.packaging_mode AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.packaging_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.oie AS OIE,
    vp.bic_ctn AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.estado_embalaje AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    NULL AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "EMBALAJE DE CONTENEDORES EN TERMINAL IMPALA TIPO CAJAS DE LONGITUD 40" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging AS vp 
INNER JOIN clients AS cts ON cts.exporter_code = vp.expotador_code
inner join items_unoee pds on pds.codigo='0000041'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000041','001')
WHERE   (  DATE(vp.packaging_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
        AND vp.jetty = 'IMP'  AND vp.packaging_mode = 'Cajas' AND (vp.state_packaging_id=3 or vp.state_packaging_id=9 or vp.state_packaging_id=10) AND (vp.iso_ctn between 39 and 50 )
ORDER BY vp.expotador_code ASC
);  

/*------------------- EMBALAJE IMPALA CONTENEDORES TIPO CACAO DE 40  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    vp.expotador_code AS CODIGO_EXPORTADOR,
    vp.exportador AS NOMBRE_EXPORTADOR,
    vp.lotes AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.packaging_type AS TIPO_SACOS,
    vp.packaging_mode AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.packaging_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.oie AS OIE,
    vp.bic_ctn AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.estado_embalaje AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    NULL AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "EMBALAJE DE CONTENEDORES EN TERMINAL IMPALA TIPO CACAO DE LONGITUD 40" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging AS vp 
INNER JOIN clients AS cts ON cts.exporter_code = vp.expotador_code
inner join items_unoee pds on pds.codigo='0000074'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000074','001')
WHERE   (  DATE(vp.packaging_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
        AND vp.jetty = 'IMP'  AND vp.packaging_mode = 'CACAO' AND (vp.state_packaging_id=3 or vp.state_packaging_id=9 or vp.state_packaging_id=10) AND (vp.iso_ctn between 39 and 50 )
ORDER BY vp.expotador_code ASC
); 

/*------------------- EMBALAJE IMPALA CONTENEDORES TIPO BIG BAGS DE 40  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    vp.expotador_code AS CODIGO_EXPORTADOR,
    vp.exportador AS NOMBRE_EXPORTADOR,
    vp.lotes AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.packaging_type AS TIPO_SACOS,
    vp.packaging_mode AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.packaging_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.oie AS OIE,
    vp.bic_ctn AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.estado_embalaje AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    NULL AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "EMBALAJE DE CONTENEDORES EN TERMINAL IMPALA TIPO BIG BAGS DE LONGITUD 40" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging AS vp 
INNER JOIN clients AS cts ON cts.exporter_code = vp.expotador_code
inner join items_unoee pds on pds.codigo='0000073'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000073','001')
WHERE   (  DATE(vp.packaging_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
        AND vp.jetty = 'IMP'  AND vp.packaging_mode LIKE 'BIG BAG%' AND (vp.state_packaging_id=3 or vp.state_packaging_id=9 or vp.state_packaging_id=10) AND (vp.iso_ctn between 39 and 50 ) 
ORDER BY vp.expotador_code ASC
);  


/*------------------- ADICIONALES IMPALA ISO 20 PAPEL KRAFT   -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    NULL AS CODIGO_EXPORTADOR,
    "SOCIEDAD PORTUARIA REGIONAL DE BUENAVENTURA S.A. S.P.R. BUN" AS NOMBRE_EXPORTADOR,
    vp.lotes AS LOTE,
    vp.capas_adiconales_kraft20 AS CANTIDAD_OPERATIVA,
    vp.packaging_type AS TIPO_SACOS,
    vp.packaging_mode AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.packaging_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.oie AS OIE,
    vp.bic_ctn AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    vp.tipo_papel AS TIPO_PAPEL,
    vp.capas_adiconales_kraft20 AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.estado_embalaje AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    NULL AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "CAPAS ADICIONALES DE CONTENEDORES EN TERMINAL IMPALA TIPO KRAFT DE LONGITUD 20" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*vp.capas_adiconales_kraft20 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging AS vp 
INNER JOIN clients AS cts ON cts.exporter_code = vp.expotador_code
inner join items_unoee pds on pds.codigo='0000156'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000156','001')
WHERE    (  DATE(vp.packaging_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
        AND vp.jetty = 'IMP'  AND (vp.state_packaging_id=3 or vp.state_packaging_id=9 or vp.state_packaging_id=10) AND (vp.iso_ctn between 19 and 30 ) 
        AND vp.capas_adiconales_kraft20 != 0
ORDER BY vp.expotador_code ASC
);



/*------------------- ADICIONALES IMPALA ISO 20 PAPEL CARTON   -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    NULL AS CODIGO_EXPORTADOR,
    "SOCIEDAD PORTUARIA REGIONAL DE BUENAVENTURA S.A. S.P.R. BUN" AS NOMBRE_EXPORTADOR,
    vp.lotes AS LOTE,
    vp.capas_adiconales_carton20 AS CANTIDAD_OPERATIVA,
    vp.packaging_type AS TIPO_SACOS,
    vp.packaging_mode AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.packaging_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.oie AS OIE,
    vp.bic_ctn AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    vp.tipo_papel AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    vp.capas_adiconales_carton20 AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.estado_embalaje AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    NULL AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "CAPAS ADICIONALES DE CONTENEDORES EN TERMINAL IMPALA TIPO CARTON DE LONGITUD 20" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*vp.capas_adiconales_carton20 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging AS vp 
INNER JOIN clients AS cts ON cts.exporter_code = vp.expotador_code
inner join items_unoee pds on pds.codigo='0000158'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000158','001')
WHERE    (  DATE(vp.packaging_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
        AND vp.jetty = 'IMP'  AND (vp.state_packaging_id=3 or vp.state_packaging_id=9 or vp.state_packaging_id=10) AND (vp.iso_ctn between 19 and 30 ) 
        AND vp.capas_adiconales_carton20 != 0
ORDER BY vp.expotador_code ASC
);


/*------------------- ADICIONALES IMPALA ISO 40 PAPEL KRAFT   -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    NULL AS CODIGO_EXPORTADOR,
    "SOCIEDAD PORTUARIA REGIONAL DE BUENAVENTURA S.A. S.P.R. BUN" AS NOMBRE_EXPORTADOR,
    vp.lotes AS LOTE,
    vp.capas_adiconales_kraft40 AS CANTIDAD_OPERATIVA,
    vp.packaging_type AS TIPO_SACOS,
    vp.packaging_mode AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.packaging_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.oie AS OIE,
    vp.bic_ctn AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    vp.tipo_papel AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    vp.capas_adiconales_kraft40 AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.estado_embalaje AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    NULL AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "CAPAS ADICIONALES DE CONTENEDORES EN TERMINAL IMPALA TIPO KRAFT DE LONGITUD 40" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*vp.capas_adiconales_kraft40 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging AS vp 
INNER JOIN clients AS cts ON cts.exporter_code = vp.expotador_code
inner join items_unoee pds on pds.codigo='0000176'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000176','001')
WHERE    (  DATE(vp.packaging_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
        AND vp.jetty = 'IMP'  AND (vp.state_packaging_id=3 or vp.state_packaging_id=9 or vp.state_packaging_id=10) AND (vp.iso_ctn between 39 and 50 ) 
        AND vp.capas_adiconales_kraft40 != 0
ORDER BY vp.expotador_code ASC
);


/*------------------- ADICIONALES IMPALA ISO 40 PAPEL CARTON   -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    NULL AS CODIGO_EXPORTADOR,
    "SOCIEDAD PORTUARIA REGIONAL DE BUENAVENTURA S.A. S.P.R. BUN" AS NOMBRE_EXPORTADOR,
    vp.lotes AS LOTE,
    vp.capas_adiconales_carton40 AS CANTIDAD_OPERATIVA,
    vp.packaging_type AS TIPO_SACOS,
    vp.packaging_mode AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.packaging_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.oie AS OIE,
    vp.bic_ctn AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    vp.tipo_papel AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    vp.capas_adiconales_carton40 AS CAPAS_ADICIONALES_CARTON_40,
    vp.estado_embalaje AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    NULL AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "CAPAS ADICIONALES DE CONTENEDORES EN TERMINAL IMPALA TIPO CARTON DE LONGITUD 40" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*vp.capas_adiconales_carton40 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging AS vp 
INNER JOIN clients AS cts ON cts.exporter_code = vp.expotador_code
inner join items_unoee pds on pds.codigo='0000178'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000178','001')
WHERE    (  DATE(vp.packaging_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
        AND vp.jetty = 'IMP'  AND (vp.state_packaging_id=3 or vp.state_packaging_id=9 or vp.state_packaging_id=10) AND (vp.iso_ctn between 39 and 50 ) 
        AND vp.capas_adiconales_carton40 != 0
ORDER BY vp.expotador_code ASC
);
         

/*------------------- VACIADOS IMPALA ISO 20  -------------------*/
/*------------------- VACIADOS IMPALA ISO 20 SACOS -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    vp.expotador_code AS CODIGO_EXPORTADOR,
    vp.exportador AS NOMBRE_EXPORTADOR,
    vp.lotes AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.packaging_type AS TIPO_SACOS,
    vp.packaging_mode AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.empty_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.oie AS OIE,
    vp.bic_ctn AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.estado_embalaje AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    NULL AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "VACIADOS DE CONTENEDORES EN TERMINAL IMPALA TIPO SACOS DE LONGITUD 20" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging AS vp 
INNER JOIN clients AS cts ON cts.exporter_code = vp.expotador_code
inner join items_unoee pds on pds.codigo='0000044'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000044','001')
WHERE  (  DATE(vp.empty_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
        AND vp.jetty = 'IMP'  AND vp.packaging_mode = 'SACOS' AND (vp.state_packaging_id=4 or vp.state_packaging_id=9) AND (vp.iso_ctn between 19 and 30 ) 
ORDER BY vp.expotador_code ASC
); 

/*------------------- VACIADOS IMPALA ISO 20 GRANEL -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    vp.expotador_code AS CODIGO_EXPORTADOR,
    vp.exportador AS NOMBRE_EXPORTADOR,
    vp.lotes AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.packaging_type AS TIPO_SACOS,
    vp.packaging_mode AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.empty_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.oie AS OIE,
    vp.bic_ctn AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.estado_embalaje AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    NULL AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "VACIADOS DE CONTENEDORES EN TERMINAL IMPALA TIPO GRANEL DE LONGITUD 20" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging AS vp 
INNER JOIN clients AS cts ON cts.exporter_code = vp.expotador_code
inner join items_unoee pds on pds.codigo='0000171'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000171','001')
WHERE   (  DATE(vp.empty_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
        AND vp.jetty = 'IMP'  AND vp.packaging_mode = 'granel' AND (vp.state_packaging_id=4 or vp.state_packaging_id=9) AND (vp.iso_ctn between 19 and 30 ) 
ORDER BY vp.expotador_code ASC
); 

/*------------------- VACIADOS IMPALA ISO 20 COMBINADOS SACOS - CAJAS -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    vp.expotador_code AS CODIGO_EXPORTADOR,
    vp.exportador AS NOMBRE_EXPORTADOR,
    vp.lotes AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.packaging_type AS TIPO_SACOS,
    vp.packaging_mode AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.empty_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.oie AS OIE,
    vp.bic_ctn AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.estado_embalaje AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    NULL AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "VACIADOS DE CONTENEDORES EN TERMINAL IMPALA TIPO COMBINADO(SACOS - CAJAS) DE LONGITUD 20" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging AS vp 
INNER JOIN clients AS cts ON cts.exporter_code = vp.expotador_code
inner join items_unoee pds on pds.codigo='0000294'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000294','001')
WHERE   (  DATE(vp.empty_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
        AND vp.jetty = 'IMP'  AND vp.packaging_mode = 'COMBINADO(SACOS - CAJAS)' AND (vp.state_packaging_id=4 or vp.state_packaging_id=9) AND (vp.iso_ctn between 19 and 30 ) 
ORDER BY vp.expotador_code ASC
); 

/*------------------- VACIADOS IMPALA ISO 20 COMBINADOS SACOS - BIG BGAS -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    vp.expotador_code AS CODIGO_EXPORTADOR,
    vp.exportador AS NOMBRE_EXPORTADOR,
    vp.lotes AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.packaging_type AS TIPO_SACOS,
    vp.packaging_mode AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.empty_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.oie AS OIE,
    vp.bic_ctn AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.estado_embalaje AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    NULL AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "VACIADOS DE CONTENEDORES EN TERMINAL IMPALA TIPO COMBINADO(SACOS - BIG BAGS) DE LONGITUD 20" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging AS vp 
INNER JOIN clients AS cts ON cts.exporter_code = vp.expotador_code
inner join items_unoee pds on pds.codigo='0000294'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000294','001')
WHERE   (  DATE(vp.empty_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
        AND vp.jetty = 'IMP'  AND vp.packaging_mode = 'COMBINADO(SACOS - BIG BAGS)' AND (vp.state_packaging_id=4 or vp.state_packaging_id=9) AND (vp.iso_ctn between 19 and 30 ) 
ORDER BY vp.expotador_code ASC
); 

/*------------------- VACIADOS IMPALA ISO 20 CAJAS -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    vp.expotador_code AS CODIGO_EXPORTADOR,
    vp.exportador AS NOMBRE_EXPORTADOR,
    vp.lotes AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.packaging_type AS TIPO_SACOS,
    vp.packaging_mode AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.empty_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.oie AS OIE,
    vp.bic_ctn AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.estado_embalaje AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    NULL AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "VACIADOS DE CONTENEDORES EN TERMINAL IMPALA TIPO CAJAS DE LONGITUD 20" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging AS vp 
INNER JOIN clients AS cts ON cts.exporter_code = vp.expotador_code
inner join items_unoee pds on pds.codigo='0000078'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000078','001')
WHERE   (  DATE(vp.empty_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
        AND vp.jetty = 'IMP'  AND vp.packaging_mode = 'Cajas' AND  (vp.state_packaging_id=4 or vp.state_packaging_id=9) AND (vp.iso_ctn between 19 and 30 ) 
ORDER BY vp.expotador_code ASC
); 

/*------------------- VACIADOS IMPALA ISO 20 CACAO -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    vp.expotador_code AS CODIGO_EXPORTADOR,
    vp.exportador AS NOMBRE_EXPORTADOR,
    vp.lotes AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.packaging_type AS TIPO_SACOS,
    vp.packaging_mode AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.empty_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.oie AS OIE,
    vp.bic_ctn AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.estado_embalaje AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    NULL AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "VACIADOS DE CONTENEDORES EN TERMINAL IMPALA TIPO CACAO DE LONGITUD 20" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging AS vp 
INNER JOIN clients AS cts ON cts.exporter_code = vp.expotador_code
inner join items_unoee pds on pds.codigo='0000293'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000293','001')
WHERE   (  DATE(vp.empty_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
        AND vp.jetty = 'IMP'  AND vp.packaging_mode = 'CACAO' AND  (vp.state_packaging_id=4 or vp.state_packaging_id=9) AND (vp.iso_ctn between 19 and 30 ) 
ORDER BY vp.expotador_code ASC
); 

/*------------------- VACIADOS IMPALA ISO 20 BIG BGAS -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    vp.expotador_code AS CODIGO_EXPORTADOR,
    vp.exportador AS NOMBRE_EXPORTADOR,
    vp.lotes AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.packaging_type AS TIPO_SACOS,
    vp.packaging_mode AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.empty_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.oie AS OIE,
    vp.bic_ctn AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.estado_embalaje AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    NULL AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "VACIADOS DE CONTENEDORES EN TERMINAL IMPALA TIPO BIG BAGS DE LONGITUD 20" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging AS vp 
INNER JOIN clients AS cts ON cts.exporter_code = vp.expotador_code
inner join items_unoee pds on pds.codigo='0000174'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000174','001')
WHERE   (  DATE(vp.empty_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
        AND vp.jetty = 'IMP'  AND vp.packaging_mode LIKE 'BIG BAG%' AND (vp.state_packaging_id=4 or vp.state_packaging_id=9) AND (vp.iso_ctn between 19 and 30 ) 
ORDER BY vp.expotador_code ASC
); 



/*------------------- VACIADOS IMPALA ISO 40  -------------------*/
/*------------------- VACIADOS IMPALA ISO 40 SACOS -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    vp.expotador_code AS CODIGO_EXPORTADOR,
    vp.exportador AS NOMBRE_EXPORTADOR,
    vp.lotes AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.packaging_type AS TIPO_SACOS,
    vp.packaging_mode AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.empty_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.oie AS OIE,
    vp.bic_ctn AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.estado_embalaje AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    NULL AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "VACIADOS DE CONTENEDORES EN TERMINAL IMPALA TIPO SACOS DE LONGITUD 40" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging AS vp 
INNER JOIN clients AS cts ON cts.exporter_code = vp.expotador_code
inner join items_unoee pds on pds.codigo='0000081'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000081','001')
WHERE   (  DATE(vp.empty_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
        AND vp.jetty = 'IMP'  AND vp.packaging_mode = 'SACOS' AND (vp.state_packaging_id=4 or vp.state_packaging_id=9) AND (vp.iso_ctn between 39 and 50 ) 
ORDER BY vp.expotador_code ASC
); 


/*------------------- VACIADOS IMPALA ISO 40 COMBINADOS SACOS - CAJAS -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    vp.expotador_code AS CODIGO_EXPORTADOR,
    vp.exportador AS NOMBRE_EXPORTADOR,
    vp.lotes AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.packaging_type AS TIPO_SACOS,
    vp.packaging_mode AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.empty_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.oie AS OIE,
    vp.bic_ctn AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.estado_embalaje AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    NULL AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "VACIADOS DE CONTENEDORES EN TERMINAL IMPALA TIPO COMBINADO(SACOS - CAJAS) DE LONGITUD 40" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging AS vp 
INNER JOIN clients AS cts ON cts.exporter_code = vp.expotador_code
inner join items_unoee pds on pds.codigo='0000294'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000294','001')
WHERE   (  DATE(vp.empty_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
        AND vp.jetty = 'IMP'  AND vp.packaging_mode = 'COMBINADO(SACOS - CAJAS)' AND (vp.state_packaging_id=4 or vp.state_packaging_id=9) AND (vp.iso_ctn between 39 and 50 )
ORDER BY vp.expotador_code ASC
); 

/*------------------- VACIADOS IMPALA ISO 40 COMBINADOS SACOS - BIG BGAS -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    vp.expotador_code AS CODIGO_EXPORTADOR,
    vp.exportador AS NOMBRE_EXPORTADOR,
    vp.lotes AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.packaging_type AS TIPO_SACOS,
    vp.packaging_mode AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.empty_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.oie AS OIE,
    vp.bic_ctn AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.estado_embalaje AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    NULL AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "VACIADOS DE CONTENEDORES EN TERMINAL IMPALA TIPO COMBINADO(SACOS - BIG BAGS) DE LONGITUD 40" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging AS vp 
INNER JOIN clients AS cts ON cts.exporter_code = vp.expotador_code
inner join items_unoee pds on pds.codigo='0000294'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000294','001')
WHERE   (  DATE(vp.empty_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
        AND vp.jetty = 'IMP'  AND vp.packaging_mode = 'COMBINADO(SACOS - BIG BAGS)' AND (vp.state_packaging_id=4 or vp.state_packaging_id=9) AND (vp.iso_ctn between 39 and 50 )
ORDER BY vp.expotador_code ASC
); 

/*------------------- VACIADOS IMPALA ISO 40 CAJAS -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    vp.expotador_code AS CODIGO_EXPORTADOR,
    vp.exportador AS NOMBRE_EXPORTADOR,
    vp.lotes AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.packaging_type AS TIPO_SACOS,
    vp.packaging_mode AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.empty_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.oie AS OIE,
    vp.bic_ctn AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.estado_embalaje AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    NULL AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "VACIADOS DE CONTENEDORES EN TERMINAL IMPALA TIPO CAJAS DE LONGITUD 40" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging AS vp 
INNER JOIN clients AS cts ON cts.exporter_code = vp.expotador_code
inner join items_unoee pds on pds.codigo='0000079'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000079','001')
WHERE   (  DATE(vp.empty_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
        AND vp.jetty = 'IMP'  AND vp.packaging_mode = 'Cajas' AND (vp.state_packaging_id=4 or vp.state_packaging_id=9) AND (vp.iso_ctn between 39 and 50 )
ORDER BY vp.expotador_code ASC
); 

/*------------------- VACIADOS IMPALA ISO 40 CACAO -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    vp.expotador_code AS CODIGO_EXPORTADOR,
    vp.exportador AS NOMBRE_EXPORTADOR,
    vp.lotes AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.packaging_type AS TIPO_SACOS,
    vp.packaging_mode AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.empty_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.oie AS OIE,
    vp.bic_ctn AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.estado_embalaje AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    NULL AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "VACIADOS DE CONTENEDORES EN TERMINAL IMPALA TIPO CACAO DE LONGITUD 40" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging AS vp 
INNER JOIN clients AS cts ON cts.exporter_code = vp.expotador_code
inner join items_unoee pds on pds.codigo='0000290'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000290','001')
WHERE   (  DATE(vp.empty_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
        AND vp.jetty = 'IMP'  AND vp.packaging_mode = 'CACAO' AND (vp.state_packaging_id=4 or vp.state_packaging_id=9) AND (vp.iso_ctn between 39 and 50 )
ORDER BY vp.expotador_code ASC
); 

/*------------------- VACIADOS IMPALA ISO 40 BIG BGAS -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    vp.expotador_code AS CODIGO_EXPORTADOR,
    vp.exportador AS NOMBRE_EXPORTADOR,
    vp.lotes AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.packaging_type AS TIPO_SACOS,
    vp.packaging_mode AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.empty_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.oie AS OIE,
    vp.bic_ctn AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.estado_embalaje AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    NULL AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "VACIADOS DE CONTENEDORES EN TERMINAL IMPALA TIPO BIG BAGS DE LONGITUD 40" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging AS vp 
INNER JOIN clients AS cts ON cts.exporter_code = vp.expotador_code
inner join items_unoee pds on pds.codigo='0000174'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000174','001')
WHERE   (  DATE(vp.empty_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
        AND vp.jetty = 'IMP'  AND vp.packaging_mode LIKE 'BIG BAG%' AND (vp.state_packaging_id=4 or vp.state_packaging_id=9)  AND (vp.iso_ctn between 39 and 50 ) 
ORDER BY vp.expotador_code ASC
); 




SELECT * FROM data_billing_full;


