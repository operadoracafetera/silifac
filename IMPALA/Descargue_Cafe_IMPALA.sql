use siscafe_pro;
/*------------------- SET @DIAS_DE_FACTURACION=5; -------------------*/

/*------------------- SET @FECHA_INF=DATE(DATE_SUB(NOW(), INTERVAL @DIAS_DE_FACTURACION DAY)); -------------------*/
/*------------------- SET @FECHA_SUP=DATE(DATE_SUB(NOW(), INTERVAL 1 DAY)); -------------------*/

truncate data_billing_full;
truncate temp_log_data_unoerp;

SET @FECHA_INF=DATE("2020-12-07");
SET @FECHA_SUP=DATE("2020-12-07");


/*------------------- DESCARGUE FNC IMPALA tipo SACOS  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rm.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat(cts.exporter_code,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in) AS LOTE,
    rm.quantity_radicated_bag_in AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    DATE(rm.created_date) AS FECHA_OPERACION,
    uc.name_unit AS UNIDAD_CAFE,
    b_rm.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "DESCARGUE DE FNC EN TERMINAL DE IMPALA TIPO SACOS" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*rm.quantity_radicated_bag_in AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join bill b_rm on rm.bill_id=b_rm.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000110'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000110','007')
where ss.ref_departament=10 and rm.client_id =1 AND rm.bill_id!=2 
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
        and (client_id,vehicle_plate,date(rm.created_date),ss.ref_departament) 
        in (
            select client_id,vehicle_plate,date(rc.created_date),ss.ref_departament
            from remittances_caffee rc
            inner join slot_store ss on ss.id=rc.slot_store_id
            where ss.ref_departament=10 and rc.client_id =1 AND rc.bill_id!=2 AND (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP )
            group by rc.vehicle_plate,rc.client_id,date(rc.created_date),ss.ref_departament
        )
    ORDER BY DATE(rm.created_date) ASC
); 


/*------------------- DESCARGUE FNC IMPALA TIPO CAJAS -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rm.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat(cts.exporter_code,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in) AS LOTE,
    rm.quantity_radicated_bag_in AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    DATE(rm.created_date) AS FECHA_OPERACION,
    uc.name_unit AS UNIDAD_CAFE,
    b_rm.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "DESCARGUE DE FNC EN TERMINAL DE IMPALA TIPO CAJAS" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*rm.quantity_radicated_bag_in AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join bill b_rm on rm.bill_id=b_rm.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000285'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000285','007')
where ss.ref_departament=10 and rm.client_id =1 AND rm.bill_id=2 and rm.vehicle_plate!= 'FIQUE' 
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
        and (client_id,vehicle_plate,date(rm.created_date),ss.ref_departament) 
        in ( 
            select client_id,vehicle_plate,date(rc.created_date),ss.ref_departament
            from remittances_caffee rc
            inner join slot_store ss on ss.id=rc.slot_store_id
            where ss.ref_departament=10 and rc.client_id =1 AND rc.bill_id=2 AND (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP )
            group by rc.vehicle_plate,rc.client_id,date(rc.created_date),ss.ref_departament
        )
    ORDER BY DATE(rm.created_date) ASC
); 



/*------------------- DESCARGUE IMPALA MICROLOTES PARTICULARES -------------------*/
/*------------------- DESCARGUE IMPALA MICROLOTES MAYOR IGUAL A 5 LOTES -------------------*/
INSERT INTO data_billing_full (
select 
    NULL AS ID,
    group_concat(rc.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rc.lot_caffee,' x ',rc.quantity_radicated_bag_in separator ' | ') AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    DATE(rc.created_date) AS FECHA_OPERACION,
    group_concat(distinct(uc.name_unit) separator ' | ') AS UNIDAD_CAFE,
    group_concat(distinct(b_rc.name) separator ' | ') AS UNIDAD_FACTURA,
    count(rc.id) AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "DESCARGUE DE MICROLOTES PARTICULARES EN TERMINAL DE IMPALA MAYOR IGUAL A 5 LOTES" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rc
inner join clients cts on cts.id=rc.client_id
inner join units_caffee uc on rc.units_cafee_id=uc.id
inner join bill b_rc on rc.bill_id=b_rc.id
inner join slot_store ss on ss.id=rc.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000114'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000114','001')
where rc.microlot=1 and ss.ref_departament=10 and rc.client_id !=1 
     AND (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP )    
group by rc.vehicle_plate, date(rc.created_date) , rc.client_id, ss.ref_departament
having count(rc.id) >= 5
ORDER BY cts.exporter_code ASC
); 



/*------------------- DESCARGUE IMPALA MENOR A 100 PARTICULARES -------------------*/
/*------------------- DESCARGUE IMPALA MENOR A 100 SACOS PARTICULARES -------------------*/
INSERT INTO data_billing_full (
select 
    NULL AS ID,
    group_concat(rc.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rc.lot_caffee,' x ',rc.quantity_radicated_bag_in separator ' | ') AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    date(rc.created_date) AS FECHA_OPERACION,
    group_concat(distinct(uc.name_unit) separator ' | ')  AS UNIDAD_CAFE,
    b_id.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "DESCARGUE DE PARTICULARES EN TERMINAL DE IMPALA MENOR A 100 SACOS CAFE" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1  AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee AS rc
inner join clients cts on cts.id = rc.client_id
INNER JOIN bill AS b_id ON b_id.id = rc.bill_id
inner join slot_store ss on ss.id=rc.slot_store_id
inner join units_caffee AS uc on rc.units_cafee_id=uc.id
inner join departaments dep on dep.id=ss.ref_departament
inner join items_unoee pds on pds.codigo='0000270'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000270','001')
where ss.ref_departament=10 and rc.client_id != 1 and rc.microlot=0 AND rc.bill_id=1
         AND (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP )
group by rc.vehicle_plate,rc.client_id,date(rc.created_date), ss.ref_departament
having sum(rc.quantity_radicated_bag_in) <=100
    ORDER BY cts.exporter_code ASC
); 


/*------------------- DESCARGUE IMPALA MENOR A 250 y MAYOR A 101 PARTICULARES -------------------*/
/*------------------- DESCARGUE IMPALA MENOR A 250 y MAYOR A 101 SACOS PARTICULARES -------------------*/
INSERT INTO data_billing_full (
select 
    NULL AS ID,
    group_concat(rc.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rc.lot_caffee,' x ',rc.quantity_radicated_bag_in separator ' | ') AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    date(rc.created_date) AS FECHA_OPERACION,
    group_concat(distinct(uc.name_unit) separator ' | ')  AS UNIDAD_CAFE,
    b_id.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "DESCARGUE DE PARTICULARES EN TERMINAL DE IMPALA DE 101 A 250 SACOS CAFE" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee AS rc
inner join clients cts on cts.id = rc.client_id
INNER JOIN bill AS b_id ON b_id.id = rc.bill_id
inner join slot_store ss on ss.id=rc.slot_store_id
inner join units_caffee AS uc on rc.units_cafee_id=uc.id
inner join departaments dep on dep.id=ss.ref_departament
inner join items_unoee pds on pds.codigo='0000287'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000287','001')
where ss.ref_departament=10 and rc.client_id != 1 and rc.microlot=0 AND rc.bill_id=1
         AND (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP )
group by rc.vehicle_plate,rc.client_id,date(rc.created_date), ss.ref_departament
having sum(rc.quantity_radicated_bag_in) BETWEEN 101 AND 250
    ORDER BY cts.exporter_code ASC
); 



/*------------------- DESCARGUE IMPALA MAYOR DE 250 PARTICULARES -------------------*/
/*------------------- DESCARGUE IMPALA MAYOR DE 250 SACOS DE 1-25 KG SACOS CAFE  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rm.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( cts.exporter_code ,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in ) AS LOTE,
    rm.quantity_radicated_bag_in AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    DATE(rm.created_date) AS FECHA_OPERACION,
    uc.name_unit AS UNIDAD_CAFE,
    b_rm.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "DESCARGUE DE PARTICULARES EN TERMINAL DE IMPALA MAYOR DE 250 SACOS DE 1-25 KG SACOS CAFE" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*rm.quantity_radicated_bag_in AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join bill b_rm on rm.bill_id=b_rm.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000222'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000222','001')
where ss.ref_departament=10 and rm.microlot=0 and rm.client_id !=1 AND rm.bill_id=1 AND uc.quantity <= 25
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
        and (client_id,vehicle_plate,date(rm.created_date),ss.ref_departament) 
        in (
            select client_id,vehicle_plate,date(rc.created_date),ss.ref_departament
            from remittances_caffee rc
            inner join slot_store ss on ss.id=rc.slot_store_id
            where ss.ref_departament=10 and rc.microlot=0 and rc.client_id !=1 AND rc.bill_id=1 AND (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP )
            group by rc.vehicle_plate,rc.client_id,date(rc.created_date),ss.ref_departament
            having sum(rc.quantity_radicated_bag_in) >250
        )
    ORDER BY cts.exporter_code ASC
); 


/*------------------- DESCARGUE IMPALA MAYOR DE 250 SACOS DE 30 KG SACOS CAFE  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rm.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( cts.exporter_code ,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in ) AS LOTE,
    rm.quantity_radicated_bag_in AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    DATE(rm.created_date) AS FECHA_OPERACION,
    uc.name_unit AS UNIDAD_CAFE,
    b_rm.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "DESCARGUE DE PARTICULARES EN TERMINAL DE IMPALA MAYOR DE 250 SACOS DE 30 KG SACOS CAFE" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*rm.quantity_radicated_bag_in AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join bill b_rm on rm.bill_id=b_rm.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000223'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000223','001')
where ss.ref_departament=10 and rm.microlot=0 and rm.client_id !=1 AND rm.bill_id=1 AND uc.quantity = 30
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
        and (client_id,vehicle_plate,date(rm.created_date),ss.ref_departament) 
        in ( 
            select client_id,vehicle_plate,date(rc.created_date),ss.ref_departament
            from remittances_caffee rc
            inner join slot_store ss on ss.id=rc.slot_store_id
            where ss.ref_departament=10 and rc.microlot=0 and rc.client_id !=1 AND rc.bill_id=1  AND (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP )
            group by rc.vehicle_plate,rc.client_id,date(rc.created_date),ss.ref_departament
            having sum(rc.quantity_radicated_bag_in) >250
        )
    ORDER BY cts.exporter_code ASC
); 


/*------------------- DESCARGUE IMPALA MAYOR DE 250 SACOS DE 35 KG SACOS CAFE  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rm.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( cts.exporter_code ,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in ) AS LOTE,
    rm.quantity_radicated_bag_in AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    DATE(rm.created_date) AS FECHA_OPERACION,
    uc.name_unit AS UNIDAD_CAFE,
    b_rm.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "DESCARGUE DE PARTICULARES EN TERMINAL DE IMPALA MAYOR DE 250 SACOS DE 35 KG SACOS CAFE" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*rm.quantity_radicated_bag_in AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join bill b_rm on rm.bill_id=b_rm.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000224'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000224','001')
where ss.ref_departament=10 and rm.microlot=0 and rm.client_id !=1 AND rm.bill_id=1 AND uc.quantity = 35
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
        and (client_id,vehicle_plate,date(rm.created_date),ss.ref_departament) 
        in (
            select client_id,vehicle_plate,date(rc.created_date),ss.ref_departament
            from remittances_caffee rc
            inner join slot_store ss on ss.id=rc.slot_store_id
            where ss.ref_departament=10 and rc.microlot=0 and rc.client_id !=1 AND rc.bill_id=1 AND (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP )
            group by rc.vehicle_plate,rc.client_id,date(rc.created_date),ss.ref_departament
            having sum(rc.quantity_radicated_bag_in) >250
        )
    ORDER BY cts.exporter_code ASC
); 


/*------------------- DESCARGUE IMPALA MAYOR DE 250 SACOS DE 40 - 70 KG SACOS CAFE  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rm.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( cts.exporter_code ,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in ) AS LOTE,
    rm.quantity_radicated_bag_in AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    DATE(rm.created_date) AS FECHA_OPERACION,
    uc.name_unit AS UNIDAD_CAFE,
    b_rm.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "DESCARGUE DE PARTICULARES EN TERMINAL DE IMPALA MAYOR DE 250 SACOS DE 40-70 KG SACOS CAFE" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*rm.quantity_radicated_bag_in AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join bill b_rm on rm.bill_id=b_rm.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000111'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000111','001')
where ss.ref_departament=10 and rm.microlot=0 and rm.client_id !=1 AND rm.bill_id=1 AND ( uc.quantity between 40 and 70 )
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
        and (client_id,vehicle_plate,date(rm.created_date),ss.ref_departament) 
        in (
            select client_id,vehicle_plate,date(rc.created_date),ss.ref_departament
            from remittances_caffee rc
            inner join slot_store ss on ss.id=rc.slot_store_id
            where ss.ref_departament=10 and rc.microlot=0 and rc.client_id !=1 AND rc.bill_id=1 AND (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP )
            group by rc.vehicle_plate,rc.client_id,date(rc.created_date),ss.ref_departament
            having sum(rc.quantity_radicated_bag_in) >250
        )
    ORDER BY cts.exporter_code ASC
); 


/*------------------- DESCARGUE IMPALA SACOS CACAO  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rm.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( cts.exporter_code ,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in ) AS LOTE,
    rm.quantity_radicated_bag_in AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    DATE(rm.created_date) AS FECHA_OPERACION,
    uc.name_unit AS UNIDAD_CAFE,
    b_rm.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "DESCARGUE DE PARTICULARES EN TERMINAL DE IMPALA SACOS CACAO" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*rm.quantity_radicated_bag_in AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join bill b_rm on rm.bill_id=b_rm.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000049'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000049','001')
where ss.ref_departament=10 and rm.microlot=0 and rm.client_id !=1 AND rm.bill_id=3 
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )        
    ORDER BY cts.exporter_code ASC
); 


/*------------------- DESCARGUE IMPALA SACOS BIG BAGS  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rm.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( cts.exporter_code ,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in ) AS LOTE,
    rm.quantity_radicated_bag_in AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    DATE(rm.created_date) AS FECHA_OPERACION,
    uc.name_unit AS UNIDAD_CAFE,
    b_rm.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "DESCARGUE DE PARTICULARES EN TERMINAL DE IMPALA TIPO BIG BAGS" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*rm.quantity_radicated_bag_in  AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join bill b_rm on rm.bill_id=b_rm.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000184'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000184','001')
where ss.ref_departament=10 and rm.microlot=0 and rm.client_id !=1 AND rm.bill_id=4 
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
    ORDER BY cts.exporter_code ASC
);  


/*------------------- DESCARGUE IMPALA CAJAS DE 1 A 10  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    DATE(rm.created_date) AS FECHA_OPERACION,
    group_concat(distinct(uc.name_unit) separator ' | ') AS UNIDAD_CAFE,
    group_concat(distinct(b_rm.name) separator ' | ') AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "DESCARGUE DE PARTICULARES EN TERMINAL DE IMPALA TIPO CAJAS DE 1 A 10 UNIDADES" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join bill b_rm on rm.bill_id=b_rm.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000032'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000032','001')
where ss.ref_departament=10 and rm.microlot=0 and rm.client_id !=1 AND rm.bill_id=2 and rm.vehicle_plate!= 'FIQUE' 
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by rm.vehicle_plate,rm.client_id,date(rm.created_date),ss.ref_departament
having sum(rm.quantity_radicated_bag_in) <= 10 
ORDER BY cts.exporter_code ASC
); 


/*------------------- DESCARGUE IMPALA CAJAS DE 11 A 100  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    DATE(rm.created_date) AS FECHA_OPERACION,
    group_concat(distinct(uc.name_unit) separator ' | ') AS UNIDAD_CAFE,
    group_concat(distinct(b_rm.name) separator ' | ') AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "DESCARGUE DE PARTICULARES EN TERMINAL DE IMPALA TIPO CAJAS DE 11 A 100 UNIDADES" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join bill b_rm on rm.bill_id=b_rm.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000203'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000203','001')
where ss.ref_departament=10 and rm.microlot=0 and rm.client_id !=1 AND rm.bill_id=2
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by rm.vehicle_plate,rm.client_id,date(rm.created_date),ss.ref_departament 
having sum(rm.quantity_radicated_bag_in) between 11 and 100      
ORDER BY cts.exporter_code ASC
); 


/*------------------- DESCARGUE IMPALA CAJAS MAYOR DE 100  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    sum(rm.quantity_radicated_bag_in) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    DATE(rm.created_date) AS FECHA_OPERACION,
    group_concat(distinct(uc.name_unit) separator ' | ') AS UNIDAD_CAFE,
    group_concat(distinct(b_rm.name) separator ' | ') AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "DESCARGUE DE PARTICULARES EN TERMINAL DE IMPALA TIPO CAJAS MAYOR A 100 UNIDADES" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*sum(rm.quantity_radicated_bag_in) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join bill b_rm on rm.bill_id=b_rm.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000215'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000215','001')
where ss.ref_departament=10 and rm.microlot=0 and rm.client_id !=1 AND rm.bill_id=2 
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by rm.vehicle_plate,rm.client_id,date(rm.created_date),ss.ref_departament 
having sum(rm.quantity_radicated_bag_in) > 100
ORDER BY cts.exporter_code ASC
); 


/*------------------- DESCARGUE IMPALA CAFE VERDE ESTIBAS  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rm.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat(cts.exporter_code,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in) AS LOTE,
    rm.quantity_radicated_bag_in AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    DATE(rm.created_date) AS FECHA_OPERACION,
    uc.name_unit AS UNIDAD_CAFE,
    b_rm.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "DESCARGUE DE PARTICULARES EN TERMINAL DE IMPALA TIPO CAFE VERDE ESTIBAS" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join bill b_rm on rm.bill_id=b_rm.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo="0000295"
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=("0000295",'001')
where ss.ref_departament=10 and rm.microlot=0 and rm.client_id !=1 AND rm.bill_id=18 
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
ORDER BY cts.exporter_code ASC
); 



/*------------------- DEVOLUCIONES FNC -------------------*/
/*------------------- DEVOLUCION FNC CAFE IMPALA CAFE  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    group_concat(rcrc.remittances_caffee_id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee,' x ' ,rcrc.qta_bags separator ' | ') as LOTE,
    sum(rcrc.qta_bags) AS CANTIDAD_OPERATIVA,
    rc.return_type AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    rc.return_date  AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    rc.state_return AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "DEVOLUCIONES FNC EN TERMINAL DE IMPALA TIPO CAFE" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*sum(rcrc.qta_bags) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from returns_caffees rc
inner join remittances_caffee_returns_caffees rcrc on rcrc.returns_caffee_id=rc.id
inner join clients as cts on cts.id=rc.client_id
inner join remittances_caffee as rm on rcrc.remittances_caffee_id = rm.id
inner join departaments dep on rc.jetty = dep.id
inner join items_unoee pds on pds.codigo='0000288'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000288','007')
where rc.jetty=10 and rc.state_return="COMPLETADA" and cts.id=1 AND rc.return_type = 'CAFE' 
        and (  DATE(rc.return_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
group by rc.vehicle_plate,cts.exporter_code,date(rc.return_date), rc.jetty
); 

/*------------------- DEVOLUCION FNC CAFE IMPALA FIQUE MAYOR A 10000  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    group_concat(rcrc.remittances_caffee_id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee,' x ' ,rcrc.qta_bags separator ' | ') as LOTE,
    1 AS CANTIDAD_OPERATIVA,
    rc.return_type AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    rc.return_date  AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    rc.state_return AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "DEVOLUCIONES FNC EN TERMINAL DE IMPALA TIPO FIQUE MAYOR A 10000" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from returns_caffees rc
inner join remittances_caffee_returns_caffees rcrc on rcrc.returns_caffee_id=rc.id
inner join clients as cts on cts.id=rc.client_id
inner join remittances_caffee as rm on rcrc.remittances_caffee_id = rm.id
inner join departaments dep on rc.jetty = dep.id
inner join items_unoee pds on pds.codigo='0000127'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000127','007')
where rc.jetty=10 and rc.state_return="COMPLETADA" and cts.id=1 AND rc.return_type = 'FIQUE' 
        and (  DATE(rc.return_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
group by rc.vehicle_plate,cts.exporter_code,date(rc.return_date), rc.jetty
having sum(rcrc.qta_bags) > 10000
); 

/*------------------- DEVOLUCION FNC CAFE IMPALA FIQUE MENOR A 10000  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    group_concat(rcrc.remittances_caffee_id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee,' x ' ,rcrc.qta_bags separator ' | ') as LOTE,
    1 AS CANTIDAD_OPERATIVA,
    rc.return_type AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    rc.return_date  AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    rc.state_return AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "DEVOLUCIONES FNC EN TERMINAL DE IMPALA TIPO FIQUE MENOR A 10000" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from returns_caffees rc
inner join remittances_caffee_returns_caffees rcrc on rcrc.returns_caffee_id=rc.id
inner join clients as cts on cts.id=rc.client_id
inner join remittances_caffee as rm on rcrc.remittances_caffee_id = rm.id
inner join departaments dep on rc.jetty = dep.id
inner join items_unoee pds on pds.codigo='0000219'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000219','007')
where rc.jetty=10 and rc.state_return="COMPLETADA" and cts.id=1 AND rc.return_type = 'FIQUE' 
    and (  DATE(rc.return_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
group by rc.vehicle_plate,cts.exporter_code,date(rc.return_date), rc.jetty
having sum(rcrc.qta_bags) <=10000
); 



/*------------------- DEVOLUCION PARTICULARES CAFE IMPALA MAYOR 250  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    group_concat(rcrc.remittances_caffee_id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee,' x ' ,rcrc.qta_bags separator ' | ') as LOTE,
    sum(rcrc.qta_bags) AS CANTIDAD_OPERATIVA,
    rc.return_type AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    rc.return_date  AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    rc.state_return AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "DEVOLUCIONES PARTICULARES EN TERMINAL DE IMPALA TIPO CAFE MAYOR A 250" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*sum(rcrc.qta_bags) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from returns_caffees rc
inner join remittances_caffee_returns_caffees rcrc on rcrc.returns_caffee_id=rc.id
inner join clients AS cts on cts.id=rc.client_id
inner join remittances_caffee as rm on rcrc.remittances_caffee_id = rm.id
inner join departaments dep on rc.jetty = dep.id
inner join items_unoee pds on pds.codigo='0000277'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000277','001')
where rc.jetty=10 and rc.state_return="COMPLETADA" and cts.id!=1 AND rc.return_type = 'CAFE' 
        and (  DATE(rc.return_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
group by rc.vehicle_plate,cts.exporter_code,date(rc.return_date), rc.jetty
having sum(rcrc.qta_bags) >250
ORDER BY exporter_code ASC
); 

/*------------------- DEVOLUCION PARTICULARES CAFE IMPALA MENOR 250  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    group_concat(rcrc.remittances_caffee_id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee,' x ' ,rcrc.qta_bags separator ' | ') as LOTE,
    1 AS CANTIDAD_OPERATIVA,
    rc.return_type AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    rc.return_date  AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    rc.state_return AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "DEVOLUCIONES PARTICULARES EN TERMINAL DE IMPALA TIPO CAFE MENOR A 250" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from returns_caffees rc
inner join remittances_caffee_returns_caffees rcrc on rcrc.returns_caffee_id=rc.id
inner join clients AS cts on cts.id=rc.client_id
inner join remittances_caffee as rm on rcrc.remittances_caffee_id = rm.id
inner join departaments dep on rc.jetty = dep.id
inner join items_unoee pds on pds.codigo='0000001'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000001','001')
where rc.jetty=10 and rc.state_return="COMPLETADA" and cts.id!=1 AND rc.return_type = 'CAFE' 
        and (  DATE(rc.return_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
group by rc.vehicle_plate,cts.exporter_code,date(rc.return_date), rc.jetty
having sum(rcrc.qta_bags) <=250
ORDER BY exporter_code ASC
); 

/*------------------- DEVOLUCION PARTICULARES IMPALA FIQUE MAYOR A 10000  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    group_concat(rcrc.remittances_caffee_id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee,' x ' ,rcrc.qta_bags separator ' | ') as LOTE,
    1 AS CANTIDAD_OPERATIVA,
    rc.return_type AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    rc.return_date  AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    rc.state_return AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "DEVOLUCIONES PARTICULARES EN TERMINAL DE IMPALA TIPO FIQUE MAYOR A 10000" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from returns_caffees rc
inner join remittances_caffee_returns_caffees rcrc on rcrc.returns_caffee_id=rc.id
inner join clients AS cts on cts.id=rc.client_id
inner join remittances_caffee as rm on rcrc.remittances_caffee_id = rm.id
inner join departaments dep on rc.jetty = dep.id
inner join items_unoee pds on pds.codigo='0000127'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000127','001')
where rc.jetty=10 and rc.state_return="COMPLETADA" and cts.id!=1 AND rc.return_type = 'FIQUE' 
        and (  DATE(rc.return_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
group by rc.vehicle_plate,cts.exporter_code,date(rc.return_date), rc.jetty
having sum(rcrc.qta_bags) >10000
ORDER BY exporter_code ASC
); 

/*------------------- DEVOLUCION PARTICULARES IMPALA FIQUE MENOR A 5000 -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    group_concat(rcrc.remittances_caffee_id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee,' x ' ,rcrc.qta_bags separator ' | ') as LOTE,
    1 AS CANTIDAD_OPERATIVA,
    rc.return_type AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    rc.return_date  AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    rc.state_return AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "DEVOLUCIONES PARTICULARES EN TERMINAL DE IMPALA TIPO FIQUE MENOR A 5000" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from returns_caffees rc
inner join remittances_caffee_returns_caffees rcrc on rcrc.returns_caffee_id=rc.id
inner join clients AS cts on cts.id=rc.client_id
inner join remittances_caffee as rm on rcrc.remittances_caffee_id = rm.id
inner join departaments dep on rc.jetty = dep.id
inner join items_unoee pds on pds.codigo='0000204'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000204','001')
where rc.jetty=10 and rc.state_return="COMPLETADA" and cts.id!=1 AND rc.return_type = 'FIQUE' 
        and (  DATE(rc.return_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
group by rc.vehicle_plate,cts.exporter_code,date(rc.return_date), rc.jetty
having sum(rcrc.qta_bags) <=5000
ORDER BY exporter_code ASC
); 


/*------------------- DEVOLUCION PARTICULARES IMPALA FIQUE MAYOR A 5001 Y MENOR 10000  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    group_concat(rcrc.remittances_caffee_id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee,' x ' ,rcrc.qta_bags separator ' | ') as LOTE,
    1 AS CANTIDAD_OPERATIVA,
    rc.return_type AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    rc.return_date  AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    rc.state_return AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "DEVOLUCIONES PARTICULARES EN TERMINAL DE IMPALA TIPO FIQUE MAYOR A 5001 Y MENOR 10000" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from returns_caffees rc
inner join remittances_caffee_returns_caffees rcrc on rcrc.returns_caffee_id=rc.id
inner join clients AS cts on cts.id=rc.client_id
inner join remittances_caffee as rm on rcrc.remittances_caffee_id = rm.id
inner join departaments dep on rc.jetty = dep.id
inner join items_unoee pds on pds.codigo='0000010'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000010','001')
where rc.jetty=10 and rc.state_return="COMPLETADA" and cts.id!=1 AND rc.return_type = 'FIQUE' 
        and (  DATE(rc.return_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
group by rc.vehicle_plate,cts.exporter_code,date(rc.return_date), rc.jetty
having (sum(rcrc.qta_bags) between 5001 and 10000)
ORDER BY exporter_code ASC
); 




/*------------------- MUESTRAS ALMACAFE IMPALA -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    NULL AS COD_EXPORTADOR_SISCAFE,
    NULL AS CODIGO_EXPORTADOR,
    NULL AS NOMBRE_EXPORTADOR,
    NULL as LOTE,
    SUM(rc.quantity_radicated_bag_in) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS, 
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    @FECHA_SUP AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    b_rc.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "860010973" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    concat("MUESTRAS ALMACAFE IMPALA (", ss.name_place,")") AS DESC_CONSULTA,
    concat(pds.descripcion," IMPALA (", ss.name_place,")")  AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*SUM(rc.quantity_radicated_bag_in) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rc
inner join departaments dp on dp.id=rc.jetty
inner join slot_store ss on ss.id=rc.slot_store_id
inner join type_units tu on rc.type_units_id=tu.id
inner join bill b_rc on rc.bill_id=b_rc.id
inner join clients AS cts on cts.id=rc.client_id
inner join service_package sp on sp.id=0006
inner join service_package_has_items_services spi on spi.service_package_id=sp.id
inner join items_unoee pds on pds.codigo='0000047'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000047','001')
where rc.vehicle_plate !='FIQUE' and rc.bill_id!=2 and rc.bill_id!=3 and tu.id!=9  AND (ss.ref_departament=10 ) 
        and (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
group by ss.ref_departament
); 




/*------------------- SOLICITUD CONTRAMUESTRA DREYFUS IMPALA  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cs.lot_coffee_ref,' x ', cs.qta_bag separator ' | ') AS LOTE,
    sum(cs.qta_bag) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    date(cs.end_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    cs.status AS ESTADO,
    group_concat(cs.id separator ' | ') AS MUESTRA,
    group_concat(distinct(cs.supply_company) separator ' | ')  AS EMPRESA_SUMINISTRO,
    group_concat(distinct(cs.sample_packing) separator ' | ') AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    tsc.name AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "900174478" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    concat("SOLICITUD DE MUESTRA Y CONTRAMUESTRA DREYFUS COMPLETADA EN TERMINAL DE IMPALA " ) AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    sum(lp.precio*cs.qta_bag) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM coffee_sample AS cs
INNER JOIN sample_coffee_has_type_sample AS sct ON sct.sample_coffee_id = cs.id
INNER JOIN type_sample_coffe AS tsc ON tsc.id = sct.type_sample_coffee_id
INNER JOIN clients AS cts ON cts.id = cs.client_id
inner join items_unoee pds on pds.codigo='0000213'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000213','001')
WHERE cs.status = 'COMPLETADA' AND cs.get_sample = 1 AND (tsc.id=13 OR tsc.id=5) 
        AND (cs.terminal LIKE '%IMP%' )  
        AND (  DATE(cs.end_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
GROUP BY cts.exporter_code ,date(cs.end_date), tsc.name
ORDER BY cs.client_id ASC
); 


/*------------------- SOLICITUD CONTRAMUESTRA INCONEXUS IMPALA  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cs.lot_coffee_ref,' x ', cs.qta_bag separator ' | ') AS LOTE,
    sum(cs.qta_bag) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    date(cs.end_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    cs.status AS ESTADO,
    group_concat(cs.id separator ' | ') AS MUESTRA,
    group_concat(distinct(cs.supply_company) separator ' | ')  AS EMPRESA_SUMINISTRO,
    group_concat(distinct(cs.sample_packing) separator ' | ') AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    tsc.name AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "830092113" AS CLIENTE_UNOERP, 
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    concat("SOLICITUD DE MUESTRA Y CONTRAMUESTRA INCONEXUS COMPLETADA EN TERMINAL DE IMPALA  " ) AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    sum(lp.precio*cs.qta_bag) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM coffee_sample AS cs
INNER JOIN sample_coffee_has_type_sample AS sct ON sct.sample_coffee_id = cs.id
INNER JOIN type_sample_coffe AS tsc ON tsc.id = sct.type_sample_coffee_id
INNER JOIN clients AS cts ON cts.id = cs.client_id
inner join items_unoee pds on pds.codigo='0000064'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000064','001')
WHERE cs.status = 'COMPLETADA' AND cs.get_sample = 1 AND (tsc.id=15 OR tsc.id=7)
        AND (cs.terminal LIKE '%IMPALA%' )  
        AND (  DATE(cs.end_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
GROUP BY cts.exporter_code ,date(cs.end_date), tsc.name
ORDER BY cs.client_id ASC
); 



/*------------------- SOLICITUD CONTRAMUESTRA SUCAFINA IMPALA  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cs.lot_coffee_ref,' x ', cs.qta_bag separator ' | ') AS LOTE,
    sum(cs.qta_bag) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    date(cs.end_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    cs.status AS ESTADO,
    group_concat(cs.id separator ' | ') AS MUESTRA,
    group_concat(distinct(cs.supply_company) separator ' | ')  AS EMPRESA_SUMINISTRO,
    group_concat(distinct(cs.sample_packing) separator ' | ') AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    tsc.name AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "901012419" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    concat("SOLICITUD DE CONTRAMUESTRA SUCAFINA COMPLETADA EN TERMINAL DE IMPALA ") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    sum(lp.precio*cs.qta_bag) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM coffee_sample AS cs
INNER JOIN sample_coffee_has_type_sample AS sct ON sct.sample_coffee_id = cs.id
INNER JOIN type_sample_coffe AS tsc ON tsc.id = sct.type_sample_coffee_id
INNER JOIN clients AS cts ON cts.id = cs.client_id
inner join items_unoee pds on pds.codigo='0000064'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000064','001')
WHERE cs.status = 'COMPLETADA' AND cs.get_sample = 1 AND tsc.id=16
        AND (cs.terminal LIKE '%IMP%' )  
        AND (  DATE(cs.end_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
GROUP BY cts.exporter_code ,date(cs.end_date), tsc.name
ORDER BY cs.client_id ASC
); 


/*------------------- SOLICITUD CONTRAMUESTRA SUCDEN IMPALA  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cs.lot_coffee_ref,' x ', cs.qta_bag separator ' | ') AS LOTE,
    sum(cs.qta_bag) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    date(cs.end_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    cs.status AS ESTADO,
    group_concat(cs.id separator ' | ') AS MUESTRA,
    group_concat(distinct(cs.supply_company) separator ' | ')  AS EMPRESA_SUMINISTRO,
    group_concat(distinct(cs.sample_packing) separator ' | ') AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    tsc.name AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "900099234" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    concat("SOLICITUD DE MUESTRA Y CONTRAMUESTRA SUCDEN COMPLETADA EN TERMINAL DE IMPALA  ") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    sum(lp.precio*cs.qta_bag) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM coffee_sample AS cs
INNER JOIN sample_coffee_has_type_sample AS sct ON sct.sample_coffee_id = cs.id
INNER JOIN type_sample_coffe AS tsc ON tsc.id = sct.type_sample_coffee_id
INNER JOIN clients AS cts ON cts.id = cs.client_id
inner join items_unoee pds on pds.codigo='0000212'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000212','001')
WHERE cs.status = 'COMPLETADA' AND cs.get_sample = 1 AND (tsc.id=10 or tsc.id=11) 
        AND (cs.terminal LIKE '%IMP%' )  
        AND (  DATE(cs.end_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
GROUP BY cts.exporter_code ,date(cs.end_date), tsc.name
ORDER BY cs.client_id ASC
); 




/*------------------- SOLICITUD MUESTRA NESTLE IMPALA  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cs.lot_coffee_ref,' x ', cs.qta_bag separator ' | ') AS LOTE,
    sum(cs.qta_bag) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    date(cs.end_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    cs.status AS ESTADO,
    group_concat(cs.id separator ' | ') AS MUESTRA,
    group_concat(distinct(cs.supply_company) separator ' | ')  AS EMPRESA_SUMINISTRO,
    group_concat(distinct(cs.sample_packing) separator ' | ') AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    tsc.name AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "860002130" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    concat("SOLICITUD DE MUESTRA NESTLE COMPLETADA EN TERMINAL DE IMPALA " ) AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    sum(lp.precio*cs.qta_bag) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM coffee_sample AS cs
INNER JOIN sample_coffee_has_type_sample AS sct ON sct.sample_coffee_id = cs.id
INNER JOIN type_sample_coffe AS tsc ON tsc.id = sct.type_sample_coffee_id
INNER JOIN clients AS cts ON cts.id = cs.client_id
inner join items_unoee pds on pds.codigo='0000289'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000289','001')
WHERE cs.status = 'COMPLETADA' AND cs.get_sample = 1 AND tsc.id=19 
        AND (cs.terminal LIKE '%IMP%' )  
        AND (  DATE(cs.end_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
GROUP BY cts.exporter_code ,date(cs.end_date), tsc.name
ORDER BY cs.client_id ASC
); 


/*------------------- SOLICITUD MUESTRA PARTICULARES IMPALA  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cs.lot_coffee_ref,' x ', cs.qta_bag separator ' | ') AS LOTE,
    sum(cs.qta_bag) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    date(cs.end_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    cs.status AS ESTADO,
    group_concat(cs.id separator ' | ') AS MUESTRA,
    group_concat(distinct(cs.supply_company) separator ' | ')  AS EMPRESA_SUMINISTRO,
    group_concat(distinct(cs.sample_packing) separator ' | ') AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    tsc.name AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    concat("SOLICITUD DE MUESTRA PARTICULARES COMPLETADA EN TERMINAL DE IMPALA " ) AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    sum(lp.precio*cs.qta_bag) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM coffee_sample AS cs
INNER JOIN sample_coffee_has_type_sample AS sct ON sct.sample_coffee_id = cs.id
INNER JOIN type_sample_coffe AS tsc ON tsc.id = sct.type_sample_coffee_id
INNER JOIN clients AS cts ON cts.id = cs.client_id
inner join items_unoee pds on pds.codigo='0000064'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000064','001')
WHERE cs.status = 'COMPLETADA' AND cs.get_sample = 1 AND tsc.id=9 
        AND (cs.terminal LIKE '%IMP%' )  
        AND (  DATE(cs.end_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
GROUP BY cts.exporter_code ,date(cs.end_date), tsc.name
ORDER BY cs.client_id ASC
); 


/*------------------- SOLICITUD MUESTRA SUCAFINA IMPALA  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cs.lot_coffee_ref,' x ', cs.qta_bag separator ' | ') AS LOTE,
    sum(cs.qta_bag) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    date(cs.end_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    cs.status AS ESTADO,
    group_concat(cs.id separator ' | ') AS MUESTRA,
    group_concat(distinct(cs.supply_company) separator ' | ')  AS EMPRESA_SUMINISTRO,
    group_concat(distinct(cs.sample_packing) separator ' | ') AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    tsc.name AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "901012419" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    concat("SOLICITUD DE MUESTRA SUCAFINA COMPLETADA EN TERMINAL DE IMPALA" ) AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    sum(lp.precio*cs.qta_bag) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM coffee_sample AS cs
INNER JOIN sample_coffee_has_type_sample AS sct ON sct.sample_coffee_id = cs.id
INNER JOIN type_sample_coffe AS tsc ON tsc.id = sct.type_sample_coffee_id
INNER JOIN clients AS cts ON cts.id = cs.client_id
inner join items_unoee pds on pds.codigo='0000064'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000064','001')
WHERE cs.status = 'COMPLETADA' AND cs.get_sample = 1 AND tsc.id=12 
        AND (cs.terminal LIKE '%IMP%' )  
        AND (  DATE(cs.end_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
GROUP BY cts.exporter_code ,date(cs.end_date), tsc.name
ORDER BY cs.client_id ASC
); 


/*------------------- SOLICITUD MUESTRA WOLTHERS IMPALA  -------------------*/
INSERT INTO data_billing_full (
SELECT
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cs.lot_coffee_ref,' x ', cs.qta_bag separator ' | ') AS LOTE,
    sum(cs.qta_bag) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    date(cs.end_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    cs.status AS ESTADO,
    group_concat(cs.id separator ' | ') AS MUESTRA,
    group_concat(distinct(cs.supply_company) separator ' | ')  AS EMPRESA_SUMINISTRO,
    group_concat(distinct(cs.sample_packing) separator ' | ') AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    tsc.name AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "901202236" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    concat("SOLICITUD DE MUESTRA WOLTHERS COMPLETADA EN TERMINAL DE IMPALA" ) AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    sum(lp.precio*cs.qta_bag) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM coffee_sample AS cs
INNER JOIN sample_coffee_has_type_sample AS sct ON sct.sample_coffee_id = cs.id
INNER JOIN type_sample_coffe AS tsc ON tsc.id = sct.type_sample_coffee_id
INNER JOIN clients AS cts ON cts.id = cs.client_id
inner join items_unoee pds on pds.codigo='0000084'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000084','001')
WHERE cs.status = 'COMPLETADA' AND cs.get_sample = 1 AND tsc.id=3 
        AND (cs.terminal LIKE '%IMP%' )  
        AND (  DATE(cs.end_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
GROUP BY cts.exporter_code ,date(cs.end_date), tsc.name 
ORDER BY cs.client_id ASC
); 


/*------------------- SOLICITUD MUESTRA B HERMETICAS FNC IMPALA  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rm.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat(cts.exporter_code,'-',rm.lot_caffee,' x ', rm.quantity_radicated_bag_in) AS LOTE,
    (rm.quantity_radicated_bag_in*0.4) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    date(rm.created_date) AS FECHA_OPERACION,
    tu.name AS UNIDAD_CAFE,
    rm.bill_id AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "860007538" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    concat("SOLICITUD DE MUESTRA B. HERME FNC COMPLETADA EN TERMINAL DE IMPALA") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*(rm.quantity_radicated_bag_in*0.4) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM remittances_caffee AS rm
inner join clients cts on cts.id=rm.client_id
inner join type_units tu on tu.id=rm.type_units_id
inner join items_unoee pds on pds.codigo='0000066'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000066','007')
where rm.jetty=10 and tu.id=4 AND rm.client_id=1
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
); 



/*------------------- TRAZABILIDAD CONTENEDORES IMPALA  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-', rc.lot_caffee ,' x ',rc.quantity_radicated_bag_in separator ' | ') AS LOTES,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    ifn.packaging_mode AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    ivn.date_reg AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    ivn.id_tracking_siscafe  AS OIE,
    rdc.bic_container AS CONTENEDOR,
    pc.iso_ctn AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    ivn.ip_conection AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    concat("TRAZABILIDAD DE CONTENEDORES DESCARGADOS PARA EL TERMINAL DE IMPALA") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM invoice_note as ivn
INNER JOIN clients AS cts ON cts.id = ivn.id_expo_pilcaf
INNER JOIN packaging_caffee AS pc ON pc.id = ivn.id_tracking_siscafe 
INNER JOIN ready_container AS rdc ON rdc.id = pc.ready_container_id
INNER JOIN detail_packaging_caffee AS dpc on dpc.packaging_caffee_id = ivn.id_tracking_siscafe
INNER JOIN info_navy AS ifn ON ifn.id = pc.info_navy_id
INNER JOIN remittances_caffee AS rc ON rc.id = dpc.remittances_caffee_id
inner join items_unoee pds on pds.codigo='0000054'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000054','001')
WHERE  ivn.type_invoice= 1 AND pc.jetty='IMP' AND 
    (  DATE(ivn.date_reg) BETWEEN @FECHA_INF AND @FECHA_SUP   )
group by ivn.id_tracking_siscafe,rc.client_id,date(ivn.date_reg), pc.jetty
); 




/*------------------- TRAZABILIDAD VEHICULOS DESCARGADOS CARAVELA  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    ( select  group_concat( cls.exporter_code,'-',rc.lot_caffee,' x ',rc.quantity_radicated_bag_in separator ' ' )
        from remittances_caffee as rc
        inner join clients as cls on cls.id=rc.client_id 
        where rc.vehicle_plate=opt.ref_text and date(rc.created_date)= date(opt.created_date)
        group by rc.vehicle_plate,rc.created_date limit 1
    ) AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    opt.ref_text AS PLACA_VEHICULO,
    opt.created_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "TRAZABILIDAD DE VEHICULOS DESCARGADOS PARA EL TERMINAL DE IMPALA DEL EXPORTADOR CARAVELA" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM operation_tracking AS opt
INNER JOIN clients AS cts ON cts.id = opt.expo_id 
inner join items_unoee pds on pds.codigo='0000214'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000214','001')
WHERE opt.type_tracking = 1 AND opt.jetty='IMP' AND opt.expo_id =29 AND
    (  DATE(opt.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP   )
group by opt.ref_text,date(opt.created_date)
ORDER BY opt.created_date ASC 
); 


/*------------------- TRAZABILIDAD VEHICULOS DESCARGADOS CARCAFE  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    ( select  group_concat( cls.exporter_code,'-',rc.lot_caffee,' x ',rc.quantity_radicated_bag_in separator ' ' )
        from remittances_caffee as rc
        inner join clients as cls on cls.id=rc.client_id 
        where rc.vehicle_plate=opt.ref_text and date(rc.created_date)= date(opt.created_date)
        group by rc.vehicle_plate,rc.created_date limit 1
    ) AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    opt.ref_text AS PLACA_VEHICULO,
    opt.created_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "TRAZABILIDAD DE VEHICULOS DESCARGADOS PARA EL TERMINAL DE IMPALA DEL EXPORTADOR CARCAFE" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM operation_tracking AS opt
INNER JOIN clients AS cts ON cts.id = opt.expo_id 
inner join items_unoee pds on pds.codigo='0000086'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000086','001')
WHERE opt.type_tracking = 1 AND opt.jetty='IMP' AND opt.expo_id =7 AND
    (  DATE(opt.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP   )
group by opt.ref_text,date(opt.created_date)
ORDER BY opt.created_date ASC
); 


/*------------------- TRAZABILIDAD VEHICULOS DESCARGADOS INCONEXUS  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    ( select  group_concat( cls.exporter_code,'-',rc.lot_caffee,' x ',rc.quantity_radicated_bag_in separator ' ' )
        from remittances_caffee as rc
        inner join clients as cls on cls.id=rc.client_id 
        where rc.vehicle_plate=opt.ref_text and date(rc.created_date)= date(opt.created_date)
        group by rc.vehicle_plate,rc.created_date limit 1
    ) AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    opt.ref_text AS PLACA_VEHICULO,
    opt.created_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "TRAZABILIDAD DE VEHICULOS DESCARGADOS PARA EL TERMINAL DE IMPALA DEL EXPORTADOR INCONEXUS" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM operation_tracking AS opt
INNER JOIN clients AS cts ON cts.id = opt.expo_id 
inner join items_unoee pds on pds.codigo='0000149'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000149','001')
WHERE opt.type_tracking = 1 AND opt.jetty='IMP' AND opt.expo_id =61 AND
    (  DATE(opt.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP   )
group by opt.ref_text,date(opt.created_date)
ORDER BY opt.created_date ASC 
); 



/*------------------- SOLICITUD TOMA DE MUESTRA GRAIN PRO  IMPALA  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rm.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat(cts.exporter_code,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in ) AS LOTE,
    rm.quantity_radicated_bag_in  AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    date(rm.created_date) AS FECHA_OPERACION,
    uc.name_unit AS UNIDAD_CAFE,
    tu.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    concat("SOLICITUD DE MUESTRA B. HERME PARTICULARES COMPLETADA EN TERMINAL DE IMPALA ") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*rm.quantity_radicated_bag_in AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join type_units tu on tu.id=rm.type_units_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join items_unoee pds on pds.codigo="0000009"
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=("0000009",'001')
where rm.jetty=10 and tu.id=4 AND rm.client_id!=1
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
ORDER BY rm.client_id ASC
); 














