use siscafe_pro;
/*------------------- SET @DIAS_DE_FACTURACION=5; -------------------*/

/*------------------- SET @FECHA_INF=DATE(DATE_SUB(NOW(), INTERVAL @DIAS_DE_FACTURACION DAY)); -------------------*/
/*------------------- SET @FECHA_SUP=DATE(DATE_SUB(NOW(), INTERVAL 1 DAY)); -------------------*/

truncate data_billing_full;
truncate temp_log_data_unoerp;

SET @FECHA_INF=DATE("2021-03-31");
SET @FECHA_SUP=DATE("2021-04-11");


/*------------------- DESCARGUE FNC SPB tipo SACOS  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rm.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat(cts.exporter_code,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in) AS LOTE,
    rm.quantity_radicated_bag_in AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    DATE(rm.created_date) AS FECHA_OPERACION,
    uc.name_unit AS UNIDAD_CAFE,
    b_rm.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "DESCARGUE DE FNC EN TERMINAL SPB TIPO SACOS" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*rm.quantity_radicated_bag_in AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join bill b_rm on rm.bill_id=b_rm.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000110'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000110','007')
where ss.ref_departament=1 and rm.client_id =1 AND rm.bill_id!=2 
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
        and (client_id,vehicle_plate,date(rm.created_date),ss.ref_departament) 
        in (
            select client_id,vehicle_plate,date(rc.created_date),ss.ref_departament
            from remittances_caffee rc
            inner join slot_store ss on ss.id=rc.slot_store_id
            where ss.ref_departament=1 and rc.client_id =1 AND rc.bill_id!=2 AND (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP )
            group by rc.vehicle_plate,rc.client_id,date(rc.created_date),ss.ref_departament
        )
    ORDER BY DATE(rm.created_date) ASC
); 


/*------------------- DESCARGUE FNC SPB TIPO CAJAS -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rm.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat(cts.exporter_code,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in) AS LOTE,
    rm.quantity_radicated_bag_in AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    DATE(rm.created_date) AS FECHA_OPERACION,
    uc.name_unit AS UNIDAD_CAFE,
    b_rm.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "DESCARGUE DE FNC EN TERMINAL SPB TIPO CAJAS" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*rm.quantity_radicated_bag_in AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join bill b_rm on rm.bill_id=b_rm.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000285'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000285','007')
where ss.ref_departament=1 and rm.client_id =1 AND rm.bill_id=2 and rm.vehicle_plate!= 'FIQUE' 
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
        and (client_id,vehicle_plate,date(rm.created_date),ss.ref_departament) 
        in ( 
            select client_id,vehicle_plate,date(rc.created_date),ss.ref_departament
            from remittances_caffee rc
            inner join slot_store ss on ss.id=rc.slot_store_id
            where ss.ref_departament=1 and rc.client_id =1 AND rc.bill_id=2 AND (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP )
            group by rc.vehicle_plate,rc.client_id,date(rc.created_date),ss.ref_departament
        )
    ORDER BY DATE(rm.created_date) ASC
); 



/*------------------- DESCARGUE SPB MICROLOTES PARTICULARES -------------------*/
/*------------------- DESCARGUE SPB MICROLOTES MAYOR IGUAL A 5 LOTES -------------------*/
INSERT INTO data_billing_full (
select 
    NULL AS ID,
    group_concat(rc.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rc.lot_caffee,' x ',rc.quantity_radicated_bag_in separator ' | ') AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    DATE(rc.created_date) AS FECHA_OPERACION,
    group_concat(distinct(uc.name_unit) separator ' | ') AS UNIDAD_CAFE,
    group_concat(distinct(b_rc.name) separator ' | ') AS UNIDAD_FACTURA,
    count(rc.id) AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "DESCARGUE DE MICROLOTES PARTICULARES EN TERMINAL SPB MAYOR IGUAL A 5 LOTES" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rc
inner join clients cts on cts.id=rc.client_id
inner join units_caffee uc on rc.units_cafee_id=uc.id
inner join bill b_rc on rc.bill_id=b_rc.id
inner join slot_store ss on ss.id=rc.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000114'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000114','001')
where rc.microlot=1 and ss.ref_departament=1 and rc.client_id !=1 
     AND (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP )    
group by rc.vehicle_plate, date(rc.created_date) , rc.client_id, ss.ref_departament
having count(rc.id) >= 5
ORDER BY cts.exporter_code ASC
); 



/*------------------- DESCARGUE SPB MENOR A 100 PARTICULARES -------------------*/
/*------------------- DESCARGUE SPB MENOR A 100 SACOS PARTICULARES -------------------*/
INSERT INTO data_billing_full (
select 
    NULL AS ID,
    group_concat(rc.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rc.lot_caffee,' x ',rc.quantity_radicated_bag_in separator ' | ') AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    date(rc.created_date) AS FECHA_OPERACION,
    group_concat(distinct(uc.name_unit) separator ' | ')  AS UNIDAD_CAFE,
    b_id.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "DESCARGUE DE PARTICULARES EN TERMINAL SPB MENOR A 100 SACOS CAFE" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1  AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee AS rc
inner join clients cts on cts.id = rc.client_id
INNER JOIN bill AS b_id ON b_id.id = rc.bill_id
inner join slot_store ss on ss.id=rc.slot_store_id
inner join units_caffee AS uc on rc.units_cafee_id=uc.id
inner join departaments dep on dep.id=ss.ref_departament
inner join items_unoee pds on pds.codigo='0000270'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000270','001')
where ss.ref_departament=1 and rc.client_id != 1 and rc.microlot=0 AND rc.bill_id=1
         AND (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP )
group by rc.vehicle_plate,rc.client_id,date(rc.created_date), ss.ref_departament
having sum(rc.quantity_radicated_bag_in) <=100
    ORDER BY cts.exporter_code ASC
); 


/*------------------- DESCARGUE SPB MENOR A 250 y MAYOR A 101 PARTICULARES -------------------*/
/*------------------- DESCARGUE SPB MENOR A 250 y MAYOR A 101 SACOS PARTICULARES -------------------*/
INSERT INTO data_billing_full (
select 
    NULL AS ID,
    group_concat(rc.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rc.lot_caffee,' x ',rc.quantity_radicated_bag_in separator ' | ') AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    date(rc.created_date) AS FECHA_OPERACION,
    group_concat(distinct(uc.name_unit) separator ' | ')  AS UNIDAD_CAFE,
    b_id.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "DESCARGUE DE PARTICULARES EN TERMINAL SPB DE 101 A 250 SACOS CAFE" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee AS rc
inner join clients cts on cts.id = rc.client_id
INNER JOIN bill AS b_id ON b_id.id = rc.bill_id
inner join slot_store ss on ss.id=rc.slot_store_id
inner join units_caffee AS uc on rc.units_cafee_id=uc.id
inner join departaments dep on dep.id=ss.ref_departament
inner join items_unoee pds on pds.codigo='0000287'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000287','001')
where ss.ref_departament=1 and rc.client_id != 1 and rc.microlot=0 AND rc.bill_id=1
         AND (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP )
group by rc.vehicle_plate,rc.client_id,date(rc.created_date), ss.ref_departament
having sum(rc.quantity_radicated_bag_in) BETWEEN 101 AND 250
    ORDER BY cts.exporter_code ASC
); 



/*------------------- DESCARGUE SPB MAYOR DE 250 PARTICULARES -------------------*/
/*------------------- DESCARGUE SPB MAYOR DE 250 SACOS DE 1-25 KG SACOS CAFE  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rm.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( cts.exporter_code ,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in ) AS LOTE,
    rm.quantity_radicated_bag_in AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    DATE(rm.created_date) AS FECHA_OPERACION,
    uc.name_unit AS UNIDAD_CAFE,
    b_rm.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "DESCARGUE DE PARTICULARES EN TERMINAL SPB MAYOR DE 250 SACOS DE 1-25 KG SACOS CAFE" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*rm.quantity_radicated_bag_in AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join bill b_rm on rm.bill_id=b_rm.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000222'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000222','001')
where ss.ref_departament=1 and rm.microlot=0 and rm.client_id !=1 AND rm.bill_id=1 AND uc.quantity <= 25
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
        and (client_id,vehicle_plate,date(rm.created_date),ss.ref_departament) 
        in (
            select client_id,vehicle_plate,date(rc.created_date),ss.ref_departament
            from remittances_caffee rc
            inner join slot_store ss on ss.id=rc.slot_store_id
            where ss.ref_departament=1 and rc.microlot=0 and rc.client_id !=1 AND rc.bill_id=1 AND (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP )
            group by rc.vehicle_plate,rc.client_id,date(rc.created_date),ss.ref_departament
            having sum(rc.quantity_radicated_bag_in) >250
        )
    ORDER BY cts.exporter_code ASC
); 


/*------------------- DESCARGUE SPB MAYOR DE 250 SACOS DE 30 KG SACOS CAFE  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rm.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( cts.exporter_code ,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in ) AS LOTE,
    rm.quantity_radicated_bag_in AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    DATE(rm.created_date) AS FECHA_OPERACION,
    uc.name_unit AS UNIDAD_CAFE,
    b_rm.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "DESCARGUE DE PARTICULARES EN TERMINAL SPB MAYOR DE 250 SACOS DE 30 KG SACOS CAFE" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*rm.quantity_radicated_bag_in AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join bill b_rm on rm.bill_id=b_rm.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000223'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000223','001')
where ss.ref_departament=1 and rm.microlot=0 and rm.client_id !=1 AND rm.bill_id=1 AND uc.quantity = 30
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
        and (client_id,vehicle_plate,date(rm.created_date),ss.ref_departament) 
        in ( 
            select client_id,vehicle_plate,date(rc.created_date),ss.ref_departament
            from remittances_caffee rc
            inner join slot_store ss on ss.id=rc.slot_store_id
            where ss.ref_departament=1 and rc.microlot=0 and rc.client_id !=1 AND rc.bill_id=1  AND (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP )
            group by rc.vehicle_plate,rc.client_id,date(rc.created_date),ss.ref_departament
            having sum(rc.quantity_radicated_bag_in) >250
        )
    ORDER BY cts.exporter_code ASC
); 


/*------------------- DESCARGUE SPB MAYOR DE 250 SACOS DE 35 KG SACOS CAFE  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rm.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( cts.exporter_code ,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in ) AS LOTE,
    rm.quantity_radicated_bag_in AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    DATE(rm.created_date) AS FECHA_OPERACION,
    uc.name_unit AS UNIDAD_CAFE,
    b_rm.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "DESCARGUE DE PARTICULARES EN TERMINAL SPB MAYOR DE 250 SACOS DE 35 KG SACOS CAFE" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*rm.quantity_radicated_bag_in AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join bill b_rm on rm.bill_id=b_rm.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000224'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000224','001')
where ss.ref_departament=1 and rm.microlot=0 and rm.client_id !=1 AND rm.bill_id=1 AND uc.quantity = 35
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
        and (client_id,vehicle_plate,date(rm.created_date),ss.ref_departament) 
        in (
            select client_id,vehicle_plate,date(rc.created_date),ss.ref_departament
            from remittances_caffee rc
            inner join slot_store ss on ss.id=rc.slot_store_id
            where ss.ref_departament=1 and rc.microlot=0 and rc.client_id !=1 AND rc.bill_id=1 AND (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP )
            group by rc.vehicle_plate,rc.client_id,date(rc.created_date),ss.ref_departament
            having sum(rc.quantity_radicated_bag_in) >250
        )
    ORDER BY cts.exporter_code ASC
); 


/*------------------- DESCARGUE SPB MAYOR DE 250 SACOS DE 40 - 70 KG SACOS CAFE  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rm.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( cts.exporter_code ,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in ) AS LOTE,
    rm.quantity_radicated_bag_in AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    DATE(rm.created_date) AS FECHA_OPERACION,
    uc.name_unit AS UNIDAD_CAFE,
    b_rm.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "DESCARGUE DE PARTICULARES EN TERMINAL SPB MAYOR DE 250 SACOS DE 40-70 KG SACOS CAFE" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*rm.quantity_radicated_bag_in AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join bill b_rm on rm.bill_id=b_rm.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000111'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000111','001')
where ss.ref_departament=1 and rm.microlot=0 and rm.client_id !=1 AND rm.bill_id=1 AND ( uc.quantity between 40 and 70 )
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
        and (client_id,vehicle_plate,date(rm.created_date),ss.ref_departament) 
        in (
            select client_id,vehicle_plate,date(rc.created_date),ss.ref_departament
            from remittances_caffee rc
            inner join slot_store ss on ss.id=rc.slot_store_id
            where ss.ref_departament=1 and rc.microlot=0 and rc.client_id !=1 AND rc.bill_id=1 AND (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP )
            group by rc.vehicle_plate,rc.client_id,date(rc.created_date),ss.ref_departament
            having sum(rc.quantity_radicated_bag_in) >250
        )
    ORDER BY cts.exporter_code ASC
); 


/*------------------- DESCARGUE SPB SACOS CACAO  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rm.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( cts.exporter_code ,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in ) AS LOTE,
    rm.quantity_radicated_bag_in AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    DATE(rm.created_date) AS FECHA_OPERACION,
    uc.name_unit AS UNIDAD_CAFE,
    b_rm.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "DESCARGUE DE PARTICULARES EN TERMINAL SPB SACOS CACAO" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*rm.quantity_radicated_bag_in AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join bill b_rm on rm.bill_id=b_rm.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000049'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000049','001')
where ss.ref_departament=1 and rm.microlot=0 and rm.client_id !=1 AND rm.bill_id=3 
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )        
    ORDER BY cts.exporter_code ASC
); 


/*------------------- DESCARGUE SPB SACOS BIG BAGS  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rm.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( cts.exporter_code ,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in ) AS LOTE,
    rm.quantity_radicated_bag_in AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    DATE(rm.created_date) AS FECHA_OPERACION,
    uc.name_unit AS UNIDAD_CAFE,
    b_rm.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "DESCARGUE DE PARTICULARES EN TERMINAL SPB TIPO BIG BAGS" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*rm.quantity_radicated_bag_in  AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join bill b_rm on rm.bill_id=b_rm.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000184'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000184','001')
where ss.ref_departament=1 and rm.microlot=0 and rm.client_id !=1 AND rm.bill_id=4  AND uc.quantity=1000
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
    ORDER BY cts.exporter_code ASC
);  



/*------------------- DESCARGUE SPB SACOS MINI BIG BAGS  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rm.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( cts.exporter_code ,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in ) AS LOTE,
    rm.quantity_radicated_bag_in AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    DATE(rm.created_date) AS FECHA_OPERACION,
    uc.name_unit AS UNIDAD_CAFE,
    b_rm.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "DESCARGUE DE PARTICULARES EN TERMINAL SPB TIPO MINI BIG BAGS" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*rm.quantity_radicated_bag_in  AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join bill b_rm on rm.bill_id=b_rm.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000347'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000347','001')
where ss.ref_departament=1 and rm.microlot=0 and rm.client_id !=1 AND rm.bill_id=4 AND uc.quantity!=1000
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
    ORDER BY cts.exporter_code ASC
); 



/*------------------- DESCARGUE SPB CAJAS DE 1 A 10  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    DATE(rm.created_date) AS FECHA_OPERACION,
    group_concat(distinct(uc.name_unit) separator ' | ') AS UNIDAD_CAFE,
    group_concat(distinct(b_rm.name) separator ' | ') AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "DESCARGUE DE PARTICULARES EN TERMINAL SPB TIPO CAJAS DE 1 A 10 UNIDADES" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join bill b_rm on rm.bill_id=b_rm.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000032'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000032','001')
where ss.ref_departament=1 and rm.microlot=0 and rm.client_id !=1 AND rm.bill_id=2 and rm.vehicle_plate!= 'FIQUE' 
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by rm.vehicle_plate,rm.client_id,date(rm.created_date),ss.ref_departament
having sum(rm.quantity_radicated_bag_in) <= 10 
ORDER BY cts.exporter_code ASC
); 


/*------------------- DESCARGUE SPB CAJAS DE 11 A 100  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    DATE(rm.created_date) AS FECHA_OPERACION,
    group_concat(distinct(uc.name_unit) separator ' | ') AS UNIDAD_CAFE,
    group_concat(distinct(b_rm.name) separator ' | ') AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "DESCARGUE DE PARTICULARES EN TERMINAL SPB TIPO CAJAS DE 11 A 100 UNIDADES" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join bill b_rm on rm.bill_id=b_rm.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000203'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000203','001')
where ss.ref_departament=1 and rm.microlot=0 and rm.client_id !=1 AND rm.bill_id=2
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by rm.vehicle_plate,rm.client_id,date(rm.created_date),ss.ref_departament 
having sum(rm.quantity_radicated_bag_in) between 11 and 100      
ORDER BY cts.exporter_code ASC
); 


/*------------------- DESCARGUE SPB CAJAS MAYOR DE 100  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    sum(rm.quantity_radicated_bag_in) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    DATE(rm.created_date) AS FECHA_OPERACION,
    group_concat(distinct(uc.name_unit) separator ' | ') AS UNIDAD_CAFE,
    group_concat(distinct(b_rm.name) separator ' | ') AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "DESCARGUE DE PARTICULARES EN TERMINAL SPB TIPO CAJAS MAYOR A 100 UNIDADES" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*sum(rm.quantity_radicated_bag_in) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join bill b_rm on rm.bill_id=b_rm.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000215'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000215','001')
where ss.ref_departament=1 and rm.microlot=0 and rm.client_id !=1 AND rm.bill_id=2 
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by rm.vehicle_plate,rm.client_id,date(rm.created_date),ss.ref_departament 
having sum(rm.quantity_radicated_bag_in) > 100
ORDER BY cts.exporter_code ASC
); 


/*------------------- DESCARGUE SPB CAFE VERDE ESTIBAS  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rm.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat(cts.exporter_code,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in) AS LOTE,
    rm.quantity_radicated_bag_in AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    DATE(rm.created_date) AS FECHA_OPERACION,
    uc.name_unit AS UNIDAD_CAFE,
    b_rm.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "DESCARGUE DE PARTICULARES EN TERMINAL SPB TIPO CAFE VERDE ESTIBAS" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join bill b_rm on rm.bill_id=b_rm.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo="0000295"
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=("0000295",'001')
where ss.ref_departament=1 and rm.microlot=0 and rm.client_id !=1 AND rm.bill_id=18 
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
ORDER BY cts.exporter_code ASC
); 



/*------------------- DEVOLUCIONES FNC -------------------*/
/*------------------- DEVOLUCION FNC CAFE SPB CAFE  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    group_concat(rcrc.remittances_caffee_id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee,' x ' ,rcrc.qta_bags separator ' | ') as LOTE,
    sum(rcrc.qta_bags) AS CANTIDAD_OPERATIVA,
    rc.return_type AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    rc.return_date  AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    rc.state_return AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "DEVOLUCIONES FNC EN TERMINAL SPB TIPO CAFE" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*sum(rcrc.qta_bags) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from returns_caffees rc
inner join remittances_caffee_returns_caffees rcrc on rcrc.returns_caffee_id=rc.id
inner join clients as cts on cts.id=rc.client_id
inner join remittances_caffee as rm on rcrc.remittances_caffee_id = rm.id
inner join departaments dep on rc.jetty = dep.id
inner join items_unoee pds on pds.codigo='0000288'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000288','007')
where rc.jetty=1 and rc.state_return="COMPLETADA" and cts.id=1 AND rc.return_type = 'CAFE' 
        and (  DATE(rc.return_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
group by rc.vehicle_plate,cts.exporter_code,date(rc.return_date), rc.jetty
); 

/*------------------- DEVOLUCION FNC CAFE SPB FIQUE MAYOR A 10000  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    group_concat(rcrc.remittances_caffee_id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee,' x ' ,rcrc.qta_bags separator ' | ') as LOTE,
    1 AS CANTIDAD_OPERATIVA,
    rc.return_type AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    rc.return_date  AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    rc.state_return AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "DEVOLUCIONES FNC EN TERMINAL SPB TIPO FIQUE MAYOR A 10000" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from returns_caffees rc
inner join remittances_caffee_returns_caffees rcrc on rcrc.returns_caffee_id=rc.id
inner join clients as cts on cts.id=rc.client_id
inner join remittances_caffee as rm on rcrc.remittances_caffee_id = rm.id
inner join departaments dep on rc.jetty = dep.id
inner join items_unoee pds on pds.codigo='0000127'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000127','007')
where rc.jetty=1 and rc.state_return="COMPLETADA" and cts.id=1 AND rc.return_type = 'FIQUE' 
        and (  DATE(rc.return_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
group by rc.vehicle_plate,cts.exporter_code,date(rc.return_date), rc.jetty
having sum(rcrc.qta_bags) > 10000
); 

/*------------------- DEVOLUCION FNC CAFE SPB FIQUE MENOR A 10000  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    group_concat(rcrc.remittances_caffee_id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee,' x ' ,rcrc.qta_bags separator ' | ') as LOTE,
    1 AS CANTIDAD_OPERATIVA,
    rc.return_type AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    rc.return_date  AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    rc.state_return AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "DEVOLUCIONES FNC EN TERMINAL SPB TIPO FIQUE MENOR A 10000" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from returns_caffees rc
inner join remittances_caffee_returns_caffees rcrc on rcrc.returns_caffee_id=rc.id
inner join clients as cts on cts.id=rc.client_id
inner join remittances_caffee as rm on rcrc.remittances_caffee_id = rm.id
inner join departaments dep on rc.jetty = dep.id
inner join items_unoee pds on pds.codigo='0000219'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000219','007')
where rc.jetty=1 and rc.state_return="COMPLETADA" and cts.id=1 AND rc.return_type = 'FIQUE' 
    and (  DATE(rc.return_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
group by rc.vehicle_plate,cts.exporter_code,date(rc.return_date), rc.jetty
having sum(rcrc.qta_bags) <=10000
); 



/*------------------- DEVOLUCION PARTICULARES CAFE SPB MAYOR 250  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    group_concat(rcrc.remittances_caffee_id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee,' x ' ,rcrc.qta_bags separator ' | ') as LOTE,
    sum(rcrc.qta_bags) AS CANTIDAD_OPERATIVA,
    rc.return_type AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    rc.return_date  AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    rc.state_return AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "DEVOLUCIONES PARTICULARES EN TERMINAL SPB TIPO CAFE MAYOR A 250" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*sum(rcrc.qta_bags) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from returns_caffees rc
inner join remittances_caffee_returns_caffees rcrc on rcrc.returns_caffee_id=rc.id
inner join clients AS cts on cts.id=rc.client_id
inner join remittances_caffee as rm on rcrc.remittances_caffee_id = rm.id
inner join departaments dep on rc.jetty = dep.id
inner join items_unoee pds on pds.codigo='0000277'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000277','001')
where rc.jetty=1 and rc.state_return="COMPLETADA" and cts.id!=1 AND rc.return_type = 'CAFE' 
        and (  DATE(rc.return_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
group by rc.vehicle_plate,cts.exporter_code,date(rc.return_date), rc.jetty
having sum(rcrc.qta_bags) >250
ORDER BY exporter_code ASC
); 

/*------------------- DEVOLUCION PARTICULARES CAFE SPB MENOR 250  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    group_concat(rcrc.remittances_caffee_id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee,' x ' ,rcrc.qta_bags separator ' | ') as LOTE,
    1 AS CANTIDAD_OPERATIVA,
    rc.return_type AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    rc.return_date  AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    rc.state_return AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "DEVOLUCIONES PARTICULARES EN TERMINAL SPB TIPO CAFE MENOR A 250" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from returns_caffees rc
inner join remittances_caffee_returns_caffees rcrc on rcrc.returns_caffee_id=rc.id
inner join clients AS cts on cts.id=rc.client_id
inner join remittances_caffee as rm on rcrc.remittances_caffee_id = rm.id
inner join departaments dep on rc.jetty = dep.id
inner join items_unoee pds on pds.codigo='0000001'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000001','001')
where rc.jetty=1 and rc.state_return="COMPLETADA" and cts.id!=1 AND rc.return_type = 'CAFE' 
        and (  DATE(rc.return_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
group by rc.vehicle_plate,cts.exporter_code,date(rc.return_date), rc.jetty
having sum(rcrc.qta_bags) <=250
ORDER BY exporter_code ASC
); 

/*------------------- DEVOLUCION PARTICULARES SPB FIQUE MAYOR A 10000  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    group_concat(rcrc.remittances_caffee_id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee,' x ' ,rcrc.qta_bags separator ' | ') as LOTE,
    1 AS CANTIDAD_OPERATIVA,
    rc.return_type AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    rc.return_date  AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    rc.state_return AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "DEVOLUCIONES PARTICULARES EN TERMINAL SPB TIPO FIQUE MAYOR A 10000" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from returns_caffees rc
inner join remittances_caffee_returns_caffees rcrc on rcrc.returns_caffee_id=rc.id
inner join clients AS cts on cts.id=rc.client_id
inner join remittances_caffee as rm on rcrc.remittances_caffee_id = rm.id
inner join departaments dep on rc.jetty = dep.id
inner join items_unoee pds on pds.codigo='0000127'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000127','001')
where rc.jetty=1 and rc.state_return="COMPLETADA" and cts.id!=1 AND rc.return_type = 'FIQUE' 
        and (  DATE(rc.return_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
group by rc.vehicle_plate,cts.exporter_code,date(rc.return_date), rc.jetty
having sum(rcrc.qta_bags) >10000
ORDER BY exporter_code ASC
); 

/*------------------- DEVOLUCION PARTICULARES SPB FIQUE MENOR A 5000 -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    group_concat(rcrc.remittances_caffee_id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee,' x ' ,rcrc.qta_bags separator ' | ') as LOTE,
    1 AS CANTIDAD_OPERATIVA,
    rc.return_type AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    rc.return_date  AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    rc.state_return AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "DEVOLUCIONES PARTICULARES EN TERMINAL SPB TIPO FIQUE MENOR A 5000" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from returns_caffees rc
inner join remittances_caffee_returns_caffees rcrc on rcrc.returns_caffee_id=rc.id
inner join clients AS cts on cts.id=rc.client_id
inner join remittances_caffee as rm on rcrc.remittances_caffee_id = rm.id
inner join departaments dep on rc.jetty = dep.id
inner join items_unoee pds on pds.codigo='0000204'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000204','001')
where rc.jetty=1 and rc.state_return="COMPLETADA" and cts.id!=1 AND rc.return_type = 'FIQUE' 
        and (  DATE(rc.return_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
group by rc.vehicle_plate,cts.exporter_code,date(rc.return_date), rc.jetty
having sum(rcrc.qta_bags) <=5000
ORDER BY exporter_code ASC
); 


/*------------------- DEVOLUCION PARTICULARES SPB FIQUE MAYOR A 5001 Y MENOR 10000  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    group_concat(rcrc.remittances_caffee_id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee,' x ' ,rcrc.qta_bags separator ' | ') as LOTE,
    1 AS CANTIDAD_OPERATIVA,
    rc.return_type AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    rc.return_date  AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    rc.state_return AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "DEVOLUCIONES PARTICULARES EN TERMINAL SPB TIPO FIQUE MAYOR A 5001 Y MENOR 10000" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from returns_caffees rc
inner join remittances_caffee_returns_caffees rcrc on rcrc.returns_caffee_id=rc.id
inner join clients AS cts on cts.id=rc.client_id
inner join remittances_caffee as rm on rcrc.remittances_caffee_id = rm.id
inner join departaments dep on rc.jetty = dep.id
inner join items_unoee pds on pds.codigo='0000010'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000010','001')
where rc.jetty=1 and rc.state_return="COMPLETADA" and cts.id!=1 AND rc.return_type = 'FIQUE' 
        and (  DATE(rc.return_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
group by rc.vehicle_plate,cts.exporter_code,date(rc.return_date), rc.jetty
having (sum(rcrc.qta_bags) between 5001 and 10000)
ORDER BY exporter_code ASC
); 




/*------------------- MUESTRAS ALMACAFE BUENAVENTURA -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    NULL AS COD_EXPORTADOR_SISCAFE,
    NULL AS CODIGO_EXPORTADOR,
    NULL AS NOMBRE_EXPORTADOR,
    NULL as LOTE,
    SUM(rc.quantity_radicated_bag_in) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    @FECHA_SUP AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    b_rc.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "860010973" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    concat("MUESTRAS ALMACAFE BUENAVENTURA (", ss.name_place,")") AS DESC_CONSULTA,
    concat(pds.descripcion," BUENAVENTURA (", ss.name_place,")")  AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*SUM(rc.quantity_radicated_bag_in) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rc
inner join departaments dp on dp.id=rc.jetty
inner join slot_store ss on ss.id=rc.slot_store_id
inner join type_units tu on rc.type_units_id=tu.id
inner join bill b_rc on rc.bill_id=b_rc.id
inner join clients AS cts on cts.id=rc.client_id
inner join service_package sp on sp.id=0006
inner join service_package_has_items_services spi on spi.service_package_id=sp.id
inner join items_unoee pds on pds.codigo='0000047'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000047','001')
where rc.vehicle_plate !='FIQUE' and rc.bill_id!=2 and rc.bill_id!=3 and tu.id!=9 
        AND (ss.ref_departament=1 OR ss.ref_departament=7) 
        and (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
group by ss.ref_departament
); 




/*------------------- SOLICITUD CONTRAMUESTRA DREYFUS SPB, SPIA Y TCBUEN  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cs.lot_coffee_ref,' x ', cs.qta_bag separator ' | ') AS LOTE,
    sum(cs.qta_bag) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    date(cs.end_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    cs.status AS ESTADO,
    group_concat(cs.id separator ' | ') AS MUESTRA,
    group_concat(distinct(cs.supply_company) separator ' | ')  AS EMPRESA_SUMINISTRO,
    group_concat(distinct(cs.sample_packing) separator ' | ') AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    tsc.name AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "900174478" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    concat("SOLICITUD DE MUESTRA Y CONTRAMUESTRA DREYFUS COMPLETADA EN PUERTO DE BUENAVENTURA TERMINAL ",IF(cs.terminal LIKE "%SPB%", "SPB",cs.terminal) ) AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    sum(lp.precio*cs.qta_bag) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM coffee_sample AS cs
INNER JOIN sample_coffee_has_type_sample AS sct ON sct.sample_coffee_id = cs.id
INNER JOIN type_sample_coffe AS tsc ON tsc.id = sct.type_sample_coffee_id
INNER JOIN clients AS cts ON cts.id = cs.client_id
inner join items_unoee pds on pds.codigo='0000213'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000213','001')
WHERE cs.status = 'COMPLETADA' AND cs.get_sample = 1 AND (tsc.id=13 OR tsc.id=5) 
        AND (cs.terminal LIKE '%SPB%'  OR cs.terminal like'%TCBUEN%' OR cs.terminal like'%SPIA%')  
        AND (  DATE(cs.end_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
GROUP BY cts.exporter_code ,date(cs.end_date), tsc.name, IF(cs.terminal LIKE "%SPB%", "SPB",cs.terminal)
ORDER BY cs.client_id ASC
); 


/*------------------- SOLICITUD CONTRAMUESTRA INCONEXUS SPB, SPIA Y TCBUEN  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cs.lot_coffee_ref,' x ', cs.qta_bag separator ' | ') AS LOTE,
    sum(cs.qta_bag) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    date(cs.end_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    cs.status AS ESTADO,
    group_concat(cs.id separator ' | ') AS MUESTRA,
    group_concat(distinct(cs.supply_company) separator ' | ')  AS EMPRESA_SUMINISTRO,
    group_concat(distinct(cs.sample_packing) separator ' | ') AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    tsc.name AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "830092113" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    concat("SOLICITUD DE MUESTRA Y CONTRAMUESTRA INCONEXUS COMPLETADA EN PUERTO DE BUENAVENTURA TERMINAL ",IF(cs.terminal LIKE "%SPB%", "SPB",cs.terminal) ) AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    sum(lp.precio*cs.qta_bag) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM coffee_sample AS cs
INNER JOIN sample_coffee_has_type_sample AS sct ON sct.sample_coffee_id = cs.id
INNER JOIN type_sample_coffe AS tsc ON tsc.id = sct.type_sample_coffee_id
INNER JOIN clients AS cts ON cts.id = cs.client_id
inner join items_unoee pds on pds.codigo='0000064'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000064','001')
WHERE cs.status = 'COMPLETADA' AND cs.get_sample = 1 AND (tsc.id=15 OR tsc.id=7)
        AND (cs.terminal LIKE '%SPB%'  OR cs.terminal like'%TCBUEN%' OR cs.terminal like'%SPIA%')  
        AND (  DATE(cs.end_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
GROUP BY cts.exporter_code ,date(cs.end_date), tsc.name, IF(cs.terminal LIKE "%SPB%", "SPB",cs.terminal)
ORDER BY cs.client_id ASC
); 



/*------------------- SOLICITUD CONTRAMUESTRA SUCAFINA SPB, SPIA Y TCBUEN  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cs.lot_coffee_ref,' x ', cs.qta_bag separator ' | ') AS LOTE,
    sum(cs.qta_bag) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    date(cs.end_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    cs.status AS ESTADO,
    group_concat(cs.id separator ' | ') AS MUESTRA,
    group_concat(distinct(cs.supply_company) separator ' | ')  AS EMPRESA_SUMINISTRO,
    group_concat(distinct(cs.sample_packing) separator ' | ') AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    tsc.name AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "901012419" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    concat("SOLICITUD DE CONTRAMUESTRA SUCAFINA COMPLETADA EN PUERTO DE BUENAVENTURA TERMINAL ",IF(cs.terminal LIKE "%SPB%", "SPB",cs.terminal) ) AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    sum(lp.precio*cs.qta_bag) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM coffee_sample AS cs
INNER JOIN sample_coffee_has_type_sample AS sct ON sct.sample_coffee_id = cs.id
INNER JOIN type_sample_coffe AS tsc ON tsc.id = sct.type_sample_coffee_id
INNER JOIN clients AS cts ON cts.id = cs.client_id
inner join items_unoee pds on pds.codigo='0000064'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000064','001')
WHERE cs.status = 'COMPLETADA' AND cs.get_sample = 1 AND tsc.id=16
        AND (cs.terminal LIKE '%SPB%'  OR cs.terminal like'%TCBUEN%' OR cs.terminal like'%SPIA%')  
        AND (  DATE(cs.end_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
GROUP BY cts.exporter_code ,date(cs.end_date), tsc.name, IF(cs.terminal LIKE "%SPB%", "SPB",cs.terminal)
ORDER BY cs.client_id ASC
); 


/*------------------- SOLICITUD CONTRAMUESTRA SUCDEN SPB, SPIA Y TCBUEN  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cs.lot_coffee_ref,' x ', cs.qta_bag separator ' | ') AS LOTE,
    sum(cs.qta_bag) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    date(cs.end_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    cs.status AS ESTADO,
    group_concat(cs.id separator ' | ') AS MUESTRA,
    group_concat(distinct(cs.supply_company) separator ' | ')  AS EMPRESA_SUMINISTRO,
    group_concat(distinct(cs.sample_packing) separator ' | ') AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    tsc.name AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "900099234" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    concat("SOLICITUD DE MUESTRA Y CONTRAMUESTRA SUCDEN COMPLETADA EN PUERTO DE BUENAVENTURA TERMINAL ",IF(cs.terminal LIKE "%SPB%", "SPB",cs.terminal) ) AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    sum(lp.precio*cs.qta_bag) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM coffee_sample AS cs
INNER JOIN sample_coffee_has_type_sample AS sct ON sct.sample_coffee_id = cs.id
INNER JOIN type_sample_coffe AS tsc ON tsc.id = sct.type_sample_coffee_id
INNER JOIN clients AS cts ON cts.id = cs.client_id
inner join items_unoee pds on pds.codigo='0000212'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000212','001')
WHERE cs.status = 'COMPLETADA' AND cs.get_sample = 1 AND (tsc.id=10 or tsc.id=11) 
        AND (cs.terminal LIKE '%SPB%'  OR cs.terminal like'%TCBUEN%' OR cs.terminal like'%SPIA%')   
        AND (  DATE(cs.end_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
GROUP BY cts.exporter_code ,date(cs.end_date), tsc.name, IF(cs.terminal LIKE "%SPB%", "SPB",cs.terminal)
ORDER BY cs.client_id ASC
); 




/*------------------- SOLICITUD MUESTRA PARTICULARES SPB, SPIA Y TCBUEN  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cs.lot_coffee_ref,' x ', cs.qta_bag separator ' | ') AS LOTE,
    sum(cs.qta_bag) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    date(cs.end_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    cs.status AS ESTADO,
    group_concat(cs.id separator ' | ') AS MUESTRA,
    group_concat(distinct(cs.supply_company) separator ' | ')  AS EMPRESA_SUMINISTRO,
    group_concat(distinct(cs.sample_packing) separator ' | ') AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    tsc.name AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    concat("SOLICITUD DE MUESTRA PARTICULARES COMPLETADA EN PUERTO DE BUENAVENTURA TERMINAL ",IF(cs.terminal LIKE "%SPB%", "SPB",cs.terminal) ) AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    sum(lp.precio*cs.qta_bag) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM coffee_sample AS cs
INNER JOIN sample_coffee_has_type_sample AS sct ON sct.sample_coffee_id = cs.id
INNER JOIN type_sample_coffe AS tsc ON tsc.id = sct.type_sample_coffee_id
INNER JOIN clients AS cts ON cts.id = cs.client_id
inner join items_unoee pds on pds.codigo='0000064'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000064','001')
WHERE cs.status = 'COMPLETADA' AND cs.get_sample = 1 AND tsc.id=9 
        AND (cs.terminal LIKE '%SPB%'  OR cs.terminal like'%TCBUEN%' OR cs.terminal like'%SPIA%')  
        AND (  DATE(cs.end_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
GROUP BY cts.exporter_code ,date(cs.end_date), tsc.name, IF(cs.terminal LIKE "%SPB%", "SPB",cs.terminal)
ORDER BY cs.client_id ASC
); 


/*------------------- SOLICITUD MUESTRA SUCAFINA SPB, SPIA Y TCBUEN  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cs.lot_coffee_ref,' x ', cs.qta_bag separator ' | ') AS LOTE,
    sum(cs.qta_bag) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    date(cs.end_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    cs.status AS ESTADO,
    group_concat(cs.id separator ' | ') AS MUESTRA,
    group_concat(distinct(cs.supply_company) separator ' | ')  AS EMPRESA_SUMINISTRO,
    group_concat(distinct(cs.sample_packing) separator ' | ') AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    tsc.name AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "901012419" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    concat("SOLICITUD DE MUESTRA SUCAFINA COMPLETADA EN PUERTO DE BUENAVENTURA TERMINAL ",IF(cs.terminal LIKE "%SPB%", "SPB",cs.terminal) ) AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    sum(lp.precio*cs.qta_bag) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM coffee_sample AS cs
INNER JOIN sample_coffee_has_type_sample AS sct ON sct.sample_coffee_id = cs.id
INNER JOIN type_sample_coffe AS tsc ON tsc.id = sct.type_sample_coffee_id
INNER JOIN clients AS cts ON cts.id = cs.client_id
inner join items_unoee pds on pds.codigo='0000064'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000064','001')
WHERE cs.status = 'COMPLETADA' AND cs.get_sample = 1 AND tsc.id=12 
        AND (cs.terminal LIKE '%SPB%'  OR cs.terminal like'%TCBUEN%' OR cs.terminal like'%SPIA%')  
        AND (  DATE(cs.end_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
GROUP BY cts.exporter_code ,date(cs.end_date), tsc.name, IF(cs.terminal LIKE "%SPB%", "SPB",cs.terminal)
ORDER BY cs.client_id ASC
); 


/*------------------- SOLICITUD MUESTRA WOLTHERS SPB, SPIA Y TCBUEN  -------------------*/
INSERT INTO data_billing_full (
SELECT
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cs.lot_coffee_ref,' x ', cs.qta_bag separator ' | ') AS LOTE,
    sum(cs.qta_bag) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    date(cs.end_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    cs.status AS ESTADO,
    group_concat(cs.id separator ' | ') AS MUESTRA,
    group_concat(distinct(cs.supply_company) separator ' | ')  AS EMPRESA_SUMINISTRO,
    group_concat(distinct(cs.sample_packing) separator ' | ') AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    tsc.name AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "901202236" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    concat("SOLICITUD DE MUESTRA WOLTHERS COMPLETADA EN PUERTO DE BUENAVENTURA TERMINAL ",IF(cs.terminal LIKE "%SPB%", "SPB",cs.terminal) ) AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    sum(lp.precio*cs.qta_bag) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM coffee_sample AS cs
INNER JOIN sample_coffee_has_type_sample AS sct ON sct.sample_coffee_id = cs.id
INNER JOIN type_sample_coffe AS tsc ON tsc.id = sct.type_sample_coffee_id
INNER JOIN clients AS cts ON cts.id = cs.client_id
inner join items_unoee pds on pds.codigo='0000084'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000084','001')
WHERE cs.status = 'COMPLETADA' AND cs.get_sample = 1 AND tsc.id=3 
        AND (cs.terminal LIKE '%SPB%'  OR cs.terminal like'%TCBUEN%' OR cs.terminal like'%SPIA%')  
        AND (  DATE(cs.end_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
GROUP BY cts.exporter_code ,date(cs.end_date), tsc.name, IF(cs.terminal LIKE "%SPB%", "SPB",cs.terminal)
ORDER BY cs.client_id ASC
); 


/*------------------- SOLICITUD MUESTRA B HERMETICAS FNC SPB  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rm.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat(cts.exporter_code,'-',rm.lot_caffee,' x ', rm.quantity_radicated_bag_in) AS LOTE,
    (CEIL(rm.quantity_radicated_bag_in*0.32)) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    date(rm.created_date) AS FECHA_OPERACION,
    tu.name AS UNIDAD_CAFE,
    rm.bill_id AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "860007538" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    concat("SOLICITUD DE MUESTRA B. HERME FNC COMPLETADA EN PUERTO DE BUENAVENTURA TERMINAL SPB") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*(CEIL(rm.quantity_radicated_bag_in*0.32)) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM remittances_caffee AS rm
inner join clients cts on cts.id=rm.client_id
inner join type_units tu on tu.id=rm.type_units_id
inner join items_unoee pds on pds.codigo='0000066'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000066','007')
where rm.jetty=1 and tu.id=4 AND rm.client_id=1
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
); 



/*------------------- TRAZABILIDAD CONTENEDORES SPB  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-', rc.lot_caffee ,' x ',rc.quantity_radicated_bag_in separator ' | ') AS LOTES,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    ifn.packaging_mode AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    ivn.date_reg AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    ivn.id_tracking_siscafe  AS OIE,
    rdc.bic_container AS CONTENEDOR,
    pc.iso_ctn AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    ivn.ip_conection AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    concat("TRAZABILIDAD DE CONTENEDORES EMBALADOS PARA EL TERMINAL DE ",pc.jetty) AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM invoice_note as ivn
INNER JOIN clients AS cts ON cts.id = ivn.id_expo_pilcaf
INNER JOIN packaging_caffee AS pc ON pc.id = ivn.id_tracking_siscafe 
INNER JOIN ready_container AS rdc ON rdc.id = pc.ready_container_id
INNER JOIN detail_packaging_caffee AS dpc on dpc.packaging_caffee_id = ivn.id_tracking_siscafe
INNER JOIN info_navy AS ifn ON ifn.id = pc.info_navy_id
INNER JOIN remittances_caffee AS rc ON rc.id = dpc.remittances_caffee_id
inner join items_unoee pds on pds.codigo='0000054'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000054','001')
WHERE  ivn.type_invoice= 1 AND pc.jetty='BUN' AND cts.id!=6 AND  
    (  DATE(ivn.date_reg) BETWEEN @FECHA_INF AND @FECHA_SUP   )
group by ivn.id_tracking_siscafe,rc.client_id,date(ivn.date_reg), pc.jetty
); 


/*------------------- TRAZABILIDAD IER (INSPECTION AT THE TIME OF RECEIPT) SPB  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,  
    group_concat(cts.exporter_code,'-', rc.lot_caffee ,' x ',rc.quantity_radicated_bag_in separator ' | ') AS LOTES,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    ifn.packaging_mode AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    ivn.date_reg AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    ivn.id_tracking_siscafe  AS OIE,
    rdc.bic_container AS CONTENEDOR,
    pc.iso_ctn AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    ivn.ip_conection AS IP_CONEXION,
    (select cls.nit from clients cls where cls.id=ivn.id_expo_pilcaf) AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    concat("TRAZABILIDAD EIR (INSPECTION AT THE TIME OF RECEIPT) PARA EL TERMINAL DE SPB") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM invoice_note as ivn
INNER JOIN clients AS cts ON cts.id = ivn.id_expo_pilcaf
INNER JOIN packaging_caffee AS pc ON pc.id = ivn.id_tracking_siscafe 
INNER JOIN ready_container AS rdc ON rdc.id = pc.ready_container_id
INNER JOIN detail_packaging_caffee AS dpc on dpc.packaging_caffee_id = ivn.id_tracking_siscafe
INNER JOIN info_navy AS ifn ON ifn.id = pc.info_navy_id
INNER JOIN remittances_caffee AS rc ON rc.id = dpc.remittances_caffee_id
inner join items_unoee pds on pds.codigo='0000268'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000268','001')
WHERE  ivn.type_invoice= 2 AND pc.jetty='BUN' AND (cts.id!=1 AND cts.id!=6) AND
    (  DATE(ivn.date_reg) BETWEEN @FECHA_INF AND @FECHA_SUP   )
group by ivn.id_tracking_siscafe,rc.client_id,date(ivn.date_reg), pc.jetty
); 


/*------------------- TRAZABILIDAD VEHICULOS DESCARGADOS CARAVELA  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rc.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat( cts.exporter_code,'-',rc.lot_caffee,' x ',rc.quantity_radicated_bag_in separator ' | ' ) AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    rc.created_date AS FECHA_OPERACION,
    tu.name AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "TRAZABILIDAD DE VEHICULOS DESCARGADOS PARA EL TERMINAL DE SPB DEL EXPORTADOR CARAVELA" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM remittances_caffee AS rc 
INNER JOIN clients AS cts ON cts.id = rc.client_id 
INNER JOIN type_units AS tu ON tu.id = rc.type_units_id
inner join items_unoee pds on pds.codigo='0000214'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000214','001')
WHERE rc.jetty=1 AND rc.client_id =29 
    AND (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP   )
group by rc.vehicle_plate,date(rc.created_date),rc.jetty
ORDER BY rc.created_date ASC 
); 


/*------------------- TRAZABILIDAD VEHICULOS DESCARGADOS CARCAFE  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rc.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat( cts.exporter_code,'-',rc.lot_caffee,' x ',rc.quantity_radicated_bag_in separator ' | ' ) AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    rc.created_date AS FECHA_OPERACION,
    tu.name AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "TRAZABILIDAD DE VEHICULOS DESCARGADOS PARA EL TERMINAL DE SPB DEL EXPORTADOR CARCAFE" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM remittances_caffee AS rc 
INNER JOIN clients AS cts ON cts.id = rc.client_id 
INNER JOIN type_units AS tu ON tu.id = rc.type_units_id
inner join items_unoee pds on pds.codigo='0000086'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000086','001')
WHERE rc.jetty=1 AND rc.client_id =7 
    AND (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP   )
group by rc.vehicle_plate,date(rc.created_date),rc.jetty
ORDER BY rc.created_date ASC 
); 


/*------------------- TRAZABILIDAD VEHICULOS DESCARGADOS INCONEXUS  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rc.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat( cts.exporter_code,'-',rc.lot_caffee,' x ',rc.quantity_radicated_bag_in separator ' | ' ) AS LOTE,
    sum(rc.quantity_radicated_bag_in) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    rc.created_date AS FECHA_OPERACION,
    tu.name AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "TRAZABILIDAD DE VEHICULOS DESCARGADOS PARA EL TERMINAL DE SPB DEL EXPORTADOR INCONEXUS" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*sum(rc.quantity_radicated_bag_in) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM remittances_caffee AS rc 
INNER JOIN clients AS cts ON cts.id = rc.client_id 
INNER JOIN type_units AS tu ON tu.id = rc.type_units_id
inner join items_unoee pds on pds.codigo='0000149'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000149','001')
WHERE rc.jetty=1 AND rc.client_id =61 
    AND (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP   )
group by rc.vehicle_plate,date(rc.created_date),rc.jetty
ORDER BY rc.created_date ASC 
); 



/*------------------- SOLICITUD TOMA DE MUESTRA GRAIN PRO  SPB  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rm.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat(cts.exporter_code,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in ) AS LOTE,
    rm.quantity_radicated_bag_in  AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    date(rm.created_date) AS FECHA_OPERACION,
    uc.name_unit AS UNIDAD_CAFE,
    tu.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    concat("SOLICITUD DE MUESTRA B. HERME PARTICULARES COMPLETADA EN PUERTO DE BUENAVENTURA TERMINAL SPB") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*rm.quantity_radicated_bag_in AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join type_units tu on tu.id=rm.type_units_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join items_unoee pds on pds.codigo="0000009"
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=("0000009",'001')
where rm.jetty=1 and tu.id=4 AND rm.client_id!=1
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
ORDER BY rm.client_id ASC
); 


/*------------------- DESCARGUE FNC CARTAGENA tipo SACOS   -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rm.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat(cts.exporter_code,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in) AS LOTE,
    rm.quantity_radicated_bag_in AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    DATE(rm.created_date) AS FECHA_OPERACION,
    uc.name_unit AS UNIDAD_CAFE,
    b_rm.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    concat("DESCARGUE DE FNC EN PUERTO DE CARTAGENA TERMINAL ",ss.name_place," TIPO SACOS") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*rm.quantity_radicated_bag_in AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join bill b_rm on rm.bill_id=b_rm.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000110'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000110','012')
where (ss.ref_departament=2 OR ss.ref_departament=4 OR ss.ref_departament=5 OR ss.ref_departament=6 OR ss.ref_departament=9 ) 
        AND rm.client_id =1 AND rm.bill_id!=2 
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
        and (client_id,vehicle_plate,date(rm.created_date),ss.ref_departament) 
        in (
            select client_id,vehicle_plate,date(rc.created_date),ss.ref_departament
            from remittances_caffee rc
            inner join slot_store ss on ss.id=rc.slot_store_id
            where (ss.ref_departament=2 OR ss.ref_departament=4 OR ss.ref_departament=5 OR ss.ref_departament=6 OR ss.ref_departament=9 ) 
                AND rc.client_id =1 AND rc.bill_id!=2 
                AND (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP )
            group by rc.vehicle_plate,rc.client_id,date(rc.created_date),ss.ref_departament
        )
    ORDER BY DATE(rm.created_date) ASC
); 



/*------------------- DESCARGUE FNC SPB TIPO CAJAS  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rm.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat(cts.exporter_code,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in) AS LOTE,
    rm.quantity_radicated_bag_in AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    DATE(rm.created_date) AS FECHA_OPERACION,
    uc.name_unit AS UNIDAD_CAFE,
    b_rm.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    concat("DESCARGUE DE FNC EN PUERTO DE CARTAGENA TERMINAL ",ss.name_place," TIPO CAJAS") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*rm.quantity_radicated_bag_in AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join bill b_rm on rm.bill_id=b_rm.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000285'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000285','012')
where (ss.ref_departament=2 OR ss.ref_departament=4 OR ss.ref_departament=5 OR ss.ref_departament=6 OR ss.ref_departament=9 ) 
        AND rm.client_id =1 AND rm.bill_id=2 and rm.vehicle_plate!= 'FIQUE' 
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
        and (client_id,vehicle_plate,date(rm.created_date),ss.ref_departament) 
        in ( 
            select client_id,vehicle_plate,date(rc.created_date),ss.ref_departament
            from remittances_caffee rc
            inner join slot_store ss on ss.id=rc.slot_store_id
            where (ss.ref_departament=2 OR ss.ref_departament=4 OR ss.ref_departament=5 OR ss.ref_departament=6 OR ss.ref_departament=9 ) 
                AND rm.client_id =1 AND rc.bill_id=2 and rm.vehicle_plate!= 'FIQUE' 
                AND (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP )
            group by rc.vehicle_plate,rc.client_id,date(rc.created_date),ss.ref_departament
        )
    ORDER BY DATE(rm.created_date) ASC
); 



/*------------------- DESCARGUE CTG MICROLOTES PARTICULARES  -------------------*/
/*------------------- DESCARGUE CTG MICROLOTES MAYOR IGUAL A 5 LOTES  -------------------*/
INSERT INTO data_billing_full (
select 
    NULL AS ID,
    group_concat(rc.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rc.lot_caffee,' x ',rc.quantity_radicated_bag_in separator ' | ') AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    DATE(rc.created_date) AS FECHA_OPERACION,
    group_concat(distinct(uc.name_unit) separator ' | ') AS UNIDAD_CAFE,
    group_concat(distinct(b_rc.name) separator ' | ') AS UNIDAD_FACTURA,
    count(rc.id) AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    concat("DESCARGUE DE MICROLOTES PARTICULARES EN PUERTO DE CARTAGENA TERMINAL ",ss.name_place," MAYOR IGUAL A 5 LOTES") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee AS rc
inner join clients AS cts on cts.id=rc.client_id
inner join units_caffee AS uc on rc.units_cafee_id=uc.id
inner join bill AS b_rc on rc.bill_id=b_rc.id
inner join slot_store AS ss on ss.id=rc.slot_store_id
inner join departaments AS dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000114'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000114','002')
where  (ss.ref_departament=2 OR ss.ref_departament=4 OR ss.ref_departament=5 OR ss.ref_departament=6 OR ss.ref_departament=9 ) 
    AND rc.microlot=1 AND rc.client_id !=1 
    AND (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP )    
group by rc.vehicle_plate, date(rc.created_date) , rc.client_id, ss.ref_departament
having count(rc.id) >= 5
ORDER BY cts.exporter_code ASC
); 



/*------------------- DESCARGUE CTG MENOR A 100 PARTICULARES  -------------------*/
/*------------------- DESCARGUE CTG MENOR A 100 SACOS PARTICULARES  -------------------*/
INSERT INTO data_billing_full (
select 
    NULL AS ID,
    group_concat(rc.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rc.lot_caffee,' x ',rc.quantity_radicated_bag_in separator ' | ') AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    date(rc.created_date) AS FECHA_OPERACION,
    group_concat(distinct(uc.name_unit) separator ' | ')  AS UNIDAD_CAFE,
    b_id.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    concat("DESCARGUE DE PARTICULARES EN PUERTO DE CARTAGENA TERMINAL ",ss.name_place," MENOR A 100 SACOS CAFE") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee AS rc
inner join clients AS cts on cts.id = rc.client_id
INNER JOIN bill AS b_id ON b_id.id = rc.bill_id
inner join slot_store AS ss on ss.id=rc.slot_store_id
inner join units_caffee AS uc on rc.units_cafee_id=uc.id
inner join departaments AS dep on dep.id=ss.ref_departament
inner join items_unoee pds on pds.codigo='0000270'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000270','002')
where (ss.ref_departament=2 OR ss.ref_departament=4 OR ss.ref_departament=5 OR ss.ref_departament=6 OR ss.ref_departament=9 ) 
    AND rc.client_id != 1 and rc.microlot=0 AND rc.bill_id=1
    AND (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP )
group by rc.vehicle_plate,rc.client_id,date(rc.created_date), ss.ref_departament
having sum(rc.quantity_radicated_bag_in) <=100
    ORDER BY cts.exporter_code ASC
); 



/*------------------- DESCARGUE CARTAGENA MENOR A 250 y MAYOR A 101 PARTICULARES  -------------------*/
/*------------------- DESCARGUE CARTAGENA MENOR A 250 y MAYOR A 101 SACOS PARTICULARES  -------------------*/
INSERT INTO data_billing_full (
select 
    NULL AS ID,
    group_concat(rc.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rc.lot_caffee,' x ',rc.quantity_radicated_bag_in separator ' | ') AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    date(rc.created_date) AS FECHA_OPERACION,
    group_concat(distinct(uc.name_unit) separator ' | ')  AS UNIDAD_CAFE,
    b_id.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    concat("DESCARGUE DE PARTICULARES EN PUERTO DE CARTAGENA TERMINAL ",ss.name_place," DE 101 A 250 SACOS CAFE") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee AS rc
inner join clients cts on cts.id = rc.client_id
INNER JOIN bill AS b_id ON b_id.id = rc.bill_id
inner join slot_store ss on ss.id=rc.slot_store_id
inner join units_caffee AS uc on rc.units_cafee_id=uc.id
inner join departaments dep on dep.id=ss.ref_departament
inner join items_unoee pds on pds.codigo='0000287'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000287','002')
where (ss.ref_departament=2 OR ss.ref_departament=4 OR ss.ref_departament=5 OR ss.ref_departament=6 OR ss.ref_departament=9 ) 
    AND rc.client_id != 1 AND rc.microlot=0 AND rc.bill_id=1
    AND (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP )
group by rc.vehicle_plate,rc.client_id,date(rc.created_date), ss.ref_departament
having sum(rc.quantity_radicated_bag_in) BETWEEN 101 AND 250
    ORDER BY cts.exporter_code ASC
); 



/*------------------- DESCARGUE CTG MAYOR DE 250 PARTICULARES  -------------------*/
/*------------------- DESCARGUE CTG MAYOR DE 250 SACOS DE 1-25 KG SACOS CAFE   -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rm.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( cts.exporter_code ,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in ) AS LOTE,
    rm.quantity_radicated_bag_in AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    DATE(rm.created_date) AS FECHA_OPERACION,
    uc.name_unit AS UNIDAD_CAFE,
    b_rm.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    concat("DESCARGUE DE PARTICULARES EN PUERTO DE CARTAGENA TERMINAL ",ss.name_place," MAYOR DE 250 SACOS DE 1-25 KG SACOS CAFE") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*rm.quantity_radicated_bag_in AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join bill b_rm on rm.bill_id=b_rm.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000222'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000222','002')
where (ss.ref_departament=2 OR ss.ref_departament=4 OR ss.ref_departament=5 OR ss.ref_departament=6 OR ss.ref_departament=9 ) 
        AND rm.microlot=0 AND rm.client_id !=1 AND rm.bill_id=1 AND uc.quantity <= 25
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
        and (client_id,vehicle_plate,date(rm.created_date),ss.ref_departament) 
        in (
            select client_id,vehicle_plate,date(rc.created_date),ss.ref_departament
            from remittances_caffee rc
            inner join slot_store ss on ss.id=rc.slot_store_id
            where (ss.ref_departament=2 OR ss.ref_departament=4 OR ss.ref_departament=5 OR ss.ref_departament=6 OR ss.ref_departament=9 ) 
                AND rc.microlot=0 and rc.client_id !=1 AND rc.bill_id=1 
                AND (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP )
            group by rc.vehicle_plate,rc.client_id,date(rc.created_date),ss.ref_departament
            having sum(rc.quantity_radicated_bag_in) >250
        )
    ORDER BY cts.exporter_code ASC
); 



/*------------------- DESCARGUE CTG MAYOR DE 250 SACOS DE 30 KG SACOS CAFE   -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rm.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( cts.exporter_code ,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in ) AS LOTE,
    rm.quantity_radicated_bag_in AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    DATE(rm.created_date) AS FECHA_OPERACION,
    uc.name_unit AS UNIDAD_CAFE,
    b_rm.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    concat("DESCARGUE DE PARTICULARES EN PUERTO DE CARTAGENA TERMINAL ",ss.name_place," MAYOR DE 250 SACOS DE 30 KG SACOS CAFE") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*rm.quantity_radicated_bag_in AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join bill b_rm on rm.bill_id=b_rm.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000223'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000223','002')
where (ss.ref_departament=2 OR ss.ref_departament=4 OR ss.ref_departament=5 OR ss.ref_departament=6 OR ss.ref_departament=9 ) 
        AND rm.microlot=0 AND rm.client_id !=1 AND rm.bill_id=1 AND uc.quantity = 30
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
        and (client_id,vehicle_plate,date(rm.created_date),ss.ref_departament) 
        in ( 
            select client_id,vehicle_plate,date(rc.created_date),ss.ref_departament
            from remittances_caffee rc
            inner join slot_store ss on ss.id=rc.slot_store_id
            where (ss.ref_departament=2 OR ss.ref_departament=4 OR ss.ref_departament=5 OR ss.ref_departament=6 OR ss.ref_departament=9 ) 
                    AND rc.microlot=0 and rc.client_id !=1 AND rc.bill_id=1 
                    AND (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP )
            group by rc.vehicle_plate,rc.client_id,date(rc.created_date),ss.ref_departament
            having sum(rc.quantity_radicated_bag_in) >250
        )
    ORDER BY cts.exporter_code ASC
); 



/*------------------- DESCARGUE CTG MAYOR DE 250 SACOS DE 35 KG SACOS CAFE   -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rm.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( cts.exporter_code ,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in ) AS LOTE,
    rm.quantity_radicated_bag_in AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    DATE(rm.created_date) AS FECHA_OPERACION,
    uc.name_unit AS UNIDAD_CAFE,
    b_rm.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    concat("DESCARGUE DE PARTICULARES EN PUERTO DE CARTAGENA TERMINAL ",ss.name_place," MAYOR DE 250 SACOS DE 35 KG SACOS CAFE") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*rm.quantity_radicated_bag_in AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join bill b_rm on rm.bill_id=b_rm.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000224'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000224','002')
where (ss.ref_departament=2 OR ss.ref_departament=4 OR ss.ref_departament=5 OR ss.ref_departament=6 OR ss.ref_departament=9 ) 
        AND rm.microlot=0 AND rm.client_id !=1 AND rm.bill_id=1 AND uc.quantity = 35
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
        and (client_id,vehicle_plate,date(rm.created_date),ss.ref_departament) 
        in (
            select client_id,vehicle_plate,date(rc.created_date),ss.ref_departament
            from remittances_caffee rc
            inner join slot_store ss on ss.id=rc.slot_store_id
            where (ss.ref_departament=2 OR ss.ref_departament=4 OR ss.ref_departament=5 OR ss.ref_departament=6 OR ss.ref_departament=9 ) 
                    AND rc.microlot=0 AND rc.client_id !=1 AND rc.bill_id=1 
                    AND (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP )
            group by rc.vehicle_plate,rc.client_id,date(rc.created_date),ss.ref_departament
            having sum(rc.quantity_radicated_bag_in) >250
        )
    ORDER BY cts.exporter_code ASC
); 



/*------------------- DESCARGUE CTG MAYOR DE 250 SACOS DE 40 - 70 KG SACOS CAFE   -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rm.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( cts.exporter_code ,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in ) AS LOTE,
    rm.quantity_radicated_bag_in AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    DATE(rm.created_date) AS FECHA_OPERACION,
    uc.name_unit AS UNIDAD_CAFE,
    b_rm.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    concat("DESCARGUE DE PARTICULARES EN PUERTO DE CARTAGENA TERMINAL ",ss.name_place," MAYOR DE 250 SACOS DE 40-70 KG SACOS CAFE") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*rm.quantity_radicated_bag_in AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join bill b_rm on rm.bill_id=b_rm.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000111'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000111','002')
where (ss.ref_departament=2 OR ss.ref_departament=4 OR ss.ref_departament=5 OR ss.ref_departament=6 OR ss.ref_departament=9 ) 
        AND rm.microlot=0 AND rm.client_id !=1 AND rm.bill_id=1 AND ( uc.quantity between 40 and 70 )
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
        and (client_id,vehicle_plate,date(rm.created_date),ss.ref_departament) 
        in (
            select client_id,vehicle_plate,date(rc.created_date),ss.ref_departament
            from remittances_caffee rc
            inner join slot_store ss on ss.id=rc.slot_store_id
            where (ss.ref_departament=2 OR ss.ref_departament=4 OR ss.ref_departament=5 OR ss.ref_departament=6 OR ss.ref_departament=9 ) 
                    AND rc.microlot=0 AND rc.client_id !=1 AND rc.bill_id=1 
                    AND (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP )
            group by rc.vehicle_plate,rc.client_id,date(rc.created_date),ss.ref_departament
            having sum(rc.quantity_radicated_bag_in) >250
        )
    ORDER BY cts.exporter_code ASC
); 



/*------------------- DESCARGUE CTG SACOS CACAO   -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rm.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( cts.exporter_code ,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in ) AS LOTE,
    rm.quantity_radicated_bag_in AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    DATE(rm.created_date) AS FECHA_OPERACION,
    uc.name_unit AS UNIDAD_CAFE,
    b_rm.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    concat("DESCARGUE DE PARTICULARES EN PUERTO DE CARTAGENA TERMINAL ",ss.name_place," TIPO SACOS CACAO") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*rm.quantity_radicated_bag_in AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join bill b_rm on rm.bill_id=b_rm.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000049'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000049','002')
where (ss.ref_departament=2 OR ss.ref_departament=4 OR ss.ref_departament=5 OR ss.ref_departament=6 OR ss.ref_departament=9 ) 
        AND rm.microlot=0 and rm.client_id !=1 AND rm.bill_id=3 
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )        
    ORDER BY cts.exporter_code ASC
); 


/*------------------- DESCARGUE CTG SACOS BIG BAGS   -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rm.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( cts.exporter_code ,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in ) AS LOTE,
    rm.quantity_radicated_bag_in AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    DATE(rm.created_date) AS FECHA_OPERACION,
    uc.name_unit AS UNIDAD_CAFE,
    b_rm.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    concat("DESCARGUE DE PARTICULARES EN PUERTO DE CARTAGENA TERMINAL ",ss.name_place," TIPO BIG BAGS") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*rm.quantity_radicated_bag_in  AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join bill b_rm on rm.bill_id=b_rm.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000184'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000184','002')
where (ss.ref_departament=2 OR ss.ref_departament=4 OR ss.ref_departament=5 OR ss.ref_departament=6 OR ss.ref_departament=9 ) 
        AND rm.microlot=0 and rm.client_id !=1 AND rm.bill_id=4 AND uc.quantity=1000
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
    ORDER BY cts.exporter_code ASC
);  



/*------------------- DESCARGUE CTG SACOS MINI BIG BAGS   -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rm.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( cts.exporter_code ,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in ) AS LOTE,
    rm.quantity_radicated_bag_in AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    DATE(rm.created_date) AS FECHA_OPERACION,
    uc.name_unit AS UNIDAD_CAFE,
    b_rm.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    concat("DESCARGUE DE PARTICULARES EN PUERTO DE CARTAGENA TERMINAL ",ss.name_place," TIPO MINI BIG BAGS") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*rm.quantity_radicated_bag_in  AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join bill b_rm on rm.bill_id=b_rm.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000347'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000347','002')
where (ss.ref_departament=2 OR ss.ref_departament=4 OR ss.ref_departament=5 OR ss.ref_departament=6 OR ss.ref_departament=9 ) 
        AND rm.microlot=0 and rm.client_id !=1 AND rm.bill_id=4 AND uc.quantity!=1000
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
    ORDER BY cts.exporter_code ASC
);  



/*------------------- DESCARGUE CTG CAJAS DE 1 A 10   -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code ,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    DATE(rm.created_date) AS FECHA_OPERACION,
    group_concat(distinct(uc.name_unit) separator ' | ') AS UNIDAD_CAFE,
    group_concat(distinct(b_rm.name) separator ' | ') AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    concat("DESCARGUE DE PARTICULARES EN PUERTO DE CARTAGENA TERMINAL ",ss.name_place," TIPO CAJAS DE 1 A 10 UNIDADES") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join bill b_rm on rm.bill_id=b_rm.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000032'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000032','002')
where (ss.ref_departament=2 OR ss.ref_departament=4 OR ss.ref_departament=5 OR ss.ref_departament=6 OR ss.ref_departament=9 ) 
        AND rm.microlot=0 AND rm.client_id !=1 AND rm.bill_id=2 AND rm.vehicle_plate!= 'FIQUE' 
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by rm.vehicle_plate,rm.client_id,date(rm.created_date),ss.ref_departament
having sum(rm.quantity_radicated_bag_in) <= 10 
ORDER BY cts.exporter_code ASC
); 



/*------------------- DESCARGUE CTG CAJAS DE 11 A 100   -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code ,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    DATE(rm.created_date) AS FECHA_OPERACION,
    group_concat(distinct(uc.name_unit) separator ' | ') AS UNIDAD_CAFE,
    group_concat(distinct(b_rm.name) separator ' | ') AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    concat("DESCARGUE DE PARTICULARES EN PUERTO DE CARTAGENA TERMINAL ",ss.name_place," TIPO CAJAS DE 11 A 100 UNIDADES") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join bill b_rm on rm.bill_id=b_rm.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000203'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000203','002')
where (ss.ref_departament=2 OR ss.ref_departament=4 OR ss.ref_departament=5 OR ss.ref_departament=6 OR ss.ref_departament=9 ) 
        AND rm.microlot=0 and rm.client_id !=1 AND rm.bill_id=2 
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by rm.vehicle_plate,rm.client_id,date(rm.created_date),ss.ref_departament
having sum(rm.quantity_radicated_bag_in) between 11 and 100
ORDER BY cts.exporter_code ASC
); 


/*------------------- DESCARGUE CTG CAJAS MAYOR DE 100   -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code ,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    sum(rm.quantity_radicated_bag_in) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    DATE(rm.created_date) AS FECHA_OPERACION,
    group_concat(distinct(uc.name_unit) separator ' | ') AS UNIDAD_CAFE,
    group_concat(distinct(b_rm.name) separator ' | ') AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    concat("DESCARGUE DE PARTICULARES EN PUERTO DE CARTAGENA TERMINAL ",ss.name_place," TIPO CAJAS MAYOR A 100 UNIDADES") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*sum(rm.quantity_radicated_bag_in) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join bill b_rm on rm.bill_id=b_rm.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000215'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000215','002')
where (ss.ref_departament=2 OR ss.ref_departament=4 OR ss.ref_departament=5 OR ss.ref_departament=6 OR ss.ref_departament=9 ) 
        AND rm.microlot=0 AND rm.client_id !=1 AND rm.bill_id=2 
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by rm.vehicle_plate,rm.client_id,date(rm.created_date),ss.ref_departament 
having sum(rm.quantity_radicated_bag_in) > 100    
ORDER BY cts.exporter_code ASC
); 


/*------------------- DESCARGUE CTG  CAFE VERDE ESTIBAS   -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rm.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat(cts.exporter_code ,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in)  AS LOTE,
    rm.quantity_radicated_bag_in AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    DATE(rm.created_date) AS FECHA_OPERACION,
    uc.name_unit AS UNIDAD_CAFE,
    b_rm.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    concat("DESCARGUE DE PARTICULARES EN PUERTO DE CARTAGENA TERMINAL ",ss.name_place," TIPO CAFE VERDE ESTIBAS") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join bill b_rm on rm.bill_id=b_rm.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo="0000295"
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=("0000295",'002')
where (ss.ref_departament=2 OR ss.ref_departament=4 OR ss.ref_departament=5 OR ss.ref_departament=6 OR ss.ref_departament=9 ) 
        AND rm.microlot=0 AND rm.client_id !=1 AND rm.bill_id=18 
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
ORDER BY cts.exporter_code ASC
); 



/*------------------- DEVOLUCIONES FNC -------------------*/
/*------------------- DEVOLUCION FNC TIPO CAFE TERMINAL CTG  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    group_concat(rcrc.remittances_caffee_id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee,' x ' ,rcrc.qta_bags separator ' | ') as LOTE,
    sum(rcrc.qta_bags) AS CANTIDAD_OPERATIVA,
    rc.return_type AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    rc.return_date  AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    rc.state_return AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    concat("DEVOLUCIONES FNC EN PUERTO DE CARTAGENA TERMINAL ",ss.name_place," TIPO CAFE") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*sum(rcrc.qta_bags) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from returns_caffees rc
inner join remittances_caffee_returns_caffees rcrc on rcrc.returns_caffee_id=rc.id
inner join clients as cts on cts.id=rc.client_id
inner join remittances_caffee as rm on rcrc.remittances_caffee_id = rm.id
inner join departaments dep on rc.jetty = dep.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join items_unoee pds on pds.codigo='0000288'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000288','012')
where (rc.jetty=2 OR rc.jetty=4 OR rc.jetty=5 OR rc.jetty=6 OR rc.jetty=9 ) 
        AND rc.state_return="COMPLETADA" AND cts.id=1 AND rc.return_type = 'CAFE' 
        AND (  DATE(rc.return_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by rc.vehicle_plate,cts.exporter_code,date(rc.return_date), rc.jetty
); 


/*------------------- DEVOLUCION FNC CTG FIQUE MAYOR A 10000  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    group_concat(rcrc.remittances_caffee_id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee,' x ' ,rcrc.qta_bags separator ' | ') as LOTE,
    1 AS CANTIDAD_OPERATIVA,
    rc.return_type AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    rc.return_date  AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    rc.state_return AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    concat("DEVOLUCIONES FNC EN PUERTO DE CARTAGENA TERMINAL ",ss.name_place," TIPO FIQUE MAYOR A 10000") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from returns_caffees rc
inner join remittances_caffee_returns_caffees rcrc on rcrc.returns_caffee_id=rc.id
inner join clients as cts on cts.id=rc.client_id
inner join remittances_caffee as rm on rcrc.remittances_caffee_id = rm.id
inner join departaments dep on rc.jetty = dep.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join items_unoee pds on pds.codigo='0000127'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000127','012')
where (rc.jetty=2 OR rc.jetty=4 OR rc.jetty=5 OR rc.jetty=6 OR rc.jetty=9 ) 
        AND rc.state_return="COMPLETADA" AND cts.id=1 AND rc.return_type = 'FIQUE' 
        and (  DATE(rc.return_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
group by rc.vehicle_plate,cts.exporter_code,date(rc.return_date), rc.jetty
having sum(rcrc.qta_bags) > 10000
); 



/*------------------- DEVOLUCION FNC CTG FIQUE MENOR A 10000  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    group_concat(rcrc.remittances_caffee_id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee,' x ' ,rcrc.qta_bags separator ' | ') as LOTE,
    1 AS CANTIDAD_OPERATIVA,
    rc.return_type AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    rc.return_date  AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    rc.state_return AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    concat("DEVOLUCIONES FNC EN PUERTO DE CARTAGENA TERMINAL ",ss.name_place," TIPO FIQUE MENOR A 10000") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from returns_caffees rc
inner join remittances_caffee_returns_caffees rcrc on rcrc.returns_caffee_id=rc.id
inner join clients as cts on cts.id=rc.client_id
inner join remittances_caffee as rm on rcrc.remittances_caffee_id = rm.id
inner join departaments dep on rc.jetty = dep.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join items_unoee pds on pds.codigo='0000219'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000219','012')
where (rc.jetty=2 OR rc.jetty=4 OR rc.jetty=5 OR rc.jetty=6 OR rc.jetty=9 ) 
        AND rc.state_return="COMPLETADA" AND cts.id=1 AND rc.return_type = 'FIQUE' 
    and (  DATE(rc.return_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
group by rc.vehicle_plate,cts.exporter_code,date(rc.return_date), rc.jetty
having sum(rcrc.qta_bags) <=10000
); 



/*------------------- DEVOLUCION PARTICULARES CAFE CTG MAYOR 250  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    group_concat(rcrc.remittances_caffee_id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee,' x ' ,rcrc.qta_bags separator ' | ') as LOTE,
    sum(rcrc.qta_bags) AS CANTIDAD_OPERATIVA,
    rc.return_type AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    rc.return_date  AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    rc.state_return AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    concat("DEVOLUCIONES PARTICULARES EN PUERTO DE CARTAGENA TERMINAL ",ss.name_place," TIPO CAFE MAYOR A 250") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*sum(rcrc.qta_bags) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from returns_caffees rc
inner join remittances_caffee_returns_caffees rcrc on rcrc.returns_caffee_id=rc.id
inner join clients AS cts on cts.id=rc.client_id
inner join remittances_caffee as rm on rcrc.remittances_caffee_id = rm.id
inner join departaments dep on rc.jetty = dep.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join items_unoee pds on pds.codigo='0000277'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000277','002')
where (rc.jetty=2 OR rc.jetty=4 OR rc.jetty=5 OR rc.jetty=6 OR rc.jetty=9 ) 
        AND rc.state_return="COMPLETADA" AND cts.id!=1 AND rc.return_type = 'CAFE' 
        and (  DATE(rc.return_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
group by rc.vehicle_plate,cts.exporter_code,date(rc.return_date), rc.jetty
having sum(rcrc.qta_bags) >250
ORDER BY exporter_code ASC
); 


/*------------------- DEVOLUCION PARTICULARES CAFE CTG MENOR 250  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    group_concat(rcrc.remittances_caffee_id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee,' x ' ,rcrc.qta_bags separator ' | ') as LOTE,
    1 AS CANTIDAD_OPERATIVA,
    rc.return_type AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    rc.return_date  AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    rc.state_return AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    concat("DEVOLUCIONES PARTICULARES EN PUERTO DE CARTAGENA TERMINAL ",ss.name_place," TIPO CAFE MENOR A 250") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from returns_caffees rc
inner join remittances_caffee_returns_caffees rcrc on rcrc.returns_caffee_id=rc.id
inner join clients AS cts on cts.id=rc.client_id
inner join remittances_caffee as rm on rcrc.remittances_caffee_id = rm.id
inner join departaments dep on rc.jetty = dep.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join items_unoee pds on pds.codigo='0000001'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000001','002')
where (rc.jetty=2 OR rc.jetty=4 OR rc.jetty=5 OR rc.jetty=6 OR rc.jetty=9 ) 
        AND rc.state_return="COMPLETADA" AND cts.id!=1 AND rc.return_type = 'CAFE' 
        and (  DATE(rc.return_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
group by rc.vehicle_plate,cts.exporter_code,date(rc.return_date), rc.jetty
having sum(rcrc.qta_bags) <=250
ORDER BY exporter_code ASC
); 


/*------------------- DEVOLUCION PARTICULARES CTG FIQUE MAYOR A 10000  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    group_concat(rcrc.remittances_caffee_id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee,' x ' ,rcrc.qta_bags separator ' | ') as LOTE,
    1 AS CANTIDAD_OPERATIVA,
    rc.return_type AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    rc.return_date  AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    rc.state_return AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    concat("DEVOLUCIONES PARTICULARES EN PUERTO DE CARTAGENA TERMINAL ",ss.name_place," TIPO FIQUE MAYOR A 10000") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from returns_caffees rc
inner join remittances_caffee_returns_caffees rcrc on rcrc.returns_caffee_id=rc.id
inner join clients AS cts on cts.id=rc.client_id
inner join remittances_caffee as rm on rcrc.remittances_caffee_id = rm.id
inner join departaments dep on rc.jetty = dep.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join items_unoee pds on pds.codigo='0000127'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000127','002')
where (rc.jetty=2 OR rc.jetty=4 OR rc.jetty=5 OR rc.jetty=6 OR rc.jetty=9 ) 
        AND rc.state_return="COMPLETADA" AND cts.id!=1 AND rc.return_type = 'FIQUE' 
        and (  DATE(rc.return_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
group by rc.vehicle_plate,cts.exporter_code,date(rc.return_date), rc.jetty
having sum(rcrc.qta_bags) >10000
ORDER BY exporter_code ASC
); 



/*------------------- DEVOLUCION PARTICULARES CTG FIQUE MENOR A 5000 -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    group_concat(rcrc.remittances_caffee_id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee,' x ' ,rcrc.qta_bags separator ' | ') as LOTE,
    1 AS CANTIDAD_OPERATIVA,
    rc.return_type AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    rc.return_date  AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    rc.state_return AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    concat("DEVOLUCIONES PARTICULARES EN PUERTO DE CARTAGENA TERMINAL ",ss.name_place," TIPO FIQUE MENOR A 5000") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from returns_caffees rc
inner join remittances_caffee_returns_caffees rcrc on rcrc.returns_caffee_id=rc.id
inner join clients AS cts on cts.id=rc.client_id
inner join remittances_caffee as rm on rcrc.remittances_caffee_id = rm.id
inner join departaments dep on rc.jetty = dep.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join items_unoee pds on pds.codigo='0000204'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000204','002')
where (rc.jetty=2 OR rc.jetty=4 OR rc.jetty=5 OR rc.jetty=6 OR rc.jetty=9 ) 
        AND rc.state_return="COMPLETADA" AND cts.id!=1 AND rc.return_type = 'FIQUE' 
        and (  DATE(rc.return_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
group by rc.vehicle_plate,cts.exporter_code,date(rc.return_date), rc.jetty
having sum(rcrc.qta_bags) <=5000
ORDER BY exporter_code ASC
); 


/*------------------- DEVOLUCION PARTICULARES CTG FIQUE MAYOR A 5001 Y MENOR 10000  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    group_concat(rcrc.remittances_caffee_id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee,' x ' ,rcrc.qta_bags separator ' | ') as LOTE,
    1 AS CANTIDAD_OPERATIVA,
    rc.return_type AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    rc.return_date  AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    rc.state_return AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    concat("DEVOLUCIONES PARTICULARES EN PUERTO DE CARTAGENA TERMINAL ",ss.name_place," TIPO FIQUE MAYOR A 5001 Y MENOR 10000") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from returns_caffees rc
inner join remittances_caffee_returns_caffees rcrc on rcrc.returns_caffee_id=rc.id
inner join clients AS cts on cts.id=rc.client_id
inner join remittances_caffee as rm on rcrc.remittances_caffee_id = rm.id
inner join departaments dep on rc.jetty = dep.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join items_unoee pds on pds.codigo='0000010'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000010','002')
where (rc.jetty=2 OR rc.jetty=4 OR rc.jetty=5 OR rc.jetty=6 OR rc.jetty=9 ) 
        AND rc.state_return="COMPLETADA" AND cts.id!=1 AND rc.return_type = 'FIQUE' 
        and (  DATE(rc.return_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
group by rc.vehicle_plate,cts.exporter_code,date(rc.return_date), rc.jetty
having (sum(rcrc.qta_bags) between 5001 and 10000)
ORDER BY exporter_code ASC
); 



/*------------------- SOLICITUD CONTRAMUESTRA DREYFUS CTG  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cs.lot_coffee_ref,' x ', cs.qta_bag separator ' | ') AS LOTE,
    sum(cs.qta_bag) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    date(cs.end_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    cs.status AS ESTADO,
    group_concat(cs.id separator ' | ') AS MUESTRA,
    group_concat(distinct(cs.supply_company) separator ' | ')  AS EMPRESA_SUMINISTRO,
    group_concat(distinct(cs.sample_packing) separator ' | ') AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    tsc.name AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "900174478" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    concat("SOLICITUD DE MUESTRA Y CONTRAMUESTRA DREYFUS COMPLETADA EN PUERTO DE CARTAGENA TERMINAL ",cs.terminal) AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    sum(lp.precio*cs.qta_bag) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM coffee_sample AS cs
INNER JOIN sample_coffee_has_type_sample AS sct ON sct.sample_coffee_id = cs.id
INNER JOIN type_sample_coffe AS tsc ON tsc.id = sct.type_sample_coffee_id
INNER JOIN clients AS cts ON cts.id = cs.client_id
inner join items_unoee pds on pds.codigo='0000213'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000213','002')
WHERE cs.status = 'COMPLETADA' AND cs.get_sample = 1 AND (tsc.id=13 OR tsc.id=5) 
        AND (cs.terminal LIKE '%CTG%'  OR cs.terminal like'%SPRC%' OR cs.terminal like'%CONTECA%' OR cs.terminal like'%BLOC%' OR cs.terminal like'%COMPAS%')  
        AND (  DATE(cs.end_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
GROUP BY cts.exporter_code ,date(cs.end_date), tsc.name, cs.terminal
ORDER BY cs.client_id ASC
); 



/*------------------- SOLICITUD CONTRAMUESTRA INCONEXUS CTG  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cs.lot_coffee_ref,' x ', cs.qta_bag separator ' | ') AS LOTE,
    sum(cs.qta_bag) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    date(cs.end_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    cs.status AS ESTADO,
    group_concat(cs.id separator ' | ') AS MUESTRA,
    group_concat(distinct(cs.supply_company) separator ' | ')  AS EMPRESA_SUMINISTRO,
    group_concat(distinct(cs.sample_packing) separator ' | ') AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    tsc.name AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "830092113" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    concat("SOLICITUD DE MUESTRA Y CONTRAMUESTRA INCONEXUS COMPLETADA EN PUERTO DE CARTAGENA TERMINAL ",cs.terminal) AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    sum(lp.precio*cs.qta_bag) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM coffee_sample AS cs
INNER JOIN sample_coffee_has_type_sample AS sct ON sct.sample_coffee_id = cs.id
INNER JOIN type_sample_coffe AS tsc ON tsc.id = sct.type_sample_coffee_id
INNER JOIN clients AS cts ON cts.id = cs.client_id
inner join items_unoee pds on pds.codigo='0000064'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000064','002')
WHERE cs.status = 'COMPLETADA' AND cs.get_sample = 1 AND (tsc.id=15 or tsc.id=7)
        AND (cs.terminal LIKE '%CTG%'  OR cs.terminal like'%SPRC%' OR cs.terminal like'%CONTECA%' OR cs.terminal like'%BLOC%' OR cs.terminal like'%COMPAS%')  
        AND (  DATE(cs.end_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
GROUP BY cts.exporter_code ,date(cs.end_date), tsc.name, cs.terminal
ORDER BY cs.client_id ASC
); 


/*------------------- SOLICITUD CONTRAMUESTRA SUCAFINA CTG  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cs.lot_coffee_ref,' x ', cs.qta_bag separator ' | ') AS LOTE,
    sum(cs.qta_bag) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    date(cs.end_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    cs.status AS ESTADO,
    group_concat(cs.id separator ' | ') AS MUESTRA,
    group_concat(distinct(cs.supply_company) separator ' | ')  AS EMPRESA_SUMINISTRO,
    group_concat(distinct(cs.sample_packing) separator ' | ') AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    tsc.name AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "901012419" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    concat("SOLICITUD DE CONTRAMUESTRA SUCAFINA COMPLETADA EN PUERTO DE CARTAGENA TERMINAL ",cs.terminal) AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    sum(lp.precio*cs.qta_bag) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM coffee_sample AS cs
INNER JOIN sample_coffee_has_type_sample AS sct ON sct.sample_coffee_id = cs.id
INNER JOIN type_sample_coffe AS tsc ON tsc.id = sct.type_sample_coffee_id
INNER JOIN clients AS cts ON cts.id = cs.client_id
inner join items_unoee pds on pds.codigo='0000064'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000064','002')
WHERE cs.status = 'COMPLETADA' AND cs.get_sample = 1 AND tsc.id=16
        AND (cs.terminal LIKE '%CTG%'  OR cs.terminal like'%SPRC%' OR cs.terminal like'%CONTECA%' OR cs.terminal like'%BLOC%' OR cs.terminal like'%COMPAS%')  
        AND (  DATE(cs.end_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
GROUP BY cts.exporter_code ,date(cs.end_date), tsc.name, cs.terminal
ORDER BY cs.client_id ASC
); 



/*------------------- SOLICITUD CONTRAMUESTRA SUCDEN CTG  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cs.lot_coffee_ref,' x ', cs.qta_bag separator ' | ') AS LOTE,
    sum(cs.qta_bag) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    date(cs.end_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    cs.status AS ESTADO,
    group_concat(cs.id separator ' | ') AS MUESTRA,
    group_concat(distinct(cs.supply_company) separator ' | ')  AS EMPRESA_SUMINISTRO,
    group_concat(distinct(cs.sample_packing) separator ' | ') AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    tsc.name AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "900099234" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    concat("SOLICITUD DE MUESTRA Y CONTRAMUESTRA SUCDEN COMPLETADA EN PUERTO DE CARTAGENA TERMINAL ",cs.terminal) AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    sum(lp.precio*cs.qta_bag) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM coffee_sample AS cs
INNER JOIN sample_coffee_has_type_sample AS sct ON sct.sample_coffee_id = cs.id
INNER JOIN type_sample_coffe AS tsc ON tsc.id = sct.type_sample_coffee_id
INNER JOIN clients AS cts ON cts.id = cs.client_id
inner join items_unoee pds on pds.codigo='0000212'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000212','002')
WHERE cs.status = 'COMPLETADA' AND cs.get_sample = 1 AND (tsc.id=10 or tsc.id=11) 
        AND (cs.terminal LIKE '%CTG%'  OR cs.terminal like'%SPRC%' OR cs.terminal like'%CONTECA%' OR cs.terminal like'%BLOC%' OR cs.terminal like'%COMPAS%')  
        AND (  DATE(cs.end_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
GROUP BY cts.exporter_code ,date(cs.end_date), tsc.name, cs.terminal
ORDER BY cs.client_id ASC
); 





/*------------------- SOLICITUD MUESTRA PARTICULARES CTG  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cs.lot_coffee_ref,' x ', cs.qta_bag separator ' | ') AS LOTE,
    sum(cs.qta_bag) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    date(cs.end_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    cs.status AS ESTADO,
    group_concat(cs.id separator ' | ') AS MUESTRA,
    group_concat(distinct(cs.supply_company) separator ' | ')  AS EMPRESA_SUMINISTRO,
    group_concat(distinct(cs.sample_packing) separator ' | ') AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    tsc.name AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    concat("SOLICITUD DE MUESTRA PARTICULARES COMPLETADA EN PUERTO DE CARTAGENA TERMINAL ",cs.terminal) AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    sum(lp.precio*cs.qta_bag) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM coffee_sample AS cs
INNER JOIN sample_coffee_has_type_sample AS sct ON sct.sample_coffee_id = cs.id
INNER JOIN type_sample_coffe AS tsc ON tsc.id = sct.type_sample_coffee_id
INNER JOIN clients AS cts ON cts.id = cs.client_id
inner join items_unoee pds on pds.codigo='0000064'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000064','002')
WHERE cs.status = 'COMPLETADA' AND cs.get_sample = 1 AND tsc.id=9 
        AND (cs.terminal LIKE '%CTG%'  OR cs.terminal like'%SPRC%' OR cs.terminal like'%CONTECA%' OR cs.terminal like'%BLOC%' OR cs.terminal like'%COMPAS%')  
        AND (  DATE(cs.end_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
GROUP BY cts.exporter_code ,date(cs.end_date), tsc.name, cs.terminal
ORDER BY cs.client_id ASC
); 


/*------------------- SOLICITUD MUESTRA SUCAFINA CTG  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cs.lot_coffee_ref,' x ', cs.qta_bag separator ' | ') AS LOTE,
    sum(cs.qta_bag) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    date(cs.end_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    cs.status AS ESTADO,
    group_concat(cs.id separator ' | ') AS MUESTRA,
    group_concat(distinct(cs.supply_company) separator ' | ')  AS EMPRESA_SUMINISTRO,
    group_concat(distinct(cs.sample_packing) separator ' | ') AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    tsc.name AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "901012419" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    concat("SOLICITUD DE MUESTRA SUCAFINA COMPLETADA EN PUERTO DE CARTAGENA TERMINAL ",cs.terminal) AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    sum(lp.precio*cs.qta_bag) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM coffee_sample AS cs
INNER JOIN sample_coffee_has_type_sample AS sct ON sct.sample_coffee_id = cs.id
INNER JOIN type_sample_coffe AS tsc ON tsc.id = sct.type_sample_coffee_id
INNER JOIN clients AS cts ON cts.id = cs.client_id
inner join items_unoee pds on pds.codigo='0000064'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000064','002')
WHERE cs.status = 'COMPLETADA' AND cs.get_sample = 1 AND tsc.id=12 
        AND (cs.terminal LIKE '%CTG%'  OR cs.terminal like'%SPRC%' OR cs.terminal like'%CONTECA%' OR cs.terminal like'%BLOC%' OR cs.terminal like'%COMPAS%')  
        AND (  DATE(cs.end_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
GROUP BY cts.exporter_code ,date(cs.end_date), tsc.name, cs.terminal
ORDER BY cs.client_id ASC
); 


/*------------------- SOLICITUD MUESTRA WOLTHERS CTG  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cs.lot_coffee_ref,' x ', cs.qta_bag separator ' | ') AS LOTE,
    sum(cs.qta_bag) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    date(cs.end_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    cs.status AS ESTADO,
    group_concat(cs.id separator ' | ') AS MUESTRA,
    group_concat(distinct(cs.supply_company) separator ' | ')  AS EMPRESA_SUMINISTRO,
    group_concat(distinct(cs.sample_packing) separator ' | ') AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    tsc.name AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "901202236" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    concat("SOLICITUD DE MUESTRA WOLTHERS COMPLETADA EN PUERTO DE CARTAGENA TERMINAL ",cs.terminal) AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    sum(lp.precio*cs.qta_bag) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM coffee_sample AS cs
INNER JOIN sample_coffee_has_type_sample AS sct ON sct.sample_coffee_id = cs.id
INNER JOIN type_sample_coffe AS tsc ON tsc.id = sct.type_sample_coffee_id
INNER JOIN clients AS cts ON cts.id = cs.client_id
inner join items_unoee pds on pds.codigo='0000084'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000084','002')
WHERE cs.status = 'COMPLETADA' AND cs.get_sample = 1 AND tsc.id=3 
        AND (cs.terminal LIKE '%CTG%'  OR cs.terminal like'%SPRC%' OR cs.terminal like'%CONTECA%' OR cs.terminal like'%BLOC%' OR cs.terminal like'%COMPAS%')  
        AND (  DATE(cs.end_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
GROUP BY cts.exporter_code ,date(cs.end_date), tsc.name, cs.terminal
ORDER BY cs.client_id ASC
); 



/*------------------- SOLICITUD MUESTRA RGC CTG  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cs.lot_coffee_ref,' x ', cs.qta_bag separator ' | ') AS LOTE,
    sum(cs.qta_bag) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    date(cs.end_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    cs.status AS ESTADO,
    group_concat(cs.id separator ' | ') AS MUESTRA,
    group_concat(distinct(cs.supply_company) separator ' | ')  AS EMPRESA_SUMINISTRO,
    group_concat(distinct(cs.sample_packing) separator ' | ') AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    tsc.name AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    concat("SOLICITUD DE MUESTRA RGC COMPLETADA EN PUERTO DE CARTAGENA TERMINAL ",cs.terminal) AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    sum(lp.precio*cs.qta_bag) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM coffee_sample AS cs
INNER JOIN sample_coffee_has_type_sample AS sct ON sct.sample_coffee_id = cs.id
INNER JOIN type_sample_coffe AS tsc ON tsc.id = sct.type_sample_coffee_id
INNER JOIN clients AS cts ON cts.id = cs.client_id
inner join items_unoee pds on pds.codigo="0000211"
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=("0000211",'002')
WHERE cs.status = 'COMPLETADA' AND cs.get_sample = 1 AND tsc.id=1 
        AND (cs.terminal LIKE '%CTG%'  OR cs.terminal like'%SPRC%' OR cs.terminal like'%CONTECA%' OR cs.terminal like'%BLOC%' OR cs.terminal like'%COMPAS%')  
        AND (  DATE(cs.end_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
GROUP BY cts.exporter_code ,date(cs.end_date), tsc.name, cs.terminal
ORDER BY cs.client_id ASC
); 


/*------------------- SOLICITUD MUESTRA B HERMETICAS FNC CTG  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rm.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat(cts.exporter_code,'-',rm.lot_caffee,' x ', rm.quantity_radicated_bag_in) AS LOTE,
    (CEIL(rm.quantity_radicated_bag_in*0.32)) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    date(rm.created_date) AS FECHA_OPERACION,
    tu.name AS UNIDAD_CAFE,
    rm.bill_id AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "860007538" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    concat("SOLICITUD DE MUESTRA B HERMETICAS FNC COMPLETADA EN PUERTO DE CARTAGENA") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*(CEIL(rm.quantity_radicated_bag_in*0.32)) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM remittances_caffee AS rm
inner join clients cts on cts.id=rm.client_id
inner join type_units tu on tu.id=rm.type_units_id
inner join items_unoee pds on pds.codigo='0000066'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000066','012')
where (rm.jetty=2 OR rm.jetty=4 OR rm.jetty=5 OR rm.jetty=6 OR rm.jetty=9) and tu.id=4 AND rm.client_id=1
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 

); 


/*------------------- TRAZABILIDAD CONTENEDORES CTG -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-', rc.lot_caffee ,' x ',rc.quantity_radicated_bag_in separator ' | ') AS LOTES,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    ifn.packaging_mode AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    ivn.date_reg AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    ivn.id_tracking_siscafe  AS OIE,
    rdc.bic_container AS CONTENEDOR,
    pc.iso_ctn AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    ivn.ip_conection AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    concat("TRAZABILIDAD DE CONTENEDORES EMBALADOS PARA EL TERMINAL DE ",pc.jetty) AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM invoice_note as ivn
INNER JOIN clients AS cts ON cts.id = ivn.id_expo_pilcaf
INNER JOIN packaging_caffee AS pc ON pc.id = ivn.id_tracking_siscafe 
INNER JOIN ready_container AS rdc ON rdc.id = pc.ready_container_id
INNER JOIN detail_packaging_caffee AS dpc on dpc.packaging_caffee_id = ivn.id_tracking_siscafe
INNER JOIN info_navy AS ifn ON ifn.id = pc.info_navy_id
INNER JOIN remittances_caffee AS rc ON rc.id = dpc.remittances_caffee_id
inner join items_unoee pds on pds.codigo='0000054'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000054','002')
WHERE  ivn.type_invoice= 1 AND cts.id!=6 
    AND (pc.jetty LIKE 'CTG' OR pc.jetty LIKE 'SPRC' OR pc.jetty LIKE 'CONTECAR' OR pc.jetty LIKE 'BLOC' OR pc.jetty LIKE 'COMPAS' ) 
    AND (  DATE(ivn.date_reg) BETWEEN @FECHA_INF AND @FECHA_SUP   )
group by ivn.id_tracking_siscafe,rc.client_id,date(ivn.date_reg), pc.jetty
); 



/*------------------- TRAZABILIDAD VEHICULOS DESCARGADOS CARAVELA CTG -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rc.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat( cts.exporter_code,'-',rc.lot_caffee,' x ',rc.quantity_radicated_bag_in separator ' | ' ) AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    rc.created_date AS FECHA_OPERACION,
    tu.name AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    concat("TRAZABILIDAD DE VEHICULOS DESCARGADOS PARA EL TERMINAL CARTAGENA DEL EXPORTADOR CARAVELA") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM remittances_caffee AS rc 
INNER JOIN clients AS cts ON cts.id = rc.client_id 
INNER JOIN type_units AS tu ON tu.id = rc.type_units_id
inner join items_unoee pds on pds.codigo='0000214'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000214','002')
WHERE  rc.client_id =29  AND ( rc.slot_store_id=320 OR rc.slot_store_id=322 OR rc.slot_store_id=334 OR rc.slot_store_id=319 ) 
    AND (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP   )
group by rc.vehicle_plate,date(rc.created_date),rc.slot_store_id 
ORDER BY rc.created_date ASC 
); 



/*------------------- TRAZABILIDAD VEHICULOS DESCARGADOS CARCAFE CTG -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rc.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat( cts.exporter_code,'-',rc.lot_caffee,' x ',rc.quantity_radicated_bag_in separator ' | ' ) AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    rc.created_date AS FECHA_OPERACION,
    tu.name AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    concat("TRAZABILIDAD DE VEHICULOS DESCARGADOS PARA EL TERMINAL CARTAGENA DEL EXPORTADOR CARCAFE") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM remittances_caffee AS rc 
INNER JOIN clients AS cts ON cts.id = rc.client_id 
INNER JOIN type_units AS tu ON tu.id = rc.type_units_id
inner join items_unoee pds on pds.codigo='0000086'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000086','002')
WHERE rc.client_id=7 AND ( rc.slot_store_id=320 OR rc.slot_store_id=322 OR rc.slot_store_id=334 OR rc.slot_store_id=319 ) 
    AND (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP   )
group by rc.vehicle_plate,date(rc.created_date),rc.slot_store_id 
ORDER BY rc.created_date ASC 
); 


/*------------------- TRAZABILIDAD VEHICULOS DESCARGADOS INCONEXUS CTG -------------------*/
INSERT INTO data_billing_full (
SELECT 
   NULL AS ID,
    rc.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat( cts.exporter_code,'-',rc.lot_caffee,' x ',rc.quantity_radicated_bag_in separator ' | ' ) AS LOTE,
    sum(rc.quantity_radicated_bag_in) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    rc.created_date AS FECHA_OPERACION,
    tu.name AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    concat("TRAZABILIDAD DE VEHICULOS DESCARGADOS PARA EL TERMINAL CARTAGENA DEL EXPORTADOR INCONEXUS") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*sum(rc.quantity_radicated_bag_in) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM remittances_caffee AS rc 
INNER JOIN clients AS cts ON cts.id = rc.client_id 
INNER JOIN type_units AS tu ON tu.id = rc.type_units_id
inner join items_unoee pds on pds.codigo='0000149'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000149','002')
WHERE rc.client_id=61 AND ( rc.slot_store_id=320 OR rc.slot_store_id=322 OR rc.slot_store_id=334 OR rc.slot_store_id=319 )
    AND (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP   )
group by rc.vehicle_plate,date(rc.created_date),rc.slot_store_id
ORDER BY rc.created_date ASC 
); 



/*------------------- SOLICITUD TOMA DE MUESTRA GRAIN PRO  CTG  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rm.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat(cts.exporter_code,' - ',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in) AS LOTE,
    rm.quantity_radicated_bag_in  AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    date(rm.created_date) AS FECHA_OPERACION,
    uc.name_unit AS UNIDAD_CAFE,
    tu.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    concat("SOLICITUD DE MUESTRA B HERMETICAS PARTICULARES COMPLETADA EN PUERTO DE CARTAGENA TERMINAL CTG") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*rm.quantity_radicated_bag_in AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join type_units tu on tu.id=rm.type_units_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join items_unoee pds on pds.codigo='0000009'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000009','002')
where (rm.jetty=2 OR  rm.jetty=4 OR rm.jetty=5 OR rm.jetty=6 OR rm.jetty=9)
        and tu.id=4 AND rm.client_id!=1
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
ORDER BY rm.client_id ASC
); 





/*------------------- MUESTRAS ALMACAFE CARTAGENA -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    NULL AS COD_EXPORTADOR_SISCAFE,
    NULL AS CODIGO_EXPORTADOR,
    NULL AS NOMBRE_EXPORTADOR,
    NULL as LOTE,
    SUM(rc.quantity_radicated_bag_in) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    @FECHA_SUP AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    b_rc.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "860010973" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    concat("MUESTRAS ALMACAFE CARTAGENA (", ss.name_place,")") AS DESC_CONSULTA,
    concat(pds.descripcion," CARTAGENA (", ss.name_place,")")  AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*SUM(rc.quantity_radicated_bag_in) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rc
inner join departaments dp on dp.id=rc.jetty
inner join slot_store ss on ss.id=rc.slot_store_id
inner join type_units tu on rc.type_units_id=tu.id
inner join bill b_rc on rc.bill_id=b_rc.id
inner join clients AS cts on cts.id=rc.client_id
inner join service_package sp on sp.id=0006
inner join service_package_has_items_services spi on spi.service_package_id=sp.id
inner join items_unoee pds on pds.codigo='0000047'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000047','002')
where rc.vehicle_plate !='FIQUE' and rc.bill_id!=2 and rc.bill_id!=3 and tu.id!=9 
        AND (ss.ref_departament=2 OR ss.ref_departament=4 OR ss.ref_departament=5 OR ss.ref_departament=9) 
        and (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
group by ss.ref_departament
); 




/*------------------- MUESTRAS ALMACAFE AGUADULCE (SPIA) -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    NULL AS COD_EXPORTADOR_SISCAFE,
    NULL AS CODIGO_EXPORTADOR,
    NULL AS NOMBRE_EXPORTADOR,
    NULL as LOTE,
    SUM(rc.quantity_radicated_bag_in) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    @FECHA_SUP AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    b_rc.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "860010973" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    concat("MUESTRAS ALMACAFE AGUADULCE (", ss.name_place,")") AS DESC_CONSULTA,
    concat(pds.descripcion," AGUADULCE (", ss.name_place,")")  AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*SUM(rc.quantity_radicated_bag_in) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rc
inner join departaments dp on dp.id=rc.jetty
inner join slot_store ss on ss.id=rc.slot_store_id
inner join type_units tu on rc.type_units_id=tu.id
inner join bill b_rc on rc.bill_id=b_rc.id
inner join clients AS cts on cts.id=rc.client_id
inner join service_package sp on sp.id=0006
inner join service_package_has_items_services spi on spi.service_package_id=sp.id
inner join items_unoee pds on pds.codigo='0000047'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000047','001')
where rc.vehicle_plate !='FIQUE' and rc.bill_id!=2 and rc.bill_id!=3 AND ss.ref_departament=8  and tu.id!=9 
        and (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
group by ss.ref_departament
); 



/*------------------- DESCARGUE FNC STM tipo SACOS  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rm.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat(cts.exporter_code,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in) AS LOTE,
    rm.quantity_radicated_bag_in AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    DATE(rm.created_date) AS FECHA_OPERACION,
    uc.name_unit AS UNIDAD_CAFE,
    b_rm.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    "DESCARGUE DE FNC EN TERMINAL SANTA MARTA TIPO SACOS" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*rm.quantity_radicated_bag_in AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join bill b_rm on rm.bill_id=b_rm.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000110'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000110','010')
where ss.ref_departament=3 and rm.client_id =1 AND rm.bill_id!=2 
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
        and (client_id,vehicle_plate,date(rm.created_date),ss.ref_departament) 
        in (
            select client_id,vehicle_plate,date(rc.created_date),ss.ref_departament
            from remittances_caffee rc
            inner join slot_store ss on ss.id=rc.slot_store_id
            where ss.ref_departament=3 and rc.client_id =1 AND rc.bill_id!=2 AND (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP )
            group by rc.vehicle_plate,rc.client_id,date(rc.created_date),ss.ref_departament
        )
    ORDER BY DATE(rm.created_date) ASC
); 


/*------------------- DESCARGUE FNC STM TIPO CAJAS -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rm.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat(cts.exporter_code,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in) AS LOTE,
    rm.quantity_radicated_bag_in AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    DATE(rm.created_date) AS FECHA_OPERACION,
    uc.name_unit AS UNIDAD_CAFE,
    b_rm.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    "DESCARGUE DE FNC EN TERMINAL SANTA MARTA TIPO CAJAS" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*rm.quantity_radicated_bag_in AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join bill b_rm on rm.bill_id=b_rm.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000285'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000285','010')
where ss.ref_departament=3 and rm.client_id =1 AND rm.bill_id=2 and rm.vehicle_plate!= 'FIQUE' 
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
        and (client_id,vehicle_plate,date(rm.created_date),ss.ref_departament) 
        in ( 
            select client_id,vehicle_plate,date(rc.created_date),ss.ref_departament
            from remittances_caffee rc
            inner join slot_store ss on ss.id=rc.slot_store_id
            where ss.ref_departament=3 and rc.client_id =1 AND rc.bill_id=2 AND (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP )
            group by rc.vehicle_plate,rc.client_id,date(rc.created_date),ss.ref_departament
        )
    ORDER BY DATE(rm.created_date) ASC
); 


/*------------------- DESCARGUE STM MICROLOTES PARTICULARES -------------------*/
/*------------------- DESCARGUE STM MICROLOTES MAYOR IGUAL A 5 LOTES -------------------*/
INSERT INTO data_billing_full (
select 
    NULL AS ID,
    group_concat(rc.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rc.lot_caffee,' x ',rc.quantity_radicated_bag_in separator ' | ') AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    DATE(rc.created_date) AS FECHA_OPERACION,
    group_concat(distinct(uc.name_unit) separator ' | ') AS UNIDAD_CAFE,
    group_concat(distinct(b_rc.name) separator ' | ') AS UNIDAD_FACTURA,
    count(rc.id) AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    "DESCARGUE DE MICROLOTES PARTICULARES EN TERMINAL SANTA MARTA MAYOR IGUAL A 5 LOTES" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rc
inner join clients cts on cts.id=rc.client_id
inner join units_caffee uc on rc.units_cafee_id=uc.id
inner join bill b_rc on rc.bill_id=b_rc.id
inner join slot_store ss on ss.id=rc.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000114'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000114','003')
where rc.microlot=1 and ss.ref_departament=3 and rc.client_id !=1 
     AND (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP )    
group by rc.vehicle_plate, date(rc.created_date) , rc.client_id, ss.ref_departament
having count(rc.id) >= 5
ORDER BY cts.exporter_code ASC
); 



/*------------------- DESCARGUE STM MENOR A 100 PARTICULARES -------------------*/
/*------------------- DESCARGUE STM MENOR A 100 SACOS PARTICULARES -------------------*/
INSERT INTO data_billing_full (
select 
    NULL AS ID,
    group_concat(rc.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rc.lot_caffee,' x ',rc.quantity_radicated_bag_in separator ' | ') AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    date(rc.created_date) AS FECHA_OPERACION,
    group_concat(distinct(uc.name_unit) separator ' | ')  AS UNIDAD_CAFE,
    b_id.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    "DESCARGUE DE PARTICULARES EN TERMINAL SANTA MARTA MENOR A 100 SACOS CAFE" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee AS rc
inner join clients cts on cts.id = rc.client_id
INNER JOIN bill AS b_id ON b_id.id = rc.bill_id
inner join slot_store ss on ss.id=rc.slot_store_id
inner join units_caffee AS uc on rc.units_cafee_id=uc.id
inner join departaments dep on dep.id=ss.ref_departament
inner join items_unoee pds on pds.codigo='0000270'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000270','003')
where ss.ref_departament=3 and rc.client_id != 1 and rc.microlot=0 AND rc.bill_id=1
         AND (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP )
group by rc.vehicle_plate,rc.client_id,date(rc.created_date), ss.ref_departament
having sum(rc.quantity_radicated_bag_in) <=100
    ORDER BY cts.exporter_code ASC
); 


/*------------------- DESCARGUE STM MENOR A 250 y MAYOR A 101 PARTICULARES -------------------*/
/*------------------- DESCARGUE STM MENOR A 250 y MAYOR A 101 SACOS PARTICULARES -------------------*/
INSERT INTO data_billing_full (
select 
    NULL AS ID,
    group_concat(rc.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rc.lot_caffee,' x ',rc.quantity_radicated_bag_in separator ' | ') AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    date(rc.created_date) AS FECHA_OPERACION,
    group_concat(distinct(uc.name_unit) separator ' | ')  AS UNIDAD_CAFE,
    b_id.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    "DESCARGUE DE PARTICULARES EN TERMINAL SANTA MARTA DE 101 A 250 SACOS CAFE" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee AS rc
inner join clients cts on cts.id = rc.client_id
INNER JOIN bill AS b_id ON b_id.id = rc.bill_id
inner join slot_store ss on ss.id=rc.slot_store_id
inner join units_caffee AS uc on rc.units_cafee_id=uc.id
inner join departaments dep on dep.id=ss.ref_departament
inner join items_unoee pds on pds.codigo='0000287'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000287','003')
where ss.ref_departament=3 and rc.client_id != 1 and rc.microlot=0 AND rc.bill_id=1
         AND (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP )
group by rc.vehicle_plate,rc.client_id,date(rc.created_date), ss.ref_departament
having sum(rc.quantity_radicated_bag_in) BETWEEN 101 AND 250
    ORDER BY cts.exporter_code ASC
); 


/*------------------- DESCARGUE STM MAYOR DE 250 PARTICULARES -------------------*/
/*------------------- DESCARGUE STM MAYOR DE 250 SACOS DE 1-25 KG SACOS CAFE  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rm.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( cts.exporter_code ,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in ) AS LOTE,
    rm.quantity_radicated_bag_in AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    DATE(rm.created_date) AS FECHA_OPERACION,
    uc.name_unit AS UNIDAD_CAFE,
    b_rm.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    "DESCARGUE DE PARTICULARES EN TERMINAL SANTA MARTA MAYOR DE 250 SACOS DE 1-25 KG SACOS CAFE" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*rm.quantity_radicated_bag_in AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join bill b_rm on rm.bill_id=b_rm.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000222'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000222','003')
where ss.ref_departament=3 and rm.microlot=0 and rm.client_id !=1 AND rm.bill_id=1 AND uc.quantity <= 25
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
        and (client_id,vehicle_plate,date(rm.created_date),ss.ref_departament) 
        in (
            select client_id,vehicle_plate,date(rc.created_date),ss.ref_departament
            from remittances_caffee rc
            inner join slot_store ss on ss.id=rc.slot_store_id
            where ss.ref_departament=3 and rc.microlot=0 and rc.client_id !=1 AND rc.bill_id=1 AND (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP )
            group by rc.vehicle_plate,rc.client_id,date(rc.created_date),ss.ref_departament
            having sum(rc.quantity_radicated_bag_in) >250
        )
    ORDER BY cts.exporter_code ASC
); 


/*------------------- DESCARGUE STM MAYOR DE 250 SACOS DE 30 KG SACOS CAFE  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rm.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( cts.exporter_code ,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in ) AS LOTE,
    rm.quantity_radicated_bag_in AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    DATE(rm.created_date) AS FECHA_OPERACION,
    uc.name_unit AS UNIDAD_CAFE,
    b_rm.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    "DESCARGUE DE PARTICULARES EN TERMINAL SANTA MARTA MAYOR DE 250 SACOS DE 30 KG SACOS CAFE" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*rm.quantity_radicated_bag_in AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join bill b_rm on rm.bill_id=b_rm.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000223'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000223','003')
where ss.ref_departament=3 and rm.microlot=0 and rm.client_id !=1 AND rm.bill_id=1 AND uc.quantity = 30
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
        and (client_id,vehicle_plate,date(rm.created_date),ss.ref_departament) 
        in ( 
            select client_id,vehicle_plate,date(rc.created_date),ss.ref_departament
            from remittances_caffee rc
            inner join slot_store ss on ss.id=rc.slot_store_id
            where ss.ref_departament=3 and rc.microlot=0 and rc.client_id !=1 AND rc.bill_id=1  AND (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP )
            group by rc.vehicle_plate,rc.client_id,date(rc.created_date),ss.ref_departament
            having sum(rc.quantity_radicated_bag_in) >250
        )
    ORDER BY cts.exporter_code ASC
); 


/*------------------- DESCARGUE STM MAYOR DE 250 SACOS DE 35 KG SACOS CAFE  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rm.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( cts.exporter_code ,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in ) AS LOTE,
    rm.quantity_radicated_bag_in AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    DATE(rm.created_date) AS FECHA_OPERACION,
    uc.name_unit AS UNIDAD_CAFE,
    b_rm.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    "DESCARGUE DE PARTICULARES EN TERMINAL SANTA MARTA MAYOR DE 250 SACOS DE 35 KG SACOS CAFE" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*rm.quantity_radicated_bag_in AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join bill b_rm on rm.bill_id=b_rm.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000224'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000224','003')
where ss.ref_departament=3 and rm.microlot=0 and rm.client_id !=1 AND rm.bill_id=1 AND uc.quantity = 35
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
        and (client_id,vehicle_plate,date(rm.created_date),ss.ref_departament) 
        in (
            select client_id,vehicle_plate,date(rc.created_date),ss.ref_departament
            from remittances_caffee rc
            inner join slot_store ss on ss.id=rc.slot_store_id
            where ss.ref_departament=3 and rc.microlot=0 and rc.client_id !=1 AND rc.bill_id=1 AND (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP )
            group by rc.vehicle_plate,rc.client_id,date(rc.created_date),ss.ref_departament
            having sum(rc.quantity_radicated_bag_in) >250
        )
    ORDER BY cts.exporter_code ASC
); 


/*------------------- DESCARGUE STM MAYOR DE 250 SACOS DE 40 - 70 KG SACOS CAFE  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rm.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( cts.exporter_code ,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in ) AS LOTE,
    rm.quantity_radicated_bag_in AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    DATE(rm.created_date) AS FECHA_OPERACION,
    uc.name_unit AS UNIDAD_CAFE,
    b_rm.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    "DESCARGUE DE PARTICULARES EN TERMINAL SANTA MARTA MAYOR DE 250 SACOS DE 40-70 KG SACOS CAFE" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*rm.quantity_radicated_bag_in AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join bill b_rm on rm.bill_id=b_rm.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000111'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000111','003')
where ss.ref_departament=3 and rm.microlot=0 and rm.client_id !=1 AND rm.bill_id=1 AND ( uc.quantity between 40 and 70 )
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
        and (client_id,vehicle_plate,date(rm.created_date),ss.ref_departament) 
        in (
            select client_id,vehicle_plate,date(rc.created_date),ss.ref_departament
            from remittances_caffee rc
            inner join slot_store ss on ss.id=rc.slot_store_id
            where ss.ref_departament=3 and rc.microlot=0 and rc.client_id !=1 AND rc.bill_id=1 AND (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP )
            group by rc.vehicle_plate,rc.client_id,date(rc.created_date),ss.ref_departament
            having sum(rc.quantity_radicated_bag_in) >250
        )
    ORDER BY cts.exporter_code ASC
); 


/*------------------- DESCARGUE STM SACOS CACAO  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rm.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( cts.exporter_code ,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in ) AS LOTE,
    rm.quantity_radicated_bag_in AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    DATE(rm.created_date) AS FECHA_OPERACION,
    uc.name_unit AS UNIDAD_CAFE,
    b_rm.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    "DESCARGUE DE PARTICULARES EN TERMINAL SANTA MARTA SACOS CACAO" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*rm.quantity_radicated_bag_in AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join bill b_rm on rm.bill_id=b_rm.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000049'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000049','003')
where ss.ref_departament=3 and rm.microlot=0 and rm.client_id !=1 AND rm.bill_id=3 
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )        
    ORDER BY cts.exporter_code ASC
); 


/*------------------- DESCARGUE STM SACOS BIG BAGS  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rm.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( cts.exporter_code ,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in ) AS LOTE,
    rm.quantity_radicated_bag_in AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    DATE(rm.created_date) AS FECHA_OPERACION,
    uc.name_unit AS UNIDAD_CAFE,
    b_rm.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    "DESCARGUE DE PARTICULARES EN TERMINAL SANTA MARTA TIPO BIG BAGS" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*rm.quantity_radicated_bag_in  AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join bill b_rm on rm.bill_id=b_rm.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000184'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000184','003')
where ss.ref_departament=3 and rm.microlot=0 and rm.client_id !=1 AND rm.bill_id=4  AND uc.quantity=1000
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
    ORDER BY cts.exporter_code ASC
);  



/*------------------- DESCARGUE STM SACOS MINI BIG BAGS  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rm.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( cts.exporter_code ,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in ) AS LOTE,
    rm.quantity_radicated_bag_in AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    DATE(rm.created_date) AS FECHA_OPERACION,
    uc.name_unit AS UNIDAD_CAFE,
    b_rm.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    "DESCARGUE DE PARTICULARES EN TERMINAL SANTA MARTA TIPO MINI BIG BAGS" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*rm.quantity_radicated_bag_in  AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join bill b_rm on rm.bill_id=b_rm.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000347'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000347','003')
where ss.ref_departament=3 and rm.microlot=0 and rm.client_id !=1 AND rm.bill_id=4  AND uc.quantity!=1000
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
    ORDER BY cts.exporter_code ASC
);  



/*------------------- DESCARGUE STM CAJAS DE 1 A 10  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    DATE(rm.created_date) AS FECHA_OPERACION,
    group_concat(distinct(uc.name_unit) separator ' | ') AS UNIDAD_CAFE,
    group_concat(distinct(b_rm.name) separator ' | ') AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    "DESCARGUE DE PARTICULARES EN TERMINAL SANTA MARTA TIPO CAJAS DE 1 A 10 UNIDADES" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join bill b_rm on rm.bill_id=b_rm.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000032'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000032','003')
where ss.ref_departament=3 and rm.microlot=0 and rm.client_id !=1 AND rm.bill_id=2 and rm.vehicle_plate!= 'FIQUE' 
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by rm.vehicle_plate,rm.client_id,date(rm.created_date),ss.ref_departament
having sum(rm.quantity_radicated_bag_in) <= 10 
ORDER BY cts.exporter_code ASC
); 


/*------------------- DESCARGUE STM CAJAS DE 11 A 100  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    DATE(rm.created_date) AS FECHA_OPERACION,
    group_concat(distinct(uc.name_unit) separator ' | ') AS UNIDAD_CAFE,
    group_concat(distinct(b_rm.name) separator ' | ') AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    "DESCARGUE DE PARTICULARES EN TERMINAL SANTA MARTA TIPO CAJAS DE 11 A 100 UNIDADES" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join bill b_rm on rm.bill_id=b_rm.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000203'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000203','003')
where ss.ref_departament=3 and rm.microlot=0 and rm.client_id !=1 AND rm.bill_id=2
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by rm.vehicle_plate,rm.client_id,date(rm.created_date),ss.ref_departament
having sum(rm.quantity_radicated_bag_in) between 11 and 100
ORDER BY cts.exporter_code ASC
); 


/*------------------- DESCARGUE STM CAJAS MAYOR DE 100  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    sum(rm.quantity_radicated_bag_in) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    DATE(rm.created_date) AS FECHA_OPERACION,
    group_concat(distinct(uc.name_unit) separator ' | ') AS UNIDAD_CAFE,
    group_concat(distinct(b_rm.name) separator ' | ') AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    "DESCARGUE DE PARTICULARES EN TERMINAL SANTA MARTA TIPO CAJAS MAYOR A 100 UNIDADES" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*sum(rm.quantity_radicated_bag_in) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join bill b_rm on rm.bill_id=b_rm.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000215'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000215','003')
where ss.ref_departament=3 and rm.microlot=0 and rm.client_id !=1 AND rm.bill_id=2 
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by rm.vehicle_plate,rm.client_id,date(rm.created_date),ss.ref_departament 
having sum(rm.quantity_radicated_bag_in) > 100    
ORDER BY cts.exporter_code ASC
); 


/*------------------- DESCARGUE STM CAFE VERDE ESTIBAS -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rm.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( cts.exporter_code,'-',rm.lot_caffee,' x ', rm.quantity_radicated_bag_in) AS LOTE,
    rm.quantity_radicated_bag_in AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    DATE(rm.created_date) AS FECHA_OPERACION,
    uc.name_unit AS UNIDAD_CAFE,
    b_rm.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    "DESCARGUE DE PARTICULARES EN TERMINAL SANTA MARTA TIPO CAFE VERDE ESTIBAS" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join bill b_rm on rm.bill_id=b_rm.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo="0000295"
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=("0000295",'003')
where ss.ref_departament=3 and rm.microlot=0 and rm.client_id !=1 AND rm.bill_id=18
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
ORDER BY cts.exporter_code ASC
); 



/*------------------- DEVOLUCIONES FNC -------------------*/
/*------------------- DEVOLUCION FNC TIPO CAFE TERMINAL STM  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    group_concat(rcrc.remittances_caffee_id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee,' x ' ,rcrc.qta_bags separator ' | ') as LOTE,
    sum(rcrc.qta_bags) AS CANTIDAD_OPERATIVA,
    rc.return_type AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    rc.return_date  AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    rc.state_return AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    concat("DEVOLUCIONES FNC EN PUERTO DE SANTA MARTA TIPO CAFE") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*sum(rcrc.qta_bags) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from returns_caffees rc
inner join remittances_caffee_returns_caffees rcrc on rcrc.returns_caffee_id=rc.id
inner join clients as cts on cts.id=rc.client_id
inner join remittances_caffee as rm on rcrc.remittances_caffee_id = rm.id
inner join departaments dep on rc.jetty = dep.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join items_unoee pds on pds.codigo='0000288'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000288','003')
where rc.jetty=3 AND rc.state_return="COMPLETADA" AND cts.id=1 AND rc.return_type = 'CAFE' 
        AND (  DATE(rc.return_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by rc.vehicle_plate,cts.exporter_code,date(rc.return_date), rc.jetty
); 


/*------------------- DEVOLUCION FNC STM FIQUE MAYOR A 10000  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    group_concat(rcrc.remittances_caffee_id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee,' x ' ,rcrc.qta_bags separator ' | ') as LOTE,
    1 AS CANTIDAD_OPERATIVA,
    rc.return_type AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    rc.return_date  AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    rc.state_return AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    concat("DEVOLUCIONES FNC EN PUERTO DE SANTA MARTA TIPO FIQUE MAYOR A 10000") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from returns_caffees rc
inner join remittances_caffee_returns_caffees rcrc on rcrc.returns_caffee_id=rc.id
inner join clients as cts on cts.id=rc.client_id
inner join remittances_caffee as rm on rcrc.remittances_caffee_id = rm.id
inner join departaments dep on rc.jetty = dep.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join items_unoee pds on pds.codigo='0000127'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000127','010')
where rc.jetty=3 AND rc.state_return="COMPLETADA" AND cts.id=1 AND rc.return_type = 'FIQUE' 
        and (  DATE(rc.return_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
group by rc.vehicle_plate,cts.exporter_code,date(rc.return_date), rc.jetty
having sum(rcrc.qta_bags) > 10000
); 


/*------------------- DEVOLUCION FNC CAFE STM FIQUE MENOR A 10000  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    group_concat(rcrc.remittances_caffee_id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee,' x ' ,rcrc.qta_bags separator ' | ') as LOTE,
    1 AS CANTIDAD_OPERATIVA,
    rc.return_type AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    rc.return_date  AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    rc.state_return AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "DEVOLUCIONES FNC EN TERMINAL SANTA MARTA TIPO FIQUE MENOR A 10000" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from returns_caffees rc
inner join remittances_caffee_returns_caffees rcrc on rcrc.returns_caffee_id=rc.id
inner join clients as cts on cts.id=rc.client_id
inner join remittances_caffee as rm on rcrc.remittances_caffee_id = rm.id
inner join departaments dep on rc.jetty = dep.id
inner join items_unoee pds on pds.codigo='0000219'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000219','010')
where rc.jetty=3 and rc.state_return="COMPLETADA" and cts.id=1 AND rc.return_type = 'FIQUE' 
    and (  DATE(rc.return_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
group by rc.vehicle_plate,cts.exporter_code,date(rc.return_date), rc.jetty
having sum(rcrc.qta_bags) <=10000
); 



/*------------------- DEVOLUCION PARTICULARES CAFE STM MAYOR 250  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    group_concat(rcrc.remittances_caffee_id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee,' x ' ,rcrc.qta_bags separator ' | ') as LOTE,
    sum(rcrc.qta_bags) AS CANTIDAD_OPERATIVA,
    rc.return_type AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    rc.return_date  AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    rc.state_return AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    "DEVOLUCIONES PARTICULARES EN TERMINAL DE SANTA MARTA TIPO CAFE MAYOR A 250" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*sum(rcrc.qta_bags) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from returns_caffees rc
inner join remittances_caffee_returns_caffees rcrc on rcrc.returns_caffee_id=rc.id
inner join clients AS cts on cts.id=rc.client_id
inner join remittances_caffee as rm on rcrc.remittances_caffee_id = rm.id
inner join departaments dep on rc.jetty = dep.id
inner join items_unoee pds on pds.codigo='0000277'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000277','003')
where rc.jetty=3 and rc.state_return="COMPLETADA" and cts.id!=1 AND rc.return_type = 'CAFE' 
        and (  DATE(rc.return_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
group by rc.vehicle_plate,cts.exporter_code,date(rc.return_date), rc.jetty
having sum(rcrc.qta_bags) >250
ORDER BY exporter_code ASC
); 


/*------------------- DEVOLUCION PARTICULARES CAFE STM MENOR 250  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    group_concat(rcrc.remittances_caffee_id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee,' x ' ,rcrc.qta_bags separator ' | ') as LOTE,
    1 AS CANTIDAD_OPERATIVA,
    rc.return_type AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    rc.return_date  AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    rc.state_return AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    "DEVOLUCIONES PARTICULARES EN TERMINAL DE SANTA MARTA TIPO CAFE MENOR A 250" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from returns_caffees rc
inner join remittances_caffee_returns_caffees rcrc on rcrc.returns_caffee_id=rc.id
inner join clients AS cts on cts.id=rc.client_id
inner join remittances_caffee as rm on rcrc.remittances_caffee_id = rm.id
inner join departaments dep on rc.jetty = dep.id
inner join items_unoee pds on pds.codigo='0000001'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000001','003')
where rc.jetty=3 and rc.state_return="COMPLETADA" and cts.id!=1 AND rc.return_type = 'CAFE' 
        and (  DATE(rc.return_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
group by rc.vehicle_plate,cts.exporter_code,date(rc.return_date), rc.jetty
having sum(rcrc.qta_bags) <=250
ORDER BY exporter_code ASC
); 


/*------------------- DEVOLUCION PARTICULARES STM FIQUE MAYOR A 10000  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    group_concat(rcrc.remittances_caffee_id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee,' x ' ,rcrc.qta_bags separator ' | ') as LOTE,
    1 AS CANTIDAD_OPERATIVA,
    rc.return_type AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    rc.return_date  AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    rc.state_return AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    "DEVOLUCIONES PARTICULARES EN TERMINAL DE SANTA MARTA TIPO FIQUE MAYOR A 10000" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from returns_caffees rc
inner join remittances_caffee_returns_caffees rcrc on rcrc.returns_caffee_id=rc.id
inner join clients AS cts on cts.id=rc.client_id
inner join remittances_caffee as rm on rcrc.remittances_caffee_id = rm.id
inner join departaments dep on rc.jetty = dep.id
inner join items_unoee pds on pds.codigo='0000127'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000127','003')
where rc.jetty=3 and rc.state_return="COMPLETADA" and cts.id!=1 AND rc.return_type = 'FIQUE' 
        and (  DATE(rc.return_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
group by rc.vehicle_plate,cts.exporter_code,date(rc.return_date), rc.jetty
having sum(rcrc.qta_bags) >10000
ORDER BY exporter_code ASC
); 


/*------------------- DEVOLUCION PARTICULARES STM FIQUE MENOR A 5000 -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    group_concat(rcrc.remittances_caffee_id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee,' x ' ,rcrc.qta_bags separator ' | ') as LOTE,
    1 AS CANTIDAD_OPERATIVA,
    rc.return_type AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    rc.return_date  AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    rc.state_return AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    "DEVOLUCIONES PARTICULARES EN TERMINAL DE SANTA MARTA TIPO FIQUE MENOR A 5000" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from returns_caffees rc
inner join remittances_caffee_returns_caffees rcrc on rcrc.returns_caffee_id=rc.id
inner join clients AS cts on cts.id=rc.client_id
inner join remittances_caffee as rm on rcrc.remittances_caffee_id = rm.id
inner join departaments dep on rc.jetty = dep.id
inner join items_unoee pds on pds.codigo='0000204'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000204','003')
where rc.jetty=3 and rc.state_return="COMPLETADA" and cts.id!=1 AND rc.return_type = 'FIQUE' 
        and (  DATE(rc.return_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
group by rc.vehicle_plate,cts.exporter_code,date(rc.return_date), rc.jetty
having sum(rcrc.qta_bags) <=5000
ORDER BY exporter_code ASC
); 



/*------------------- DEVOLUCION PARTICULARES STM FIQUE MAYOR A 5001 Y MENOR 10000  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    group_concat(rcrc.remittances_caffee_id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee,' x ' ,rcrc.qta_bags separator ' | ') as LOTE,
    1 AS CANTIDAD_OPERATIVA,
    rc.return_type AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    rc.return_date  AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    rc.state_return AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    "DEVOLUCIONES PARTICULARES EN TERMINAL DE SANTA MARTA TIPO FIQUE MAYOR A 5001 Y MENOR 10000" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from returns_caffees rc
inner join remittances_caffee_returns_caffees rcrc on rcrc.returns_caffee_id=rc.id
inner join clients AS cts on cts.id=rc.client_id
inner join remittances_caffee as rm on rcrc.remittances_caffee_id = rm.id
inner join departaments dep on rc.jetty = dep.id
inner join items_unoee pds on pds.codigo='0000010'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000010','003')
where rc.jetty=3 and rc.state_return="COMPLETADA" and cts.id!=1 AND rc.return_type = 'FIQUE' 
        and (  DATE(rc.return_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
group by rc.vehicle_plate,cts.exporter_code,date(rc.return_date), rc.jetty
having (sum(rcrc.qta_bags) between 5001 and 10000)
ORDER BY exporter_code ASC
); 



/*------------------- SOLICITUD CONTRAMUESTRA DREYFUS STM -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cs.lot_coffee_ref,' x ', cs.qta_bag separator ' | ') AS LOTE,
    sum(cs.qta_bag) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    date(cs.end_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    cs.status AS ESTADO,
    group_concat(cs.id separator ' | ') AS MUESTRA,
    group_concat(distinct(cs.supply_company) separator ' | ')  AS EMPRESA_SUMINISTRO,
    group_concat(distinct(cs.sample_packing) separator ' | ') AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    tsc.name AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "900174478" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    concat("SOLICITUD DE MUESTRA Y CONTRAMUESTRA DREYFUS COMPLETADA EN PUERTO DE SANTA MARTA TERMINAL ",cs.terminal) AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    sum(lp.precio*cs.qta_bag) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM coffee_sample AS cs
INNER JOIN sample_coffee_has_type_sample AS sct ON sct.sample_coffee_id = cs.id
INNER JOIN type_sample_coffe AS tsc ON tsc.id = sct.type_sample_coffee_id
INNER JOIN clients AS cts ON cts.id = cs.client_id
inner join items_unoee pds on pds.codigo='0000213'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000213','003')
WHERE cs.status = 'COMPLETADA' AND cs.get_sample = 1 AND (tsc.id=13 OR tsc.id=5) 
        AND (cs.terminal LIKE '%STM%'  OR cs.terminal like'%STA%' OR cs.terminal like'%SMITCO%')  
        AND (  DATE(cs.end_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
GROUP BY cts.exporter_code ,date(cs.end_date), tsc.name, cs.terminal
ORDER BY cs.client_id ASC
); 


/*------------------- SOLICITUD CONTRAMUESTRA INCONEXUS STM  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cs.lot_coffee_ref,' x ', cs.qta_bag separator ' | ') AS LOTE,
    sum(cs.qta_bag) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    date(cs.end_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    cs.status AS ESTADO,
    group_concat(cs.id separator ' | ') AS MUESTRA,
    group_concat(distinct(cs.supply_company) separator ' | ')  AS EMPRESA_SUMINISTRO,
    group_concat(distinct(cs.sample_packing) separator ' | ') AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    tsc.name AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "830092113" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    concat("SOLICITUD DE MUESTRA Y CONTRAMUESTRA INCONEXUS COMPLETADA EN PUERTO DE SANTA MARTA TERMINAL ",cs.terminal) AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    sum(lp.precio*cs.qta_bag) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM coffee_sample AS cs
INNER JOIN sample_coffee_has_type_sample AS sct ON sct.sample_coffee_id = cs.id
INNER JOIN type_sample_coffe AS tsc ON tsc.id = sct.type_sample_coffee_id
INNER JOIN clients AS cts ON cts.id = cs.client_id
inner join items_unoee pds on pds.codigo='0000064'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000064','003')
WHERE cs.status = 'COMPLETADA' AND cs.get_sample = 1 AND (tsc.id=15 or tsc.id=7)
        AND (cs.terminal LIKE '%STM%'  OR cs.terminal like'%STA%' OR cs.terminal like'%SMITCO%')  
        AND (  DATE(cs.end_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
GROUP BY cts.exporter_code ,date(cs.end_date), tsc.name, cs.terminal
ORDER BY cs.client_id ASC
); 


/*------------------- SOLICITUD CONTRAMUESTRA SUCAFINA STM  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cs.lot_coffee_ref,' x ', cs.qta_bag separator ' | ') AS LOTE,
    sum(cs.qta_bag) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    date(cs.end_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    cs.status AS ESTADO,
    group_concat(cs.id separator ' | ') AS MUESTRA,
    group_concat(distinct(cs.supply_company) separator ' | ')  AS EMPRESA_SUMINISTRO,
    group_concat(distinct(cs.sample_packing) separator ' | ') AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    tsc.name AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "901012419" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    concat("SOLICITUD DE CONTRAMUESTRA SUCAFINA COMPLETADA EN PUERTO DE SANTA MARTA TERMINAL ",cs.terminal) AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    sum(lp.precio*cs.qta_bag) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM coffee_sample AS cs
INNER JOIN sample_coffee_has_type_sample AS sct ON sct.sample_coffee_id = cs.id
INNER JOIN type_sample_coffe AS tsc ON tsc.id = sct.type_sample_coffee_id
INNER JOIN clients AS cts ON cts.id = cs.client_id
inner join items_unoee pds on pds.codigo='0000064'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000064','003')
WHERE cs.status = 'COMPLETADA' AND cs.get_sample = 1 AND tsc.id=16
        AND (cs.terminal LIKE '%STM%'  OR cs.terminal like'%STA%' OR cs.terminal like'%SMITCO%')  
        AND (  DATE(cs.end_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
GROUP BY cts.exporter_code ,date(cs.end_date), tsc.name, cs.terminal
ORDER BY cs.client_id ASC
); 


/*------------------- SOLICITUD CONTRAMUESTRA SUCDEN STM -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cs.lot_coffee_ref,' x ', cs.qta_bag separator ' | ') AS LOTE,
    sum(cs.qta_bag) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    date(cs.end_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    cs.status AS ESTADO,
    group_concat(cs.id separator ' | ') AS MUESTRA,
    group_concat(distinct(cs.supply_company) separator ' | ')  AS EMPRESA_SUMINISTRO,
    group_concat(distinct(cs.sample_packing) separator ' | ') AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    tsc.name AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "900099234" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    concat("SOLICITUD DE MUESTRA Y CONTRAMUESTRA SUCDEN COMPLETADA EN PUERTO DE SANTA MARTA TERMINAL ",cs.terminal) AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    sum(lp.precio*cs.qta_bag) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM coffee_sample AS cs
INNER JOIN sample_coffee_has_type_sample AS sct ON sct.sample_coffee_id = cs.id
INNER JOIN type_sample_coffe AS tsc ON tsc.id = sct.type_sample_coffee_id
INNER JOIN clients AS cts ON cts.id = cs.client_id
inner join items_unoee pds on pds.codigo='0000212'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000212','003')
WHERE cs.status = 'COMPLETADA' AND cs.get_sample = 1 AND (tsc.id=10 or tsc.id=11) 
        AND (cs.terminal LIKE '%STM%'  OR cs.terminal like'%STA%' OR cs.terminal like'%SMITCO%')  
        AND (  DATE(cs.end_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
GROUP BY cts.exporter_code ,date(cs.end_date), tsc.name, cs.terminal
ORDER BY cs.client_id ASC
); 




/*------------------- SOLICITUD MUESTRA PARTICULARES STM  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cs.lot_coffee_ref,' x ', cs.qta_bag separator ' | ') AS LOTE,
    sum(cs.qta_bag) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    date(cs.end_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    cs.status AS ESTADO,
    group_concat(cs.id separator ' | ') AS MUESTRA,
    group_concat(distinct(cs.supply_company) separator ' | ')  AS EMPRESA_SUMINISTRO,
    group_concat(distinct(cs.sample_packing) separator ' | ') AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    tsc.name AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    concat("SOLICITUD DE MUESTRA PARTICULARES COMPLETADA EN PUERTO DE SANTA MARTA TERMINAL ",cs.terminal) AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    sum(lp.precio*cs.qta_bag) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM coffee_sample AS cs
INNER JOIN sample_coffee_has_type_sample AS sct ON sct.sample_coffee_id = cs.id
INNER JOIN type_sample_coffe AS tsc ON tsc.id = sct.type_sample_coffee_id
INNER JOIN clients AS cts ON cts.id = cs.client_id
inner join items_unoee pds on pds.codigo='0000064'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000064','003')
WHERE cs.status = 'COMPLETADA' AND cs.get_sample = 1 AND tsc.id=9 
        AND (cs.terminal LIKE '%STM%'  OR cs.terminal like'%STA%' OR cs.terminal like'%SMITCO%')  
        AND (  DATE(cs.end_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
GROUP BY cts.exporter_code ,date(cs.end_date), tsc.name, cs.terminal
ORDER BY cs.client_id ASC
); 


/*------------------- SOLICITUD MUESTRA SUCAFINA STM  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cs.lot_coffee_ref,' x ', cs.qta_bag separator ' | ') AS LOTE,
    sum(cs.qta_bag) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    date(cs.end_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    cs.status AS ESTADO,
    group_concat(cs.id separator ' | ') AS MUESTRA,
    group_concat(distinct(cs.supply_company) separator ' | ')  AS EMPRESA_SUMINISTRO,
    group_concat(distinct(cs.sample_packing) separator ' | ') AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    tsc.name AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "901012419" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    concat("SOLICITUD DE MUESTRA SUCAFINA COMPLETADA EN PUERTO DE SANTA MARTA TERMINAL ",cs.terminal) AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    sum(lp.precio*cs.qta_bag) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM coffee_sample AS cs
INNER JOIN sample_coffee_has_type_sample AS sct ON sct.sample_coffee_id = cs.id
INNER JOIN type_sample_coffe AS tsc ON tsc.id = sct.type_sample_coffee_id
INNER JOIN clients AS cts ON cts.id = cs.client_id
inner join items_unoee pds on pds.codigo='0000064'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000064','003')
WHERE cs.status = 'COMPLETADA' AND cs.get_sample = 1 AND tsc.id=12 
        AND (cs.terminal LIKE '%STM%'  OR cs.terminal like'%STA%' OR cs.terminal like'%SMITCO%')  
        AND (  DATE(cs.end_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
GROUP BY cts.exporter_code ,date(cs.end_date), tsc.name, cs.terminal
ORDER BY cs.client_id ASC
); 


/*------------------- SOLICITUD MUESTRA WOLTHERS STM  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cs.lot_coffee_ref,' x ', cs.qta_bag separator ' | ') AS LOTE,
    sum(cs.qta_bag) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    date(cs.end_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    cs.status AS ESTADO,
    group_concat(cs.id separator ' | ') AS MUESTRA,
    group_concat(distinct(cs.supply_company) separator ' | ')  AS EMPRESA_SUMINISTRO,
    group_concat(distinct(cs.sample_packing) separator ' | ') AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    tsc.name AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "901202236" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    concat("SOLICITUD DE MUESTRA WOLTHERS COMPLETADA EN PUERTO DE SANTA MARTA TERMINAL ",cs.terminal) AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    sum(lp.precio*cs.qta_bag) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM coffee_sample AS cs
INNER JOIN sample_coffee_has_type_sample AS sct ON sct.sample_coffee_id = cs.id
INNER JOIN type_sample_coffe AS tsc ON tsc.id = sct.type_sample_coffee_id
INNER JOIN clients AS cts ON cts.id = cs.client_id
inner join items_unoee pds on pds.codigo='0000084'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000084','003')
WHERE cs.status = 'COMPLETADA' AND cs.get_sample = 1 AND tsc.id=3 
        AND (cs.terminal LIKE '%STM%'  OR cs.terminal like'%STA%' OR cs.terminal like'%SMITCO%')  
        AND (  DATE(cs.end_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
GROUP BY cts.exporter_code ,date(cs.end_date), tsc.name, cs.terminal
ORDER BY cs.client_id ASC
); 


/*------------------- SOLICITUD MUESTRA B HERMETICAS FNC STM  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rm.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat(cts.exporter_code,'-',rm.lot_caffee,' x ', rm.quantity_radicated_bag_in) AS LOTE,
    (CEIL(rm.quantity_radicated_bag_in*0.32)) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    date(rm.created_date) AS FECHA_OPERACION,
    tu.name AS UNIDAD_CAFE,
    rm.bill_id AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "860007538" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    concat("SOLICITUD DE MUESTRA B. HERME FNC COMPLETADA EN PUERTO DE SANTA MARTA") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*(CEIL(rm.quantity_radicated_bag_in*0.32)) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM remittances_caffee AS rm
inner join clients cts on cts.id=rm.client_id
inner join type_units tu on tu.id=rm.type_units_id
inner join items_unoee pds on pds.codigo='0000066'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000066','010')
where rm.jetty=3 and tu.id=4 AND rm.client_id=1
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
); 



/*------------------- TRAZABILIDAD CONTENEDORES STM  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(vp2.LOTES separator ' | ') AS LOTES,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    vp2.MODALIDAD AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    ivn.date_reg AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    ivn.id_tracking_siscafe  AS OIE,
    rdc.bic_container AS CONTENEDOR,
    pc.iso_ctn AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    ivn.ip_conection AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    concat("TRAZABILIDAD DE CONTENEDORES EMBALADOS PARA EL TERMINAL DE ",pc.jetty) AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM invoice_note as ivn
INNER JOIN clients AS cts ON cts.id = ivn.id_expo_pilcaf
INNER JOIN packaging_caffee AS pc ON pc.id = ivn.id_tracking_siscafe 
INNER JOIN ready_container AS rdc ON rdc.id = pc.ready_container_id
inner join view_packaging2 AS vp2 on vp2.OIE = ivn.id_tracking_siscafe
inner join items_unoee pds on pds.codigo='0000054'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000054','003')
WHERE  ivn.type_invoice= 1 AND cts.id!=6  AND (pc.jetty='stm' OR pc.jetty='sta')
     AND (  DATE(ivn.date_reg) BETWEEN @FECHA_INF AND @FECHA_SUP   )
group by ivn.id_tracking_siscafe,date(ivn.date_reg), pc.jetty
); 



/*------------------- TRAZABILIDAD VEHICULOS DESCARGADOS CARAVELA  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rc.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat( cts.exporter_code,'-',rc.lot_caffee,' x ',rc.quantity_radicated_bag_in separator ' | ' ) AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    rc.created_date AS FECHA_OPERACION,
    tu.name AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    "TRAZABILIDAD DE VEHICULOS DESCARGADOS PARA EL TERMINAL DE SANTA MARTA DEL EXPORTADOR CARAVELA" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM remittances_caffee AS rc 
INNER JOIN clients AS cts ON cts.id = rc.client_id 
INNER JOIN type_units AS tu ON tu.id = rc.type_units_id
inner join items_unoee pds on pds.codigo='0000214'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000214','003')
WHERE rc.jetty=3 AND rc.client_id =29 
    AND (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP   )
group by rc.vehicle_plate,date(rc.created_date),rc.jetty
ORDER BY rc.created_date ASC 
); 


/*------------------- TRAZABILIDAD VEHICULOS DESCARGADOS CARCAFE  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rc.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat( cts.exporter_code,'-',rc.lot_caffee,' x ',rc.quantity_radicated_bag_in separator ' | ' ) AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    rc.created_date AS FECHA_OPERACION,
    tu.name AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    "TRAZABILIDAD DE VEHICULOS DESCARGADOS PARA EL TERMINAL DE SANTA MARTA DEL EXPORTADOR CARCAFE" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM remittances_caffee AS rc 
INNER JOIN clients AS cts ON cts.id = rc.client_id 
INNER JOIN type_units AS tu ON tu.id = rc.type_units_id
inner join items_unoee pds on pds.codigo='0000086'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000086','003')
WHERE rc.jetty=3 AND rc.client_id =7 
    AND (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP   )
group by rc.vehicle_plate,date(rc.created_date),rc.jetty
ORDER BY rc.created_date ASC 
); 


/*------------------- TRAZABILIDAD VEHICULOS DESCARGADOS INCONEXUS  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rc.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat( cts.exporter_code,'-',rc.lot_caffee,' x ',rc.quantity_radicated_bag_in separator ' | ' ) AS LOTE,
    sum(rc.quantity_radicated_bag_in) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    rc.created_date AS FECHA_OPERACION,
    tu.name AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    "TRAZABILIDAD DE VEHICULOS DESCARGADOS PARA EL TERMINAL DE SANTA MARTA DEL EXPORTADOR INCONEXUS" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*sum(rc.quantity_radicated_bag_in) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM remittances_caffee AS rc 
INNER JOIN clients AS cts ON cts.id = rc.client_id 
INNER JOIN type_units AS tu ON tu.id = rc.type_units_id
inner join items_unoee pds on pds.codigo='0000149'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000149','003')
where rc.jetty=3 AND rc.client_id =61 
    AND (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP   )
group by rc.vehicle_plate,date(rc.created_date),rc.jetty
ORDER BY rc.created_date ASC  
); 



/*------------------- SOLICITUD TOMA DE MUESTRA GRAIN PRO  STM  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rm.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat(cts.exporter_code,'-',rm.lot_caffee,' x ', rm.quantity_radicated_bag_in) AS LOTE,
    rm.quantity_radicated_bag_in  AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    date(rm.created_date) AS FECHA_OPERACION,
    uc.name_unit AS UNIDAD_CAFE,
    tu.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    concat("SOLICITUD DE MUESTRA B. HERME PARTICULARES COMPLETADA EN PUERTO DE SANTA MARTA ") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*rm.quantity_radicated_bag_in AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join type_units tu on tu.id=rm.type_units_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join items_unoee pds on pds.codigo='0000009'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000009','003')
where rm.jetty=3 and tu.id=4 AND rm.client_id!=1
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
ORDER BY rm.client_id ASC
); 




/*------------------- MUESTRAS ALMACAFE SANTA MARTA -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    NULL AS COD_EXPORTADOR_SISCAFE,
    NULL AS CODIGO_EXPORTADOR,
    NULL AS NOMBRE_EXPORTADOR,
    NULL as LOTE,
    SUM(rc.quantity_radicated_bag_in) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    @FECHA_SUP AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    b_rc.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "860010973" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    concat("MUESTRAS ALMACAFE SANTA MARTA (", ss.name_place,")") AS DESC_CONSULTA,
    concat(pds.descripcion," SANTA MARTA (", ss.name_place,")")  AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*SUM(rc.quantity_radicated_bag_in) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rc
inner join departaments dp on dp.id=rc.jetty
inner join slot_store ss on ss.id=rc.slot_store_id
inner join type_units tu on rc.type_units_id=tu.id
inner join bill b_rc on rc.bill_id=b_rc.id
inner join clients AS cts on cts.id=rc.client_id
inner join service_package sp on sp.id=0006
inner join service_package_has_items_services spi on spi.service_package_id=sp.id
inner join items_unoee pds on pds.codigo='0000047'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000047','003')
where rc.vehicle_plate !='FIQUE' and rc.bill_id!=2 and rc.bill_id!=3 AND ss.ref_departament=3  and tu.id!=9 
        and (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
group by ss.ref_departament
); 

