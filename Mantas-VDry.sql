



SET @FECHA_INF=DATE("2021-03-31");
SET @FECHA_SUP=DATE("2021-04-11");
truncate data_billing_full;
-- truncate temp_log_data_unoerp;


/*------------------- MANTAS V-DRY TERMINAL DE SPB ISO 20  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    NULL AS CODIGO_EXPORTADOR,
    NULL AS NOMBRE_EXPORTADOR,
    vp.lotes AS LOTE,
    manta_termica AS CANTIDAD_OPERATIVA,
    vp.packaging_type AS TIPO_SACOS,
    vp.packaging_mode AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.packaging_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.oie AS OIE,
    vp.bic_ctn AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.estado_embalaje AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "800089188" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    "INSTALACION DE MANTAS V-DRY TERMINAL DE SPB" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*manta_termica AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging AS vp 
INNER JOIN clients AS cts ON cts.exporter_code = vp.expotador_code
inner join items_unoee pds on pds.codigo='0000300'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000300','001')
WHERE    (  DATE(vp.packaging_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
        AND vp.jetty = 'BUN' AND (vp.estado_embalaje="EMBALAJE COMPLETADO" or vp.estado_embalaje="EMBALAJE VACIADO")   AND vp.manta_termica != 0
ORDER BY vp.expotador_code ASC

); 




/*------------------- MANTAS V-DRY TERMINAL DE SPB ISO 40  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    NULL AS CODIGO_EXPORTADOR,
    NULL AS NOMBRE_EXPORTADOR,
    vp.lotes AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.packaging_type AS TIPO_SACOS,
    vp.packaging_mode AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.packaging_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.oie AS OIE,
    vp.bic_ctn AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.estado_embalaje AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "800089188" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    "INSTALACION DE MANTAS V-DRY TERMINAL DE SPB DE ISO 40" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*vp.mantas_vdry_40 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging AS vp 
INNER JOIN clients AS cts ON cts.exporter_code = vp.expotador_code
inner join items_unoee pds on pds.codigo='0000300'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000300','001')
WHERE    (  DATE(vp.packaging_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
        AND vp.jetty = 'BUN' AND (vp.estado_embalaje="EMBALAJE COMPLETADO" or vp.estado_embalaje="EMBALAJE VACIADO")  AND vp.mantas_vdry_40 != 0
ORDER BY vp.expotador_code ASC

); 


/*------------------- MANTAS V-DRY TERMINAL DE CARTAGENA ISO 20  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    NULL AS CODIGO_EXPORTADOR,
    NULL AS NOMBRE_EXPORTADOR,
    vp.LOTES AS LOTE,
    adictional_num_manta AS CANTIDAD_OPERATIVA,
    vp.TIPO_EMBALAJE AS TIPO_SACOS,
    vp.MODALIDAD AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.FECHA_REGISTROS AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.OIE AS OIE, 
    vp.BIC_CTN AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    null AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.ESTADO AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "800089188" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    "INSTALACION DE MANTAS V-DRY TERMINAL DE CARTAGENA" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*adictional_num_manta AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging2 AS vp
INNER JOIN info_navy AS ifn ON ifn.id = vp.ID_PROFORMA 
INNER JOIN clients AS cts ON cts.business_name = vp.EXPORTADOR
inner join items_unoee pds on pds.codigo='0000300'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000300','002')
WHERE   (vp.PUERTO = 'SPRC' OR vp.PUERTO like '%CONTECA%' OR vp.PUERTO = 'COMPAS' OR vp.PUERTO LIKE '%BLOC%' OR vp.PUERTO = 'CTG') 
        AND ifn.mantas_vdry_40!=0 AND (vp.STATUS_ID=3 or vp.STATUS_ID=4) 
        AND (  DATE(vp.FECHA_REGISTROS) BETWEEN @FECHA_INF AND @FECHA_SUP  )
ORDER BY vp.EXPORTADOR ASC
); 



/*------------------- MANTAS V-DRY TERMINAL DE CARTAGENA ISO 40  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    NULL AS CODIGO_EXPORTADOR,
    NULL AS NOMBRE_EXPORTADOR,
    vp.LOTES AS LOTE,
    adictional_num_manta AS CANTIDAD_OPERATIVA,
    vp.TIPO_EMBALAJE AS TIPO_SACOS,
    vp.MODALIDAD AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.FECHA_REGISTROS AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.OIE AS OIE, 
    vp.BIC_CTN AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    null AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.ESTADO AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "800089188" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    "INSTALACION DE MANTAS V-DRY TERMINAL DE CARTAGENA" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*adictional_num_manta AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging2 AS vp
INNER JOIN info_navy AS ifn ON ifn.id = vp.ID_PROFORMA 
INNER JOIN clients AS cts ON cts.business_name = vp.EXPORTADOR
inner join items_unoee pds on pds.codigo='0000300'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000300','002')
WHERE   (vp.PUERTO = 'SPRC' OR vp.PUERTO like '%CONTECA%' OR vp.PUERTO = 'COMPAS' OR vp.PUERTO LIKE '%BLOC%' OR vp.PUERTO = 'CTG') 
        AND ifn.adictional_num_manta!=0 AND (vp.STATUS_ID=3 or vp.STATUS_ID=4) 
        AND (  DATE(vp.FECHA_REGISTROS) BETWEEN @FECHA_INF AND @FECHA_SUP  )
ORDER BY vp.EXPORTADOR ASC
); 



/*------------------- MANTAS V-DRY TERMINAL DE SPIA  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    NULL AS CODIGO_EXPORTADOR,
    NULL AS NOMBRE_EXPORTADOR,
    vp.LOTES AS LOTE,
    adictional_num_manta AS CANTIDAD_OPERATIVA,
    vp.TIPO_EMBALAJE AS TIPO_SACOS,
    vp.MODALIDAD AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.FECHA_REGISTROS AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.OIE AS OIE, 
    vp.BIC_CTN AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    null AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.ESTADO AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "800089188" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    "INSTALACION DE MANTAS V-DRY TERMINAL DE SPIA" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*adictional_num_manta AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging2 AS vp
INNER JOIN info_navy AS ifn ON ifn.id = vp.ID_PROFORMA 
INNER JOIN clients AS cts ON cts.business_name = vp.EXPORTADOR
inner join items_unoee pds on pds.codigo='0000300'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000300','001')
WHERE   vp.PUERTO = 'SPIA' AND  ifn.adictional_num_manta!=0 AND (vp.STATUS_ID=3 or vp.STATUS_ID=4) 
        AND (  DATE(vp.FECHA_REGISTROS) BETWEEN @FECHA_INF AND @FECHA_SUP  )
ORDER BY vp.EXPORTADOR ASC
); 




