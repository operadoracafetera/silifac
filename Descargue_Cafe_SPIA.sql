
truncate data_billing_full;

SET @FECHA_INF=DATE("2021-03-08");
SET @FECHA_SUP=DATE("2021-03-14");



/*------------------- VEHICULO DESCARPADO EN TERMINAL SPIA  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    group_concat(rc.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rc.lot_caffee,' x ',rc.quantity_radicated_bag_in separator ' | ') AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    DATE(rc.created_date) AS FECHA_OPERACION,
    uc.name_unit AS UNIDAD_CAFE,
    b_rm.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "835000149" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,    
    "VEHICULO DESCARPADO EN TERMINAL SPIA" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM remittances_caffee AS rc
INNER JOIN clients AS cts ON cts.id=rc.client_id
INNER JOIN units_caffee AS uc ON rc.units_cafee_id=uc.id
inner join bill b_rm on rc.bill_id=b_rm.id
INNER JOIN slot_store AS ss ON ss.id=rc.slot_store_id
INNER JOIN departaments AS dep ON ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000192'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000192','006')
WHERE ss.ref_departament=8  AND (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP ) 
GROUP BY rc.vehicle_plate,rc.client_id,date(rc.created_date)
ORDER BY cts.exporter_code ASC
); 


/*------------------- DESCARGUE EN TERMINAL SPIA MICROLOTES MAYOR IGUAL A 5 LOTES -------------------*/
INSERT INTO data_billing_full (
select 
    NULL AS ID,
    group_concat(rc.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rc.lot_caffee,' x ',rc.quantity_radicated_bag_in separator ' | ') AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    DATE(rc.created_date) AS FECHA_OPERACION,
    group_concat(distinct(uc.name_unit) separator ' | ') AS UNIDAD_CAFE,
    group_concat(distinct(b_rm.name) separator ' | ') AS UNIDAD_FACTURA,
    count(rc.id) AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "835000149" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,     
    "DESCARGUE DE MICROLOTES EN TERMINAL SPIA MAYOR IGUAL A 5 LOTES" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM remittances_caffee AS rc
INNER JOIN clients AS cts ON cts.id=rc.client_id
INNER JOIN units_caffee AS uc ON rc.units_cafee_id=uc.id
inner join bill AS b_rm on rc.bill_id=b_rm.id
INNER JOIN slot_store AS ss ON ss.id=rc.slot_store_id
INNER JOIN departaments AS dep ON ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='  '
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000114','001')
where rc.microlot=1 and ss.ref_departament=8 
     AND (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP )    
group by rc.vehicle_plate, date(rc.created_date) , rc.client_id, ss.ref_departament
having count(rc.id) >= 5
ORDER BY cts.exporter_code ASC
); 



/*------------------- DESCARGUE SPIA MENOR A 100 SACOS CAFE  -------------------*/
INSERT INTO data_billing_full (
select 
    NULL AS ID,
    group_concat(rc.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rc.lot_caffee,' x ',rc.quantity_radicated_bag_in separator ' | ') AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    date(rc.created_date) AS FECHA_OPERACION,
    group_concat(distinct(uc.name_unit) separator ' | ')  AS UNIDAD_CAFE,
    b_id.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "835000149" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    "DESCARGUE EN TERMINAL SPIA MENOR A 100 SACOS CAFE" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee AS rc
inner join clients AS cts on cts.id = rc.client_id
INNER JOIN bill AS b_id ON b_id.id = rc.bill_id
inner join slot_store AS ss on ss.id=rc.slot_store_id
inner join units_caffee AS uc on rc.units_cafee_id=uc.id
inner join departaments AS dep on dep.id=ss.ref_departament
inner join items_unoee pds on pds.codigo='0000270'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000270','001')
where (ss.ref_departament=8 ) AND rc.microlot=0 AND rc.bill_id=1
    AND (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP )
group by rc.vehicle_plate,rc.client_id,date(rc.created_date), ss.ref_departament
having sum(rc.quantity_radicated_bag_in) <=100
    ORDER BY cts.exporter_code ASC
); 


/*------------------- DESCARGUE SPIA MENOR A 250 y MAYOR A 101 SACOS CAFE  -------------------*/
INSERT INTO data_billing_full (
select 
    NULL AS ID,
    group_concat(rc.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rc.lot_caffee,' x ',rc.quantity_radicated_bag_in separator ' | ') AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    date(rc.created_date) AS FECHA_OPERACION,
    group_concat(distinct(uc.name_unit) separator ' | ')  AS UNIDAD_CAFE,
    b_id.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "835000149" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    "DESCARGUE EN TERMINAL SPIA DE 101 A 250 SACOS CAFE" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee AS rc
inner join clients cts on cts.id = rc.client_id
INNER JOIN bill AS b_id ON b_id.id = rc.bill_id
inner join slot_store ss on ss.id=rc.slot_store_id
inner join units_caffee AS uc on rc.units_cafee_id=uc.id
inner join departaments dep on dep.id=ss.ref_departament
inner join items_unoee pds on pds.codigo='0000287'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000287','001')
where (ss.ref_departament=8 ) AND rc.microlot=0 AND rc.bill_id=1
    AND (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP )
group by rc.vehicle_plate,rc.client_id,date(rc.created_date), ss.ref_departament
having sum(rc.quantity_radicated_bag_in) BETWEEN 101 AND 250
    ORDER BY cts.exporter_code ASC
); 



/*------------------- DESCARGUE SPIA MAYOR DE 250 SACOS DE 1-25 KG SACOS CAFE  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rm.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( cts.exporter_code ,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in ) AS LOTE,
    rm.quantity_radicated_bag_in AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    DATE(rm.created_date) AS FECHA_OPERACION,
    uc.name_unit AS UNIDAD_CAFE,
    b_rm.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "835000149" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    "DESCARGUE EN TERMINAL SPIA MAYOR DE 250 SACOS DE 1-25 KG SACOS CAFE" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*rm.quantity_radicated_bag_in AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee AS rm
inner join clients AS cts on cts.id=rm.client_id
inner join units_caffee AS uc on rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join slot_store AS ss on ss.id=rm.slot_store_id
inner join departaments AS dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000222'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000222','001')
where ss.ref_departament=8 and rm.microlot=0 AND uc.quantity <= 25
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
        and (client_id,vehicle_plate,date(rm.created_date),ss.ref_departament) 
        in (
            select client_id,vehicle_plate,date(rc.created_date),ss.ref_departament
            from remittances_caffee rc
            inner join slot_store ss on ss.id=rc.slot_store_id
            where ss.ref_departament=8 and rc.microlot=0  AND (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP )
            group by rc.vehicle_plate,rc.client_id,date(rc.created_date),ss.ref_departament
            having sum(rc.quantity_radicated_bag_in) >250
        )
    ORDER BY cts.exporter_code ASC
); 


/*------------------- DESCARGUE SPIA MAYOR DE 250 SACOS DE 30 KG SACOS CAFE  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rm.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( cts.exporter_code ,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in ) AS LOTE,
    rm.quantity_radicated_bag_in AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    DATE(rm.created_date) AS FECHA_OPERACION,
    uc.name_unit AS UNIDAD_CAFE,
    b_rm.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "835000149" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    "DESCARGUE EN TERMINAL SPIA MAYOR DE 250 SACOS DE 30 KG SACOS CAFE" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*rm.quantity_radicated_bag_in AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee AS rm
inner join clients AS cts on cts.id=rm.client_id
inner join units_caffee AS uc on rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join slot_store AS ss on ss.id=rm.slot_store_id
inner join departaments AS dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000223'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000223','001')
where ss.ref_departament=8 and rm.microlot=0 AND uc.quantity = 30
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
        and (client_id,vehicle_plate,date(rm.created_date),ss.ref_departament) 
        in (
            select client_id,vehicle_plate,date(rc.created_date),ss.ref_departament
            from remittances_caffee rc
            inner join slot_store ss on ss.id=rc.slot_store_id
            where ss.ref_departament=8 and rc.microlot=0  AND (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP )
            group by rc.vehicle_plate,rc.client_id,date(rc.created_date),ss.ref_departament
            having sum(rc.quantity_radicated_bag_in) >250
        )
    ORDER BY cts.exporter_code ASC
); 

 
/*------------------- DESCARGUE SPIA MAYOR DE 250 SACOS DE 35 KG SACOS CAFE  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rm.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( cts.exporter_code ,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in ) AS LOTE,
    rm.quantity_radicated_bag_in AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    DATE(rm.created_date) AS FECHA_OPERACION,
    uc.name_unit AS UNIDAD_CAFE,
    b_rm.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "835000149" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    "DESCARGUE EN TERMINAL SPIA MAYOR DE 250 SACOS DE 35 KG SACOS CAFE" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*rm.quantity_radicated_bag_in AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee AS rm
inner join clients AS cts on cts.id=rm.client_id
inner join units_caffee AS uc on rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join slot_store AS ss on ss.id=rm.slot_store_id
inner join departaments AS dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000224'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000224','001')
where ss.ref_departament=8 and rm.microlot=0 AND uc.quantity = 35
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
        and (client_id,vehicle_plate,date(rm.created_date),ss.ref_departament) 
        in (
            select client_id,vehicle_plate,date(rc.created_date),ss.ref_departament
            from remittances_caffee rc
            inner join slot_store ss on ss.id=rc.slot_store_id
            where ss.ref_departament=8 and rc.microlot=0  AND (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP )
            group by rc.vehicle_plate,rc.client_id,date(rc.created_date),ss.ref_departament
            having sum(rc.quantity_radicated_bag_in) >250
        )
    ORDER BY cts.exporter_code ASC
); 


/*------------------- DESCARGUE SPIA MAYOR DE 250 SACOS DE 40 - 70 KG SACOS CAFE  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rm.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( cts.exporter_code ,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in ) AS LOTE,
    rm.quantity_radicated_bag_in AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    DATE(rm.created_date) AS FECHA_OPERACION,
    uc.name_unit AS UNIDAD_CAFE,
    b_rm.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "835000149" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    "DESCARGUE EN TERMINAL SPIA MAYOR DE 250 SACOS DE 40-70 KG SACOS CAFE" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*rm.quantity_radicated_bag_in AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee AS rm
inner join clients AS cts on cts.id=rm.client_id
inner join units_caffee AS uc on rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join slot_store AS ss on ss.id=rm.slot_store_id
inner join departaments AS dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000111'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000111','001')
where ss.ref_departament=8 and rm.microlot=0 AND ( uc.quantity between 40 and 70 )
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
        and (client_id,vehicle_plate,date(rm.created_date),ss.ref_departament) 
        in (
            select client_id,vehicle_plate,date(rc.created_date),ss.ref_departament
            from remittances_caffee rc
            inner join slot_store ss on ss.id=rc.slot_store_id
            where ss.ref_departament=8 and rc.microlot=0  AND (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP )
            group by rc.vehicle_plate,rc.client_id,date(rc.created_date),ss.ref_departament
            having sum(rc.quantity_radicated_bag_in) >250
        )
    ORDER BY cts.exporter_code ASC
); 


/*------------------- DESCARGUE SPIA SACOS CACAO  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rm.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,  
    concat( cts.exporter_code ,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in ) AS LOTE,
    rm.quantity_radicated_bag_in AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    DATE(rm.created_date) AS FECHA_OPERACION,
    uc.name_unit AS UNIDAD_CAFE,
    b_rm.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "835000149" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    "DESCARGUE DE PARTICULARES EN TERMINAL DE SPIA TIPO SACOS CACAO" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*rm.quantity_radicated_bag_in AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join bill b_rm on rm.bill_id=b_rm.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000049'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000049','006')
where ss.ref_departament=8 AND rm.microlot=0 and rm.client_id !=1 AND rm.bill_id=3 
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )        
    ORDER BY cts.exporter_code ASC
); 


/*------------------- DESCARGUE SPIA SACOS BIG BAGS  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rm.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,  
    concat( cts.exporter_code ,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in ) AS LOTE,
    rm.quantity_radicated_bag_in AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    DATE(rm.created_date) AS FECHA_OPERACION,
    uc.name_unit AS UNIDAD_CAFE,
    b_rm.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "835000149" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    "DESCARGUE DE PARTICULARES EN TERMINAL DE SPIA TIPO BIG BAGS" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*rm.quantity_radicated_bag_in  AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join bill b_rm on rm.bill_id=b_rm.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000184'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000184','001')
where ss.ref_departament=8 AND rm.microlot=0 and rm.client_id !=1 AND rm.bill_id=4 
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
    ORDER BY cts.exporter_code ASC
);  



/*------------------- DESCARGUE SPIA CAJAS DE 1 A 10  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,  
    group_concat(cts.exporter_code ,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    DATE(rm.created_date) AS FECHA_OPERACION,
    group_concat(distinct(uc.name_unit) separator ' | ') AS UNIDAD_CAFE,
    group_concat(distinct(b_rm.name) separator ' | ') AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "835000149" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    "DESCARGUE DE PARTICULARES EN TERMINAL DE SPIA TIPO CAJAS DE 1 A 10 UNIDADES" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join bill b_rm on rm.bill_id=b_rm.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000032'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000032','006')
where ss.ref_departament=8 AND rm.microlot=0 AND rm.client_id !=1 AND rm.bill_id=2 AND rm.vehicle_plate!= 'FIQUE' 
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by rm.vehicle_plate,rm.client_id,date(rm.created_date),ss.ref_departament
having sum(rm.quantity_radicated_bag_in) <= 10 
ORDER BY cts.exporter_code ASC
); 


/*------------------- DESCARGUE SPIA CAJAS DE 11 A 100  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,  
    group_concat(cts.exporter_code ,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    DATE(rm.created_date) AS FECHA_OPERACION,
    group_concat(distinct(uc.name_unit) separator ' | ') AS UNIDAD_CAFE,
    group_concat(distinct(b_rm.name) separator ' | ') AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "835000149" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    "DESCARGUE DE PARTICULARES EN TERMINAL DE SPIA TIPO CAJAS DE 11 A 100 UNIDADES" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join bill b_rm on rm.bill_id=b_rm.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000203'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000203','006')
where ss.ref_departament=8 AND rm.microlot=0 and rm.client_id !=1 AND rm.bill_id=2
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by rm.vehicle_plate,rm.client_id,date(rm.created_date),ss.ref_departament 
having sum(rm.quantity_radicated_bag_in) between 11 and 100      
ORDER BY cts.exporter_code ASC
); 


/*------------------- DESCARGUE SPIA CAJAS MAYOR DE 100  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,  
    group_concat(cts.exporter_code ,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    sum(rm.quantity_radicated_bag_in) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    DATE(rm.created_date) AS FECHA_OPERACION,
    group_concat(distinct(uc.name_unit) separator ' | ') AS UNIDAD_CAFE,
    group_concat(distinct(b_rm.name) separator ' | ') AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "835000149" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    "DESCARGUE DE PARTICULARES EN TERMINAL DE SPIA TIPO CAJAS MAYOR A 100 UNIDADES" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*sum(rm.quantity_radicated_bag_in) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join bill b_rm on rm.bill_id=b_rm.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000215'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000215','006')
where ss.ref_departament=8 and rm.microlot=0 and rm.client_id !=1 AND rm.bill_id=2 
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by rm.vehicle_plate,rm.client_id,date(rm.created_date),ss.ref_departament 
having sum(rm.quantity_radicated_bag_in) > 100
ORDER BY cts.exporter_code ASC
); 


/*------------------- DESCARGUE SPIA CAFE VERDE ESTIBAS  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rm.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,  
    concat( cts.exporter_code,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in ) AS LOTE,
    rm.quantity_radicated_bag_in AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    DATE(rm.created_date) AS FECHA_OPERACION,
    uc.name_unit AS UNIDAD_CAFE,
    b_rm.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "835000149" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    "DESCARGUE DE PARTICULARES EN TERMINAL DE SPIA TIPO CAFE VERDE ESTIBAS" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join bill b_rm on rm.bill_id=b_rm.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo="0000295"
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=("0000295",'006')
where ss.ref_departament=8 and rm.microlot=0 and rm.client_id !=1 AND rm.bill_id=18
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
ORDER BY cts.exporter_code ASC
); 



/*------------------- DEVOLUCIONES DE CAFE EN TERMINAL SPIA MAYOR A 250  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    group_concat(rcrc.remittances_caffee_id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,  
    group_concat(cts.exporter_code,'-',rm.lot_caffee,' x ' ,rcrc.qta_bags separator ' | ') as LOTE,
    sum(rcrc.qta_bags) AS CANTIDAD_OPERATIVA,
    rc.return_type AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    rc.return_date  AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    rc.state_return AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "835000149" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    "DEVOLUCIONES EN TERMINAL SPIA TIPO CAFE MAYOR A 250" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*sum(rcrc.qta_bags) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from returns_caffees rc
inner join remittances_caffee_returns_caffees rcrc on rcrc.returns_caffee_id=rc.id
inner join clients AS cts on cts.id=rc.client_id
inner join remittances_caffee as rm on rcrc.remittances_caffee_id = rm.id
inner join departaments dep on rc.jetty = dep.id
inner join items_unoee pds on pds.codigo='0000277'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000277','001')
where rc.jetty=8 and rc.state_return="COMPLETADA" AND rc.return_type = 'CAFE' 
        and (  DATE(rc.return_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
group by rc.vehicle_plate,cts.exporter_code,date(rc.return_date), rc.jetty
having sum(rcrc.qta_bags) >250
ORDER BY exporter_code ASC
); 


/*------------------- DEVOLUCIONES DE CAFE EN TERMINAL SPIA MENOR A 250  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    group_concat(rcrc.remittances_caffee_id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,  
    group_concat(cts.exporter_code,'-',rm.lot_caffee,' x ' ,rcrc.qta_bags separator ' | ') as LOTE,
    1 AS CANTIDAD_OPERATIVA,
    rc.return_type AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    rc.return_date  AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    rc.state_return AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "835000149" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    "DEVOLUCIONES EN TERMINAL SPIA TIPO CAFE MENOR A 250" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from returns_caffees rc
inner join remittances_caffee_returns_caffees rcrc on rcrc.returns_caffee_id=rc.id
inner join clients AS cts on cts.id=rc.client_id
inner join remittances_caffee as rm on rcrc.remittances_caffee_id = rm.id
inner join departaments dep on rc.jetty = dep.id
inner join items_unoee pds on pds.codigo='0000001'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000001','001')
where rc.jetty=8 and rc.state_return="COMPLETADA" AND rc.return_type = 'CAFE' 
        and (  DATE(rc.return_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
group by rc.vehicle_plate,cts.exporter_code,date(rc.return_date), rc.jetty
having sum(rcrc.qta_bags) <=250
ORDER BY exporter_code ASC
); 


/*------------------- SOLICITUD TOMA DE MUESTRA GRAIN PRO  SPIA  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rm.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,  
    concat(cts.exporter_code,'-',rm.lot_caffee,' x ',rm.quantity_radicated_bag_in) AS LOTE,
    rm.quantity_radicated_bag_in  AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rm.vehicle_plate AS PLACA_VEHICULO,
    date(rm.created_date) AS FECHA_OPERACION,
    uc.name_unit AS UNIDAD_CAFE,
    tu.name AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "835000149" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    "SOLICITUD DE MUESTRA B. HERMETICAS COMPLETADAS EN EL TERMINAL DE SPIA" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*rm.quantity_radicated_bag_in AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM remittances_caffee rm
inner join clients cts on cts.id=rm.client_id
inner join type_units tu on tu.id=rm.type_units_id
inner join units_caffee uc on rm.units_cafee_id=uc.id
inner join slot_store ss on ss.id=rm.slot_store_id
inner join items_unoee pds on pds.codigo="0000009"
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=("0000009",'001')
where rm.jetty=8 and tu.id=4 
        AND (  DATE(rm.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
ORDER BY rm.client_id ASC
); 

