
/*------------------- SOLICITUD MUESTRA MITSUI SPB, SPIA Y TCBUEN  -------------------*/

truncate data_billing_full;

SET @FECHA_INF=DATE("2020-10-11");
SET @FECHA_SUP=DATE("2020-10-31");



/*------------------- SOLICITUD MUESTRA NESTLE STM  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cs.lot_coffee_ref,' x ', cs.qta_bag separator ' | ') AS LOTE,
    sum(cs.qta_bag) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    date(cs.end_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    cs.status AS ESTADO,
    group_concat(cs.id separator ' | ') AS MUESTRA,
    group_concat(distinct(cs.supply_company) separator ' | ')  AS EMPRESA_SUMINISTRO,
    group_concat(distinct(cs.sample_packing) separator ' | ') AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    tsc.name AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "860002130" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    concat("SOLICITUD DE MUESTRA NESTLE COMPLETADA EN PUERTO DE SANTA MARTA TERMINAL ",cs.terminal) AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    sum(lp.precio*cs.qta_bag) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM coffee_sample AS cs
INNER JOIN sample_coffee_has_type_sample AS sct ON sct.sample_coffee_id = cs.id
INNER JOIN type_sample_coffe AS tsc ON tsc.id = sct.type_sample_coffee_id
INNER JOIN clients AS cts ON cts.id = cs.client_id
inner join items_unoee pds on pds.codigo='0000289'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000289','003')
WHERE cs.status = 'COMPLETADA' AND cs.get_sample = 1 AND tsc.id=19 
        AND (cs.terminal LIKE '%STM%'  OR cs.terminal like'%STA%' OR cs.terminal like'%SMITCO%')  
        AND (  DATE(cs.end_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
GROUP BY cts.exporter_code ,date(cs.end_date), tsc.name, cs.terminal
ORDER BY cs.client_id ASC
); 




/*------------------- SOLICITUD MUESTRA NESTLE SPB, SPIA Y TCBUEN  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cs.lot_coffee_ref,' x ', cs.qta_bag separator ' | ') AS LOTE,
    sum(cs.qta_bag) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    date(cs.end_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    cs.status AS ESTADO,
    group_concat(cs.id separator ' | ') AS MUESTRA,
    group_concat(distinct(cs.supply_company) separator ' | ')  AS EMPRESA_SUMINISTRO,
    group_concat(distinct(cs.sample_packing) separator ' | ') AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    tsc.name AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "860002130" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    concat("SOLICITUD DE MUESTRA NESTLE COMPLETADA EN PUERTO DE BUENAVENTURA TERMINAL ",IF(cs.terminal LIKE "%SPB%", "SPB",cs.terminal) ) AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    sum(lp.precio*cs.qta_bag) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM coffee_sample AS cs
INNER JOIN sample_coffee_has_type_sample AS sct ON sct.sample_coffee_id = cs.id
INNER JOIN type_sample_coffe AS tsc ON tsc.id = sct.type_sample_coffee_id
INNER JOIN clients AS cts ON cts.id = cs.client_id
inner join items_unoee pds on pds.codigo='0000289'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000289','001')
WHERE cs.status = 'COMPLETADA' AND cs.get_sample = 1 AND tsc.id=19 
        AND (cs.terminal LIKE '%SPB%'  OR cs.terminal like'%TCBUEN%' OR cs.terminal like'%SPIA%')  
        AND (  DATE(cs.end_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
GROUP BY cts.exporter_code ,date(cs.end_date), tsc.name, IF(cs.terminal LIKE "%SPB%", "SPB",cs.terminal)
ORDER BY cs.client_id ASC
); 




/*------------------- SOLICITUD MUESTRA NESTLE CTG  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cs.lot_coffee_ref,' x ', cs.qta_bag separator ' | ') AS LOTE,
    sum(cs.qta_bag) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    date(cs.end_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    cs.status AS ESTADO,
    group_concat(cs.id separator ' | ') AS MUESTRA,
    group_concat(distinct(cs.supply_company) separator ' | ')  AS EMPRESA_SUMINISTRO,
    group_concat(distinct(cs.sample_packing) separator ' | ') AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    tsc.name AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,    
    "860002130" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    concat("SOLICITUD DE MUESTRA NESTLE COMPLETADA EN PUERTO DE CARTAGENA TERMINAL ",cs.terminal) AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    sum(lp.precio*cs.qta_bag) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM coffee_sample AS cs
INNER JOIN sample_coffee_has_type_sample AS sct ON sct.sample_coffee_id = cs.id
INNER JOIN type_sample_coffe AS tsc ON tsc.id = sct.type_sample_coffee_id
INNER JOIN clients AS cts ON cts.id = cs.client_id
inner join items_unoee pds on pds.codigo='0000289'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000289','002')
WHERE cs.status = 'COMPLETADA' AND cs.get_sample = 1 AND tsc.id=19 
        AND (cs.terminal LIKE '%CTG%'  OR cs.terminal like'%SPRC%' OR cs.terminal like'%CONTECA%' OR cs.terminal like'%BLOC%' OR cs.terminal like'%COMPAS%')  
        AND (  DATE(cs.end_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
GROUP BY cts.exporter_code ,date(cs.end_date), tsc.name, cs.terminal
ORDER BY cs.client_id ASC
); 
