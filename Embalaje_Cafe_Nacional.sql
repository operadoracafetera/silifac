
/*------------------- SET @DIAS_DE_FACTURACION=5; -------------------*/

/*------------------- SET @FECHA_INF=DATE(DATE_SUB(NOW(), INTERVAL @DIAS_DE_FACTURACION DAY)); -------------------*/
/*------------------- SET @FECHA_SUP=DATE(DATE_SUB(NOW(), INTERVAL 1 DAY)); -------------------*/
SET @FECHA_INF=DATE("2020-10-01");
SET @FECHA_SUP=DATE("2020-10-08");
SET @TRM=3859; 
 


/*------------------- EMBALAJE SPB CONTENEDORES DE 20  -------------------*/
/*------------------- EMBALAJE SPB CONTENEDORES TIPO SACOS DE 20 CON PAPEL KRAFT -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    vp.expotador_code AS CODIGO_EXPORTADOR,
    vp.exportador AS NOMBRE_EXPORTADOR,
    vp.lotes AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.packaging_type AS TIPO_SACOS,
    vp.packaging_mode AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.packaging_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.oie AS OIE, 
    vp.bic_ctn AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    vp.tipo_papel AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.estado_embalaje AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "800215775" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "EMBALAJE DE CONTENEDORES EN TERMINAL SPB TIPO SACOS DE LONGITUD 20 PAPEL TIPO KRAFT" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE, 
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging AS vp 
INNER JOIN clients AS cts ON cts.exporter_code = vp.expotador_code
inner join items_unoee pds on pds.codigo='0000160'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000160','001')
WHERE   (  DATE(vp.packaging_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
        AND vp.jetty = 'bun' AND vp.packaging_mode = 'SACOS' 
        AND (vp.state_packaging_id=3 or vp.state_packaging_id=9 or vp.state_packaging_id=10) 
        AND (vp.iso_ctn LIKE '2%' ) 
        AND vp.tipo_papel = "KRAFT"
ORDER BY vp.expotador_code ASC
); 


/*------------------- EMBALAJE SPB CONTENEDORES TIPO SACOS DE 20 CON PAPEL CARTON -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    vp.expotador_code AS CODIGO_EXPORTADOR,
    vp.exportador AS NOMBRE_EXPORTADOR,
    vp.lotes AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.packaging_type AS TIPO_SACOS,
    vp.packaging_mode AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.packaging_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.oie AS OIE, 
    vp.bic_ctn AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    vp.tipo_papel AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.estado_embalaje AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "800215775" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "EMBALAJE DE CONTENEDORES EN TERMINAL SPB TIPO SACOS DE LONGITUD 20 PAPEL TIPO CARTON" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging AS vp 
INNER JOIN clients AS cts ON cts.exporter_code = vp.expotador_code
inner join items_unoee pds on pds.codigo='0000162'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000162','001')
WHERE   (  DATE(vp.packaging_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
        AND vp.jetty = 'bun' AND vp.packaging_mode = 'SACOS' 
        AND (vp.state_packaging_id=3 or vp.state_packaging_id=9 or vp.state_packaging_id=10) 
        AND (vp.iso_ctn LIKE '2%' ) 
        AND vp.tipo_papel = "CARTON"
ORDER BY vp.expotador_code ASC
); 


/*------------------- EMBALAJE SPB CONTENEDORES TIPO GRANEL DE 20 TIPO KRAFT  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    vp.expotador_code AS CODIGO_EXPORTADOR,
    vp.exportador AS NOMBRE_EXPORTADOR,
    vp.lotes AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.packaging_type AS TIPO_SACOS,
    vp.packaging_mode AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.packaging_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.oie AS OIE,
    vp.bic_ctn AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    vp.tipo_papel AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.estado_embalaje AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "800215775" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "EMBALAJE DE CONTENEDORES EN TERMINAL SPB TIPO GRANEL DE LONGITUD 20 TIPO PAPEL KRAFT" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging AS vp 
INNER JOIN clients AS cts ON cts.exporter_code = vp.expotador_code
inner join items_unoee pds on pds.codigo='0000168'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000168','001')
WHERE   (  DATE(vp.packaging_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
        AND vp.jetty = 'bun' AND vp.packaging_mode = 'granel' 
        AND (vp.state_packaging_id=3 or vp.state_packaging_id=9 or vp.state_packaging_id=10) 
        AND (vp.iso_ctn LIKE '2%' ) 
        AND vp.tipo_papel = "KRAFT"
ORDER BY vp.expotador_code ASC
); 


/*------------------- EMBALAJE SPB CONTENEDORES TIPO GRANEL DE 20 TIPO CARTON  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    vp.expotador_code AS CODIGO_EXPORTADOR,
    vp.exportador AS NOMBRE_EXPORTADOR,
    vp.lotes AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.packaging_type AS TIPO_SACOS,
    vp.packaging_mode AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.packaging_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.oie AS OIE,
    vp.bic_ctn AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    vp.tipo_papel AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.estado_embalaje AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "800215775" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "EMBALAJE DE CONTENEDORES EN TERMINAL SPB TIPO GRANEL DE LONGITUD 20 TIPO PAPEL CARTON" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging AS vp 
INNER JOIN clients AS cts ON cts.exporter_code = vp.expotador_code
inner join items_unoee pds on pds.codigo='0000169'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000169','001')
WHERE   (  DATE(vp.packaging_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
        AND vp.jetty = 'bun' AND vp.packaging_mode = 'granel' 
        AND (vp.state_packaging_id=3 or vp.state_packaging_id=9 or vp.state_packaging_id=10) 
        AND (vp.iso_ctn LIKE '2%' ) 
        AND vp.tipo_papel = "CARTON"
ORDER BY vp.expotador_code ASC
); 


/*------------------- EMBALAJE SPB CONTENEDORES TIPO COMBINADOS SACOS - CAJAS DE 20 TIPO PAPEL KRAFT -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    vp.expotador_code AS CODIGO_EXPORTADOR,
    vp.exportador AS NOMBRE_EXPORTADOR,
    vp.lotes AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.packaging_type AS TIPO_SACOS,
    vp.packaging_mode AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.packaging_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.oie AS OIE,
    vp.bic_ctn AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    vp.tipo_papel AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.estado_embalaje AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "800215775" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "EMBALAJE DE CONTENEDORES EN TERMINAL SPB TIPO COMBINADO(SACOS - CAJAS) DE LONGITUD 20 TIPO PAPEL KRAFT" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging AS vp 
INNER JOIN clients AS cts ON cts.exporter_code = vp.expotador_code
inner join items_unoee pds on pds.codigo='0000291'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000291','001')
WHERE   (  DATE(vp.packaging_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
        AND vp.jetty = 'bun' AND vp.packaging_mode = 'COMBINADO(SACOS - CAJAS)' 
        AND (vp.state_packaging_id=3 or vp.state_packaging_id=9 or vp.state_packaging_id=10) 
        AND (vp.iso_ctn LIKE '2%' ) 
        AND vp.tipo_papel = "KRAFT"
ORDER BY vp.expotador_code ASC
); 

/*------------------- EMBALAJE SPB CONTENEDORES TIPO COMBINADOS SACOS - CAJAS DE 20 TIPO PAPEL CARTON -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    vp.expotador_code AS CODIGO_EXPORTADOR,
    vp.exportador AS NOMBRE_EXPORTADOR,
    vp.lotes AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.packaging_type AS TIPO_SACOS,
    vp.packaging_mode AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.packaging_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.oie AS OIE,
    vp.bic_ctn AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    vp.tipo_papel AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.estado_embalaje AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "800215775" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "EMBALAJE DE CONTENEDORES EN TERMINAL SPB TIPO COMBINADO(SACOS - CAJAS) DE LONGITUD 20 TIPO PAPEL CARTON" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging AS vp 
INNER JOIN clients AS cts ON cts.exporter_code = vp.expotador_code
inner join items_unoee pds on pds.codigo='0000292'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000292','001')
WHERE   (  DATE(vp.packaging_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
        AND vp.jetty = 'bun' AND vp.packaging_mode = 'COMBINADO(SACOS - CAJAS)' 
        AND (vp.state_packaging_id=3 or vp.state_packaging_id=9 or vp.state_packaging_id=10) 
        AND (vp.iso_ctn LIKE '2%' ) 
        AND vp.tipo_papel = "CARTON"
ORDER BY vp.expotador_code ASC
); 

/*------------------- EMBALAJE SPB CONTENEDORES TIPO CONBINADOS SACOS - BIG BGAS DE 20 TIPO PAPEL KRAFT -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    vp.expotador_code AS CODIGO_EXPORTADOR,
    vp.exportador AS NOMBRE_EXPORTADOR,
    vp.lotes AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.packaging_type AS TIPO_SACOS,
    vp.packaging_mode AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.packaging_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.oie AS OIE,
    vp.bic_ctn AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    vp.tipo_papel AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.estado_embalaje AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "800215775" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "EMBALAJE DE CONTENEDORES EN TERMINAL SPB TIPO COMBINADO(SACOS - BIG BAGS) DE LONGITUD 20 TIPO PAPEL KRAFT" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1  AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging AS vp 
INNER JOIN clients AS cts ON cts.exporter_code = vp.expotador_code
inner join items_unoee pds on pds.codigo='0000291'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000291','001')
WHERE   (  DATE(vp.packaging_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
        AND vp.jetty = 'bun' AND vp.packaging_mode = 'COMBINADO(SACOS - BIG BAGS)' 
        AND (vp.state_packaging_id=3 or vp.state_packaging_id=9 or vp.state_packaging_id=10) 
        AND (vp.iso_ctn LIKE '2%' ) 
        AND vp.tipo_papel = "KRAFT"
ORDER BY vp.expotador_code ASC
); 

/*------------------- EMBALAJE SPB CONTENEDORES TIPO CONBINADOS SACOS - BIG BGAS DE 20 TIPO PAPEL CARTON -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    vp.expotador_code AS CODIGO_EXPORTADOR,
    vp.exportador AS NOMBRE_EXPORTADOR,
    vp.lotes AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.packaging_type AS TIPO_SACOS,
    vp.packaging_mode AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.packaging_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.oie AS OIE,
    vp.bic_ctn AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    vp.tipo_papel AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.estado_embalaje AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "800215775" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "EMBALAJE DE CONTENEDORES EN TERMINAL SPB TIPO COMBINADO(SACOS - BIG BAGS) DE LONGITUD 20 TIPO PAPEL CARTON" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1  AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging AS vp 
INNER JOIN clients AS cts ON cts.exporter_code = vp.expotador_code
inner join items_unoee pds on pds.codigo='0000292'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000292','001')
WHERE   (  DATE(vp.packaging_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
        AND vp.jetty = 'bun' AND vp.packaging_mode = 'COMBINADO(SACOS - BIG BAGS)' 
        AND (vp.state_packaging_id=3 or vp.state_packaging_id=9 or vp.state_packaging_id=10) 
        AND (vp.iso_ctn LIKE '2%' ) 
        AND vp.tipo_papel = "CARTON"
ORDER BY vp.expotador_code ASC
); 



/*------------------- EMBALAJE SPB CONTENEDORES TIPO CAJAS DE 20  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    vp.expotador_code AS CODIGO_EXPORTADOR,
    vp.exportador AS NOMBRE_EXPORTADOR,
    vp.lotes AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.packaging_type AS TIPO_SACOS,
    vp.packaging_mode AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.packaging_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.oie AS OIE,
    vp.bic_ctn AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    vp.tipo_papel AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.estado_embalaje AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "800215775" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "EMBALAJE DE CONTENEDORES EN TERMINAL SPB TIPO CAJAS DE LONGITUD 20" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging AS vp 
INNER JOIN clients AS cts ON cts.exporter_code = vp.expotador_code
inner join items_unoee pds on pds.codigo='0000041'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000041','001')
WHERE   (  DATE(vp.packaging_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
        AND vp.jetty = 'bun' AND vp.packaging_mode = 'Cajas' AND (vp.state_packaging_id=3 or vp.state_packaging_id=9 or vp.state_packaging_id=10) AND (vp.iso_ctn LIKE '2%' ) 
ORDER BY vp.expotador_code ASC
);  

/*------------------- EMBALAJE SPB CONTENEDORES TIPO CACAO DE 20  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    vp.expotador_code AS CODIGO_EXPORTADOR,
    vp.exportador AS NOMBRE_EXPORTADOR,
    vp.lotes AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.packaging_type AS TIPO_SACOS,
    vp.packaging_mode AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.packaging_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.oie AS OIE,
    vp.bic_ctn AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    vp.tipo_papel AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.estado_embalaje AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "800215775" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "EMBALAJE DE CONTENEDORES EN TERMINAL SPB TIPO CACAO DE LONGITUD 20" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging AS vp 
INNER JOIN clients AS cts ON cts.exporter_code = vp.expotador_code
inner join items_unoee pds on pds.codigo='0000050'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000050','001')
WHERE   (  DATE(vp.packaging_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
        AND vp.jetty = 'bun' AND vp.packaging_mode = 'CACAO' AND (vp.state_packaging_id=3 or vp.state_packaging_id=9 or vp.state_packaging_id=10) AND (vp.iso_ctn LIKE '2%' ) 
ORDER BY vp.expotador_code ASC
); 

/*------------------- EMBALAJE SPB CONTENEDORES TIPO BIG BAGS DE 20  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    vp.expotador_code AS CODIGO_EXPORTADOR,
    vp.exportador AS NOMBRE_EXPORTADOR,
    vp.lotes AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.packaging_type AS TIPO_SACOS,
    vp.packaging_mode AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.packaging_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.oie AS OIE,
    vp.bic_ctn AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    vp.tipo_papel AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.estado_embalaje AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "800215775" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "EMBALAJE DE CONTENEDORES EN TERMINAL SPB TIPO BIG BAGS DE LONGITUD 20" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging AS vp 
INNER JOIN clients AS cts ON cts.exporter_code = vp.expotador_code
inner join items_unoee pds on pds.codigo='0000172'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000172','001')
WHERE   (  DATE(vp.packaging_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
        AND vp.jetty = 'bun' AND vp.packaging_mode LIKE 'BIG BAG%' AND (vp.state_packaging_id=3 or vp.state_packaging_id=9 or vp.state_packaging_id=10) AND (vp.iso_ctn LIKE '2%' ) 
ORDER BY vp.expotador_code ASC
); 


/*------------------- EMBALAJE SPB CONTENEDORES TIPO REFRIGERADOS  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    vp.expotador_code AS CODIGO_EXPORTADOR,
    vp.exportador AS NOMBRE_EXPORTADOR,
    vp.lotes AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.packaging_type AS TIPO_SACOS,
    vp.packaging_mode AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.packaging_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.oie AS OIE, 
    vp.bic_ctn AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    vp.tipo_papel AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.estado_embalaje AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "800215775" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "EMBALAJE DE CONTENEDORES EN TERMINAL SPB TIPO REFRIGERADO" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging AS vp 
INNER JOIN clients AS cts ON cts.exporter_code = vp.expotador_code
inner join items_unoee pds on pds.codigo='0000286'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000286','001')
WHERE   (  DATE(vp.packaging_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
        AND vp.jetty = 'bun' AND (vp.state_packaging_id=3 or vp.state_packaging_id=9 or vp.state_packaging_id=10) 
        AND vp.tipo_papel = "REF-CTN"
ORDER BY vp.expotador_code ASC
); 


/*------------------- EMBALAJE SPB CONTENEDORES DE 40  -------------------*/
/*------------------- EMBALAJE SPB CONTENEDORES TIPO SACOS DE 40 TIPO PAPEL KRAFT  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    vp.expotador_code AS CODIGO_EXPORTADOR,
    vp.exportador AS NOMBRE_EXPORTADOR,
    vp.lotes AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.packaging_type AS TIPO_SACOS,
    vp.packaging_mode AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.packaging_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.oie AS OIE,
    vp.bic_ctn AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    vp.tipo_papel AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.estado_embalaje AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "800215775" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "EMBALAJE DE CONTENEDORES EN TERMINAL SPB TIPO SACOS DE LONGITUD 40 TIPO PAPEL KRAFT" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging AS vp 
INNER JOIN clients AS cts ON cts.exporter_code = vp.expotador_code
inner join items_unoee pds on pds.codigo='0000161'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000161','001')
WHERE   (  DATE(vp.packaging_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
        AND vp.jetty = 'bun' AND vp.packaging_mode = 'SACOS' 
        AND (vp.state_packaging_id=3 or vp.state_packaging_id=9 or vp.state_packaging_id=10) 
        AND (vp.iso_ctn LIKE '4%' ) 
        AND vp.tipo_papel = "KRAFT"
ORDER BY vp.expotador_code ASC
); 

/*------------------- EMBALAJE SPB CONTENEDORES TIPO SACOS DE 40 TIPO PAPEL CARTON  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    vp.expotador_code AS CODIGO_EXPORTADOR,
    vp.exportador AS NOMBRE_EXPORTADOR,
    vp.lotes AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.packaging_type AS TIPO_SACOS,
    vp.packaging_mode AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.packaging_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.oie AS OIE,
    vp.bic_ctn AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    vp.tipo_papel AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.estado_embalaje AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "800215775" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "EMBALAJE DE CONTENEDORES EN TERMINAL SPB TIPO SACOS DE LONGITUD 40 TIPO PAPEL CARTON" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging AS vp 
INNER JOIN clients AS cts ON cts.exporter_code = vp.expotador_code
inner join items_unoee pds on pds.codigo='0000163'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000163','001')
WHERE   (  DATE(vp.packaging_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
        AND vp.jetty = 'bun' AND vp.packaging_mode = 'SACOS' 
        AND (vp.state_packaging_id=3 or vp.state_packaging_id=9 or vp.state_packaging_id=10) 
        AND (vp.iso_ctn LIKE '4%' ) 
        AND vp.tipo_papel = "CARTON"
ORDER BY vp.expotador_code ASC
); 


/*------------------- EMBALAJE SPB CONTENEDORES TIPO COMBINADOS SACOS - CAJAS DE 40 TIPO PAPEL KRAFT -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    vp.expotador_code AS CODIGO_EXPORTADOR,
    vp.exportador AS NOMBRE_EXPORTADOR,
    vp.lotes AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.packaging_type AS TIPO_SACOS,
    vp.packaging_mode AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.packaging_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.oie AS OIE,
    vp.bic_ctn AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    vp.tipo_papel AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.estado_embalaje AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "800215775" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "EMBALAJE DE CONTENEDORES EN TERMINAL SPB TIPO COMBINADO(SACOS - CAJAS) DE LONGITUD 40 TIPO PAPEL KRAFT" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging AS vp 
INNER JOIN clients AS cts ON cts.exporter_code = vp.expotador_code
inner join items_unoee pds on pds.codigo='0000291'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000291','001')
WHERE   (  DATE(vp.packaging_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
        AND vp.jetty = 'bun' AND vp.packaging_mode = 'COMBINADO(SACOS - CAJAS)' 
        AND (vp.state_packaging_id=3 or vp.state_packaging_id=9 or vp.state_packaging_id=10) 
        AND (vp.iso_ctn LIKE '4%' )
        AND vp.tipo_papel = "KRAFT"
ORDER BY vp.expotador_code ASC
); 


/*------------------- EMBALAJE SPB CONTENEDORES TIPO COMBINADOS SACOS - CAJAS DE 40 TIPO PAPEL CARTON -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    vp.expotador_code AS CODIGO_EXPORTADOR,
    vp.exportador AS NOMBRE_EXPORTADOR,
    vp.lotes AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.packaging_type AS TIPO_SACOS,
    vp.packaging_mode AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.packaging_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.oie AS OIE,
    vp.bic_ctn AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    vp.tipo_papel AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.estado_embalaje AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "800215775" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "EMBALAJE DE CONTENEDORES EN TERMINAL SPB TIPO COMBINADO(SACOS - CAJAS) DE LONGITUD 40 TIPO PAPEL CARTON" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging AS vp 
INNER JOIN clients AS cts ON cts.exporter_code = vp.expotador_code
inner join items_unoee pds on pds.codigo='0000292'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000292','001')
WHERE   (  DATE(vp.packaging_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
        AND vp.jetty = 'bun' AND vp.packaging_mode = 'COMBINADO(SACOS - CAJAS)' 
        AND (vp.state_packaging_id=3 or vp.state_packaging_id=9 or vp.state_packaging_id=10) 
        AND (vp.iso_ctn LIKE '4%' )
        AND vp.tipo_papel = "CARTON"
ORDER BY vp.expotador_code ASC
); 



/*------------------- EMBALAJE SPB CONTENEDORES TIPO CONBINADOS SACOS - BIG BGAS DE 40 TIPO PAPEL KRAFT -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    vp.expotador_code AS CODIGO_EXPORTADOR,
    vp.exportador AS NOMBRE_EXPORTADOR,
    vp.lotes AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.packaging_type AS TIPO_SACOS,
    vp.packaging_mode AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.packaging_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.oie AS OIE,
    vp.bic_ctn AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    vp.tipo_papel AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.estado_embalaje AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "800215775" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "EMBALAJE DE CONTENEDORES EN TERMINAL SPB TIPO COMBINADO(SACOS - BIG BAGS) DE LONGITUD 40 TIPO PAPEL KRAFT" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging AS vp 
INNER JOIN clients AS cts ON cts.exporter_code = vp.expotador_code
inner join items_unoee pds on pds.codigo='0000291'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000291','001')
WHERE   (  DATE(vp.packaging_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
        AND vp.jetty = 'bun' AND vp.packaging_mode = 'COMBINADO(SACOS - BIG BAGS)' 
        AND (vp.state_packaging_id=3 or vp.state_packaging_id=9 or vp.state_packaging_id=10) 
        AND (vp.iso_ctn LIKE '4%' )
        AND vp.tipo_papel = "KRAFT"
ORDER BY vp.expotador_code ASC
); 

/*------------------- EMBALAJE SPB CONTENEDORES TIPO CONBINADOS SACOS - BIG BGAS DE 40 TIPO PAPEL CARTON -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    vp.expotador_code AS CODIGO_EXPORTADOR,
    vp.exportador AS NOMBRE_EXPORTADOR,
    vp.lotes AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.packaging_type AS TIPO_SACOS,
    vp.packaging_mode AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.packaging_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.oie AS OIE,
    vp.bic_ctn AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    vp.tipo_papel AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.estado_embalaje AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "800215775" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "EMBALAJE DE CONTENEDORES EN TERMINAL SPB TIPO COMBINADO(SACOS - BIG BAGS) DE LONGITUD 40 TIPO PAPEL CARTON" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging AS vp 
INNER JOIN clients AS cts ON cts.exporter_code = vp.expotador_code
inner join items_unoee pds on pds.codigo='0000292'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000292','001')
WHERE   (  DATE(vp.packaging_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
        AND vp.jetty = 'bun' AND vp.packaging_mode = 'COMBINADO(SACOS - BIG BAGS)' 
        AND (vp.state_packaging_id=3 or vp.state_packaging_id=9 or vp.state_packaging_id=10) 
        AND (vp.iso_ctn LIKE '4%' )
        AND vp.tipo_papel = "CARTON"
ORDER BY vp.expotador_code ASC
); 


/*------------------- EMBALAJE SPB CONTENEDORES TIPO CAJAS DE 40  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    vp.expotador_code AS CODIGO_EXPORTADOR,
    vp.exportador AS NOMBRE_EXPORTADOR,
    vp.lotes AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.packaging_type AS TIPO_SACOS,
    vp.packaging_mode AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.packaging_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.oie AS OIE,
    vp.bic_ctn AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.estado_embalaje AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "800215775" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "EMBALAJE DE CONTENEDORES EN TERMINAL SPB TIPO CAJAS DE LONGITUD 40" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging AS vp 
INNER JOIN clients AS cts ON cts.exporter_code = vp.expotador_code
inner join items_unoee pds on pds.codigo='0000041'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000041','001')
WHERE   (  DATE(vp.packaging_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
        AND vp.jetty = 'bun' AND vp.packaging_mode = 'Cajas' AND (vp.state_packaging_id=3 or vp.state_packaging_id=9 or vp.state_packaging_id=10) AND (vp.iso_ctn LIKE '4%' )
ORDER BY vp.expotador_code ASC
);  

/*------------------- EMBALAJE SPB CONTENEDORES TIPO CACAO DE 40  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    vp.expotador_code AS CODIGO_EXPORTADOR,
    vp.exportador AS NOMBRE_EXPORTADOR,
    vp.lotes AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.packaging_type AS TIPO_SACOS,
    vp.packaging_mode AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.packaging_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.oie AS OIE,
    vp.bic_ctn AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.estado_embalaje AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "800215775" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "EMBALAJE DE CONTENEDORES EN TERMINAL SPB TIPO CACAO DE LONGITUD 40" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging AS vp 
INNER JOIN clients AS cts ON cts.exporter_code = vp.expotador_code
inner join items_unoee pds on pds.codigo='0000074'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000074','001')
WHERE   (  DATE(vp.packaging_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
        AND vp.jetty = 'bun' AND vp.packaging_mode = 'CACAO' AND (vp.state_packaging_id=3 or vp.state_packaging_id=9 or vp.state_packaging_id=10) AND (vp.iso_ctn LIKE '4%' )
ORDER BY vp.expotador_code ASC
); 

/*------------------- EMBALAJE SPB CONTENEDORES TIPO BIG BAGS DE 40  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    vp.expotador_code AS CODIGO_EXPORTADOR,
    vp.exportador AS NOMBRE_EXPORTADOR,
    vp.lotes AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.packaging_type AS TIPO_SACOS,
    vp.packaging_mode AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.packaging_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.oie AS OIE,
    vp.bic_ctn AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.estado_embalaje AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "800215775" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "EMBALAJE DE CONTENEDORES EN TERMINAL SPB TIPO BIG BAGS DE LONGITUD 40" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging AS vp 
INNER JOIN clients AS cts ON cts.exporter_code = vp.expotador_code
inner join items_unoee pds on pds.codigo='0000073'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000073','001')
WHERE   (  DATE(vp.packaging_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
        AND vp.jetty = 'bun' AND vp.packaging_mode LIKE 'BIG BAG%' AND (vp.state_packaging_id=3 or vp.state_packaging_id=9 or vp.state_packaging_id=10) AND (vp.iso_ctn LIKE '4%' ) 
ORDER BY vp.expotador_code ASC
);  


/*------------------- ADICIONALES SPB ISO 20 PAPEL KRAFT   -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    NULL AS CODIGO_EXPORTADOR,
    "SOCIEDAD PORTUARIA REGIONAL DE BUENAVENTURA S.A. S.P.R. BUN" AS NOMBRE_EXPORTADOR,
    vp.lotes AS LOTE,
    vp.capas_adiconales_kraft20 AS CANTIDAD_OPERATIVA,
    vp.packaging_type AS TIPO_SACOS,
    vp.packaging_mode AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.packaging_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.oie AS OIE,
    vp.bic_ctn AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    vp.tipo_papel AS TIPO_PAPEL,
    vp.capas_adiconales_kraft20 AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.estado_embalaje AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "800215775" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "CAPAS ADICIONALES DE CONTENEDORES EN TERMINAL SPB TIPO KRAFT DE LONGITUD 20" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*vp.capas_adiconales_kraft20 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging AS vp 
INNER JOIN clients AS cts ON cts.exporter_code = vp.expotador_code
inner join items_unoee pds on pds.codigo='0000156'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000156','001')
WHERE    (  DATE(vp.packaging_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
        AND vp.jetty = 'bun' AND (vp.state_packaging_id=3 or vp.state_packaging_id=9 or vp.state_packaging_id=10) AND (vp.iso_ctn LIKE '2%' ) 
        AND vp.capas_adiconales_kraft20 != 0
ORDER BY vp.expotador_code ASC
);



/*------------------- ADICIONALES SPB ISO 20 PAPEL CARTON   -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    NULL AS CODIGO_EXPORTADOR,
    "SOCIEDAD PORTUARIA REGIONAL DE BUENAVENTURA S.A. S.P.R. BUN" AS NOMBRE_EXPORTADOR,
    vp.lotes AS LOTE,
    vp.capas_adiconales_carton20 AS CANTIDAD_OPERATIVA,
    vp.packaging_type AS TIPO_SACOS,
    vp.packaging_mode AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.packaging_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.oie AS OIE,
    vp.bic_ctn AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    vp.tipo_papel AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    vp.capas_adiconales_carton20 AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.estado_embalaje AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "800215775" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "CAPAS ADICIONALES DE CONTENEDORES EN TERMINAL SPB TIPO CARTON DE LONGITUD 20" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*vp.capas_adiconales_carton20 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging AS vp 
INNER JOIN clients AS cts ON cts.exporter_code = vp.expotador_code
inner join items_unoee pds on pds.codigo='0000158'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000158','001')
WHERE    (  DATE(vp.packaging_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
        AND vp.jetty = 'bun' AND (vp.state_packaging_id=3 or vp.state_packaging_id=9 or vp.state_packaging_id=10) AND (vp.iso_ctn LIKE '2%' ) 
        AND vp.capas_adiconales_carton20 != 0
ORDER BY vp.expotador_code ASC
);


/*------------------- ADICIONALES SPB ISO 40 PAPEL KRAFT   -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    NULL AS CODIGO_EXPORTADOR,
    "SOCIEDAD PORTUARIA REGIONAL DE BUENAVENTURA S.A. S.P.R. BUN" AS NOMBRE_EXPORTADOR,
    vp.lotes AS LOTE,
    vp.capas_adiconales_kraft40 AS CANTIDAD_OPERATIVA,
    vp.packaging_type AS TIPO_SACOS,
    vp.packaging_mode AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.packaging_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.oie AS OIE,
    vp.bic_ctn AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    vp.tipo_papel AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    vp.capas_adiconales_kraft40 AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.estado_embalaje AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "800215775" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "CAPAS ADICIONALES DE CONTENEDORES EN TERMINAL SPB TIPO KRAFT DE LONGITUD 40" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*vp.capas_adiconales_kraft40 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging AS vp 
INNER JOIN clients AS cts ON cts.exporter_code = vp.expotador_code
inner join items_unoee pds on pds.codigo='0000176'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000176','001')
WHERE    (  DATE(vp.packaging_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
        AND vp.jetty = 'bun' AND (vp.state_packaging_id=3 or vp.state_packaging_id=9 or vp.state_packaging_id=10) AND (vp.iso_ctn LIKE '4%' ) 
        AND vp.capas_adiconales_kraft40 != 0
ORDER BY vp.expotador_code ASC
);


/*------------------- ADICIONALES SPB ISO 40 PAPEL CARTON   -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    NULL AS CODIGO_EXPORTADOR,
    "SOCIEDAD PORTUARIA REGIONAL DE BUENAVENTURA S.A. S.P.R. BUN" AS NOMBRE_EXPORTADOR,
    vp.lotes AS LOTE,
    vp.capas_adiconales_carton40 AS CANTIDAD_OPERATIVA,
    vp.packaging_type AS TIPO_SACOS,
    vp.packaging_mode AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.packaging_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.oie AS OIE,
    vp.bic_ctn AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    vp.tipo_papel AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    vp.capas_adiconales_carton40 AS CAPAS_ADICIONALES_CARTON_40,
    vp.estado_embalaje AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "800215775" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "CAPAS ADICIONALES DE CONTENEDORES EN TERMINAL SPB TIPO CARTON DE LONGITUD 40" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*vp.capas_adiconales_carton40 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging AS vp 
INNER JOIN clients AS cts ON cts.exporter_code = vp.expotador_code
inner join items_unoee pds on pds.codigo='0000178'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000178','001')
WHERE    (  DATE(vp.packaging_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
        AND vp.jetty = 'bun' AND (vp.state_packaging_id=3 or vp.state_packaging_id=9 or vp.state_packaging_id=10) AND (vp.iso_ctn LIKE '4%' ) 
        AND vp.capas_adiconales_carton40 != 0
ORDER BY vp.expotador_code ASC
);
         

/*------------------- VACIADOS SPB ISO 20  -------------------*/
/*------------------- VACIADOS SPB ISO 20 SACOS -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    vp.expotador_code AS CODIGO_EXPORTADOR,
    vp.exportador AS NOMBRE_EXPORTADOR,
    vp.lotes AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.packaging_type AS TIPO_SACOS,
    vp.packaging_mode AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.empty_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.oie AS OIE,
    vp.bic_ctn AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.estado_embalaje AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "800215775" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "VACIADOS DE CONTENEDORES EN TERMINAL SPB TIPO SACOS DE LONGITUD 20" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging AS vp 
INNER JOIN clients AS cts ON cts.exporter_code = vp.expotador_code
inner join items_unoee pds on pds.codigo='0000044'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000044','001')
WHERE  (  DATE(vp.empty_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
        AND vp.jetty = 'bun' AND vp.packaging_mode = 'SACOS' AND (vp.state_packaging_id=4 or vp.state_packaging_id=9) AND (vp.iso_ctn LIKE '2%' ) 
ORDER BY vp.expotador_code ASC
); 

/*------------------- VACIADOS SPB ISO 20 GRANEL -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    vp.expotador_code AS CODIGO_EXPORTADOR,
    vp.exportador AS NOMBRE_EXPORTADOR,
    vp.lotes AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.packaging_type AS TIPO_SACOS,
    vp.packaging_mode AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.empty_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.oie AS OIE,
    vp.bic_ctn AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.estado_embalaje AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "800215775" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "VACIADOS DE CONTENEDORES EN TERMINAL SPB TIPO GRANEL DE LONGITUD 20" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging AS vp 
INNER JOIN clients AS cts ON cts.exporter_code = vp.expotador_code
inner join items_unoee pds on pds.codigo='0000171'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000171','001')
WHERE   (  DATE(vp.empty_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
        AND vp.jetty = 'bun' AND vp.packaging_mode = 'granel' AND (vp.state_packaging_id=4 or vp.state_packaging_id=9) AND (vp.iso_ctn LIKE '2%' ) 
ORDER BY vp.expotador_code ASC
); 

/*------------------- VACIADOS SPB ISO 20 COMBINADOS SACOS - CAJAS -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    vp.expotador_code AS CODIGO_EXPORTADOR,
    vp.exportador AS NOMBRE_EXPORTADOR,
    vp.lotes AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.packaging_type AS TIPO_SACOS,
    vp.packaging_mode AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.empty_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.oie AS OIE,
    vp.bic_ctn AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.estado_embalaje AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "800215775" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "VACIADOS DE CONTENEDORES EN TERMINAL SPB TIPO COMBINADO(SACOS - CAJAS) DE LONGITUD 20" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging AS vp 
INNER JOIN clients AS cts ON cts.exporter_code = vp.expotador_code
inner join items_unoee pds on pds.codigo='0000294'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000294','001')
WHERE   (  DATE(vp.empty_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
        AND vp.jetty = 'bun' AND vp.packaging_mode = 'COMBINADO(SACOS - CAJAS)' AND (vp.state_packaging_id=4 or vp.state_packaging_id=9) AND (vp.iso_ctn LIKE '2%' ) 
ORDER BY vp.expotador_code ASC
); 

/*------------------- VACIADOS SPB ISO 20 COMBINADOS SACOS - BIG BGAS -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    vp.expotador_code AS CODIGO_EXPORTADOR,
    vp.exportador AS NOMBRE_EXPORTADOR,
    vp.lotes AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.packaging_type AS TIPO_SACOS,
    vp.packaging_mode AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.empty_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.oie AS OIE,
    vp.bic_ctn AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.estado_embalaje AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "800215775" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "VACIADOS DE CONTENEDORES EN TERMINAL SPB TIPO COMBINADO(SACOS - BIG BAGS) DE LONGITUD 20" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging AS vp 
INNER JOIN clients AS cts ON cts.exporter_code = vp.expotador_code
inner join items_unoee pds on pds.codigo='0000294'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000294','001')
WHERE   (  DATE(vp.empty_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
        AND vp.jetty = 'bun' AND vp.packaging_mode = 'COMBINADO(SACOS - BIG BAGS)' AND (vp.state_packaging_id=4 or vp.state_packaging_id=9) AND (vp.iso_ctn LIKE '2%' ) 
ORDER BY vp.expotador_code ASC
); 

/*------------------- VACIADOS SPB ISO 20 CAJAS -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    vp.expotador_code AS CODIGO_EXPORTADOR,
    vp.exportador AS NOMBRE_EXPORTADOR,
    vp.lotes AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.packaging_type AS TIPO_SACOS,
    vp.packaging_mode AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.empty_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.oie AS OIE,
    vp.bic_ctn AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.estado_embalaje AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "800215775" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "VACIADOS DE CONTENEDORES EN TERMINAL SPB TIPO CAJAS DE LONGITUD 20" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging AS vp 
INNER JOIN clients AS cts ON cts.exporter_code = vp.expotador_code
inner join items_unoee pds on pds.codigo='0000078'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000078','001')
WHERE   (  DATE(vp.empty_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
        AND vp.jetty = 'bun' AND vp.packaging_mode = 'Cajas' AND  (vp.state_packaging_id=4 or vp.state_packaging_id=9) AND (vp.iso_ctn LIKE '2%' ) 
ORDER BY vp.expotador_code ASC
); 

/*------------------- VACIADOS SPB ISO 20 CACAO -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    vp.expotador_code AS CODIGO_EXPORTADOR,
    vp.exportador AS NOMBRE_EXPORTADOR,
    vp.lotes AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.packaging_type AS TIPO_SACOS,
    vp.packaging_mode AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.empty_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.oie AS OIE,
    vp.bic_ctn AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.estado_embalaje AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "800215775" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "VACIADOS DE CONTENEDORES EN TERMINAL SPB TIPO CACAO DE LONGITUD 20" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging AS vp 
INNER JOIN clients AS cts ON cts.exporter_code = vp.expotador_code
inner join items_unoee pds on pds.codigo='0000293'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000293','001')
WHERE   (  DATE(vp.empty_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
        AND vp.jetty = 'bun' AND vp.packaging_mode = 'CACAO' AND  (vp.state_packaging_id=4 or vp.state_packaging_id=9) AND (vp.iso_ctn LIKE '2%' ) 
ORDER BY vp.expotador_code ASC
); 

/*------------------- VACIADOS SPB ISO 20 BIG BGAS -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    vp.expotador_code AS CODIGO_EXPORTADOR,
    vp.exportador AS NOMBRE_EXPORTADOR,
    vp.lotes AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.packaging_type AS TIPO_SACOS,
    vp.packaging_mode AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.empty_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.oie AS OIE,
    vp.bic_ctn AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.estado_embalaje AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "800215775" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "VACIADOS DE CONTENEDORES EN TERMINAL SPB TIPO BIG BAGS DE LONGITUD 20" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging AS vp 
INNER JOIN clients AS cts ON cts.exporter_code = vp.expotador_code
inner join items_unoee pds on pds.codigo='0000174'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000174','001')
WHERE   (  DATE(vp.empty_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
        AND vp.jetty = 'bun' AND vp.packaging_mode LIKE 'BIG BAG%' AND (vp.state_packaging_id=4 or vp.state_packaging_id=9) AND (vp.iso_ctn LIKE '2%' ) 
ORDER BY vp.expotador_code ASC
); 



/*------------------- VACIADOS SPB ISO 40  -------------------*/
/*------------------- VACIADOS SPB ISO 40 SACOS -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    vp.expotador_code AS CODIGO_EXPORTADOR,
    vp.exportador AS NOMBRE_EXPORTADOR,
    vp.lotes AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.packaging_type AS TIPO_SACOS,
    vp.packaging_mode AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.empty_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.oie AS OIE,
    vp.bic_ctn AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.estado_embalaje AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "800215775" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "VACIADOS DE CONTENEDORES EN TERMINAL SPB TIPO SACOS DE LONGITUD 40" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging AS vp 
INNER JOIN clients AS cts ON cts.exporter_code = vp.expotador_code
inner join items_unoee pds on pds.codigo='0000081'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000081','001')
WHERE   (  DATE(vp.empty_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
        AND vp.jetty = 'bun' AND vp.packaging_mode = 'SACOS' AND (vp.state_packaging_id=4 or vp.state_packaging_id=9) AND (vp.iso_ctn LIKE '4%' ) 
ORDER BY vp.expotador_code ASC
); 


/*------------------- VACIADOS SPB ISO 40 COMBINADOS SACOS - CAJAS -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    vp.expotador_code AS CODIGO_EXPORTADOR,
    vp.exportador AS NOMBRE_EXPORTADOR,
    vp.lotes AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.packaging_type AS TIPO_SACOS,
    vp.packaging_mode AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.empty_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.oie AS OIE,
    vp.bic_ctn AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.estado_embalaje AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "800215775" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "VACIADOS DE CONTENEDORES EN TERMINAL SPB TIPO COMBINADO(SACOS - CAJAS) DE LONGITUD 40" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging AS vp 
INNER JOIN clients AS cts ON cts.exporter_code = vp.expotador_code
inner join items_unoee pds on pds.codigo='0000294'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000294','001')
WHERE   (  DATE(vp.empty_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
        AND vp.jetty = 'bun' AND vp.packaging_mode = 'COMBINADO(SACOS - CAJAS)' AND (vp.state_packaging_id=4 or vp.state_packaging_id=9) AND (vp.iso_ctn LIKE '4%' )
ORDER BY vp.expotador_code ASC
); 

/*------------------- VACIADOS SPB ISO 40 COMBINADOS SACOS - BIG BGAS -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    vp.expotador_code AS CODIGO_EXPORTADOR,
    vp.exportador AS NOMBRE_EXPORTADOR,
    vp.lotes AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.packaging_type AS TIPO_SACOS,
    vp.packaging_mode AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.empty_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.oie AS OIE,
    vp.bic_ctn AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.estado_embalaje AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "800215775" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "VACIADOS DE CONTENEDORES EN TERMINAL SPB TIPO COMBINADO(SACOS - BIG BAGS) DE LONGITUD 40" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging AS vp 
INNER JOIN clients AS cts ON cts.exporter_code = vp.expotador_code
inner join items_unoee pds on pds.codigo='0000294'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000294','001')
WHERE   (  DATE(vp.empty_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
        AND vp.jetty = 'bun' AND vp.packaging_mode = 'COMBINADO(SACOS - BIG BAGS)' AND (vp.state_packaging_id=4 or vp.state_packaging_id=9) AND (vp.iso_ctn LIKE '4%' )
ORDER BY vp.expotador_code ASC
); 

/*------------------- VACIADOS SPB ISO 40 CAJAS -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    vp.expotador_code AS CODIGO_EXPORTADOR,
    vp.exportador AS NOMBRE_EXPORTADOR,
    vp.lotes AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.packaging_type AS TIPO_SACOS,
    vp.packaging_mode AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.empty_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.oie AS OIE,
    vp.bic_ctn AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.estado_embalaje AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "800215775" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "VACIADOS DE CONTENEDORES EN TERMINAL SPB TIPO CAJAS DE LONGITUD 40" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging AS vp 
INNER JOIN clients AS cts ON cts.exporter_code = vp.expotador_code
inner join items_unoee pds on pds.codigo='0000079'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000079','001')
WHERE   (  DATE(vp.empty_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
        AND vp.jetty = 'bun' AND vp.packaging_mode = 'Cajas' AND (vp.state_packaging_id=4 or vp.state_packaging_id=9) AND (vp.iso_ctn LIKE '4%' )
ORDER BY vp.expotador_code ASC
); 

/*------------------- VACIADOS SPB ISO 40 CACAO -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    vp.expotador_code AS CODIGO_EXPORTADOR,
    vp.exportador AS NOMBRE_EXPORTADOR,
    vp.lotes AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.packaging_type AS TIPO_SACOS,
    vp.packaging_mode AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.empty_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.oie AS OIE,
    vp.bic_ctn AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.estado_embalaje AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "800215775" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "VACIADOS DE CONTENEDORES EN TERMINAL SPB TIPO CACAO DE LONGITUD 40" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging AS vp 
INNER JOIN clients AS cts ON cts.exporter_code = vp.expotador_code
inner join items_unoee pds on pds.codigo='0000290'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000290','001')
WHERE   (  DATE(vp.empty_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
        AND vp.jetty = 'bun' AND vp.packaging_mode = 'CACAO' AND (vp.state_packaging_id=4 or vp.state_packaging_id=9) AND (vp.iso_ctn LIKE '4%' )
ORDER BY vp.expotador_code ASC
); 

/*------------------- VACIADOS SPB ISO 40 BIG BGAS -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    vp.expotador_code AS CODIGO_EXPORTADOR,
    vp.exportador AS NOMBRE_EXPORTADOR,
    vp.lotes AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.packaging_type AS TIPO_SACOS,
    vp.packaging_mode AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.empty_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.oie AS OIE,
    vp.bic_ctn AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.estado_embalaje AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "800215775" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "VACIADOS DE CONTENEDORES EN TERMINAL SPB TIPO BIG BAGS DE LONGITUD 40" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging AS vp 
INNER JOIN clients AS cts ON cts.exporter_code = vp.expotador_code
inner join items_unoee pds on pds.codigo='0000174'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000174','001')
WHERE   (  DATE(vp.empty_date) BETWEEN @FECHA_INF AND @FECHA_SUP   ) 
        AND vp.jetty = 'bun' AND vp.packaging_mode LIKE 'BIG BAG%' AND (vp.state_packaging_id=4 or vp.state_packaging_id=9)  AND (vp.iso_ctn LIKE '4%' ) 
ORDER BY vp.expotador_code ASC
); 




/*------------------- TRAZABILIDAD IER (INSPECTION AT THE TIME OF RECEIPT) SPB  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,  
    group_concat(cts.exporter_code,'-', rc.lot_caffee ,' x ',rc.quantity_radicated_bag_in separator ' | ') AS LOTES,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    ifn.packaging_mode AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    ivn.date_reg AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    ivn.id_tracking_siscafe  AS OIE,
    rdc.bic_container AS CONTENEDOR,
    pc.iso_ctn AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    ivn.ip_conection AS IP_CONEXION,
    (select cls.nit from clients cls where cls.id=ivn.id_expo_pilcaf) AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    concat("TRAZABILIDAD IER (INSPECTION AT THE TIME OF RECEIPT) PARA EL TERMINAL DE SPB") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM invoice_note as ivn
INNER JOIN clients AS cts ON cts.id = ivn.id_expo_pilcaf
INNER JOIN packaging_caffee AS pc ON pc.id = ivn.id_tracking_siscafe 
INNER JOIN ready_container AS rdc ON rdc.id = pc.ready_container_id
INNER JOIN detail_packaging_caffee AS dpc on dpc.packaging_caffee_id = ivn.id_tracking_siscafe
INNER JOIN info_navy AS ifn ON ifn.id = pc.info_navy_id
INNER JOIN remittances_caffee AS rc ON rc.id = dpc.remittances_caffee_id
inner join items_unoee pds on pds.codigo='0000268'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000268','001')
WHERE  ivn.type_invoice= 2 AND pc.jetty='BUN' AND 
    (  DATE(ivn.date_reg) BETWEEN @FECHA_INF AND @FECHA_SUP   )
group by ivn.id_tracking_siscafe,rc.client_id,date(ivn.date_reg), pc.jetty
); 






















/*------------------- EMBALAJE CONTENEDOR SPIA TIPO SACOS DE 20 TIPO DE PAPEL KRAFT  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,  
    vp.LOTES AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.TIPO_EMBALAJE AS TIPO_SACOS,
    vp.MODALIDAD AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.FECHA_REGISTROS AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.OIE AS OIE, 
    vp.BIC_CTN AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    vp.type_papper AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.ESTADO AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "835000149" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    "EMBALAJE DE CONTENEDORES EN TERMINAL SPIA TIPO SACOS DE LONGITUD 20 TIPO DE PAPEL KRAFT " AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    (1*57)*@TRM AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging2 AS vp 
INNER JOIN clients AS cts ON cts.business_name = vp.EXPORTADOR
inner join items_unoee pds on pds.codigo='0000160'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000160','001')
WHERE   vp.puerto = 'SPIA' AND vp.MODALIDAD LIKE 'SACOS%' AND (vp.iso_ctn LIKE '2%' ) 
		AND (vp.STATUS_ID=3 or vp.STATUS_ID=9 or vp.STATUS_ID=10) AND vp.type_papper="KRAFT"
        AND (  DATE(vp.FECHA_REGISTROS) BETWEEN @FECHA_INF AND @FECHA_SUP  )
ORDER BY vp.EXPORTADOR ASC
); 


/*------------------- EMBALAJE CONTENEDOR SPIA TIPO SACOS DE 20 TIPO DE PAPEL KRAFT2  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,  
    vp.LOTES AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.TIPO_EMBALAJE AS TIPO_SACOS,
    vp.MODALIDAD AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.FECHA_REGISTROS AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.OIE AS OIE, 
    vp.BIC_CTN AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    vp.type_papper AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.ESTADO AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "835000149" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    "EMBALAJE DE CONTENEDORES EN TERMINAL SPIA TIPO SACOS DE LONGITUD 20 TIPO DE PAPEL KRAFT2 " AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    (1*76)*@TRM AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging2 AS vp 
INNER JOIN clients AS cts ON cts.business_name = vp.EXPORTADOR
inner join items_unoee pds on pds.codigo='0000160'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000160','001')
WHERE   vp.puerto = 'SPIA' AND vp.MODALIDAD LIKE 'SACOS%' AND (vp.iso_ctn LIKE '2%' ) 
        AND (vp.STATUS_ID=3 or vp.STATUS_ID=9 or vp.STATUS_ID=10) AND vp.type_papper="KRAFT2"
        AND (  DATE(vp.FECHA_REGISTROS) BETWEEN @FECHA_INF AND @FECHA_SUP  )
ORDER BY vp.EXPORTADOR ASC
); 



/*------------------- EMBALAJE CONTENEDOR SPIA TIPO SACOS DE 20 TIPO DE PAPEL CARTON  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,  
    vp.LOTES AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.TIPO_EMBALAJE AS TIPO_SACOS,
    vp.MODALIDAD AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.FECHA_REGISTROS AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.OIE AS OIE, 
    vp.BIC_CTN AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    vp.type_papper AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.ESTADO AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "835000149" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    "EMBALAJE DE CONTENEDORES EN TERMINAL SPIA TIPO SACOS DE LONGITUD 20 TIPO DE PAPEL CARTON " AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    (1*109)*@TRM AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging2 AS vp 
INNER JOIN clients AS cts ON cts.business_name = vp.EXPORTADOR
inner join items_unoee pds on pds.codigo='0000162'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000162','001')
WHERE   vp.puerto = 'SPIA' AND vp.MODALIDAD LIKE 'SACOS%' AND (vp.iso_ctn LIKE '2%' ) 
        AND (vp.STATUS_ID=3 or vp.STATUS_ID=9 or vp.STATUS_ID=10) AND vp.type_papper="CARTON"
        AND (  DATE(vp.FECHA_REGISTROS) BETWEEN @FECHA_INF AND @FECHA_SUP  )
ORDER BY vp.EXPORTADOR ASC
); 



/*------------------- EMBALAJE CONTENEDOR SPIA TIPO GRANEL DE 20 TIPO DE PAPEL KRAFT  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,  
    vp.LOTES AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.TIPO_EMBALAJE AS TIPO_SACOS,
    vp.MODALIDAD AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.FECHA_REGISTROS AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.OIE AS OIE, 
    vp.BIC_CTN AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    vp.type_papper AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.ESTADO AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "835000149" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    "EMBALAJE DE CONTENEDORES EN TERMINAL SPIA TIPO GRANEL DE LONGITUD 20 TIPO DE PAPEL KRAFT" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    (1*76)*@TRM AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging2 AS vp 
INNER JOIN clients AS cts ON cts.business_name = vp.EXPORTADOR
inner join items_unoee pds on pds.codigo='0000168'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000168','001')
WHERE   vp.puerto = 'SPIA' AND vp.MODALIDAD LIKE 'GRANEL%' AND (vp.iso_ctn LIKE '2%' ) 
		AND (vp.STATUS_ID=3 or vp.STATUS_ID=9 or vp.STATUS_ID=10) AND vp.type_papper="KRAFT"
        AND (  DATE(vp.FECHA_REGISTROS) BETWEEN @FECHA_INF AND @FECHA_SUP  )
ORDER BY vp.EXPORTADOR ASC
); 

/*------------------- EMBALAJE CONTENEDOR SPIA TIPO GRANEL DE 20 TIPO DE PAPEL CARTON  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,  
    vp.LOTES AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.TIPO_EMBALAJE AS TIPO_SACOS,
    vp.MODALIDAD AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.FECHA_REGISTROS AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.OIE AS OIE, 
    vp.BIC_CTN AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    vp.type_papper AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.ESTADO AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "835000149" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    "EMBALAJE DE CONTENEDORES EN TERMINAL SPIA TIPO GRANEL DE LONGITUD 20 TIPO DE PAPEL CARTON" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    (1*114)*@TRM AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging2 AS vp 
INNER JOIN clients AS cts ON cts.business_name = vp.EXPORTADOR
inner join items_unoee pds on pds.codigo='0000169'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000169','001')
WHERE   vp.puerto = 'SPIA' AND vp.MODALIDAD LIKE 'GRANEL%' AND (vp.iso_ctn LIKE '2%' ) 
        AND (vp.STATUS_ID=3 or vp.STATUS_ID=9 or vp.STATUS_ID=10) AND vp.type_papper LIKE "%CARTON%"
        AND (  DATE(vp.FECHA_REGISTROS) BETWEEN @FECHA_INF AND @FECHA_SUP  )
ORDER BY vp.EXPORTADOR ASC
); 


/*------------------- EMBALAJE CONTENEDOR SPIA TIPO BIG BAG DE 20  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,  
    vp.LOTES AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.TIPO_EMBALAJE AS TIPO_SACOS,
    vp.MODALIDAD AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.FECHA_REGISTROS AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.OIE AS OIE, 
    vp.BIC_CTN AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    vp.type_papper AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.ESTADO AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "835000149" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    '006' AS LISTA_PRECIO,   
    "EMBALAJE DE CONTENEDORES EN TERMINAL SPIA TIPO BIG BAGS DE LONGITUD 20" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    (1*114)*@TRM AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging2 AS vp 
INNER JOIN clients AS cts ON cts.business_name = vp.EXPORTADOR
inner join items_unoee pds on pds.codigo='0000031'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000031','001')
WHERE   vp.puerto = 'SPIA' AND vp.MODALIDAD LIKE 'BIG BAG%' AND (vp.iso_ctn LIKE '2%' ) 
		AND (vp.STATUS_ID=3 or vp.STATUS_ID=9 or vp.STATUS_ID=10) 
        AND (  DATE(vp.FECHA_REGISTROS) BETWEEN @FECHA_INF AND @FECHA_SUP  )
ORDER BY vp.EXPORTADOR ASC
); 


/*------------------- EMBALAJE CONTENEDOR SPIA TIPO CACAO DE 20  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,  
    vp.LOTES AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.TIPO_EMBALAJE AS TIPO_SACOS,
    vp.MODALIDAD AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.FECHA_REGISTROS AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.OIE AS OIE, 
    vp.BIC_CTN AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    vp.type_papper AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.ESTADO AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "835000149" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    "EMBALAJE DE CONTENEDORES EN TERMINAL SPIA TIPO CACAO DE LONGITUD 20" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging2 AS vp 
INNER JOIN clients AS cts ON cts.business_name = vp.EXPORTADOR
inner join items_unoee pds on pds.codigo='0000050'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000050','001')
WHERE   vp.puerto = 'SPIA' AND vp.MODALIDAD LIKE 'CACAO%' AND (vp.iso_ctn LIKE '2%' ) 
		AND (vp.STATUS_ID=3 or vp.STATUS_ID=9 or vp.STATUS_ID=10)
        AND (  DATE(vp.FECHA_REGISTROS) BETWEEN @FECHA_INF AND @FECHA_SUP  )
ORDER BY vp.EXPORTADOR ASC
); 


/*------------------- EMBALAJE CONTENEDOR SPIA TIPO SACOS DE 40 TIPO DE PAPEL KRAFT  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,  
    vp.LOTES AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.TIPO_EMBALAJE AS TIPO_SACOS,
    vp.MODALIDAD AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.FECHA_REGISTROS AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.OIE AS OIE, 
    vp.BIC_CTN AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    vp.type_papper AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.ESTADO AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "835000149" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    "EMBALAJE DE CONTENEDORES EN TERMINAL SPIA TIPO SACOS DE LONGITUD 40 TIPO DE PAPEL KRAFT" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    (1*76)*@TRM AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging2 AS vp 
INNER JOIN clients AS cts ON cts.business_name = vp.EXPORTADOR
inner join items_unoee pds on pds.codigo='0000161'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000161','001')
WHERE   vp.puerto = 'SPIA' AND vp.MODALIDAD LIKE 'SACOS%' AND (vp.iso_ctn LIKE '4%' ) 
		AND (vp.STATUS_ID=3 or vp.STATUS_ID=9 or vp.STATUS_ID=10) AND vp.type_papper="KRAFT"
        AND (  DATE(vp.FECHA_REGISTROS) BETWEEN @FECHA_INF AND @FECHA_SUP  )
ORDER BY vp.EXPORTADOR ASC
); 


/*------------------- EMBALAJE CONTENEDOR SPIA TIPO SACOS DE 40 TIPO DE PAPEL KRAFT2  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,  
    vp.LOTES AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.TIPO_EMBALAJE AS TIPO_SACOS,
    vp.MODALIDAD AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.FECHA_REGISTROS AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.OIE AS OIE, 
    vp.BIC_CTN AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    vp.type_papper AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.ESTADO AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "835000149" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    "EMBALAJE DE CONTENEDORES EN TERMINAL SPIA TIPO SACOS DE LONGITUD 40 TIPO DE PAPEL KRAFT2" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    (1*114)*@TRM AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging2 AS vp 
INNER JOIN clients AS cts ON cts.business_name = vp.EXPORTADOR
inner join items_unoee pds on pds.codigo='0000161'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000161','001')
WHERE   vp.puerto = 'SPIA' AND vp.MODALIDAD LIKE 'SACOS%' AND (vp.iso_ctn LIKE '4%' ) 
        AND (vp.STATUS_ID=3 or vp.STATUS_ID=9 or vp.STATUS_ID=10) AND vp.type_papper="KRAFT2"
        AND (  DATE(vp.FECHA_REGISTROS) BETWEEN @FECHA_INF AND @FECHA_SUP  )
ORDER BY vp.EXPORTADOR ASC
); 


/*------------------- EMBALAJE CONTENEDOR SPIA TIPO SACOS DE 40 TIPO DE PAPEL CARTON  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,  
    vp.LOTES AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.TIPO_EMBALAJE AS TIPO_SACOS,
    vp.MODALIDAD AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.FECHA_REGISTROS AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.OIE AS OIE, 
    vp.BIC_CTN AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    vp.type_papper AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.ESTADO AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "835000149" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    "EMBALAJE DE CONTENEDORES EN TERMINAL SPIA TIPO SACOS DE LONGITUD 40 TIPO DE PAPEL CARTON" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    (1*166)*@TRM AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging2 AS vp 
INNER JOIN clients AS cts ON cts.business_name = vp.EXPORTADOR
inner join items_unoee pds on pds.codigo='0000163'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000163','001')
WHERE   vp.puerto = 'SPIA' AND vp.MODALIDAD LIKE 'SACOS%' AND (vp.iso_ctn LIKE '4%' ) 
        AND (vp.STATUS_ID=3 or vp.STATUS_ID=9 or vp.STATUS_ID=10) AND vp.type_papper="CARTON"
        AND (  DATE(vp.FECHA_REGISTROS) BETWEEN @FECHA_INF AND @FECHA_SUP  )
ORDER BY vp.EXPORTADOR ASC
); 


/*------------------- EMBALAJE CONTENEDOR SPIA TIPO BIG BAG DE 40  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,  
    vp.LOTES AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.TIPO_EMBALAJE AS TIPO_SACOS,
    vp.MODALIDAD AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.FECHA_REGISTROS AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.OIE AS OIE, 
    vp.BIC_CTN AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    vp.type_papper AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.ESTADO AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "835000149" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    "EMBALAJE DE CONTENEDORES EN TERMINAL SPIA TIPO BIG BAGS DE LONGITUD 40" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    (1*114)*@TRM AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging2 AS vp 
INNER JOIN clients AS cts ON cts.business_name = vp.EXPORTADOR
inner join items_unoee pds on pds.codigo='0000073'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000073','001')
WHERE   vp.puerto = 'SPIA' AND vp.MODALIDAD LIKE 'BIG BAG%' AND (vp.iso_ctn LIKE '4%' ) 
		AND (vp.STATUS_ID=3 or vp.STATUS_ID=9 or vp.STATUS_ID=10)
        AND (  DATE(vp.FECHA_REGISTROS) BETWEEN @FECHA_INF AND @FECHA_SUP  )
ORDER BY vp.EXPORTADOR ASC
); 


/*------------------- EMBALAJE CONTENEDOR SPIA TIPO CACAO DE 40  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,  
    vp.LOTES AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.TIPO_EMBALAJE AS TIPO_SACOS,
    vp.MODALIDAD AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.FECHA_REGISTROS AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.OIE AS OIE, 
    vp.BIC_CTN AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    vp.type_papper AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.ESTADO AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "835000149" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    "EMBALAJE DE CONTENEDORES EN TERMINAL SPIA TIPO CACAO DE LONGITUD 40" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging2 AS vp 
INNER JOIN clients AS cts ON cts.business_name = vp.EXPORTADOR
inner join items_unoee pds on pds.codigo='0000074'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000074','001')
WHERE   vp.puerto = 'SPIA' AND vp.MODALIDAD LIKE 'CACAO%' AND (vp.iso_ctn LIKE '4%' ) 
		AND (vp.STATUS_ID=3 or vp.STATUS_ID=9 or vp.STATUS_ID=10)
        AND (  DATE(vp.FECHA_REGISTROS) BETWEEN @FECHA_INF AND @FECHA_SUP  )
ORDER BY vp.EXPORTADOR ASC
); 

/*------------------- EMBALAJE SPIA CONTENEDORES TIPO REFRIGERADOS  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,  
    vp.LOTES AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.TIPO_EMBALAJE AS TIPO_SACOS,
    vp.MODALIDAD AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.FECHA_REGISTROS AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.OIE AS OIE, 
    vp.BIC_CTN AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    vp.type_papper AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.ESTADO AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "835000149" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,  
    "EMBALAJE DE CONTENEDORES EN TERMINAL SPIA TIPO REFRIGERADO" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging2 AS vp 
INNER JOIN clients AS cts ON cts.business_name = vp.EXPORTADOR
inner join items_unoee pds on pds.codigo='0000286'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000286','001')
WHERE   vp.puerto = 'SPIA' AND (vp.STATUS_ID=3 or vp.STATUS_ID=9 or vp.STATUS_ID=10)
        AND vp.type_papper = "REF-CTN"
        AND (  DATE(vp.FECHA_REGISTROS) BETWEEN @FECHA_INF AND @FECHA_SUP  )
ORDER BY vp.EXPORTADOR ASC
); 


/*------------------- ADICIONALES SPIA ISO 20 PAPEL KRAFT   -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    vp.LOTES AS LOTE,
    vp.adictional_layer_kraft_20 AS CANTIDAD_OPERATIVA,
    vp.TIPO_EMBALAJE AS TIPO_SACOS,
    vp.MODALIDAD AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.FECHA_REGISTROS AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.OIE AS OIE, 
    vp.BIC_CTN AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    vp.type_papper AS TIPO_PAPEL,
    vp.adictional_layer_kraft_20 AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.ESTADO AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "835000149" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    "CAPAS ADICIONALES DE CONTENEDORES EN TERMINAL SPIA TIPO KRAFT DE LONGITUD 20" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    (vp.adictional_layer_kraft_20*lp.precio)*@TRM AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging2 AS vp 
inner join items_unoee pds on pds.codigo='0000156'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000156','006')
INNER JOIN clients AS cts ON cts.business_name = vp.EXPORTADOR
WHERE   vp.PUERTO = 'SPIA'  AND (vp.iso_ctn LIKE '2%' ) AND vp.adictional_layer_kraft_20 != 0
		AND (vp.STATUS_ID=3 or vp.STATUS_ID=9 or vp.STATUS_ID=10)
        AND (  DATE(vp.FECHA_REGISTROS) BETWEEN @FECHA_INF AND @FECHA_SUP  )
ORDER BY vp.EXPORTADOR ASC
);


/*------------------- ADICIONALES SPIA ISO 20 PAPEL CARTON   -------------------*/
INSERT INTO data_billing_full (
SELECT 
	NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    vp.LOTES AS LOTE,
    vp.adictional_layer_carton_20 AS CANTIDAD_OPERATIVA,
    vp.TIPO_EMBALAJE AS TIPO_SACOS,
    vp.MODALIDAD AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.FECHA_REGISTROS AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.OIE AS OIE, 
    vp.BIC_CTN AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    vp.type_papper AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    vp.adictional_layer_carton_20 AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.ESTADO AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "835000149" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    "CAPAS ADICIONALES DE CONTENEDORES EN TERMINAL SPIA TIPO CARTON DE LONGITUD 20" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    (57*vp.adictional_layer_carton_20)*@TRM AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging2 AS vp 
INNER JOIN clients AS cts ON cts.business_name = vp.EXPORTADOR
inner join items_unoee pds on pds.codigo='0000158'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000158','001')
WHERE   vp.PUERTO = 'SPIA'  AND (vp.iso_ctn LIKE '2%' ) AND vp.adictional_layer_carton_20 != 0
		AND (vp.STATUS_ID=3 or vp.STATUS_ID=9 or vp.STATUS_ID=10)
        AND (  DATE(vp.FECHA_REGISTROS) BETWEEN @FECHA_INF AND @FECHA_SUP  )
ORDER BY vp.EXPORTADOR ASC
);


/*------------------- ADICIONALES SPIA ISO 40 PAPEL KRAFT   -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    vp.LOTES AS LOTE,
    vp.adictional_layer_kraft_40 AS CANTIDAD_OPERATIVA,
    vp.TIPO_EMBALAJE AS TIPO_SACOS,
    vp.MODALIDAD AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.FECHA_REGISTROS AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.OIE AS OIE, 
    vp.BIC_CTN AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    vp.type_papper AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    vp.adictional_layer_kraft_40 AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.ESTADO AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "835000149" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    "CAPAS ADICIONALES DE CONTENEDORES EN TERMINAL SPIA TIPO KRAFT DE LONGITUD 40" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    (lp.precio*vp.adictional_layer_kraft_40)*@TRM AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging2 AS vp 
INNER JOIN clients AS cts ON cts.business_name = vp.EXPORTADOR
inner join items_unoee pds on pds.codigo='0000176'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000176','006')
WHERE   vp.PUERTO = 'SPIA'  AND (vp.iso_ctn LIKE '4%' ) AND vp.adictional_layer_kraft_40 != 0
		AND (vp.STATUS_ID=3 or vp.STATUS_ID=9 or vp.STATUS_ID=10)
        AND (  DATE(vp.FECHA_REGISTROS) BETWEEN @FECHA_INF AND @FECHA_SUP  )
ORDER BY vp.EXPORTADOR ASC
);


/*------------------- ADICIONALES SPIA ISO 40 PAPEL CARTON   -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    vp.LOTES AS LOTE,
    vp.adictional_layer_carton_40 AS CANTIDAD_OPERATIVA,
    vp.TIPO_EMBALAJE AS TIPO_SACOS,
    vp.MODALIDAD AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.FECHA_REGISTROS AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.OIE AS OIE, 
    vp.BIC_CTN AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    vp.type_papper AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    vp.adictional_layer_carton_40 AS CAPAS_ADICIONALES_CARTON_40,
    vp.ESTADO AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "835000149" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    "CAPAS ADICIONALES DE CONTENEDORES EN TERMINAL SPIA TIPO CARTON DE LONGITUD 40" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    (114*vp.adictional_layer_carton_40)*@TRM AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging2 AS vp 
INNER JOIN clients AS cts ON cts.business_name = vp.EXPORTADOR
inner join items_unoee pds on pds.codigo='0000178'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000178','001')
WHERE   vp.PUERTO = 'SPIA'  AND (vp.iso_ctn LIKE '4%' ) AND vp.adictional_layer_carton_40 != 0
		AND (vp.STATUS_ID=3 or vp.STATUS_ID=9 or vp.STATUS_ID=10)
        AND (  DATE(vp.FECHA_REGISTROS) BETWEEN @FECHA_INF AND @FECHA_SUP  )
ORDER BY vp.EXPORTADOR ASC
);


/*-------------------  COLOCACION DE BOLSAS TSL ADICIONAL SPIA    -------------------*/
INSERT INTO data_billing_full (
SELECT 
	NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    vp.LOTES AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.TIPO_EMBALAJE AS TIPO_SACOS,
    vp.MODALIDAD AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.FECHA_REGISTROS AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.OIE AS OIE, 
    vp.BIC_CTN AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.ESTADO AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "835000149" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    "COLOCACION DE BOLSAS TSL ADICIONAL AL CONTENEDOR EN TERMINAL SPIA" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    (1*30)*@TRM AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging2 AS vp 
INNER JOIN clients AS cts ON cts.business_name = vp.EXPORTADOR
inner join items_unoee pds on pds.codigo='0000254'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000254','001')
WHERE   vp.PUERTO = 'SPIA'  AND vp.tsl != 0
		AND (vp.STATUS_ID=3 or vp.STATUS_ID=9 or vp.STATUS_ID=10)
        AND (  DATE(vp.FECHA_REGISTROS) BETWEEN @FECHA_INF AND @FECHA_SUP  )
ORDER BY vp.EXPORTADOR ASC
); 


/*-------------------  COLOCACION DE BOLSAS TSL CON DESECANTES SPIA   -------------------*/
INSERT INTO data_billing_full (
SELECT 
	NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    vp.LOTES AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.TIPO_EMBALAJE AS TIPO_SACOS,
    vp.MODALIDAD AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.FECHA_REGISTROS AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.OIE AS OIE, 
    vp.BIC_CTN AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.ESTADO AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "835000149" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    "COLOCACION DE BOLSAS TSL CON DESECANTES ADICIONAL AL CONTENEDOR EN TERMINAL SPIA" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    (1*45)*@TRM AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging2 AS vp 
INNER JOIN clients AS cts ON cts.business_name = vp.EXPORTADOR
inner join items_unoee pds on pds.codigo='0000284'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000284','001')
WHERE   vp.PUERTO = 'SPIA'  AND vp.tls_desiccant != 0
		AND (vp.STATUS_ID=3 or vp.STATUS_ID=9 or vp.STATUS_ID=10)
        AND (  DATE(vp.FECHA_REGISTROS) BETWEEN @FECHA_INF AND @FECHA_SUP  )
ORDER BY vp.EXPORTADOR ASC
); 


/*-------------------  INSTALACION HC SPIA   -------------------*/
INSERT INTO data_billing_full (
SELECT 
	NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    vp.LOTES AS LOTE,
    vp.hc_qta AS CANTIDAD_OPERATIVA,
    vp.TIPO_EMBALAJE AS TIPO_SACOS,
    vp.MODALIDAD AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.FECHA_REGISTROS AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.OIE AS OIE, 
    vp.BIC_CTN AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.ESTADO AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "835000149" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    "INSTALACION HC ADICIONAL AL CONTENEDOR EN TERMINAL SPIA" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    (1*45)*@TRM AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging2 AS vp 
INNER JOIN clients AS cts ON cts.business_name = vp.EXPORTADOR
inner join items_unoee pds on pds.codigo='0000271'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000271','001')
WHERE   vp.PUERTO = 'SPIA'  AND vp.hc_qta != 0
		AND (vp.STATUS_ID=3 or vp.STATUS_ID=9 or vp.STATUS_ID=10)
        AND (  DATE(vp.FECHA_REGISTROS) BETWEEN @FECHA_INF AND @FECHA_SUP  )
ORDER BY vp.EXPORTADOR ASC
); 



/*------------------- VACIADOS CONTENEDOR SPIA TIPO SACOS DE 20  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,  
    vp.LOTES AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.TIPO_EMBALAJE AS TIPO_SACOS,
    vp.MODALIDAD AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.FECHA_REGISTROS AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.OIE AS OIE, 
    vp.BIC_CTN AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.ESTADO AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "835000149" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    "VACIADOS DE CONTENEDORES EN TERMINAL SPIA TIPO SACOS DE LONGITUD 20" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    (lp.precio*1)*@TRM AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging2 AS vp 
INNER JOIN clients AS cts ON cts.business_name = vp.EXPORTADOR
inner join items_unoee pds on pds.codigo='0000044'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000044','006')
WHERE   vp.puerto = 'SPIA' AND vp.MODALIDAD LIKE 'SACOS%' AND (vp.iso_ctn LIKE '2%' ) 
		AND (vp.STATUS_ID=4 or vp.STATUS_ID=9) 
        AND (  DATE(vp.FECHA_REGISTROS) BETWEEN @FECHA_INF AND @FECHA_SUP  )
	ORDER BY vp.EXPORTADOR ASC
); 


/*------------------- VACIADOS CONTENEDOR SPIA TIPO GRANEL DE 20  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,  
    vp.LOTES AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.TIPO_EMBALAJE AS TIPO_SACOS,
    vp.MODALIDAD AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.FECHA_REGISTROS AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.OIE AS OIE, 
    vp.BIC_CTN AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.ESTADO AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "835000149" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    "VACIADOS DE CONTENEDORES EN TERMINAL SPIA TIPO GRANEL DE LONGITUD 20" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging2 AS vp 
INNER JOIN clients AS cts ON cts.business_name = vp.EXPORTADOR
inner join items_unoee pds on pds.codigo='0000171'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000171','002')
WHERE   vp.puerto = 'SPIA' AND vp.MODALIDAD LIKE 'GRANEL%' AND (vp.iso_ctn LIKE '2%' ) 
		AND (vp.STATUS_ID=4 or vp.STATUS_ID=9) 
        AND (  DATE(vp.FECHA_REGISTROS) BETWEEN @FECHA_INF AND @FECHA_SUP  )
ORDER BY vp.EXPORTADOR ASC
); 


/*------------------- VACIADOS CONTENEDOR SPIA TIPO BIG BAG DE 20  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,  
    vp.LOTES AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.TIPO_EMBALAJE AS TIPO_SACOS,
    vp.MODALIDAD AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.FECHA_REGISTROS AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.OIE AS OIE, 
    vp.BIC_CTN AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.ESTADO AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "835000149" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    "VACIADOS DE CONTENEDORES EN TERMINAL SPIA TIPO BIG BAGS DE LONGITUD 20" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging2 AS vp 
INNER JOIN clients AS cts ON cts.business_name = vp.EXPORTADOR
inner join items_unoee pds on pds.codigo='0000174'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000174','002')
WHERE   vp.puerto = 'SPIA' AND vp.MODALIDAD LIKE 'BIG BAG%' AND (vp.iso_ctn LIKE '2%' ) 
		AND (vp.STATUS_ID=4 or vp.STATUS_ID=9) 
        AND (  DATE(vp.FECHA_REGISTROS) BETWEEN @FECHA_INF AND @FECHA_SUP  )
ORDER BY vp.EXPORTADOR ASC
); 


/*------------------- VACIADOS CONTENEDOR SPIA TIPO CACAO DE 20  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,  
    vp.LOTES AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.TIPO_EMBALAJE AS TIPO_SACOS,
    vp.MODALIDAD AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.FECHA_REGISTROS AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.OIE AS OIE, 
    vp.BIC_CTN AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.ESTADO AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "835000149" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    "VACIADOS DE CONTENEDORES EN TERMINAL SPIA TIPO CACAO DE LONGITUD 20" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging2 AS vp 
INNER JOIN clients AS cts ON cts.business_name = vp.EXPORTADOR
inner join items_unoee pds on pds.codigo='0000293'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000293','001')
WHERE   vp.puerto = 'SPIA' AND vp.MODALIDAD LIKE 'CACAO%' AND (vp.iso_ctn LIKE '2%' ) 
		AND (vp.STATUS_ID=4 or vp.STATUS_ID=9) 
        AND (  DATE(vp.FECHA_REGISTROS) BETWEEN @FECHA_INF AND @FECHA_SUP  )
ORDER BY vp.EXPORTADOR ASC
); 


/*------------------- VACIADOS CONTENEDOR SPIA TIPO SACOS DE 40  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,  
    vp.LOTES AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.TIPO_EMBALAJE AS TIPO_SACOS,
    vp.MODALIDAD AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.FECHA_REGISTROS AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.OIE AS OIE, 
    vp.BIC_CTN AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.ESTADO AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "835000149" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    "VACIADOS DE CONTENEDORES EN TERMINAL SPIA TIPO SACOS DE LONGITUD 40" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    (lp.precio*1)*@TRM AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging2 AS vp 
INNER JOIN clients AS cts ON cts.business_name = vp.EXPORTADOR
inner join items_unoee pds on pds.codigo='0000081'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000081','006')
WHERE   vp.puerto = 'SPIA' AND vp.MODALIDAD LIKE 'SACOS%' AND (vp.iso_ctn LIKE '4%' ) 
		AND (vp.STATUS_ID=4 or vp.STATUS_ID=9) 
        AND (  DATE(vp.FECHA_REGISTROS) BETWEEN @FECHA_INF AND @FECHA_SUP  )
ORDER BY vp.EXPORTADOR ASC
); 


/*------------------- VACIADOS CONTENEDOR SPIA TIPO BIG BAG DE 40  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,  
    vp.LOTES AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.TIPO_EMBALAJE AS TIPO_SACOS,
    vp.MODALIDAD AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.FECHA_REGISTROS AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.OIE AS OIE, 
    vp.BIC_CTN AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.ESTADO AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "835000149" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    "VACIADOS DE CONTENEDORES EN TERMINAL SPIA TIPO BIG BAGS DE LONGITUD 40" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging2 AS vp 
INNER JOIN clients AS cts ON cts.business_name = vp.EXPORTADOR
inner join items_unoee pds on pds.codigo="0000174"
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=("0000174",'002')
WHERE   vp.puerto = 'SPIA' AND vp.MODALIDAD LIKE 'BIG BAG%' AND (vp.iso_ctn LIKE '4%' ) 
		AND (vp.STATUS_ID=4 or vp.STATUS_ID=9) 
        AND (  DATE(vp.FECHA_REGISTROS) BETWEEN @FECHA_INF AND @FECHA_SUP  )
ORDER BY vp.EXPORTADOR ASC
); 


/*------------------- VACIADOS CONTENEDOR SPIA TIPO CACAO DE 40  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,  
    vp.LOTES AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.TIPO_EMBALAJE AS TIPO_SACOS,
    vp.MODALIDAD AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.FECHA_REGISTROS AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.OIE AS OIE, 
    vp.BIC_CTN AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.ESTADO AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "835000149" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    "VACIADOS DE CONTENEDORES EN TERMINAL SPIA TIPO CACAO DE LONGITUD 40" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging2 AS vp 
INNER JOIN clients AS cts ON cts.business_name = vp.EXPORTADOR
inner join items_unoee pds on pds.codigo="0000290"
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=("0000290",'001')
WHERE   vp.puerto = 'SPIA' AND vp.MODALIDAD LIKE 'CACAO%' AND (vp.iso_ctn LIKE '4%' ) 
		AND (vp.STATUS_ID=4 or vp.STATUS_ID=9) 
        AND (  DATE(vp.FECHA_REGISTROS) BETWEEN @FECHA_INF AND @FECHA_SUP  )
ORDER BY vp.EXPORTADOR ASC
); 





























/*------------------- EMBALAJE CONTENEDOR STM TIPO GRANEL DE 20 -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    vp.LOTES AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.TIPO_EMBALAJE AS TIPO_SACOS,
    vp.MODALIDAD AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.FECHA_REGISTROS AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.OIE AS OIE, 
    vp.BIC_CTN AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    vp.type_papper AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.ESTADO AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "900114897" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    concat("EMBALAJE DE CONTENEDORES EN PUERTO DE SANTA MARTA TIPO GRANEL DE LONGITUD 20") AS DESC_CONSULTA,
	pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging2 AS vp 
INNER JOIN clients AS cts ON cts.business_name = vp.EXPORTADOR
inner join items_unoee pds on pds.codigo='0000020'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000020','003')
WHERE   (vp.PUERTO = 'STA' OR vp.PUERTO = 'STM') 
        AND vp.MODALIDAD LIKE 'GRANEL%' AND (vp.iso_ctn LIKE '2%' ) 
        AND (vp.STATUS_ID=3 or vp.STATUS_ID=9 or vp.STATUS_ID=10)
        AND (  DATE(vp.FECHA_REGISTROS) BETWEEN @FECHA_INF AND @FECHA_SUP  )
ORDER BY vp.EXPORTADOR ASC
); 



/*------------------- EMBALAJE CONTENEDOR STM TIPO BIG BAG DE 20  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    vp.LOTES AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.TIPO_EMBALAJE AS TIPO_SACOS,
    vp.MODALIDAD AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.FECHA_REGISTROS AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.OIE AS OIE, 
    vp.BIC_CTN AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    vp.type_papper AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.ESTADO AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "900114897" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    concat("EMBALAJE DE CONTENEDORES EN PUERTO DE SANTA MARTA TIPO BIG BAGS DE LONGITUD 20") AS DESC_CONSULTA,
	pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging2 AS vp 
INNER JOIN clients AS cts ON cts.business_name = vp.EXPORTADOR
inner join items_unoee pds on pds.codigo='0000031'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000031','003')
WHERE   (vp.PUERTO = 'STA' OR vp.PUERTO = 'STM') 
        AND vp.MODALIDAD LIKE 'BIG BAG%' AND (vp.iso_ctn LIKE '2%' ) 
        AND (vp.STATUS_ID=3 or vp.STATUS_ID=9 or vp.STATUS_ID=10)
        AND (  DATE(vp.FECHA_REGISTROS) BETWEEN @FECHA_INF AND @FECHA_SUP  )
ORDER BY vp.EXPORTADOR ASC
); 



/*------------------- EMBALAJE CONTENEDOR STM TIPO CACAO DE 20  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    vp.LOTES AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.TIPO_EMBALAJE AS TIPO_SACOS,
    vp.MODALIDAD AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.FECHA_REGISTROS AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.OIE AS OIE, 
    vp.BIC_CTN AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    vp.type_papper AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.ESTADO AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "900114897" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    concat("EMBALAJE DE CONTENEDORES EN PUERTO DE SANTA MARTA TIPO CACAO DE LONGITUD 20") AS DESC_CONSULTA,
	pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging2 AS vp 
INNER JOIN clients AS cts ON cts.business_name = vp.EXPORTADOR
inner join items_unoee pds on pds.codigo='0000050'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000050','003')
WHERE   (vp.PUERTO = 'STA' OR vp.PUERTO = 'STM') 
        AND vp.MODALIDAD LIKE 'CACAO%' AND (vp.iso_ctn LIKE '2%' ) 
        AND (vp.STATUS_ID=3 or vp.STATUS_ID=9 or vp.STATUS_ID=10)
        AND (  DATE(vp.FECHA_REGISTROS) BETWEEN @FECHA_INF AND @FECHA_SUP  )
ORDER BY vp.EXPORTADOR ASC
); 

/*------------------- EMBALAJE STM CONTENEDORES TIPO REFRIGERADOS  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,  
    vp.LOTES AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.TIPO_EMBALAJE AS TIPO_SACOS,
    vp.MODALIDAD AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.FECHA_REGISTROS AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.OIE AS OIE, 
    vp.BIC_CTN AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    vp.type_papper AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.ESTADO AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "900114897" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,  
    "EMBALAJE DE CONTENEDORES EN TERMINAL STM TIPO REFRIGERADO" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging2 AS vp 
INNER JOIN clients AS cts ON cts.business_name = vp.EXPORTADOR
inner join items_unoee pds on pds.codigo='0000286'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000286','003')
WHERE   (vp.PUERTO = 'STA' OR vp.PUERTO = 'STM')  AND (vp.STATUS_ID=3 or vp.STATUS_ID=9 or vp.STATUS_ID=10)
        AND vp.type_papper = "REF-CTN"
        AND (  DATE(vp.FECHA_REGISTROS) BETWEEN @FECHA_INF AND @FECHA_SUP  )
ORDER BY vp.EXPORTADOR ASC
); 






/*------------------- EMBALAJE CONTENEDOR STM TIPO BIG BAG DE 40  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    vp.LOTES AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.TIPO_EMBALAJE AS TIPO_SACOS,
    vp.MODALIDAD AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.FECHA_REGISTROS AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.OIE AS OIE, 
    vp.BIC_CTN AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    vp.type_papper AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.ESTADO AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "900114897" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    concat("EMBALAJE DE CONTENEDORES EN PUERTO DE SANTA MARTA TIPO BIG BAGS DE LONGITUD 40") AS DESC_CONSULTA,
	pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging2 AS vp 
INNER JOIN clients AS cts ON cts.business_name = vp.EXPORTADOR
inner join items_unoee pds on pds.codigo='0000073'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000073','003')
WHERE   (vp.PUERTO = 'STA' OR vp.PUERTO = 'STM') 
        AND vp.MODALIDAD LIKE 'BIG BAG%' AND (vp.iso_ctn LIKE '4%' ) 
        AND (vp.STATUS_ID=3 or vp.STATUS_ID=9 or vp.STATUS_ID=10)
        AND (  DATE(vp.FECHA_REGISTROS) BETWEEN @FECHA_INF AND @FECHA_SUP  )
ORDER BY vp.EXPORTADOR ASC
); 


/*------------------- EMBALAJE CONTENEDOR STM TIPO CACAO DE 40  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    vp.LOTES AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.TIPO_EMBALAJE AS TIPO_SACOS,
    vp.MODALIDAD AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.FECHA_REGISTROS AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.OIE AS OIE, 
    vp.BIC_CTN AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    vp.type_papper AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.ESTADO AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "900114897" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    concat("EMBALAJE DE CONTENEDORES EN PUERTO DE SANTA MARTA TIPO CACAO DE LONGITUD 40") AS DESC_CONSULTA,
	pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging2 AS vp 
INNER JOIN clients AS cts ON cts.business_name = vp.EXPORTADOR
inner join items_unoee pds on pds.codigo='0000074'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000074','003')
WHERE   (vp.PUERTO = 'STA' OR vp.PUERTO = 'STM') 
        AND vp.MODALIDAD LIKE 'CACAO%' AND (vp.iso_ctn LIKE '4%' ) 
        AND (vp.STATUS_ID=3 or vp.STATUS_ID=9 or vp.STATUS_ID=10)
        AND (  DATE(vp.FECHA_REGISTROS) BETWEEN @FECHA_INF AND @FECHA_SUP  )
ORDER BY vp.EXPORTADOR ASC
); 



/*------------------- ADICIONALES STM ISO 20 PAPEL KRAFT   -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    vp.LOTES AS LOTE,
    vp.adictional_layer_kraft_20 AS CANTIDAD_OPERATIVA,
    vp.TIPO_EMBALAJE AS TIPO_SACOS,
    vp.MODALIDAD AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.FECHA_REGISTROS AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.OIE AS OIE, 
    vp.BIC_CTN AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    vp.type_papper AS TIPO_PAPEL,
    vp.adictional_layer_kraft_20 AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.ESTADO AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "900114897" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    concat("CAPAS ADICIONALES DE CONTENEDORES EN PUERTO DE SANTA MARTA TIPO KRAFT DE LONGITUD 20") AS DESC_CONSULTA,
	pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*vp.adictional_layer_kraft_20 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging2 AS vp 
INNER JOIN clients AS cts ON cts.business_name = vp.EXPORTADOR
inner join items_unoee pds on pds.codigo='0000156'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000156','003')
WHERE   (vp.PUERTO = 'STA' OR vp.PUERTO = 'STM') 
        AND (vp.iso_ctn LIKE '2%' ) AND vp.adictional_layer_kraft_20 != 0
        AND (vp.STATUS_ID=3 or vp.STATUS_ID=9 or vp.STATUS_ID=10)
        AND (  DATE(vp.FECHA_REGISTROS) BETWEEN @FECHA_INF AND @FECHA_SUP  )
ORDER BY vp.EXPORTADOR ASC
);


/*------------------- ADICIONALES STM ISO 20 PAPEL CARTON   -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    vp.LOTES AS LOTE,
    vp.adictional_layer_carton_20 AS CANTIDAD_OPERATIVA,
    vp.TIPO_EMBALAJE AS TIPO_SACOS,
    vp.MODALIDAD AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.FECHA_REGISTROS AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.OIE AS OIE, 
    vp.BIC_CTN AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    vp.type_papper AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    vp.adictional_layer_carton_20 AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.ESTADO AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "900114897" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    concat("CAPAS ADICIONALES DE CONTENEDORES EN PUERTO DE SANTA MARTA TIPO CARTON DE LONGITUD 20") AS DESC_CONSULTA,
	pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*vp.adictional_layer_carton_20 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging2 AS vp 
INNER JOIN clients AS cts ON cts.business_name = vp.EXPORTADOR
inner join items_unoee pds on pds.codigo='0000158'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000158','003')
WHERE   (vp.PUERTO = 'STA' OR vp.PUERTO = 'STM') 
        AND (vp.iso_ctn LIKE '2%' ) AND vp.adictional_layer_carton_20 != 0
        AND (vp.STATUS_ID=3 or vp.STATUS_ID=9 or vp.STATUS_ID=10)
        AND (  DATE(vp.FECHA_REGISTROS) BETWEEN @FECHA_INF AND @FECHA_SUP  )
ORDER BY vp.EXPORTADOR ASC
);


/*------------------- ADICIONALES STM ISO 40 PAPEL KRAFT   -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    vp.LOTES AS LOTE,
    vp.adictional_layer_kraft_40 AS CANTIDAD_OPERATIVA,
    vp.TIPO_EMBALAJE AS TIPO_SACOS,
    vp.MODALIDAD AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.FECHA_REGISTROS AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.OIE AS OIE, 
    vp.BIC_CTN AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    vp.type_papper AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    vp.adictional_layer_kraft_40 AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.ESTADO AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "900114897" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    concat("CAPAS ADICIONALES DE CONTENEDORES EN PUERTO DE SANTA MARTA TIPO KRAFT DE LONGITUD 40") AS DESC_CONSULTA,
	pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*vp.adictional_layer_kraft_40 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging2 AS vp 
INNER JOIN clients AS cts ON cts.business_name = vp.EXPORTADOR
inner join items_unoee pds on pds.codigo='0000176'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000176','003')
WHERE   (vp.PUERTO = 'STA' OR vp.PUERTO = 'STM') 
        AND (vp.iso_ctn LIKE '4%' ) AND vp.adictional_layer_kraft_40 != 0
        AND (vp.STATUS_ID=3 or vp.STATUS_ID=9 or vp.STATUS_ID=10)
        AND (  DATE(vp.FECHA_REGISTROS) BETWEEN @FECHA_INF AND @FECHA_SUP  )
ORDER BY vp.EXPORTADOR ASC
);


/*------------------- ADICIONALES STM ISO 40 PAPEL CARTON   -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    vp.LOTES AS LOTE,
    vp.adictional_layer_carton_40 AS CANTIDAD_OPERATIVA,
    vp.TIPO_EMBALAJE AS TIPO_SACOS,
    vp.MODALIDAD AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.FECHA_REGISTROS AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.OIE AS OIE, 
    vp.BIC_CTN AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    vp.type_papper AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    vp.adictional_layer_carton_40 AS CAPAS_ADICIONALES_CARTON_40,
    vp.ESTADO AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "900114897" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    concat("CAPAS ADICIONALES DE CONTENEDORES EN PUERTO DE SANTA MARTA TIPO CARTON DE LONGITUD 40") AS DESC_CONSULTA,
	pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*vp.adictional_layer_carton_40 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging2 AS vp 
INNER JOIN clients AS cts ON cts.business_name = vp.EXPORTADOR
inner join items_unoee pds on pds.codigo='0000178'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000178','003')
WHERE   (vp.PUERTO = 'STA' OR vp.PUERTO = 'STM') 
        AND (vp.iso_ctn LIKE '4%' ) AND vp.adictional_layer_carton_40 != 0
        AND (vp.STATUS_ID=3 or vp.STATUS_ID=9 or vp.STATUS_ID=10)
        AND (  DATE(vp.FECHA_REGISTROS) BETWEEN @FECHA_INF AND @FECHA_SUP  )
ORDER BY vp.EXPORTADOR ASC
);



/*-------------------  COLOCACION DE BOLSAS TSL ADICIONAL STM    -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    vp.LOTES AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.TIPO_EMBALAJE AS TIPO_SACOS,
    vp.MODALIDAD AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.FECHA_REGISTROS AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.OIE AS OIE, 
    vp.BIC_CTN AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.ESTADO AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "900114897" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    concat("COLOCACION DE BOLSAS TSL ADICIONAL AL CONTENEDOR EN PUERTO SANTA MARTA") AS DESC_CONSULTA,
	pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging2 AS vp 
INNER JOIN clients AS cts ON cts.business_name = vp.EXPORTADOR
inner join items_unoee pds on pds.codigo='0000254'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000254','003')
WHERE   (vp.PUERTO = 'STA' OR vp.PUERTO = 'STM') 
        AND vp.tsl != 0  AND (vp.STATUS_ID=3 or vp.STATUS_ID=9 or vp.STATUS_ID=10)
        AND (  DATE(vp.FECHA_REGISTROS) BETWEEN @FECHA_INF AND @FECHA_SUP  )
ORDER BY vp.EXPORTADOR ASC
); 


/*-------------------  COLOCACION DE BOLSAS TSL CON DESECANTES STM   -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    vp.LOTES AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.TIPO_EMBALAJE AS TIPO_SACOS,
    vp.MODALIDAD AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.FECHA_REGISTROS AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.OIE AS OIE, 
    vp.BIC_CTN AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.ESTADO AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "900114897" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    concat("COLOCACION DE BOLSAS TSL CON DESECANTES ADICIONAL AL CONTENEDOR EN PUERTO DE SANTA MARTA") AS DESC_CONSULTA,
	pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging2 AS vp 
INNER JOIN clients AS cts ON cts.business_name = vp.EXPORTADOR
inner join items_unoee pds on pds.codigo='0000284'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000284','003')
WHERE   (vp.PUERTO = 'STA' OR vp.PUERTO = 'STM') 
        AND vp.tls_desiccant != 0 AND (vp.STATUS_ID=3 or vp.STATUS_ID=9 or vp.STATUS_ID=10)
        AND (  DATE(vp.FECHA_REGISTROS) BETWEEN @FECHA_INF AND @FECHA_SUP  )
ORDER BY vp.EXPORTADOR ASC
); 


/*-------------------  INSTALACION HC STM   -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    vp.LOTES AS LOTE,
    vp.hc_qta AS CANTIDAD_OPERATIVA,
    vp.TIPO_EMBALAJE AS TIPO_SACOS,
    vp.MODALIDAD AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.FECHA_REGISTROS AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.OIE AS OIE, 
    vp.BIC_CTN AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.ESTADO AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "900114897" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    concat("INSTALACION HC ADICIONAL AL CONTENEDOR EN PUERTO DE SANTA MARTA") AS DESC_CONSULTA,
	pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging2 AS vp 
INNER JOIN clients AS cts ON cts.business_name = vp.EXPORTADOR
inner join items_unoee pds on pds.codigo='0000271'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000271','003')
WHERE   (vp.PUERTO = 'STA' OR vp.PUERTO = 'STM') 
        AND vp.hc_qta != 0 AND (vp.STATUS_ID=3 or vp.STATUS_ID=9 or vp.STATUS_ID=10)
        AND (  DATE(vp.FECHA_REGISTROS) BETWEEN @FECHA_INF AND @FECHA_SUP  )
ORDER BY vp.EXPORTADOR ASC
); 



/*------------------- VACIADOS CONTENEDOR STM TIPO SACOS DE 20  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    vp.LOTES AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.TIPO_EMBALAJE AS TIPO_SACOS,
    vp.MODALIDAD AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.FECHA_REGISTROS AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.OIE AS OIE, 
    vp.BIC_CTN AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.ESTADO AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "900114897" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    concat("VACIADOS DE CONTENEDOR EN PUERTO DE SANTA MARTA TIPO SACOS DE LONGITUD 20") AS DESC_CONSULTA,
	pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging2 AS vp 
INNER JOIN clients AS cts ON cts.business_name = vp.EXPORTADOR
inner join items_unoee pds on pds.codigo='0000044'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000044','003')
WHERE   (vp.PUERTO = 'STA' OR vp.PUERTO = 'STM')  
        AND vp.MODALIDAD LIKE 'SACOS%' AND (vp.iso_ctn LIKE '2%' ) 
        AND (vp.STATUS_ID=4 or vp.STATUS_ID=9) 
        AND (  DATE(vp.FECHA_REGISTROS) BETWEEN @FECHA_INF AND @FECHA_SUP  )
    ORDER BY vp.EXPORTADOR ASC
); 



/*------------------- VACIADOS CONTENEDOR STM TIPO SACOS DE 40  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    vp.LOTES AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.TIPO_EMBALAJE AS TIPO_SACOS,
    vp.MODALIDAD AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.FECHA_REGISTROS AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.OIE AS OIE, 
    vp.BIC_CTN AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.ESTADO AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "900114897" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    concat("VACIADOS DE CONTENEDOR EN PUERTO DE SANTA MARTA TIPO SACOS DE LONGITUD 40") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging2 AS vp 
INNER JOIN clients AS cts ON cts.business_name = vp.EXPORTADOR
inner join items_unoee pds on pds.codigo='0000081'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000081','003')
WHERE   (vp.PUERTO = 'STA' OR vp.PUERTO = 'STM')  
        AND vp.MODALIDAD LIKE 'SACOS%' AND (vp.iso_ctn LIKE '4%') 
        AND (vp.STATUS_ID=4 or vp.STATUS_ID=9) 
        AND (  DATE(vp.FECHA_REGISTROS) BETWEEN @FECHA_INF AND @FECHA_SUP  )
    ORDER BY vp.EXPORTADOR ASC
); 



/*------------------- VACIADOS CONTENEDOR STM TIPO GRANEL DE 20  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    vp.LOTES AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.TIPO_EMBALAJE AS TIPO_SACOS,
    vp.MODALIDAD AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.FECHA_REGISTROS AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.OIE AS OIE, 
    vp.BIC_CTN AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.ESTADO AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    "900114897" AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,
    concat("VACIADOS DE CONTENEDOR EN PUERTO DE SANTA MARTA TIPO GRANEL DE LONGITUD 20") AS DESC_CONSULTA,
	pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging2 AS vp 
INNER JOIN clients AS cts ON cts.business_name = vp.EXPORTADOR
inner join items_unoee pds on pds.codigo='0000043'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000043','003')
WHERE   (vp.PUERTO = 'STA' OR vp.PUERTO = 'STM')  
        AND vp.MODALIDAD LIKE 'GRANEL%' AND (vp.iso_ctn LIKE '2%' ) 
        AND (vp.STATUS_ID=4 or vp.STATUS_ID=9) 
        AND (  DATE(vp.FECHA_REGISTROS) BETWEEN @FECHA_INF AND @FECHA_SUP  )
    ORDER BY vp.EXPORTADOR ASC
); 


























/*------------------- EMBALAJE CONTENEDOR CTG TIPO SACOS  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    vp.LOTES AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.TIPO_EMBALAJE AS TIPO_SACOS,
    vp.MODALIDAD AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.FECHA_REGISTROS AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.OIE AS OIE, 
    vp.BIC_CTN AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    vp.type_papper AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.ESTADO AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    IF(vp.PUERTO="SPRC", "800200969", 
        IF(vp.PUERTO="CONTECAR", "800116164", 
            IF(vp.PUERTO="BLOCPORT", "NULL",             
                IF(vp.PUERTO="COMPAS", "900915647", "800200969")
            )
        )
    ) AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    concat("EMBALAJE DE CONTENEDORES EN PUERTO DE CARTAGENA TERMINAL ",vp.PUERTO," TIPO SACOS") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging2 AS vp 
INNER JOIN clients AS cts ON cts.business_name = vp.EXPORTADOR
inner join items_unoee pds on pds.codigo='0000130'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000130','002')
WHERE   (vp.PUERTO = 'SPRC' OR vp.PUERTO = 'CONTECAR' OR vp.PUERTO = 'COMPAS' OR vp.PUERTO LIKE '%BLOC%' OR vp.PUERTO = 'CTG') 
        AND vp.MODALIDAD LIKE 'SACOS%' AND (vp.iso_ctn LIKE '2%' ) 
        AND (vp.STATUS_ID=3 or vp.STATUS_ID=9 or vp.STATUS_ID=10)
        AND (  DATE(vp.FECHA_REGISTROS) BETWEEN @FECHA_INF AND @FECHA_SUP  )
ORDER BY vp.EXPORTADOR ASC
); 


/*------------------- EMBALAJE CONTENEDOR CTG TIPO GRANEL -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    vp.LOTES AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.TIPO_EMBALAJE AS TIPO_SACOS,
    vp.MODALIDAD AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.FECHA_REGISTROS AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.OIE AS OIE, 
    vp.BIC_CTN AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    vp.type_papper AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.ESTADO AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    IF(vp.PUERTO="SPRC", "800200969", 
        IF(vp.PUERTO="CONTECAR", "800116164", 
            IF(vp.PUERTO="BLOCPORT", "NULL",             
                IF(vp.PUERTO="COMPAS", "900915647", "800200969")
            )
        )
    ) AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    concat("EMBALAJE DE CONTENEDORES EN PUERTO DE CARTAGENA TERMINAL ",vp.PUERTO," TIPO GRANEL") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging2 AS vp 
INNER JOIN clients AS cts ON cts.business_name = vp.EXPORTADOR
inner join items_unoee pds on pds.codigo='0000129'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000129','002')
WHERE   (vp.PUERTO = 'SPRC' OR vp.PUERTO = 'CONTECAR' OR vp.PUERTO = 'COMPAS' OR vp.PUERTO LIKE '%BLOC%' OR vp.PUERTO = 'CTG') 
        AND vp.MODALIDAD LIKE 'GRANEL%' AND (vp.iso_ctn LIKE '2%' )
        AND (vp.STATUS_ID=3 or vp.STATUS_ID=9 or vp.STATUS_ID=10)
        AND (  DATE(vp.FECHA_REGISTROS) BETWEEN @FECHA_INF AND @FECHA_SUP  )
ORDER BY vp.EXPORTADOR ASC
); 



/*------------------- EMBALAJE CONTENEDOR CTG TIPO BIG BAG  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    vp.LOTES AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.TIPO_EMBALAJE AS TIPO_SACOS,
    vp.MODALIDAD AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.FECHA_REGISTROS AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.OIE AS OIE, 
    vp.BIC_CTN AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    vp.type_papper AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.ESTADO AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    IF(vp.PUERTO="SPRC", "800200969", 
        IF(vp.PUERTO="CONTECAR", "800116164", 
            IF(vp.PUERTO="BLOCPORT", "NULL",             
                IF(vp.PUERTO="COMPAS", "900915647", "800200969")
            )
        )
    ) AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    concat("EMBALAJE DE CONTENEDORES EN PUERTO DE CARTAGENA TERMINAL ",vp.PUERTO," TIPO BIG BAGS") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging2 AS vp 
INNER JOIN clients AS cts ON cts.business_name = vp.EXPORTADOR
inner join items_unoee pds on pds.codigo='0000136'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000136','002')
WHERE   (vp.PUERTO = 'SPRC' OR vp.PUERTO = 'CONTECAR' OR vp.PUERTO = 'COMPAS' OR vp.PUERTO LIKE '%BLOC%' OR vp.PUERTO = 'CTG') 
        AND vp.MODALIDAD LIKE 'BIG BAG%' AND (vp.iso_ctn LIKE '2%' ) 
        AND (vp.STATUS_ID=3 or vp.STATUS_ID=9 or vp.STATUS_ID=10)
        AND (  DATE(vp.FECHA_REGISTROS) BETWEEN @FECHA_INF AND @FECHA_SUP  )
ORDER BY vp.EXPORTADOR ASC
); 


/*------------------- EMBALAJE CONTENEDOR CTG TIPO CACAO  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    vp.LOTES AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.TIPO_EMBALAJE AS TIPO_SACOS,
    vp.MODALIDAD AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.FECHA_REGISTROS AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.OIE AS OIE, 
    vp.BIC_CTN AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    vp.type_papper AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.ESTADO AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    IF(vp.PUERTO="SPRC", "800200969", 
        IF(vp.PUERTO="CONTECAR", "800116164", 
            IF(vp.PUERTO="BLOCPORT", "NULL",             
                IF(vp.PUERTO="COMPAS", "900915647", "800200969")
            )
        )
    ) AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    concat("EMBALAJE DE CONTENEDORES EN PUERTO DE CARTAGENA TERMINAL ",vp.PUERTO," TIPO CACAO") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging2 AS vp 
INNER JOIN clients AS cts ON cts.business_name = vp.EXPORTADOR
inner join items_unoee pds on pds.codigo='0000135'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000135','002')
WHERE   (vp.PUERTO = 'SPRC' OR vp.PUERTO = 'CONTECAR' OR vp.PUERTO = 'COMPAS' OR vp.PUERTO LIKE '%BLOC%' OR vp.PUERTO = 'CTG') 
        AND vp.MODALIDAD LIKE 'CACAO%' AND (vp.iso_ctn LIKE '2%' ) 
        AND (vp.STATUS_ID=3 or vp.STATUS_ID=9 or vp.STATUS_ID=10)
        AND (  DATE(vp.FECHA_REGISTROS) BETWEEN @FECHA_INF AND @FECHA_SUP  )
ORDER BY vp.EXPORTADOR ASC
); 


/*------------------- EMBALAJE CTG CONTENEDORES TIPO REFRIGERADOS  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,  
    vp.LOTES AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.TIPO_EMBALAJE AS TIPO_SACOS,
    vp.MODALIDAD AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.FECHA_REGISTROS AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.OIE AS OIE, 
    vp.BIC_CTN AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    vp.type_papper AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.ESTADO AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    IF(vp.PUERTO="SPRC", "800200969", 
        IF(vp.PUERTO="CONTECAR", "800116164", 
            IF(vp.PUERTO="BLOCPORT", "NULL",             
                IF(vp.PUERTO="COMPAS", "900915647", "800200969")
            )
        )
    ) AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,  
    "EMBALAJE DE CONTENEDORES EN PUERTO DE CARTAGENA TERMINAL TIPO REFRIGERADO" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging2 AS vp 
INNER JOIN clients AS cts ON cts.business_name = vp.EXPORTADOR
inner join items_unoee pds on pds.codigo='0000286'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000286','002')
WHERE   (vp.PUERTO = 'SPRC' OR vp.PUERTO = 'CONTECAR' OR vp.PUERTO = 'COMPAS' OR vp.PUERTO LIKE '%BLOC%' OR vp.PUERTO = 'CTG') 
        AND (vp.STATUS_ID=3 or vp.STATUS_ID=9 or vp.STATUS_ID=10) AND vp.type_papper = "REF-CTN"
        AND (  DATE(vp.FECHA_REGISTROS) BETWEEN @FECHA_INF AND @FECHA_SUP  )
ORDER BY vp.EXPORTADOR ASC
); 



/*------------------- ADICIONALES CTG ISO 20 PAPEL KRAFT   -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    vp.LOTES AS LOTE,
    vp.adictional_layer_kraft_20 AS CANTIDAD_OPERATIVA,
    vp.TIPO_EMBALAJE AS TIPO_SACOS,
    vp.MODALIDAD AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.FECHA_REGISTROS AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.OIE AS OIE, 
    vp.BIC_CTN AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    vp.type_papper AS TIPO_PAPEL,
    vp.adictional_layer_kraft_20 AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.ESTADO AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    IF(vp.PUERTO="SPRC", "800200969", 
        IF(vp.PUERTO="CONTECAR", "800116164", 
            IF(vp.PUERTO="BLOCPORT", "NULL",             
                IF(vp.PUERTO="COMPAS", "900915647", "800200969")
            )
        )
    ) AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    concat("CAPAS ADICIONALES DE CONTENEDORES EN PUERTO DE CARTAGENA TERMINAL ",vp.PUERTO," TIPO KRAFT DE LONGITUD 20") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*vp.adictional_layer_kraft_20 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging2 AS vp 
INNER JOIN clients AS cts ON cts.business_name = vp.EXPORTADOR
inner join items_unoee pds on pds.codigo='0000156'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000156','002')
WHERE   (vp.PUERTO = 'SPRC' OR vp.PUERTO = 'CONTECAR' OR vp.PUERTO = 'COMPAS' OR vp.PUERTO LIKE '%BLOC%' OR vp.PUERTO = 'CTG') 
        AND (vp.iso_ctn LIKE '2%' ) AND vp.adictional_layer_kraft_20 != 0
        AND (vp.STATUS_ID=3 or vp.STATUS_ID=9 or vp.STATUS_ID=10)
        AND (  DATE(vp.FECHA_REGISTROS) BETWEEN @FECHA_INF AND @FECHA_SUP  )
ORDER BY vp.EXPORTADOR ASC
);


/*------------------- ADICIONALES CTG ISO 20 PAPEL CARTON   -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    vp.LOTES AS LOTE,
    vp.adictional_layer_carton_20 AS CANTIDAD_OPERATIVA,
    vp.TIPO_EMBALAJE AS TIPO_SACOS,
    vp.MODALIDAD AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.FECHA_REGISTROS AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.OIE AS OIE, 
    vp.BIC_CTN AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    vp.type_papper AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    vp.adictional_layer_carton_20 AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.ESTADO AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    IF(vp.PUERTO="SPRC", "800200969", 
        IF(vp.PUERTO="CONTECAR", "800116164", 
            IF(vp.PUERTO="BLOCPORT", "NULL",             
                IF(vp.PUERTO="COMPAS", "900915647", "800200969")
            )
        )
    ) AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    concat("CAPAS ADICIONALES DE CONTENEDORES EN PUERTO DE CARTAGENA TERMINAL ",vp.PUERTO," TIPO CARTON DE LONGITUD 20") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*vp.adictional_layer_carton_20 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging2 AS vp 
INNER JOIN clients AS cts ON cts.business_name = vp.EXPORTADOR
inner join items_unoee pds on pds.codigo='0000158'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000158','002')
WHERE   (vp.PUERTO = 'SPRC' OR vp.PUERTO = 'CONTECAR' OR vp.PUERTO = 'COMPAS' OR vp.PUERTO LIKE '%BLOC%' OR vp.PUERTO = 'CTG') 
        AND (vp.iso_ctn LIKE '2%' ) AND vp.adictional_layer_carton_20 != 0
        AND (vp.STATUS_ID=3 or vp.STATUS_ID=9 or vp.STATUS_ID=10)
        AND (  DATE(vp.FECHA_REGISTROS) BETWEEN @FECHA_INF AND @FECHA_SUP  )
ORDER BY vp.EXPORTADOR ASC
);


/*------------------- ADICIONALES CTG ISO 40 PAPEL KRAFT   -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    vp.LOTES AS LOTE,
    vp.adictional_layer_kraft_40 AS CANTIDAD_OPERATIVA,
    vp.TIPO_EMBALAJE AS TIPO_SACOS,
    vp.MODALIDAD AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.FECHA_REGISTROS AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.OIE AS OIE, 
    vp.BIC_CTN AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    vp.type_papper AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    vp.adictional_layer_kraft_40 AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.ESTADO AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    IF(vp.PUERTO="SPRC", "800200969", 
        IF(vp.PUERTO="CONTECAR", "800116164", 
            IF(vp.PUERTO="BLOCPORT", "NULL",             
                IF(vp.PUERTO="COMPAS", "900915647", "800200969")
            )
        )
    ) AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    concat("CAPAS ADICIONALES DE CONTENEDORES EN PUERTO DE CARTAGENA TERMINAL ",vp.PUERTO," TIPO KRAFT DE LONGITUD 40") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*vp.adictional_layer_kraft_40 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging2 AS vp 
INNER JOIN clients AS cts ON cts.business_name = vp.EXPORTADOR
inner join items_unoee pds on pds.codigo='0000176'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000176','002')
WHERE   (vp.PUERTO = 'SPRC' OR vp.PUERTO = 'CONTECAR' OR vp.PUERTO = 'COMPAS' OR vp.PUERTO LIKE '%BLOC%' OR vp.PUERTO = 'CTG') 
        AND (vp.iso_ctn LIKE '4%' ) AND vp.adictional_layer_kraft_40 != 0
        AND (vp.STATUS_ID=3 or vp.STATUS_ID=9 or vp.STATUS_ID=10)
        AND (  DATE(vp.FECHA_REGISTROS) BETWEEN @FECHA_INF AND @FECHA_SUP  )
ORDER BY vp.EXPORTADOR ASC
);


/*------------------- ADICIONALES CTG ISO 40 PAPEL CARTON   -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    vp.LOTES AS LOTE,
    vp.adictional_layer_carton_40 AS CANTIDAD_OPERATIVA,
    vp.TIPO_EMBALAJE AS TIPO_SACOS,
    vp.MODALIDAD AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.FECHA_REGISTROS AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.OIE AS OIE, 
    vp.BIC_CTN AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    vp.type_papper AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    vp.adictional_layer_carton_40 AS CAPAS_ADICIONALES_CARTON_40,
    vp.ESTADO AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    IF(vp.PUERTO="SPRC", "800200969", 
        IF(vp.PUERTO="CONTECAR", "800116164", 
            IF(vp.PUERTO="BLOCPORT", "NULL",             
                IF(vp.PUERTO="COMPAS", "900915647", "800200969")
            )
        )
    ) AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    concat("CAPAS ADICIONALES DE CONTENEDORES EN PUERTO DE CARTAGENA TERMINAL ",vp.PUERTO," TIPO CARTON DE LONGITUD 40") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*vp.adictional_layer_carton_40 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging2 AS vp 
INNER JOIN clients AS cts ON cts.business_name = vp.EXPORTADOR
inner join items_unoee pds on pds.codigo='0000178'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000178','002')
WHERE   (vp.PUERTO = 'SPRC' OR vp.PUERTO = 'CONTECAR' OR vp.PUERTO = 'COMPAS' OR vp.PUERTO LIKE '%BLOC%' OR vp.PUERTO = 'CTG') 
        AND (vp.iso_ctn LIKE '4%' ) AND vp.adictional_layer_carton_40 != 0
        AND (vp.STATUS_ID=3 or vp.STATUS_ID=9 or vp.STATUS_ID=10)
        AND (  DATE(vp.FECHA_REGISTROS) BETWEEN @FECHA_INF AND @FECHA_SUP  )
ORDER BY vp.EXPORTADOR ASC
);


/*-------------------  COLOCACION DE BOLSAS TSL ADICIONAL CTG    -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    vp.LOTES AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.TIPO_EMBALAJE AS TIPO_SACOS,
    vp.MODALIDAD AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.FECHA_REGISTROS AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.OIE AS OIE, 
    vp.BIC_CTN AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.ESTADO AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    IF(vp.PUERTO="SPRC", "800200969", 
        IF(vp.PUERTO="CONTECAR", "800116164", 
            IF(vp.PUERTO="BLOCPORT", "NULL",             
                IF(vp.PUERTO="COMPAS", "900915647", "800200969")
            )
        )
    ) AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    concat("COLOCACION DE BOLSAS TSL ADICIONAL AL CONTENEDOR EN PUERTO DE CARTAGENA TERMINAL ",vp.PUERTO,".") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging2 AS vp 
INNER JOIN clients AS cts ON cts.business_name = vp.EXPORTADOR
inner join items_unoee pds on pds.codigo='0000254'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000254','002')
WHERE   (vp.PUERTO = 'SPRC' OR vp.PUERTO = 'CONTECAR' OR vp.PUERTO = 'COMPAS' OR vp.PUERTO LIKE '%BLOC%' OR vp.PUERTO = 'CTG') 
        AND vp.tsl != 0  AND (vp.STATUS_ID=3 or vp.STATUS_ID=9 or vp.STATUS_ID=10)
        AND (  DATE(vp.FECHA_REGISTROS) BETWEEN @FECHA_INF AND @FECHA_SUP  )
ORDER BY vp.EXPORTADOR ASC
); 


/*-------------------  COLOCACION DE BOLSAS TSL CON DESECANTES CTG   -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    vp.LOTES AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.TIPO_EMBALAJE AS TIPO_SACOS,
    vp.MODALIDAD AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.FECHA_REGISTROS AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.OIE AS OIE, 
    vp.BIC_CTN AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.ESTADO AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    IF(vp.PUERTO="SPRC", "800200969", 
        IF(vp.PUERTO="CONTECAR", "800116164", 
            IF(vp.PUERTO="BLOCPORT", "NULL",             
                IF(vp.PUERTO="COMPAS", "900915647", "800200969")
            )
        )
    ) AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    concat("COLOCACION DE BOLSAS TSL CON DESECANTES ADICIONAL AL CONTENEDOR EN PUERTO DE CARTAGENA TERMINAL ",vp.PUERTO,".") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging2 AS vp 
INNER JOIN clients AS cts ON cts.business_name = vp.EXPORTADOR
inner join items_unoee pds on pds.codigo='0000284'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000284','002')
WHERE   (vp.PUERTO = 'SPRC' OR vp.PUERTO = 'CONTECAR' OR vp.PUERTO = 'COMPAS' OR vp.PUERTO LIKE '%BLOC%' OR vp.PUERTO = 'CTG') 
        AND vp.tls_desiccant != 0 AND (vp.STATUS_ID=3 or vp.STATUS_ID=9 or vp.STATUS_ID=10)
        AND (  DATE(vp.FECHA_REGISTROS) BETWEEN @FECHA_INF AND @FECHA_SUP  )
ORDER BY vp.EXPORTADOR ASC
); 


/*-------------------  INSTALACION HC CTG   -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    vp.LOTES AS LOTE,
    vp.hc_qta AS CANTIDAD_OPERATIVA,
    vp.TIPO_EMBALAJE AS TIPO_SACOS,
    vp.MODALIDAD AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.FECHA_REGISTROS AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.OIE AS OIE, 
    vp.BIC_CTN AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.ESTADO AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    IF(vp.PUERTO="SPRC", "800200969", 
        IF(vp.PUERTO="CONTECAR", "800116164", 
            IF(vp.PUERTO="BLOCPORT", "NULL",             
                IF(vp.PUERTO="COMPAS", "900915647", "800200969")
            )
        )
    ) AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    concat("INSTALACION HC ADICIONAL AL CONTENEDOR EN PUERTO DE CARTAGENA TERMINAL ",vp.PUERTO,".") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging2 AS vp 
INNER JOIN clients AS cts ON cts.business_name = vp.EXPORTADOR
inner join items_unoee pds on pds.codigo='0000271'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000271','002')
WHERE   (vp.PUERTO = 'SPRC' OR vp.PUERTO = 'CONTECAR' OR vp.PUERTO = 'COMPAS' OR vp.PUERTO LIKE '%BLOC%' OR vp.PUERTO = 'CTG') 
        AND vp.hc_qta != 0 AND (vp.STATUS_ID=3 or vp.STATUS_ID=9 or vp.STATUS_ID=10)
        AND (  DATE(vp.FECHA_REGISTROS) BETWEEN @FECHA_INF AND @FECHA_SUP  )
ORDER BY vp.EXPORTADOR ASC
); 



/*------------------- VACIADOS CONTENEDOR CTG TIPO SACOS -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    vp.LOTES AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    vp.TIPO_EMBALAJE AS TIPO_SACOS,
    vp.MODALIDAD AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    vp.FECHA_REGISTROS AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    vp.OIE AS OIE, 
    vp.BIC_CTN AS CONTENEDOR,
    vp.iso_ctn AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    vp.ESTADO AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    IF(vp.PUERTO="SPRC", "800200969", 
        IF(vp.PUERTO="CONTECAR", "800116164", 
            IF(vp.PUERTO="BLOCPORT", "NULL",             
                IF(vp.PUERTO="COMPAS", "900915647", "800200969")
            )
        )
    ) AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO,   
    concat("VACIADOS DE CONTENEDOR EN PUERTO DE CARTAGENA TERMINAL ",vp.PUERTO," TIPO SACOS") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM view_packaging2 AS vp 
INNER JOIN clients AS cts ON cts.business_name = vp.EXPORTADOR
inner join items_unoee pds on pds.codigo='0000134'
inner join list_prices_unoee lp on (lp.referencia,lp.id_lista)=('0000134','002')
WHERE   (vp.PUERTO = 'SPRC' OR vp.PUERTO = 'CONTECAR' OR vp.PUERTO = 'COMPAS' OR vp.PUERTO LIKE '%BLOC%' OR vp.PUERTO = 'CTG') 
        AND vp.MODALIDAD LIKE 'SACOS%' AND (vp.iso_ctn LIKE '2%' ) 
        AND (vp.STATUS_ID=4 or vp.STATUS_ID=9) 
        AND (  DATE(vp.FECHA_REGISTROS) BETWEEN @FECHA_INF AND @FECHA_SUP  )
    ORDER BY vp.EXPORTADOR ASC
); 







SELECT * FROM data_billing_full;


