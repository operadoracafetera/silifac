

SET @FECHA_INF=DATE("2021-04-12");
SET @FECHA_SUP=DATE("2021-04-30");

truncate data_billing_full;


/*------------------- TRAZABILIDAD VEHICULOS DESCARGADOS TERMINAL SPB EXPORTADOR RACAFE & CIA S.C.A  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rc.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat( cts.exporter_code,'-',rc.lot_caffee,' x ',rc.quantity_radicated_bag_in separator ' | ' ) AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    rc.created_date AS FECHA_OPERACION,
    tu.name AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "TRAZABILIDAD DE VEHICULOS DESCARGADOS PARA EL TERMINAL DE SPB DEL EXPORTADOR RACAFE & CIA S.C.A" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM remittances_caffee AS rc 
INNER JOIN clients AS cts ON cts.id = rc.client_id 
INNER JOIN type_units AS tu ON tu.id = rc.type_units_id
inner join items_unoee pds on pds.codigo='0000334'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000334','001')
WHERE rc.jetty=1 AND rc.client_id =6 
    AND (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP   )
group by rc.vehicle_plate,date(rc.created_date),rc.jetty
ORDER BY rc.created_date ASC 
); 



/*------------------- TRAZABILIDAD VEHICULOS DESCARGADOS TERMINAL CTG EXPORTADOR RACAFE & CIA S.C.A  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rc.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat( cts.exporter_code,'-',rc.lot_caffee,' x ',rc.quantity_radicated_bag_in separator ' | ' ) AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    rc.created_date AS FECHA_OPERACION,
    tu.name AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    concat("TRAZABILIDAD DE VEHICULOS DESCARGADOS PARA EL TERMINAL DE ",ss.name_place," DEL EXPORTADOR RACAFE & CIA S.C.A") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM remittances_caffee AS rc 
INNER JOIN clients AS cts ON cts.id = rc.client_id 
INNER JOIN type_units AS tu ON tu.id = rc.type_units_id
inner join slot_store ss on ss.id=rc.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000334'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000334','002')
WHERE (ss.ref_departament=2 OR ss.ref_departament=4 OR ss.ref_departament=5 OR ss.ref_departament=6 OR ss.ref_departament=9 ) AND rc.client_id =6 
    AND (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP   )
group by rc.vehicle_plate,date(rc.created_date),rc.slot_store_id
ORDER BY rc.created_date ASC 
); 



/*------------------- TRAZABILIDAD VEHICULOS DESCARGADOS TERMINAL STM EXPORTADOR RACAFE & CIA S.C.A  -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    rc.id AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat( cts.exporter_code,'-',rc.lot_caffee,' x ',rc.quantity_radicated_bag_in separator ' | ' ) AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    rc.vehicle_plate AS PLACA_VEHICULO,
    rc.created_date AS FECHA_OPERACION,
    tu.name AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    concat("TRAZABILIDAD DE VEHICULOS DESCARGADOS PARA EL TERMINAL DE ",ss.name_place," DEL EXPORTADOR RACAFE & CIA S.C.A") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM remittances_caffee AS rc 
INNER JOIN clients AS cts ON cts.id = rc.client_id 
INNER JOIN type_units AS tu ON tu.id = rc.type_units_id
inner join slot_store ss on ss.id=rc.slot_store_id
inner join departaments dep on ss.ref_departament=dep.id
inner join items_unoee pds on pds.codigo='0000334'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000334','003')
WHERE ss.ref_departament=3 AND rc.client_id =6 
    AND (  DATE(rc.created_date) BETWEEN @FECHA_INF AND @FECHA_SUP   )
group by rc.vehicle_plate,date(rc.created_date),rc.jetty
ORDER BY rc.created_date ASC 
); 



/*------------------- TRAZABILIDAD CONTENEDORES SPB EXPORTADOR RACAFE & CIA S.C.A -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-', rc.lot_caffee ,' x ',rc.quantity_radicated_bag_in separator ' | ') AS LOTES,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    ifn.packaging_mode AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    ivn.date_reg AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    ivn.id_tracking_siscafe  AS OIE,
    rdc.bic_container AS CONTENEDOR,
    pc.iso_ctn AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    ivn.ip_conection AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    concat("TRAZABILIDAD DE CONTENEDORES EMBALADOS PARA EL TERMINAL DE ",pc.jetty) AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM invoice_note as ivn
INNER JOIN clients AS cts ON cts.id = ivn.id_expo_pilcaf
INNER JOIN packaging_caffee AS pc ON pc.id = ivn.id_tracking_siscafe 
INNER JOIN ready_container AS rdc ON rdc.id = pc.ready_container_id
INNER JOIN detail_packaging_caffee AS dpc on dpc.packaging_caffee_id = ivn.id_tracking_siscafe
INNER JOIN info_navy AS ifn ON ifn.id = pc.info_navy_id
INNER JOIN remittances_caffee AS rc ON rc.id = dpc.remittances_caffee_id
inner join items_unoee pds on pds.codigo='0000334'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000334','001')
WHERE  ivn.type_invoice= 1 AND pc.jetty='BUN' AND cts.id=6 
        AND (  DATE(ivn.date_reg) BETWEEN @FECHA_INF AND @FECHA_SUP   )
group by ivn.id_tracking_siscafe,rc.client_id,date(ivn.date_reg), pc.jetty
); 



/*------------------- TRAZABILIDAD IER (INSPECTION AT THE TIME OF RECEIPT) SPB EXPORTADOR RACAFE & CIA S.C.A -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,    
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,  
    group_concat(cts.exporter_code,'-', rc.lot_caffee ,' x ',rc.quantity_radicated_bag_in separator ' | ') AS LOTES,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    ifn.packaging_mode AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    ivn.date_reg AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    ivn.id_tracking_siscafe  AS OIE,
    rdc.bic_container AS CONTENEDOR,
    pc.iso_ctn AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    ivn.ip_conection AS IP_CONEXION,
    (select cls.nit from clients cls where cls.id=ivn.id_expo_pilcaf) AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    concat("TRAZABILIDAD EIR (INSPECTION AT THE TIME OF RECEIPT) PARA EL TERMINAL DE SPB EXPORTADOR RACAFE & CIA S.C.A") AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM invoice_note as ivn
INNER JOIN clients AS cts ON cts.id = ivn.id_expo_pilcaf
INNER JOIN packaging_caffee AS pc ON pc.id = ivn.id_tracking_siscafe 
INNER JOIN ready_container AS rdc ON rdc.id = pc.ready_container_id
INNER JOIN detail_packaging_caffee AS dpc on dpc.packaging_caffee_id = ivn.id_tracking_siscafe
INNER JOIN info_navy AS ifn ON ifn.id = pc.info_navy_id
INNER JOIN remittances_caffee AS rc ON rc.id = dpc.remittances_caffee_id
inner join items_unoee pds on pds.codigo='0000334'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000334','001')
WHERE  ivn.type_invoice= 2 AND pc.jetty='BUN' AND cts.id=6 AND
    (  DATE(ivn.date_reg) BETWEEN @FECHA_INF AND @FECHA_SUP   )
group by ivn.id_tracking_siscafe,rc.client_id,date(ivn.date_reg), pc.jetty
); 


