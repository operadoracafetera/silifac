
SET @FECHA_INF=DATE("2021-03-31");
SET @FECHA_SUP=DATE("2021-04-11");
truncate data_billing_full;

/* --PARTICULARES spb--*/

/*-------------------13. FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL SPB SIN PRESENCIA ICA DE 1 A 25 SACOS POR LOTE -------------------*/
INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rcfs.remittances_caffee_id separator ' | ')  AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( (select group_concat(cls.exporter_code,'-',rm.lot_caffee) from remittances_caffee rm inner join clients cls on cls.id=rm.client_id where rm.id=rcfs.remittances_caffee_id) , ' x ' ,  '(',group_concat(rcfs.qta_coffee separator ' | '), ')'  ) AS LOTE,
    (select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    fs.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    st.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL DE SPB SIN PRESENCIA ICA DE 1 A 25 SACOS POR LOTE " AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
inner join remittances_caffee_has_fumigation_services rcfs on rcfs.fumigation_services_id=fs.id 
INNER JOIN remittances_caffee AS rm ON rcfs.remittances_caffee_id = rm.id
inner join hookup_status st on st.id=fs.hookup_status_id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join departaments dept on dept.id=fs.departament_id
inner join items_unoee pds on pds.codigo='0000302'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000302','001')
WHERE (fs.departament_id=1 OR fs.departament_id=7) AND fs.completed=1 AND fs.ica=0 AND rm.client_id !=1  
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by (select rm.lot_caffee from remittances_caffee rm where rm.id=rcfs.remittances_caffee_id ), fs.id 
having  sum(rcfs.qta_coffee)  between 1 and 25
order by fs.id DESC
); 



/*-------------------12. FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL SPB SIN PRESENCIA ICA DE 26 A 50 SACOS POR LOTE -------------------*/
INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rcfs.remittances_caffee_id separator ' | ')  AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( (select group_concat(cls.exporter_code,'-',rm.lot_caffee) from remittances_caffee rm inner join clients cls on cls.id=rm.client_id where rm.id=rcfs.remittances_caffee_id) , ' x ' ,  '(',group_concat(rcfs.qta_coffee separator ' | '), ')'  ) AS LOTE,
    (select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    fs.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    st.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL DE SPB SIN PRESENCIA ICA  DE 26 A 50 SACOS POR LOTE " AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
inner join remittances_caffee_has_fumigation_services rcfs on rcfs.fumigation_services_id=fs.id 
INNER JOIN remittances_caffee AS rm ON rcfs.remittances_caffee_id = rm.id
inner join hookup_status st on st.id=fs.hookup_status_id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join departaments dept on dept.id=fs.departament_id
inner join items_unoee pds on pds.codigo='0000304'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000304','001')
WHERE (fs.departament_id=1 OR fs.departament_id=7) AND fs.completed=1 AND fs.ica=0 AND rm.client_id !=1  
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by (select rm.lot_caffee from remittances_caffee rm where rm.id=rcfs.remittances_caffee_id ), fs.id 
having  sum(rcfs.qta_coffee)  between 26 and 50
order by fs.id DESC
); 


/*-------------------1. FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL SPB SIN PRESENCIA ICA DE 51 A 100 SACOS POR LOTE -------------------*/
INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rcfs.remittances_caffee_id separator ' | ')  AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( (select group_concat(cls.exporter_code,'-',rm.lot_caffee) from remittances_caffee rm inner join clients cls on cls.id=rm.client_id where rm.id=rcfs.remittances_caffee_id) , ' x ' ,  '(',group_concat(rcfs.qta_coffee separator ' | '), ')'  ) AS LOTE,
    (select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    fs.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    st.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL DE SPB SIN PRESENCIA ICA  DE 51 A 100 SACOS POR LOTE " AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
inner join remittances_caffee_has_fumigation_services rcfs on rcfs.fumigation_services_id=fs.id 
INNER JOIN remittances_caffee AS rm ON rcfs.remittances_caffee_id = rm.id
inner join hookup_status st on st.id=fs.hookup_status_id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join departaments dept on dept.id=fs.departament_id
inner join items_unoee pds on pds.codigo='0000306'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000306','001')
WHERE (fs.departament_id=1 OR fs.departament_id=7) AND fs.completed=1 AND fs.ica=0 AND rm.client_id !=1  
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by (select rm.lot_caffee from remittances_caffee rm where rm.id=rcfs.remittances_caffee_id ), fs.id 
having  sum(rcfs.qta_coffee)  between 51 and 100
order by fs.id DESC
); 


/*-------------------2. FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL SPB SIN PRESENCIA ICA DE 101 A 250 SACOS POR LOTE -------------------*/
INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rcfs.remittances_caffee_id separator ' | ')  AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( (select group_concat(cls.exporter_code,'-',rm.lot_caffee) from remittances_caffee rm inner join clients cls on cls.id=rm.client_id where rm.id=rcfs.remittances_caffee_id) , ' x ' ,  '(',group_concat(rcfs.qta_coffee separator ' | '), ')'  ) AS LOTE,
    (select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    fs.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    st.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL DE SPB SIN PRESENCIA ICA  DE 101 A 250 SACOS POR LOTE " AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
inner join remittances_caffee_has_fumigation_services rcfs on rcfs.fumigation_services_id=fs.id 
INNER JOIN remittances_caffee AS rm ON rcfs.remittances_caffee_id = rm.id
inner join hookup_status st on st.id=fs.hookup_status_id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join departaments dept on dept.id=fs.departament_id
inner join items_unoee pds on pds.codigo='0000307'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000307','001')
WHERE (fs.departament_id=1 OR fs.departament_id=7) AND fs.completed=1 AND fs.ica=0 AND rm.client_id !=1  
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by (select rm.lot_caffee from remittances_caffee rm where rm.id=rcfs.remittances_caffee_id ), fs.id 
having  sum(rcfs.qta_coffee)  between 101 and 250
order by fs.id DESC
); 

 

/*-------------------3. FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL SPB SIN PRESENCIA ICA MAYOR A 251 SACOS POR LOTE -------------------*/
INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rcfs.remittances_caffee_id separator ' | ')  AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( (select group_concat(cls.exporter_code,'-',rm.lot_caffee) from remittances_caffee rm inner join clients cls on cls.id=rm.client_id where rm.id=rcfs.remittances_caffee_id) , ' x ' ,  '(',group_concat(rcfs.qta_coffee separator ' | '), ')'  ) AS LOTE,
    (select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    fs.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    st.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL DE SPB SIN PRESENCIA ICA MAYOR A 251 SACOS POR LOTE " AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*(select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
inner join remittances_caffee_has_fumigation_services rcfs on rcfs.fumigation_services_id=fs.id 
INNER JOIN remittances_caffee AS rm ON rcfs.remittances_caffee_id = rm.id
inner join hookup_status st on st.id=fs.hookup_status_id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join departaments dept on dept.id=fs.departament_id
inner join items_unoee pds on pds.codigo='0000023'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000023','001')
WHERE (fs.departament_id=1 OR fs.departament_id=7) AND fs.completed=1 AND fs.ica=0 AND rm.client_id !=1  
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by (select rm.lot_caffee from remittances_caffee rm where rm.id=rcfs.remittances_caffee_id ), fs.id 
having  sum(rcfs.qta_coffee) > 250
order by fs.id DESC
); 



/*-----------4. HORAS DE ELEVADOR SERVICIO FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL SPB SIN PRESENCIA ICA -------------------*/
INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee, ' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    fs.hour_elevator AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "HORAS DE ELEVADOR SERVICIO DE FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL DE SPB SIN PRESENCIA ICA" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*fs.hour_elevator AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
INNER JOIN remittances_caffee_has_fumigation_services AS rfs ON fs.id = rfs.fumigation_services_id
INNER JOIN remittances_caffee AS rm ON rfs.remittances_caffee_id = rm.id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join items_unoee pds on pds.codigo='0000006'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000006','001')
WHERE (fs.departament_id=1 OR fs.departament_id=7) AND fs.completed=1 AND fs.ica=0 AND fs.hour_elevator !=0 AND rm.client_id !=1
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
GROUP BY fs.id
ORDER BY cts.exporter_code ASC
); 





/*-------------------14. FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL SPB CON PRESENCIA ICA DE 1 A 25 SACOS POR LOTE -------------------*/
INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rcfs.remittances_caffee_id separator ' | ')  AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( (select group_concat(cls.exporter_code,'-',rm.lot_caffee) from remittances_caffee rm inner join clients cls on cls.id=rm.client_id where rm.id=rcfs.remittances_caffee_id) , ' x ' ,  '(',group_concat(rcfs.qta_coffee separator ' | '), ')'  ) AS LOTE,
    (select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    fs.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    st.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL DE SPB CON PRESENCIA ICA  DE 1 A 25 SACOS POR LOTE " AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
inner join remittances_caffee_has_fumigation_services rcfs on rcfs.fumigation_services_id=fs.id 
INNER JOIN remittances_caffee AS rm ON rcfs.remittances_caffee_id = rm.id
inner join hookup_status st on st.id=fs.hookup_status_id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join departaments dept on dept.id=fs.departament_id
inner join items_unoee pds on pds.codigo='0000303'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000303','001')
WHERE (fs.departament_id=1 OR fs.departament_id=7) AND fs.completed=1 AND fs.ica=1 AND rm.client_id !=1  
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by (select rm.lot_caffee from remittances_caffee rm where rm.id=rcfs.remittances_caffee_id ), fs.id 
having  sum(rcfs.qta_coffee)  between 1 and 25
order by fs.id DESC
); 



/*-------------------14. FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL SPB CON PRESENCIA ICA DE 26 A 50 SACOS POR LOTE -------------------*/
INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rcfs.remittances_caffee_id separator ' | ')  AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( (select group_concat(cls.exporter_code,'-',rm.lot_caffee) from remittances_caffee rm inner join clients cls on cls.id=rm.client_id where rm.id=rcfs.remittances_caffee_id) , ' x ' ,  '(',group_concat(rcfs.qta_coffee separator ' | '), ')'  ) AS LOTE,
    (select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    fs.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    st.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL DE SPB CON PRESENCIA ICA  DE 26 A 50 SACOS POR LOTE " AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
inner join remittances_caffee_has_fumigation_services rcfs on rcfs.fumigation_services_id=fs.id 
INNER JOIN remittances_caffee AS rm ON rcfs.remittances_caffee_id = rm.id
inner join hookup_status st on st.id=fs.hookup_status_id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join departaments dept on dept.id=fs.departament_id
inner join items_unoee pds on pds.codigo='0000305'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000305','001')
WHERE (fs.departament_id=1 OR fs.departament_id=7) AND fs.completed=1 AND fs.ica=1 AND rm.client_id !=1  
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by (select rm.lot_caffee from remittances_caffee rm where rm.id=rcfs.remittances_caffee_id ), fs.id 
having  sum(rcfs.qta_coffee)  between 26 and 50
order by fs.id DESC
); 



/*-------------------5. FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL SPB CON PRESENCIA ICA DE 51 A 100 SACOS POR LOTE -------------------*/
INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rcfs.remittances_caffee_id separator ' | ')  AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( (select group_concat(cls.exporter_code,'-',rm.lot_caffee) from remittances_caffee rm inner join clients cls on cls.id=rm.client_id where rm.id=rcfs.remittances_caffee_id) , ' x ' ,  '(',group_concat(rcfs.qta_coffee separator ' | '), ')'  ) AS LOTE,
    (select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    fs.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    st.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL DE SPB CON PRESENCIA ICA  DE 51 A 100 SACOS POR LOTE " AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
inner join remittances_caffee_has_fumigation_services rcfs on rcfs.fumigation_services_id=fs.id 
INNER JOIN remittances_caffee AS rm ON rcfs.remittances_caffee_id = rm.id
inner join hookup_status st on st.id=fs.hookup_status_id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join departaments dept on dept.id=fs.departament_id
inner join items_unoee pds on pds.codigo='0000308'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000308','001')
WHERE (fs.departament_id=1 OR fs.departament_id=7) AND fs.completed=1 AND fs.ica=1 AND rm.client_id !=1  
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by (select rm.lot_caffee from remittances_caffee rm where rm.id=rcfs.remittances_caffee_id ), fs.id 
having  sum(rcfs.qta_coffee)  between 51 and 100
order by fs.id DESC
); 


/*-------------------6. FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL SPB CON PRESENCIA ICA DE 101 A 250 SACOS POR LOTE -------------------*/
INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rcfs.remittances_caffee_id separator ' | ')  AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( (select group_concat(cls.exporter_code,'-',rm.lot_caffee) from remittances_caffee rm inner join clients cls on cls.id=rm.client_id where rm.id=rcfs.remittances_caffee_id) , ' x ' ,  '(',group_concat(rcfs.qta_coffee separator ' | '), ')'  ) AS LOTE,
    (select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    fs.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    st.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL DE SPB CON PRESENCIA ICA DE 101 A 250 SACOS POR LOTE " AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
inner join remittances_caffee_has_fumigation_services rcfs on rcfs.fumigation_services_id=fs.id 
INNER JOIN remittances_caffee AS rm ON rcfs.remittances_caffee_id = rm.id
inner join hookup_status st on st.id=fs.hookup_status_id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join departaments dept on dept.id=fs.departament_id
inner join items_unoee pds on pds.codigo='0000309'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000309','001')
WHERE (fs.departament_id=1 OR fs.departament_id=7) AND fs.completed=1 AND fs.ica=1 AND rm.client_id !=1  
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by (select rm.lot_caffee from remittances_caffee rm where rm.id=rcfs.remittances_caffee_id ), fs.id 
having  sum(rcfs.qta_coffee)  between 101 and 250
order by fs.id DESC
); 

 

/*-------------------7. FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL SPB CON PRESENCIA ICA MAYOR A 251 SACOS POR LOTE -------------------*/
INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rcfs.remittances_caffee_id separator ' | ')  AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( (select group_concat(cls.exporter_code,'-',rm.lot_caffee) from remittances_caffee rm inner join clients cls on cls.id=rm.client_id where rm.id=rcfs.remittances_caffee_id) , ' x ' ,  '(',group_concat(rcfs.qta_coffee separator ' | '), ')'  ) AS LOTE,
    (select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    fs.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    st.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL DE SPB CON PRESENCIA ICA MAYOR A 251 SACOS POR LOTE " AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*(select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
inner join remittances_caffee_has_fumigation_services rcfs on rcfs.fumigation_services_id=fs.id 
INNER JOIN remittances_caffee AS rm ON rcfs.remittances_caffee_id = rm.id
inner join hookup_status st on st.id=fs.hookup_status_id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join departaments dept on dept.id=fs.departament_id
inner join items_unoee pds on pds.codigo='0000092'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000092','001')
WHERE (fs.departament_id=1 OR fs.departament_id=7) AND fs.completed=1 AND fs.ica=1 AND rm.client_id !=1  
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by (select rm.lot_caffee from remittances_caffee rm where rm.id=rcfs.remittances_caffee_id ), fs.id 
having  sum(rcfs.qta_coffee) > 250
order by fs.id DESC
); 



/*------------------- 8.RECOBRO FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL SPB CON PRESENCIA ICA HASTA 100 TON-------------------*/
INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee, ' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "RECOBRO FUMIGACION ICA PARTICULARES HASTA 100 TON" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
INNER JOIN remittances_caffee_has_fumigation_services AS rfs ON fs.id = rfs.fumigation_services_id
INNER JOIN remittances_caffee AS rm ON rfs.remittances_caffee_id = rm.id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join items_unoee pds on pds.codigo='0000091'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000091','001')
WHERE (fs.departament_id=1 OR fs.departament_id=7) AND fs.completed=1 AND fs.ica=1 AND rm.client_id !=1
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  ) AND (fs.qta_bags*70/1000) <= 100
GROUP BY fs.id
ORDER BY cts.exporter_code ASC
); 



/*------------------- 9. RECOBRO DE FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL SPB CON PRESENCIA ICA DE 100 HASTA 200 TON-------------------*/
INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee, ' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "RECOBRO FUMIGACION ICA DESDE 100 HASTA 200 TON" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
INNER JOIN remittances_caffee_has_fumigation_services AS rfs ON fs.id = rfs.fumigation_services_id
INNER JOIN remittances_caffee AS rm ON rfs.remittances_caffee_id = rm.id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join items_unoee pds on pds.codigo='0000301'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000301','001')
WHERE (fs.departament_id=1 OR fs.departament_id=7) AND fs.completed=1 AND fs.ica=1 AND rm.client_id !=1
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  ) AND ( (fs.qta_bags*70/1000) between 100 and 200)
GROUP BY fs.id
ORDER BY cts.exporter_code ASC
);




/*-------------------10. HORAS ELEVADOR PARA FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL SPB CON PRESENCIA ICA HASTA 100 TON-------------------*/
INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee, ' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    fs.hour_elevator AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "HORAS ELEVADOR DE FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL DE SPB CON PRESENCIA ICA HASTA 100 TON" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*fs.hour_elevator AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
INNER JOIN remittances_caffee_has_fumigation_services AS rfs ON fs.id = rfs.fumigation_services_id
INNER JOIN remittances_caffee AS rm ON rfs.remittances_caffee_id = rm.id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join items_unoee pds on pds.codigo='0000006'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000006','001')
WHERE (fs.departament_id=1 OR fs.departament_id=7) AND fs.completed=1 AND fs.ica=1 AND fs.hour_elevator !=0 AND rm.client_id !=1
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  ) AND (fs.qta_bags*70/1000) <= 100
GROUP BY fs.id
ORDER BY cts.exporter_code ASC
);



/*-------------------11. HORAS ELEVADOR PARA FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL SPB CON PRESENCIA ICA DE 100 HASTA 200 TON-------------------*/
INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee, ' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    fs.hour_elevator AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "HORAS ELEVADOR DE FUMIGACION ICA PARTICULAR DESDE 100 HASTA 200 TON" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*hour_elevator AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
INNER JOIN remittances_caffee_has_fumigation_services AS rfs ON fs.id = rfs.fumigation_services_id
INNER JOIN remittances_caffee AS rm ON rfs.remittances_caffee_id = rm.id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join items_unoee pds on pds.codigo='0000006'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000006','001')
WHERE (fs.departament_id=1 OR fs.departament_id=7) AND fs.completed=1 AND fs.ica=1 AND fs.hour_elevator !=0 AND rm.client_id !=1
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  ) AND ( (fs.qta_bags*70/1000) between 100 and 200)
GROUP BY fs.id
ORDER BY cts.exporter_code ASC
);




/* --FNC spb--*/

/*-------------------5. FUMIGACION DE CAFE PARA FNC EN TERMINAL SPB SIN PRESENCIA ICA -------------------*/
INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee, ' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    sum(rfs.qta_coffee) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA FNC EN TERMINAL DE SPB SIN PRESENCIA ICA" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*SUM(rfs.qta_coffee) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
INNER JOIN remittances_caffee_has_fumigation_services AS rfs ON fs.id = rfs.fumigation_services_id
INNER JOIN remittances_caffee AS rm ON rfs.remittances_caffee_id = rm.id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join items_unoee pds on pds.codigo='0000023'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000023','007')
WHERE (fs.departament_id=1 OR fs.departament_id=7) AND fs.completed=1 AND fs.ica=0 AND rm.client_id=1
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
GROUP BY fs.id
ORDER BY cts.exporter_code ASC
); 



/*-------------------6. HORAS DE ELEVADOR SERVICIO FUMIGACION DE CAFE PARA FNC EN TERMINAL SPB SIN PRESENCIA ICA -------------------*/
INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee, ' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    fs.hour_elevator AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "HORAS DE ELEVADOR SERVICIO DE FUMIGACION DE CAFE PARA FNC EN TERMINAL DE SPB SIN PRESENCIA ICA" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*fs.hour_elevator AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
INNER JOIN remittances_caffee_has_fumigation_services AS rfs ON fs.id = rfs.fumigation_services_id
INNER JOIN remittances_caffee AS rm ON rfs.remittances_caffee_id = rm.id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join items_unoee pds on pds.codigo='0000006'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000006','007')
WHERE (fs.departament_id=1 OR fs.departament_id=7) AND fs.completed=1 AND fs.ica=0 AND fs.hour_elevator !=0 AND rm.client_id=1
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
GROUP BY fs.id
ORDER BY cts.exporter_code ASC
); 



/*-------------------7. FUMIGACION DE CAFE PARA FNC EN TERMINAL SPB CON PRESENCIA ICA -------------------*/
INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee, ' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    sum(rfs.qta_coffee) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA FNC EN TERMINAL DE SPB CON PRESENCIA ICA" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*SUM(rfs.qta_coffee) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
INNER JOIN remittances_caffee_has_fumigation_services AS rfs ON fs.id = rfs.fumigation_services_id
INNER JOIN remittances_caffee AS rm ON rfs.remittances_caffee_id = rm.id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join items_unoee pds on pds.codigo='0000092'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000092','007')
WHERE (fs.departament_id=1 OR fs.departament_id=7) AND fs.completed=1 AND fs.ica=1 AND rm.client_id=1
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
GROUP BY fs.id
ORDER BY cts.exporter_code ASC
); 

/*-------------------2. RECOBRO FUMIGACION DE CAFE PARA FNC EN TERMINAL SPB CON PRESENCIA ICA HASTA 100 TON-------------------*/
INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee, ' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "RECOBRO FUMIGACION ICA FNC HASTA 100 TON" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
INNER JOIN remittances_caffee_has_fumigation_services AS rfs ON fs.id = rfs.fumigation_services_id
INNER JOIN remittances_caffee AS rm ON rfs.remittances_caffee_id = rm.id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join items_unoee pds on pds.codigo='0000091'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000091','007')
WHERE (fs.departament_id=1 OR fs.departament_id=7) AND fs.completed=1 AND fs.ica=1 AND rm.client_id=1
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  ) AND (fs.qta_bags*70/1000) <= 100
GROUP BY fs.id
ORDER BY cts.exporter_code ASC
); 


/*-------------------1. HORAS ELEVADOR PARA FUMIGACION DE CAFE PARA FNC EN TERMINAL SPB CON PRESENCIA ICA HASTA 100 TON-------------------*/
INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee, ' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    fs.hour_elevator AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "HORAS ELEVADOR DE FUMIGACION DE CAFE PARA FNC EN TERMINAL DE SPB CON PRESENCIA ICA" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*fs.hour_elevator AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
INNER JOIN remittances_caffee_has_fumigation_services AS rfs ON fs.id = rfs.fumigation_services_id
INNER JOIN remittances_caffee AS rm ON rfs.remittances_caffee_id = rm.id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join items_unoee pds on pds.codigo='0000006'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000006','007')
WHERE (fs.departament_id=1 OR fs.departament_id=7) AND fs.completed=1 AND fs.ica=1 AND fs.hour_elevator !=0 AND rm.client_id=1
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  ) AND (fs.qta_bags*70/1000) <= 100
GROUP BY fs.id
ORDER BY cts.exporter_code ASC
);


/*-------------------3. RECOBRO DE FUMIGACION DE CAFE PARA FNC EN TERMINAL SPB CON PRESENCIA ICA DE 100 HASTA 200 TON-------------------*/
INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee, ' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "RECOBRO FUMIGACION ICA FNC DESDE 100 HASTA 200 TON" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
INNER JOIN remittances_caffee_has_fumigation_services AS rfs ON fs.id = rfs.fumigation_services_id
INNER JOIN remittances_caffee AS rm ON rfs.remittances_caffee_id = rm.id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join items_unoee pds on pds.codigo='0000301'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000301','007')
WHERE (fs.departament_id=1 OR fs.departament_id=7) AND fs.completed=1 AND fs.ica=1 AND rm.client_id=1
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  ) AND ( (fs.qta_bags*70/1000) between 100 and 200)
GROUP BY fs.id
ORDER BY cts.exporter_code ASC
);

 

/*-------------------4. HORAS ELEVADOR PARA FUMIGACION DE CAFE PARA FNC EN TERMINAL SPB CON PRESENCIA ICA DE 100 HASTA 200 TON-------------------*/
INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee, ' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    fs.hour_elevator AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "001" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00102" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "HORAS ELEVADOR DE FUMIGACION ICA FNC DESDE 100 HASTA 200 TON" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*hour_elevator AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
INNER JOIN remittances_caffee_has_fumigation_services AS rfs ON fs.id = rfs.fumigation_services_id
INNER JOIN remittances_caffee AS rm ON rfs.remittances_caffee_id = rm.id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join items_unoee pds on pds.codigo='0000006'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000006','007')
WHERE (fs.departament_id=1 OR fs.departament_id=7) AND fs.completed=1 AND fs.ica=1 AND rm.client_id=1
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  ) AND ( (fs.qta_bags*70/1000) between 100 and 200)
GROUP BY fs.id
ORDER BY cts.exporter_code ASC
);













































/* --PARTICULARES SPIA--*/


/*-------------------12. FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL SPIA SIN PRESENCIA ICA DE 1 A 25 SACOS POR LOTE -------------------*/
INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rcfs.remittances_caffee_id separator ' | ')  AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( (select group_concat(cls.exporter_code,'-',rm.lot_caffee) from remittances_caffee rm inner join clients cls on cls.id=rm.client_id where rm.id=rcfs.remittances_caffee_id) , ' x ' ,  '(',group_concat(rcfs.qta_coffee separator ' | '), ')'  ) AS LOTE,
    (select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    fs.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    st.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL DE SPIA SIN PRESENCIA ICA  DE 1 A 25 SACOS POR LOTE " AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
inner join remittances_caffee_has_fumigation_services rcfs on rcfs.fumigation_services_id=fs.id 
INNER JOIN remittances_caffee AS rm ON rcfs.remittances_caffee_id = rm.id
inner join hookup_status st on st.id=fs.hookup_status_id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join departaments dept on dept.id=fs.departament_id
inner join items_unoee pds on pds.codigo='0000302'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000302','001')
WHERE (fs.departament_id=8) AND fs.completed=1 AND fs.ica=0 AND rm.client_id !=1  
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by (select rm.lot_caffee from remittances_caffee rm where rm.id=rcfs.remittances_caffee_id ), fs.id 
having  sum(rcfs.qta_coffee)  between 1 and 25
order by fs.id DESC
); 



/*-------------------13. FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL SPIA SIN PRESENCIA ICA DE 26 A 50 SACOS POR LOTE -------------------*/
INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rcfs.remittances_caffee_id separator ' | ')  AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( (select group_concat(cls.exporter_code,'-',rm.lot_caffee) from remittances_caffee rm inner join clients cls on cls.id=rm.client_id where rm.id=rcfs.remittances_caffee_id) , ' x ' ,  '(',group_concat(rcfs.qta_coffee separator ' | '), ')'  ) AS LOTE,
    (select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    fs.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    st.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL DE SPIA SIN PRESENCIA ICA  DE 26 A 50 SACOS POR LOTE " AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
inner join remittances_caffee_has_fumigation_services rcfs on rcfs.fumigation_services_id=fs.id 
INNER JOIN remittances_caffee AS rm ON rcfs.remittances_caffee_id = rm.id
inner join hookup_status st on st.id=fs.hookup_status_id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join departaments dept on dept.id=fs.departament_id
inner join items_unoee pds on pds.codigo='0000304'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000304','001')
WHERE (fs.departament_id=8) AND fs.completed=1 AND fs.ica=0 AND rm.client_id !=1  
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by (select rm.lot_caffee from remittances_caffee rm where rm.id=rcfs.remittances_caffee_id ), fs.id 
having  sum(rcfs.qta_coffee)  between 26 and 50
order by fs.id DESC
); 



/*-------------------1. FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL SPIA SIN PRESENCIA ICA DE 51 A 100 SACOS POR LOTE -------------------*/
INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rcfs.remittances_caffee_id separator ' | ')  AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( (select group_concat(cls.exporter_code,'-',rm.lot_caffee) from remittances_caffee rm inner join clients cls on cls.id=rm.client_id where rm.id=rcfs.remittances_caffee_id) , ' x ' ,  '(',group_concat(rcfs.qta_coffee separator ' | '), ')'  ) AS LOTE,
    (select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    fs.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    st.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL DE SPIA SIN PRESENCIA ICA DE 51 A 100 SACOS POR LOTE " AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
inner join remittances_caffee_has_fumigation_services rcfs on rcfs.fumigation_services_id=fs.id 
INNER JOIN remittances_caffee AS rm ON rcfs.remittances_caffee_id = rm.id
inner join hookup_status st on st.id=fs.hookup_status_id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join departaments dept on dept.id=fs.departament_id
inner join items_unoee pds on pds.codigo='0000306'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000306','001')
WHERE (fs.departament_id=8) AND fs.completed=1 AND fs.ica=0 AND rm.client_id !=1  
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by (select rm.lot_caffee from remittances_caffee rm where rm.id=rcfs.remittances_caffee_id ), fs.id 
having  sum(rcfs.qta_coffee)  between 51 and 100
order by fs.id DESC
); 


/*-------------------2. FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL SPIA SIN PRESENCIA ICA DE 101 A 250 SACOS POR LOTE -------------------*/
INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rcfs.remittances_caffee_id separator ' | ')  AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( (select group_concat(cls.exporter_code,'-',rm.lot_caffee) from remittances_caffee rm inner join clients cls on cls.id=rm.client_id where rm.id=rcfs.remittances_caffee_id) , ' x ' ,  '(',group_concat(rcfs.qta_coffee separator ' | '), ')'  ) AS LOTE,
    (select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    fs.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    st.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL DE SPIA SIN PRESENCIA ICA  DE 101 A 250 SACOS POR LOTE " AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
inner join remittances_caffee_has_fumigation_services rcfs on rcfs.fumigation_services_id=fs.id 
INNER JOIN remittances_caffee AS rm ON rcfs.remittances_caffee_id = rm.id
inner join hookup_status st on st.id=fs.hookup_status_id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join departaments dept on dept.id=fs.departament_id
inner join items_unoee pds on pds.codigo='0000307'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000307','001')
WHERE (fs.departament_id=8) AND fs.completed=1 AND fs.ica=0 AND rm.client_id !=1  
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by (select rm.lot_caffee from remittances_caffee rm where rm.id=rcfs.remittances_caffee_id ), fs.id 
having  sum(rcfs.qta_coffee)  between 101 and 250
order by fs.id DESC
); 

 

/*-------------------3. FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL SPIA SIN PRESENCIA ICA MAYOR A 251 SACOS POR LOTE -------------------*/
INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rcfs.remittances_caffee_id separator ' | ')  AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( (select group_concat(cls.exporter_code,'-',rm.lot_caffee) from remittances_caffee rm inner join clients cls on cls.id=rm.client_id where rm.id=rcfs.remittances_caffee_id) , ' x ' ,  '(',group_concat(rcfs.qta_coffee separator ' | '), ')'  ) AS LOTE,
    (select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    fs.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    st.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL DE SPIA SIN PRESENCIA ICA MAYOR A 251 SACOS POR LOTE " AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*(select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
inner join remittances_caffee_has_fumigation_services rcfs on rcfs.fumigation_services_id=fs.id 
INNER JOIN remittances_caffee AS rm ON rcfs.remittances_caffee_id = rm.id
inner join hookup_status st on st.id=fs.hookup_status_id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join departaments dept on dept.id=fs.departament_id
inner join items_unoee pds on pds.codigo='0000023'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000023','001')
WHERE (fs.departament_id=8) AND fs.completed=1 AND fs.ica=0 AND rm.client_id !=1  
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by (select rm.lot_caffee from remittances_caffee rm where rm.id=rcfs.remittances_caffee_id ), fs.id 
having  sum(rcfs.qta_coffee) > 250
order by fs.id DESC
); 



/*-----------4. HORAS DE ELEVADOR SERVICIO FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL SPIA SIN PRESENCIA ICA -------------------*/
INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee, ' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    fs.hour_elevator AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "HORAS DE ELEVADOR SERVICIO DE FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL DE SPIA SIN PRESENCIA ICA" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*fs.hour_elevator AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
INNER JOIN remittances_caffee_has_fumigation_services AS rfs ON fs.id = rfs.fumigation_services_id
INNER JOIN remittances_caffee AS rm ON rfs.remittances_caffee_id = rm.id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join items_unoee pds on pds.codigo='0000006'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000006','001')
WHERE (fs.departament_id=8) AND fs.completed=1 AND fs.ica=0 AND fs.hour_elevator !=0 AND rm.client_id !=1
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
GROUP BY fs.id
ORDER BY cts.exporter_code ASC
); 








/*-------------------14. FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL SPIA CON PRESENCIA ICA DE 1 A 25 SACOS POR LOTE -------------------*/
INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rcfs.remittances_caffee_id separator ' | ')  AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( (select group_concat(cls.exporter_code,'-',rm.lot_caffee) from remittances_caffee rm inner join clients cls on cls.id=rm.client_id where rm.id=rcfs.remittances_caffee_id) , ' x ' ,  '(',group_concat(rcfs.qta_coffee separator ' | '), ')'  ) AS LOTE,
    (select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    fs.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    st.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL DE SPIA CON PRESENCIA ICA  DE 1 A 25 SACOS POR LOTE " AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
inner join remittances_caffee_has_fumigation_services rcfs on rcfs.fumigation_services_id=fs.id 
INNER JOIN remittances_caffee AS rm ON rcfs.remittances_caffee_id = rm.id
inner join hookup_status st on st.id=fs.hookup_status_id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join departaments dept on dept.id=fs.departament_id
inner join items_unoee pds on pds.codigo='0000303'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000303','001')
WHERE (fs.departament_id=8) AND fs.completed=1 AND fs.ica=1 AND rm.client_id !=1  
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by (select rm.lot_caffee from remittances_caffee rm where rm.id=rcfs.remittances_caffee_id ), fs.id 
having  sum(rcfs.qta_coffee)  between 1 and 25
order by fs.id DESC
); 


/*-------------------15. FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL SPIA CON PRESENCIA ICA DE 26 A 50 SACOS POR LOTE -------------------*/
INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rcfs.remittances_caffee_id separator ' | ')  AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( (select group_concat(cls.exporter_code,'-',rm.lot_caffee) from remittances_caffee rm inner join clients cls on cls.id=rm.client_id where rm.id=rcfs.remittances_caffee_id) , ' x ' ,  '(',group_concat(rcfs.qta_coffee separator ' | '), ')'  ) AS LOTE,
    (select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    fs.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    st.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL DE SPIA CON PRESENCIA ICA  DE 26 A 50 SACOS POR LOTE " AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
inner join remittances_caffee_has_fumigation_services rcfs on rcfs.fumigation_services_id=fs.id 
INNER JOIN remittances_caffee AS rm ON rcfs.remittances_caffee_id = rm.id
inner join hookup_status st on st.id=fs.hookup_status_id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join departaments dept on dept.id=fs.departament_id
inner join items_unoee pds on pds.codigo='0000305'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000305','001')
WHERE (fs.departament_id=8) AND fs.completed=1 AND fs.ica=1 AND rm.client_id !=1  
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by (select rm.lot_caffee from remittances_caffee rm where rm.id=rcfs.remittances_caffee_id ), fs.id 
having  sum(rcfs.qta_coffee)  between 26 and 50
order by fs.id DESC
); 


/*-------------------5. FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL SPIA CON PRESENCIA ICA DE 51 A 100 SACOS POR LOTE -------------------*/
INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rcfs.remittances_caffee_id separator ' | ')  AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( (select group_concat(cls.exporter_code,'-',rm.lot_caffee) from remittances_caffee rm inner join clients cls on cls.id=rm.client_id where rm.id=rcfs.remittances_caffee_id) , ' x ' ,  '(',group_concat(rcfs.qta_coffee separator ' | '), ')'  ) AS LOTE,
    (select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    fs.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    st.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL DE SPIA CON PRESENCIA ICA  DE 51 A 100 SACOS POR LOTE " AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
inner join remittances_caffee_has_fumigation_services rcfs on rcfs.fumigation_services_id=fs.id 
INNER JOIN remittances_caffee AS rm ON rcfs.remittances_caffee_id = rm.id
inner join hookup_status st on st.id=fs.hookup_status_id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join departaments dept on dept.id=fs.departament_id
inner join items_unoee pds on pds.codigo='0000308'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000308','001')
WHERE (fs.departament_id=8) AND fs.completed=1 AND fs.ica=1 AND rm.client_id !=1  
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by (select rm.lot_caffee from remittances_caffee rm where rm.id=rcfs.remittances_caffee_id ), fs.id 
having  sum(rcfs.qta_coffee)  between 51 and 100
order by fs.id DESC
); 


/*-------------------6. FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL SPIA CON PRESENCIA ICA DE 101 A 250 SACOS POR LOTE -------------------*/
INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rcfs.remittances_caffee_id separator ' | ')  AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( (select group_concat(cls.exporter_code,'-',rm.lot_caffee) from remittances_caffee rm inner join clients cls on cls.id=rm.client_id where rm.id=rcfs.remittances_caffee_id) , ' x ' ,  '(',group_concat(rcfs.qta_coffee separator ' | '), ')'  ) AS LOTE,
    (select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    fs.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    st.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL DE SPIA CON PRESENCIA ICA DE 101 A 250 SACOS POR LOTE " AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
inner join remittances_caffee_has_fumigation_services rcfs on rcfs.fumigation_services_id=fs.id 
INNER JOIN remittances_caffee AS rm ON rcfs.remittances_caffee_id = rm.id
inner join hookup_status st on st.id=fs.hookup_status_id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join departaments dept on dept.id=fs.departament_id
inner join items_unoee pds on pds.codigo='0000309'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000309','001')
WHERE (fs.departament_id=8) AND fs.completed=1 AND fs.ica=1 AND rm.client_id !=1  
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by (select rm.lot_caffee from remittances_caffee rm where rm.id=rcfs.remittances_caffee_id ), fs.id 
having  sum(rcfs.qta_coffee)  between 101 and 250
order by fs.id DESC
); 

 

/*-------------------7. FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL SPIA CON PRESENCIA ICA MAYOR A 251 SACOS POR LOTE -------------------*/
INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rcfs.remittances_caffee_id separator ' | ')  AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( (select group_concat(cls.exporter_code,'-',rm.lot_caffee) from remittances_caffee rm inner join clients cls on cls.id=rm.client_id where rm.id=rcfs.remittances_caffee_id) , ' x ' ,  '(',group_concat(rcfs.qta_coffee separator ' | '), ')'  ) AS LOTE,
    (select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    fs.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    st.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL DE SPIA CON PRESENCIA ICA MAYOR A 251 SACOS POR LOTE " AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*(select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
inner join remittances_caffee_has_fumigation_services rcfs on rcfs.fumigation_services_id=fs.id 
INNER JOIN remittances_caffee AS rm ON rcfs.remittances_caffee_id = rm.id
inner join hookup_status st on st.id=fs.hookup_status_id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join departaments dept on dept.id=fs.departament_id
inner join items_unoee pds on pds.codigo='0000092'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000092','001')
WHERE (fs.departament_id=8) AND fs.completed=1 AND fs.ica=1 AND rm.client_id !=1  
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by (select rm.lot_caffee from remittances_caffee rm where rm.id=rcfs.remittances_caffee_id ), fs.id 
having  sum(rcfs.qta_coffee) > 250
order by fs.id DESC
); 



/*------------------- 8.RECOBRO FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL SPIA CON PRESENCIA ICA HASTA 100 TON-------------------*/
INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee, ' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "RECOBRO FUMIGACION ICA PARTICULARES HASTA 100 TON" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
INNER JOIN remittances_caffee_has_fumigation_services AS rfs ON fs.id = rfs.fumigation_services_id
INNER JOIN remittances_caffee AS rm ON rfs.remittances_caffee_id = rm.id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join items_unoee pds on pds.codigo='0000091'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000091','001')
WHERE (fs.departament_id=8) AND fs.completed=1 AND fs.ica=1 AND rm.client_id !=1
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  ) AND (fs.qta_bags*70/1000) <= 100
GROUP BY fs.id
ORDER BY cts.exporter_code ASC
); 



/*------------------- 9. RECOBRO DE FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL SPIA CON PRESENCIA ICA DE 100 HASTA 200 TON-------------------*/
INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee, ' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "RECOBRO FUMIGACION ICA DESDE 100 HASTA 200 TON" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
INNER JOIN remittances_caffee_has_fumigation_services AS rfs ON fs.id = rfs.fumigation_services_id
INNER JOIN remittances_caffee AS rm ON rfs.remittances_caffee_id = rm.id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join items_unoee pds on pds.codigo='0000301'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000301','001')
WHERE (fs.departament_id=8) AND fs.completed=1 AND fs.ica=1 AND rm.client_id !=1
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  ) AND ( (fs.qta_bags*70/1000) between 100 and 200)
GROUP BY fs.id
ORDER BY cts.exporter_code ASC
);




/*-------------------10. HORAS ELEVADOR PARA FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL SPIA CON PRESENCIA ICA HASTA 100 TON-------------------*/
INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee, ' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    fs.hour_elevator AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "HORAS ELEVADOR DE FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL DE SPIA CON PRESENCIA ICA HASTA 100 TON" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*fs.hour_elevator AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
INNER JOIN remittances_caffee_has_fumigation_services AS rfs ON fs.id = rfs.fumigation_services_id
INNER JOIN remittances_caffee AS rm ON rfs.remittances_caffee_id = rm.id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join items_unoee pds on pds.codigo='0000006'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000006','001')
WHERE (fs.departament_id=8) AND fs.completed=1 AND fs.ica=1 AND fs.hour_elevator !=0 AND rm.client_id !=1
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  ) AND (fs.qta_bags*70/1000) <= 100
GROUP BY fs.id
ORDER BY cts.exporter_code ASC
);



/*-------------------11. HORAS ELEVADOR PARA FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL SPIA CON PRESENCIA ICA DE 100 HASTA 200 TON-------------------*/
INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee, ' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    fs.hour_elevator AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "HORAS ELEVADOR DE FUMIGACION ICA PARTICULAR DESDE 100 HASTA 200 TON" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*hour_elevator AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
INNER JOIN remittances_caffee_has_fumigation_services AS rfs ON fs.id = rfs.fumigation_services_id
INNER JOIN remittances_caffee AS rm ON rfs.remittances_caffee_id = rm.id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join items_unoee pds on pds.codigo='0000006'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000006','001')
WHERE (fs.departament_id=8) AND fs.completed=1 AND fs.ica=1 AND fs.hour_elevator !=0 AND rm.client_id !=1
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  ) AND ( (fs.qta_bags*70/1000) between 100 and 200)
GROUP BY fs.id
ORDER BY cts.exporter_code ASC
);










/* --FNC SPIA--*/


/*-------------------5. FUMIGACION DE CAFE PARA FNC EN TERMINAL SPIA SIN PRESENCIA ICA -------------------*/
INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee, ' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    sum(rfs.qta_coffee) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA FNC EN TERMINAL DE SPIA SIN PRESENCIA ICA" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*SUM(rfs.qta_coffee) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
INNER JOIN remittances_caffee_has_fumigation_services AS rfs ON fs.id = rfs.fumigation_services_id
INNER JOIN remittances_caffee AS rm ON rfs.remittances_caffee_id = rm.id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join items_unoee pds on pds.codigo='0000023'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000023','007')
WHERE (fs.departament_id=8) AND fs.completed=1 AND fs.ica=0 AND rm.client_id=1
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
GROUP BY fs.id
ORDER BY cts.exporter_code ASC
); 



/*-------------------6. HORAS DE ELEVADOR SERVICIO FUMIGACION DE CAFE PARA FNC EN TERMINAL SPIA SIN PRESENCIA ICA -------------------*/
INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee, ' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    fs.hour_elevator AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "HORAS DE ELEVADOR SERVICIO DE FUMIGACION DE CAFE PARA FNC EN TERMINAL DE SPIA SIN PRESENCIA ICA" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*fs.hour_elevator AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
INNER JOIN remittances_caffee_has_fumigation_services AS rfs ON fs.id = rfs.fumigation_services_id
INNER JOIN remittances_caffee AS rm ON rfs.remittances_caffee_id = rm.id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join items_unoee pds on pds.codigo='0000006'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000006','007')
WHERE (fs.departament_id=8) AND fs.completed=1 AND fs.ica=0 AND fs.hour_elevator !=0 AND rm.client_id=1
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
GROUP BY fs.id
ORDER BY cts.exporter_code ASC
); 



/*-------------------7. FUMIGACION DE CAFE PARA FNC EN TERMINAL SPIA CON PRESENCIA ICA -------------------*/
INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee, ' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    sum(rfs.qta_coffee) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA FNC EN TERMINAL DE SPIA CON PRESENCIA ICA" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*SUM(rfs.qta_coffee) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
INNER JOIN remittances_caffee_has_fumigation_services AS rfs ON fs.id = rfs.fumigation_services_id
INNER JOIN remittances_caffee AS rm ON rfs.remittances_caffee_id = rm.id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join items_unoee pds on pds.codigo='0000092'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000092','007')
WHERE (fs.departament_id=8) AND fs.completed=1 AND fs.ica=1 AND rm.client_id=1
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
GROUP BY fs.id
ORDER BY cts.exporter_code ASC
); 

/*-------------------2. RECOBRO FUMIGACION DE CAFE PARA FNC EN TERMINAL SPIA CON PRESENCIA ICA HASTA 100 TON-------------------*/
INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee, ' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "RECOBRO FUMIGACION ICA FNC HASTA 100 TON" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
INNER JOIN remittances_caffee_has_fumigation_services AS rfs ON fs.id = rfs.fumigation_services_id
INNER JOIN remittances_caffee AS rm ON rfs.remittances_caffee_id = rm.id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join items_unoee pds on pds.codigo='0000091'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000091','007')
WHERE (fs.departament_id=8) AND fs.completed=1 AND fs.ica=1 AND rm.client_id=1
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  ) AND (fs.qta_bags*70/1000) <= 100
GROUP BY fs.id
ORDER BY cts.exporter_code ASC
); 


/*-------------------1. HORAS ELEVADOR PARA FUMIGACION DE CAFE PARA FNC EN TERMINAL SPIA CON PRESENCIA ICA HASTA 100 TON-------------------*/
INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee, ' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    fs.hour_elevator AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "HORAS ELEVADOR DE FUMIGACION DE CAFE PARA FNC EN TERMINAL DE SPIA CON PRESENCIA ICA" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*fs.hour_elevator AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
INNER JOIN remittances_caffee_has_fumigation_services AS rfs ON fs.id = rfs.fumigation_services_id
INNER JOIN remittances_caffee AS rm ON rfs.remittances_caffee_id = rm.id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join items_unoee pds on pds.codigo='0000006'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000006','007')
WHERE (fs.departament_id=8) AND fs.completed=1 AND fs.ica=1 AND fs.hour_elevator !=0 AND rm.client_id=1
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  ) AND (fs.qta_bags*70/1000) <= 100
GROUP BY fs.id
ORDER BY cts.exporter_code ASC
);


/*-------------------3. RECOBRO DE FUMIGACION DE CAFE PARA FNC EN TERMINAL SPIA CON PRESENCIA ICA DE 100 HASTA 200 TON-------------------*/
INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee, ' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    1 AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "RECOBRO FUMIGACION ICA FNC DESDE 100 HASTA 200 TON" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
INNER JOIN remittances_caffee_has_fumigation_services AS rfs ON fs.id = rfs.fumigation_services_id
INNER JOIN remittances_caffee AS rm ON rfs.remittances_caffee_id = rm.id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join items_unoee pds on pds.codigo='0000301'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000301','007')
WHERE (fs.departament_id=8) AND fs.completed=1 AND fs.ica=1 AND rm.client_id=1
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  ) AND ( (fs.qta_bags*70/1000) between 100 and 200)
GROUP BY fs.id
ORDER BY cts.exporter_code ASC
);

 

/*-------------------4. HORAS ELEVADOR PARA FUMIGACION DE CAFE PARA FNC EN TERMINAL SPIA CON PRESENCIA ICA DE 100 HASTA 200 TON-------------------*/
INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee, ' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    fs.hour_elevator AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    "004" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    "00105" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "HORAS ELEVADOR DE FUMIGACION ICA FNC DESDE 100 HASTA 200 TON" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*hour_elevator AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
INNER JOIN remittances_caffee_has_fumigation_services AS rfs ON fs.id = rfs.fumigation_services_id
INNER JOIN remittances_caffee AS rm ON rfs.remittances_caffee_id = rm.id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join items_unoee pds on pds.codigo='0000006'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000006','007')
WHERE (fs.departament_id=8) AND fs.completed=1 AND fs.ica=1 AND rm.client_id=1
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  ) AND ( (fs.qta_bags*70/1000) between 100 and 200)
GROUP BY fs.id
ORDER BY cts.exporter_code ASC
);



















/*----------------------------------------------------------------------------*/
/*-------------------------- SERVICIOS DE CONEXOS ----------------------------*/
/*----------------------------------------------------------------------------*/

/*------------------- CONEXOS PARTICULAR BUN -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat(cts.exporter_code,"-",sob.lot_caffee)  as LOTE,
    sob.qty AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    so.closed_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    so.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    hs.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    if(dp.lista_precio="006","004","001") AS CO_MOV,
    sob.tipo_invent AS CCOSTO_MOV,
    sob.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    if(dp.lista_precio="006","00105","00102")  AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    sob.items_unoee AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    sob.lista_precio AS LISTA_PRECIO, 
    CONCAT(sob.observation," (",dp.name,")" ) AS DESC_CONSULTA,
    sob.observation2 AS DESCRIPCION_PRODUCTTO_UNOEE,
    sob.value AS VALOR_FACTURA_UNOEE,
    sob.valor_unitario_rango AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
     NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from services_orders_billing sob
inner join services_orders so on so.id=sob.id_services_order
inner join departaments dp on dp.id=so.departaments_id
inner join clients AS cts on cts.id=so.exporter_code
inner join hookup_status hs on hs.id=so.hookup_status_id
where hs.id = 4 and dp.cod_ce_unoee = '001' and cts.id != 1 and sob.lot_caffee != 0 and sob.items_unoee!=0000021
        and (  DATE(so.closed_date) BETWEEN @FECHA_INF AND @FECHA_SUP )
order by so.closed_date desc
);


/*------------------- CONEXOS PARTICULAR BUN -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat(cts.exporter_code,"-",sob.lot_caffee)  as LOTE,
    sob.qty AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    so.closed_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    so.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    hs.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    if(dp.lista_precio="006","004","001") AS CO_MOV,
    sob.tipo_invent AS CCOSTO_MOV,
    sob.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    if(dp.lista_precio="006","00105","00104")  AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    sob.items_unoee AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    sob.lista_precio AS LISTA_PRECIO, 
    CONCAT(sob.observation," (",dp.name,")" ) AS DESC_CONSULTA,
    sob.observation2 AS DESCRIPCION_PRODUCTTO_UNOEE,
    sob.value AS VALOR_FACTURA_UNOEE,
    sob.valor_unitario_rango AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
     NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from services_orders_billing sob
inner join services_orders so on so.id=sob.id_services_order
inner join departaments dp on dp.id=so.departaments_id
inner join clients AS cts on cts.id=so.exporter_code
inner join hookup_status hs on hs.id=so.hookup_status_id
where hs.id = 4 and dp.cod_ce_unoee = '001' and cts.id != 1 and sob.lot_caffee != 0 and sob.items_unoee=0000021
        and (  DATE(so.closed_date) BETWEEN @FECHA_INF AND @FECHA_SUP )
order by so.closed_date desc
);




/*------------------- CONEXOS FNC BUN -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat(cts.exporter_code,"-",sob.lot_caffee)  as LOTE,
    sob.qty AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    so.closed_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    so.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    hs.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    if(dp.lista_precio="006","004","001") AS CO_MOV,
    sob.tipo_invent AS CCOSTO_MOV,
    sob.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    if(dp.lista_precio="006","00105","00102")  AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    sob.items_unoee AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    sob.lista_precio AS LISTA_PRECIO, 
    CONCAT(sob.observation," (",dp.name,")" ) AS DESC_CONSULTA,
    sob.observation2 AS DESCRIPCION_PRODUCTTO_UNOEE,
    sob.value AS VALOR_FACTURA_UNOEE,
    sob.valor_unitario_rango AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
     NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from services_orders_billing sob
inner join services_orders so on so.id=sob.id_services_order
inner join departaments dp on dp.id=so.departaments_id
inner join clients AS cts on cts.id=so.exporter_code
inner join hookup_status hs on hs.id=so.hookup_status_id
where hs.id = 4 and dp.cod_ce_unoee = '001' and cts.id = 1 and sob.lot_caffee != 0 and sob.items_unoee!=0000021
        and (  DATE(so.closed_date) BETWEEN @FECHA_INF AND @FECHA_SUP )
order by so.closed_date desc
);


/*------------------- CONEXOS FNC BUN -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat(cts.exporter_code,"-",sob.lot_caffee)  as LOTE,
    sob.qty AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    so.closed_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    so.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    hs.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    if(dp.lista_precio="006","004","001") AS CO_MOV,
    sob.tipo_invent AS CCOSTO_MOV,
    sob.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    if(dp.lista_precio="006","00105","00104")  AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    sob.items_unoee AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    sob.lista_precio AS LISTA_PRECIO, 
    CONCAT(sob.observation," (",dp.name,")" ) AS DESC_CONSULTA,
    sob.observation2 AS DESCRIPCION_PRODUCTTO_UNOEE,
    sob.value AS VALOR_FACTURA_UNOEE,
    sob.valor_unitario_rango AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
     NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from services_orders_billing sob
inner join services_orders so on so.id=sob.id_services_order
inner join departaments dp on dp.id=so.departaments_id
inner join clients AS cts on cts.id=so.exporter_code
inner join hookup_status hs on hs.id=so.hookup_status_id
where hs.id = 4 and dp.cod_ce_unoee = '001' and cts.id = 1 and sob.lot_caffee != 0 and sob.items_unoee=0000021
        and (  DATE(so.closed_date) BETWEEN @FECHA_INF AND @FECHA_SUP )
order by so.closed_date desc
);



/*------------------- CONEXOS PARTICULAR CTG -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat(cts.exporter_code,"-",sob.lot_caffee)  as LOTE,
    sob.qty AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    so.closed_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    so.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    hs.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    sob.tipo_invent AS CCOSTO_MOV,
    sob.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    sob.items_unoee AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    sob.lista_precio AS LISTA_PRECIO, 
    CONCAT(sob.observation," (",dp.name,")" ) AS DESC_CONSULTA,
    sob.observation2 AS DESCRIPCION_PRODUCTTO_UNOEE,
    sob.value AS VALOR_FACTURA_UNOEE,
    sob.valor_unitario_rango AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
     NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from services_orders_billing sob
inner join services_orders so on so.id=sob.id_services_order
inner join departaments dp on dp.id=so.departaments_id
inner join clients AS cts on cts.id=so.exporter_code
inner join hookup_status hs on hs.id=so.hookup_status_id
where hs.id = 4 and dp.cod_ce_unoee = '002' and cts.id != 1 and sob.lot_caffee != 0
        and (  DATE(so.closed_date) BETWEEN @FECHA_INF AND @FECHA_SUP )
order by so.closed_date desc
);



/*------------------- CONEXOS FNC CTG -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat(cts.exporter_code,"-",sob.lot_caffee)  as LOTE,
    sob.qty AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    so.closed_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    so.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    hs.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    sob.tipo_invent AS CCOSTO_MOV,
    sob.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    sob.items_unoee AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    sob.lista_precio AS LISTA_PRECIO, 
    CONCAT(sob.observation," (",dp.name,")" ) AS DESC_CONSULTA,
    sob.observation2 AS DESCRIPCION_PRODUCTTO_UNOEE,
    sob.value AS VALOR_FACTURA_UNOEE,
    sob.valor_unitario_rango AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
     NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from services_orders_billing sob
inner join services_orders so on so.id=sob.id_services_order
inner join departaments dp on dp.id=so.departaments_id
inner join clients AS cts on cts.id=so.exporter_code
inner join hookup_status hs on hs.id=so.hookup_status_id
where hs.id = 4 and dp.cod_ce_unoee = '002' and cts.id = 1 and sob.lot_caffee != 0
        and (  DATE(so.closed_date) BETWEEN @FECHA_INF AND @FECHA_SUP )
order by so.closed_date desc
);





/*------------------- CONEXOS PARTICULAR STM -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat(cts.exporter_code,"-",sob.lot_caffee)  as LOTE,
    sob.qty AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    so.closed_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    so.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    hs.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    sob.tipo_invent AS CCOSTO_MOV,
    sob.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    sob.items_unoee AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    sob.lista_precio AS LISTA_PRECIO, 
    CONCAT(sob.observation," (",dp.name,")" ) AS DESC_CONSULTA,
    sob.observation2 AS DESCRIPCION_PRODUCTTO_UNOEE,
    sob.value AS VALOR_FACTURA_UNOEE,
    sob.valor_unitario_rango AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
     NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from services_orders_billing sob
inner join services_orders so on so.id=sob.id_services_order
inner join departaments dp on dp.id=so.departaments_id
inner join clients AS cts on cts.id=so.exporter_code
inner join hookup_status hs on hs.id=so.hookup_status_id
where hs.id = 4 and dp.cod_ce_unoee = '003' and cts.id != 1 and sob.lot_caffee != 0
        and (  DATE(so.closed_date) BETWEEN @FECHA_INF AND @FECHA_SUP )
order by so.closed_date desc
);



/*------------------- CONEXOS FNC STM -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat(cts.exporter_code,"-",sob.lot_caffee)  as LOTE,
    sob.qty AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    so.closed_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    so.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    hs.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    sob.tipo_invent AS CCOSTO_MOV,
    sob.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00302" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    sob.items_unoee AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    sob.lista_precio AS LISTA_PRECIO, 
    CONCAT(sob.observation," (",dp.name,")" ) AS DESC_CONSULTA,
    sob.observation2 AS DESCRIPCION_PRODUCTTO_UNOEE,
    sob.value AS VALOR_FACTURA_UNOEE,
    sob.valor_unitario_rango AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
     NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from services_orders_billing sob
inner join services_orders so on so.id=sob.id_services_order
inner join departaments dp on dp.id=so.departaments_id
inner join clients AS cts on cts.id=so.exporter_code
inner join hookup_status hs on hs.id=so.hookup_status_id
where hs.id = 4 and dp.cod_ce_unoee = '003' and cts.id = 1 and sob.lot_caffee != 0
        and (  DATE(so.closed_date) BETWEEN @FECHA_INF AND @FECHA_SUP )
order by so.closed_date desc
);








/*----------------------------------------------------------------------------*/
/*-------------------------- SUMINISTROS DE CONEXOS ----------------------------*/
/*----------------------------------------------------------------------------*/

/*------------------- CONEXOS PARTICULAR BUN -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    NULL AS LOTE,
    sob.qty AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    so.closed_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    so.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    hs.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    if(dp.lista_precio="006","004","001") AS CO_MOV,
    sob.tipo_invent AS CCOSTO_MOV,
    sob.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    if(dp.lista_precio="006","00106","00104")  AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    sob.items_unoee AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    sob.lista_precio AS LISTA_PRECIO, 
    CONCAT(sob.observation2," (",dp.name,")" ) AS DESC_CONSULTA,
    sob.observation2 AS DESCRIPCION_PRODUCTTO_UNOEE,
    sob.value AS VALOR_FACTURA_UNOEE,
    sob.valor_unitario_rango AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
     NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from services_orders_billing sob
inner join services_orders so on so.id=sob.id_services_order
inner join departaments dp on dp.id=so.departaments_id
inner join clients AS cts on cts.id=so.exporter_code
inner join hookup_status hs on hs.id=so.hookup_status_id
where hs.id = 4 and dp.cod_ce_unoee = '001' and cts.id != 1 and sob.lot_caffee = 0
        and (  DATE(so.closed_date) BETWEEN @FECHA_INF AND @FECHA_SUP )
order by so.closed_date desc
);



/*------------------- CONEXOS FNC BUN -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    NULL AS LOTE,
    sob.qty AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    so.closed_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    so.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    hs.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "BFE" AS TIPO_DOC,
    "001" AS CO_DOC,
    if(dp.lista_precio="006","004","001") AS CO_MOV,
    sob.tipo_invent AS CCOSTO_MOV,
    sob.unidad_medida AS UND_ITEM,
    "001" AS SUCURSAL,
    if(dp.lista_precio="006","00106","00104")  AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    sob.items_unoee AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    sob.lista_precio AS LISTA_PRECIO, 
    CONCAT(sob.observation2," (",dp.name,")" ) AS DESC_CONSULTA,
    sob.observation2 AS DESCRIPCION_PRODUCTTO_UNOEE,
    sob.value AS VALOR_FACTURA_UNOEE,
    sob.valor_unitario_rango AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
     NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from services_orders_billing sob
inner join services_orders so on so.id=sob.id_services_order
inner join departaments dp on dp.id=so.departaments_id
inner join clients AS cts on cts.id=so.exporter_code
inner join hookup_status hs on hs.id=so.hookup_status_id
where hs.id = 4 and dp.cod_ce_unoee = '001' and cts.id = 1 and sob.lot_caffee = 0
        and (  DATE(so.closed_date) BETWEEN @FECHA_INF AND @FECHA_SUP )
order by so.closed_date desc
);



/*------------------- CONEXOS PARTICULAR CTG -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    NULL AS LOTE,
    sob.qty AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    so.closed_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    so.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    hs.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    sob.tipo_invent AS CCOSTO_MOV,
    sob.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00202" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    sob.items_unoee AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    sob.lista_precio AS LISTA_PRECIO, 
    CONCAT(sob.observation2," (",dp.name,")" ) AS DESC_CONSULTA,
    sob.observation2 AS DESCRIPCION_PRODUCTTO_UNOEE,
    sob.value AS VALOR_FACTURA_UNOEE,
    sob.valor_unitario_rango AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
     NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from services_orders_billing sob
inner join services_orders so on so.id=sob.id_services_order
inner join departaments dp on dp.id=so.departaments_id
inner join clients AS cts on cts.id=so.exporter_code
inner join hookup_status hs on hs.id=so.hookup_status_id
where hs.id = 4 and dp.cod_ce_unoee = '002' and cts.id != 1 and sob.lot_caffee = 0
        and (  DATE(so.closed_date) BETWEEN @FECHA_INF AND @FECHA_SUP )
order by so.closed_date desc
);



/*------------------- CONEXOS FNC CTG -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    NULL AS LOTE,
    sob.qty AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    so.closed_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    so.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    hs.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    sob.tipo_invent AS CCOSTO_MOV,
    sob.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00202" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    sob.items_unoee AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    sob.lista_precio AS LISTA_PRECIO, 
    CONCAT(sob.observation2," (",dp.name,")" ) AS DESC_CONSULTA,
    sob.observation2 AS DESCRIPCION_PRODUCTTO_UNOEE,
    sob.value AS VALOR_FACTURA_UNOEE,
    sob.valor_unitario_rango AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
     NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from services_orders_billing sob
inner join services_orders so on so.id=sob.id_services_order
inner join departaments dp on dp.id=so.departaments_id
inner join clients AS cts on cts.id=so.exporter_code
inner join hookup_status hs on hs.id=so.hookup_status_id
where hs.id = 4 and dp.cod_ce_unoee = '002' and cts.id = 1 and sob.lot_caffee = 0
        and (  DATE(so.closed_date) BETWEEN @FECHA_INF AND @FECHA_SUP )
order by so.closed_date desc
);





/*------------------- CONEXOS PARTICULAR STM -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    NULL AS LOTE,
    sob.qty AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    so.closed_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    so.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    hs.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    sob.tipo_invent AS CCOSTO_MOV,
    sob.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00301" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    sob.items_unoee AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    sob.lista_precio AS LISTA_PRECIO, 
    CONCAT(sob.observation2," (",dp.name,")" ) AS DESC_CONSULTA,
    sob.observation2 AS DESCRIPCION_PRODUCTTO_UNOEE,
    sob.value AS VALOR_FACTURA_UNOEE,
    sob.valor_unitario_rango AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
     NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from services_orders_billing sob
inner join services_orders so on so.id=sob.id_services_order
inner join departaments dp on dp.id=so.departaments_id
inner join clients AS cts on cts.id=so.exporter_code
inner join hookup_status hs on hs.id=so.hookup_status_id
where hs.id = 4 and dp.cod_ce_unoee = '003' and cts.id != 1 and sob.lot_caffee = 0
        and (  DATE(so.closed_date) BETWEEN @FECHA_INF AND @FECHA_SUP )
order by so.closed_date desc
);



/*------------------- CONEXOS FNC STM -------------------*/
INSERT INTO data_billing_full (
SELECT 
    NULL AS ID,
    NULL AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    NULL AS LOTE,
    sob.qty AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    so.closed_date AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    so.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    hs.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "SFE" AS TIPO_DOC,
    "003" AS CO_DOC,
    "003" AS CO_MOV,
    sob.tipo_invent AS CCOSTO_MOV,
    sob.unidad_medida AS UND_ITEM,
    "003" AS SUCURSAL,
    "00301" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    sob.items_unoee AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    sob.lista_precio AS LISTA_PRECIO, 
    CONCAT(sob.observation2," (",dp.name,")" ) AS DESC_CONSULTA,
    sob.observation2 AS DESCRIPCION_PRODUCTTO_UNOEE,
    sob.value AS VALOR_FACTURA_UNOEE,
    sob.valor_unitario_rango AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
     NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
from services_orders_billing sob
inner join services_orders so on so.id=sob.id_services_order
inner join departaments dp on dp.id=so.departaments_id
inner join clients AS cts on cts.id=so.exporter_code
inner join hookup_status hs on hs.id=so.hookup_status_id
where hs.id = 4 and dp.cod_ce_unoee = '003' and cts.id = 1 and sob.lot_caffee = 0
        and (  DATE(so.closed_date) BETWEEN @FECHA_INF AND @FECHA_SUP )
order by so.closed_date desc
);























































/* --PARTICULARES CTG--*/

/*-------------------13. FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL CTG SIN PRESENCIA ICA DE 1 A 25 SACOS POR LOTE -------------------*/
INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rcfs.remittances_caffee_id separator ' | ')  AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( (select group_concat(cls.exporter_code,'-',rm.lot_caffee) from remittances_caffee rm inner join clients cls on cls.id=rm.client_id where rm.id=rcfs.remittances_caffee_id) , ' x ' ,  '(',group_concat(rcfs.qta_coffee separator ' | '), ')'  ) AS LOTE,
    (select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    fs.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    st.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL DE CTG SIN PRESENCIA ICA DE 1 A 25 SACOS POR LOTE " AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
inner join remittances_caffee_has_fumigation_services rcfs on rcfs.fumigation_services_id=fs.id 
INNER JOIN remittances_caffee AS rm ON rcfs.remittances_caffee_id = rm.id
inner join hookup_status st on st.id=fs.hookup_status_id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join departaments dept on dept.id=fs.departament_id
inner join items_unoee pds on pds.codigo='0000302'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000302','002')
WHERE (fs.departament_id=2 OR fs.departament_id=4 OR fs.departament_id=5 OR fs.departament_id=6 OR fs.departament_id=9) AND fs.completed=1 AND fs.ica=0 AND rm.client_id !=1  
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by (select rm.lot_caffee from remittances_caffee rm where rm.id=rcfs.remittances_caffee_id ), fs.id 
having  sum(rcfs.qta_coffee)  between 1 and 25
order by fs.id DESC
); 



/*-------------------12. FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL CTG SIN PRESENCIA ICA DE 26 A 50 SACOS POR LOTE -------------------*/
INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rcfs.remittances_caffee_id separator ' | ')  AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( (select group_concat(cls.exporter_code,'-',rm.lot_caffee) from remittances_caffee rm inner join clients cls on cls.id=rm.client_id where rm.id=rcfs.remittances_caffee_id) , ' x ' ,  '(',group_concat(rcfs.qta_coffee separator ' | '), ')'  ) AS LOTE,
    (select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    fs.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    st.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL DE CTG SIN PRESENCIA ICA  DE 26 A 50 SACOS POR LOTE " AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
inner join remittances_caffee_has_fumigation_services rcfs on rcfs.fumigation_services_id=fs.id 
INNER JOIN remittances_caffee AS rm ON rcfs.remittances_caffee_id = rm.id
inner join hookup_status st on st.id=fs.hookup_status_id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join departaments dept on dept.id=fs.departament_id
inner join items_unoee pds on pds.codigo='0000304'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000304','002')
WHERE (fs.departament_id=2 OR fs.departament_id=4 OR fs.departament_id=5 OR fs.departament_id=6 OR fs.departament_id=9) AND fs.completed=1 AND fs.ica=0 AND rm.client_id !=1  
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by (select rm.lot_caffee from remittances_caffee rm where rm.id=rcfs.remittances_caffee_id ), fs.id 
having  sum(rcfs.qta_coffee)  between 26 and 50
order by fs.id DESC
); 


/*-------------------1. FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL CTG SIN PRESENCIA ICA DE 51 A 100 SACOS POR LOTE -------------------*/
INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rcfs.remittances_caffee_id separator ' | ')  AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( (select group_concat(cls.exporter_code,'-',rm.lot_caffee) from remittances_caffee rm inner join clients cls on cls.id=rm.client_id where rm.id=rcfs.remittances_caffee_id) , ' x ' ,  '(',group_concat(rcfs.qta_coffee separator ' | '), ')'  ) AS LOTE,
    (select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    fs.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    st.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL DE CTG SIN PRESENCIA ICA  DE 51 A 100 SACOS POR LOTE " AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
inner join remittances_caffee_has_fumigation_services rcfs on rcfs.fumigation_services_id=fs.id 
INNER JOIN remittances_caffee AS rm ON rcfs.remittances_caffee_id = rm.id
inner join hookup_status st on st.id=fs.hookup_status_id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join departaments dept on dept.id=fs.departament_id
inner join items_unoee pds on pds.codigo='0000306'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000306','002')
WHERE (fs.departament_id=2 OR fs.departament_id=4 OR fs.departament_id=5 OR fs.departament_id=6 OR fs.departament_id=9) AND fs.completed=1 AND fs.ica=0 AND rm.client_id !=1  
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by (select rm.lot_caffee from remittances_caffee rm where rm.id=rcfs.remittances_caffee_id ), fs.id 
having  sum(rcfs.qta_coffee)  between 51 and 100
order by fs.id DESC
); 


/*-------------------2. FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL CTG SIN PRESENCIA ICA DE 101 A 250 SACOS POR LOTE -------------------*/
INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rcfs.remittances_caffee_id separator ' | ')  AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( (select group_concat(cls.exporter_code,'-',rm.lot_caffee) from remittances_caffee rm inner join clients cls on cls.id=rm.client_id where rm.id=rcfs.remittances_caffee_id) , ' x ' ,  '(',group_concat(rcfs.qta_coffee separator ' | '), ')'  ) AS LOTE,
    (select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    fs.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    st.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL DE CTG SIN PRESENCIA ICA  DE 101 A 250 SACOS POR LOTE " AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
inner join remittances_caffee_has_fumigation_services rcfs on rcfs.fumigation_services_id=fs.id 
INNER JOIN remittances_caffee AS rm ON rcfs.remittances_caffee_id = rm.id
inner join hookup_status st on st.id=fs.hookup_status_id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join departaments dept on dept.id=fs.departament_id
inner join items_unoee pds on pds.codigo='0000307'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000307','002')
WHERE (fs.departament_id=2 OR fs.departament_id=4 OR fs.departament_id=5 OR fs.departament_id=6 OR fs.departament_id=9) AND fs.completed=1 AND fs.ica=0 AND rm.client_id !=1  
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by (select rm.lot_caffee from remittances_caffee rm where rm.id=rcfs.remittances_caffee_id ), fs.id 
having  sum(rcfs.qta_coffee)  between 101 and 250
order by fs.id DESC
); 

 

/*-------------------3. FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL CTG SIN PRESENCIA ICA MAYOR A 251 SACOS POR LOTE -------------------*/
INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rcfs.remittances_caffee_id separator ' | ')  AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( (select group_concat(cls.exporter_code,'-',rm.lot_caffee) from remittances_caffee rm inner join clients cls on cls.id=rm.client_id where rm.id=rcfs.remittances_caffee_id) , ' x ' ,  '(',group_concat(rcfs.qta_coffee separator ' | '), ')'  ) AS LOTE,
    (select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    fs.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    st.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL DE CTG SIN PRESENCIA ICA MAYOR A 251 SACOS POR LOTE " AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*(select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
inner join remittances_caffee_has_fumigation_services rcfs on rcfs.fumigation_services_id=fs.id 
INNER JOIN remittances_caffee AS rm ON rcfs.remittances_caffee_id = rm.id
inner join hookup_status st on st.id=fs.hookup_status_id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join departaments dept on dept.id=fs.departament_id
inner join items_unoee pds on pds.codigo='0000023'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000023','002')
WHERE (fs.departament_id=2 OR fs.departament_id=4 OR fs.departament_id=5 OR fs.departament_id=6 OR fs.departament_id=9) AND fs.completed=1 AND fs.ica=0 AND rm.client_id !=1  
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by (select rm.lot_caffee from remittances_caffee rm where rm.id=rcfs.remittances_caffee_id ), fs.id 
having  sum(rcfs.qta_coffee) > 250
order by fs.id DESC
); 



/*-----------4. HORAS DE ELEVADOR SERVICIO FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL CTG SIN PRESENCIA ICA -------------------*/
INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee, ' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    fs.hour_elevator AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "HORAS DE ELEVADOR SERVICIO DE FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL DE CTG SIN PRESENCIA ICA" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*fs.hour_elevator AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
INNER JOIN remittances_caffee_has_fumigation_services AS rfs ON fs.id = rfs.fumigation_services_id
INNER JOIN remittances_caffee AS rm ON rfs.remittances_caffee_id = rm.id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join items_unoee pds on pds.codigo='0000006'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000006','002')
WHERE (fs.departament_id=2 OR fs.departament_id=4 OR fs.departament_id=5 OR fs.departament_id=6 OR fs.departament_id=9) AND fs.completed=1 AND fs.ica=0 AND fs.hour_elevator !=0 AND rm.client_id !=1
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
GROUP BY fs.id
ORDER BY cts.exporter_code ASC
); 





/*-------------------14. FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL CTG CON PRESENCIA ICA DE 1 A 25 SACOS POR LOTE -------------------*/
INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rcfs.remittances_caffee_id separator ' | ')  AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( (select group_concat(cls.exporter_code,'-',rm.lot_caffee) from remittances_caffee rm inner join clients cls on cls.id=rm.client_id where rm.id=rcfs.remittances_caffee_id) , ' x ' ,  '(',group_concat(rcfs.qta_coffee separator ' | '), ')'  ) AS LOTE,
    (select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    fs.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    st.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL DE CTG CON PRESENCIA ICA  DE 1 A 25 SACOS POR LOTE " AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
inner join remittances_caffee_has_fumigation_services rcfs on rcfs.fumigation_services_id=fs.id 
INNER JOIN remittances_caffee AS rm ON rcfs.remittances_caffee_id = rm.id
inner join hookup_status st on st.id=fs.hookup_status_id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join departaments dept on dept.id=fs.departament_id
inner join items_unoee pds on pds.codigo='0000303'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000303','002')
WHERE (fs.departament_id=2 OR fs.departament_id=4 OR fs.departament_id=5 OR fs.departament_id=6 OR fs.departament_id=9) AND fs.completed=1 AND fs.ica=1 AND rm.client_id !=1  
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by (select rm.lot_caffee from remittances_caffee rm where rm.id=rcfs.remittances_caffee_id ), fs.id 
having  sum(rcfs.qta_coffee)  between 1 and 25
order by fs.id DESC
); 



/*-------------------14. FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL CTG CON PRESENCIA ICA DE 26 A 50 SACOS POR LOTE -------------------*/
INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rcfs.remittances_caffee_id separator ' | ')  AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( (select group_concat(cls.exporter_code,'-',rm.lot_caffee) from remittances_caffee rm inner join clients cls on cls.id=rm.client_id where rm.id=rcfs.remittances_caffee_id) , ' x ' ,  '(',group_concat(rcfs.qta_coffee separator ' | '), ')'  ) AS LOTE,
    (select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    fs.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    st.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL DE CTG CON PRESENCIA ICA  DE 26 A 50 SACOS POR LOTE " AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
inner join remittances_caffee_has_fumigation_services rcfs on rcfs.fumigation_services_id=fs.id 
INNER JOIN remittances_caffee AS rm ON rcfs.remittances_caffee_id = rm.id
inner join hookup_status st on st.id=fs.hookup_status_id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join departaments dept on dept.id=fs.departament_id
inner join items_unoee pds on pds.codigo='0000305'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000305','002')
WHERE (fs.departament_id=2 OR fs.departament_id=4 OR fs.departament_id=5 OR fs.departament_id=6 OR fs.departament_id=9) AND fs.completed=1 AND fs.ica=1 AND rm.client_id !=1  
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by (select rm.lot_caffee from remittances_caffee rm where rm.id=rcfs.remittances_caffee_id ), fs.id 
having  sum(rcfs.qta_coffee)  between 26 and 50
order by fs.id DESC
); 



/*-------------------5. FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL CTG CON PRESENCIA ICA DE 51 A 100 SACOS POR LOTE -------------------*/
INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rcfs.remittances_caffee_id separator ' | ')  AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( (select group_concat(cls.exporter_code,'-',rm.lot_caffee) from remittances_caffee rm inner join clients cls on cls.id=rm.client_id where rm.id=rcfs.remittances_caffee_id) , ' x ' ,  '(',group_concat(rcfs.qta_coffee separator ' | '), ')'  ) AS LOTE,
    (select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    fs.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    st.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL DE CTG CON PRESENCIA ICA  DE 51 A 100 SACOS POR LOTE " AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
inner join remittances_caffee_has_fumigation_services rcfs on rcfs.fumigation_services_id=fs.id 
INNER JOIN remittances_caffee AS rm ON rcfs.remittances_caffee_id = rm.id
inner join hookup_status st on st.id=fs.hookup_status_id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join departaments dept on dept.id=fs.departament_id
inner join items_unoee pds on pds.codigo='0000308'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000308','002')
WHERE (fs.departament_id=2 OR fs.departament_id=4 OR fs.departament_id=5 OR fs.departament_id=6 OR fs.departament_id=9) AND fs.completed=1 AND fs.ica=1 AND rm.client_id !=1  
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by (select rm.lot_caffee from remittances_caffee rm where rm.id=rcfs.remittances_caffee_id ), fs.id 
having  sum(rcfs.qta_coffee)  between 51 and 100
order by fs.id DESC
); 


/*-------------------6. FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL CTG CON PRESENCIA ICA DE 101 A 250 SACOS POR LOTE -------------------*/
INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rcfs.remittances_caffee_id separator ' | ')  AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( (select group_concat(cls.exporter_code,'-',rm.lot_caffee) from remittances_caffee rm inner join clients cls on cls.id=rm.client_id where rm.id=rcfs.remittances_caffee_id) , ' x ' ,  '(',group_concat(rcfs.qta_coffee separator ' | '), ')'  ) AS LOTE,
    (select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    fs.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    st.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL DE CTG CON PRESENCIA ICA DE 101 A 250 SACOS POR LOTE " AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*1 AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
inner join remittances_caffee_has_fumigation_services rcfs on rcfs.fumigation_services_id=fs.id 
INNER JOIN remittances_caffee AS rm ON rcfs.remittances_caffee_id = rm.id
inner join hookup_status st on st.id=fs.hookup_status_id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join departaments dept on dept.id=fs.departament_id
inner join items_unoee pds on pds.codigo='0000309'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000309','002')
WHERE (fs.departament_id=2 OR fs.departament_id=4 OR fs.departament_id=5 OR fs.departament_id=6 OR fs.departament_id=9) AND fs.completed=1 AND fs.ica=1 AND rm.client_id !=1  
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by (select rm.lot_caffee from remittances_caffee rm where rm.id=rcfs.remittances_caffee_id ), fs.id 
having  sum(rcfs.qta_coffee)  between 101 and 250
order by fs.id DESC
); 

 

/*-------------------7. FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL CTG CON PRESENCIA ICA MAYOR A 251 SACOS POR LOTE -------------------*/
INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rcfs.remittances_caffee_id separator ' | ')  AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    concat( (select group_concat(cls.exporter_code,'-',rm.lot_caffee) from remittances_caffee rm inner join clients cls on cls.id=rm.client_id where rm.id=rcfs.remittances_caffee_id) , ' x ' ,  '(',group_concat(rcfs.qta_coffee separator ' | '), ')'  ) AS LOTE,
    (select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    fs.id AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    st.status_name AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL DE CTG CON PRESENCIA ICA MAYOR A 251 SACOS POR LOTE " AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*(select sum( rm.quantity_radicated_bag_in ) from remittances_caffee_has_fumigation_services rcfs
            where rcfs.fumigation_services_id=fs.id  and rm.lot_caffee = (select rc.lot_caffee from remittances_caffee rc where rc.id=rcfs.remittances_caffee_id  ) and rm.id = rcfs.remittances_caffee_id
            group by (rm.lot_caffee ) limit 1 ) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
inner join remittances_caffee_has_fumigation_services rcfs on rcfs.fumigation_services_id=fs.id 
INNER JOIN remittances_caffee AS rm ON rcfs.remittances_caffee_id = rm.id
inner join hookup_status st on st.id=fs.hookup_status_id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join departaments dept on dept.id=fs.departament_id
inner join items_unoee pds on pds.codigo='0000092'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000092','002')
WHERE (fs.departament_id=2 OR fs.departament_id=4 OR fs.departament_id=5 OR fs.departament_id=6 OR fs.departament_id=9) AND fs.completed=1 AND fs.ica=1 AND rm.client_id !=1  
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
group by (select rm.lot_caffee from remittances_caffee rm where rm.id=rcfs.remittances_caffee_id ), fs.id 
having  sum(rcfs.qta_coffee) > 250
order by fs.id DESC
); 





/*-------------------10. HORAS ELEVADOR PARA FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL CTG CON PRESENCIA ICA HASTA 100 TON-------------------*/
INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee, ' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    fs.hour_elevator AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "HORAS ELEVADOR DE FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL DE CTG CON PRESENCIA ICA HASTA 100 TON" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*fs.hour_elevator AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
INNER JOIN remittances_caffee_has_fumigation_services AS rfs ON fs.id = rfs.fumigation_services_id
INNER JOIN remittances_caffee AS rm ON rfs.remittances_caffee_id = rm.id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join items_unoee pds on pds.codigo='0000006'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000006','002')
WHERE (fs.departament_id=2 OR fs.departament_id=4 OR fs.departament_id=5 OR fs.departament_id=6 OR fs.departament_id=9) AND fs.completed=1 AND fs.ica=1 AND fs.hour_elevator !=0 AND rm.client_id !=1
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  ) AND (fs.qta_bags*70/1000) <= 100
GROUP BY fs.id
ORDER BY cts.exporter_code ASC
);



/*-------------------11. HORAS ELEVADOR PARA FUMIGACION DE CAFE PARA PARTICULARES EN TERMINAL CTG CON PRESENCIA ICA DE 100 HASTA 200 TON-------------------*/
INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee, ' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    fs.hour_elevator AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "HORAS ELEVADOR DE FUMIGACION ICA PARTICULAR CTG DESDE 100 HASTA 200 TON" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*hour_elevator AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
INNER JOIN remittances_caffee_has_fumigation_services AS rfs ON fs.id = rfs.fumigation_services_id
INNER JOIN remittances_caffee AS rm ON rfs.remittances_caffee_id = rm.id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join items_unoee pds on pds.codigo='0000006'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000006','002')
WHERE (fs.departament_id=2 OR fs.departament_id=4 OR fs.departament_id=5 OR fs.departament_id=6 OR fs.departament_id=9) AND fs.completed=1 AND fs.ica=1 AND fs.hour_elevator !=0 AND rm.client_id !=1
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  ) AND ( (fs.qta_bags*70/1000) between 100 and 200)
GROUP BY fs.id
ORDER BY cts.exporter_code ASC
);




/* --FNC CTG--*/

/*-------------------5. FUMIGACION DE CAFE PARA FNC EN TERMINAL CTG SIN PRESENCIA ICA -------------------*/
INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee, ' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    sum(rfs.qta_coffee) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA FNC EN TERMINAL DE CTG SIN PRESENCIA ICA" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*SUM(rfs.qta_coffee) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
INNER JOIN remittances_caffee_has_fumigation_services AS rfs ON fs.id = rfs.fumigation_services_id
INNER JOIN remittances_caffee AS rm ON rfs.remittances_caffee_id = rm.id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join items_unoee pds on pds.codigo='0000023'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000023','007')
WHERE (fs.departament_id=2 OR fs.departament_id=4 OR fs.departament_id=5 OR fs.departament_id=6 OR fs.departament_id=9) AND fs.completed=1 AND fs.ica=0 AND rm.client_id=1
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
GROUP BY fs.id
ORDER BY cts.exporter_code ASC
); 



/*-------------------6. HORAS DE ELEVADOR SERVICIO FUMIGACION DE CAFE PARA FNC EN TERMINAL CTG SIN PRESENCIA ICA -------------------*/
INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee, ' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    fs.hour_elevator AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "HORAS DE ELEVADOR SERVICIO DE FUMIGACION DE CAFE PARA FNC EN TERMINAL DE CTG SIN PRESENCIA ICA" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*fs.hour_elevator AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
INNER JOIN remittances_caffee_has_fumigation_services AS rfs ON fs.id = rfs.fumigation_services_id
INNER JOIN remittances_caffee AS rm ON rfs.remittances_caffee_id = rm.id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join items_unoee pds on pds.codigo='0000006'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000006','007')
WHERE (fs.departament_id=2 OR fs.departament_id=4 OR fs.departament_id=5 OR fs.departament_id=6 OR fs.departament_id=9) AND fs.completed=1 AND fs.ica=0 AND fs.hour_elevator !=0 AND rm.client_id=1
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
GROUP BY fs.id
ORDER BY cts.exporter_code ASC
); 



/*-------------------7. FUMIGACION DE CAFE PARA FNC EN TERMINAL CTG CON PRESENCIA ICA -------------------*/
INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee, ' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    sum(rfs.qta_coffee) AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "SERVICIO DE FUMIGACION DE CAFE PARA FNC EN TERMINAL DE CTG CON PRESENCIA ICA" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*SUM(rfs.qta_coffee) AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
INNER JOIN remittances_caffee_has_fumigation_services AS rfs ON fs.id = rfs.fumigation_services_id
INNER JOIN remittances_caffee AS rm ON rfs.remittances_caffee_id = rm.id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join items_unoee pds on pds.codigo='0000092'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000092','007')
WHERE (fs.departament_id=2 OR fs.departament_id=4 OR fs.departament_id=5 OR fs.departament_id=6 OR fs.departament_id=9) AND fs.completed=1 AND fs.ica=1 AND rm.client_id=1
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  )
GROUP BY fs.id
ORDER BY cts.exporter_code ASC
); 



/*-------------------1. HORAS ELEVADOR PARA FUMIGACION DE CAFE PARA FNC EN TERMINAL CTG CON PRESENCIA ICA HASTA 100 TON-------------------*/
INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee, ' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    fs.hour_elevator AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "HORAS ELEVADOR DE FUMIGACION DE CAFE PARA FNC EN TERMINAL DE CTG CON PRESENCIA ICA" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*fs.hour_elevator AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
INNER JOIN remittances_caffee_has_fumigation_services AS rfs ON fs.id = rfs.fumigation_services_id
INNER JOIN remittances_caffee AS rm ON rfs.remittances_caffee_id = rm.id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join items_unoee pds on pds.codigo='0000006'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000006','007')
WHERE (fs.departament_id=2 OR fs.departament_id=4 OR fs.departament_id=5 OR fs.departament_id=6 OR fs.departament_id=9) AND fs.completed=1 AND fs.ica=1 AND fs.hour_elevator !=0 AND rm.client_id=1
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  ) AND (fs.qta_bags*70/1000) <= 100
GROUP BY fs.id
ORDER BY cts.exporter_code ASC
);


 

/*-------------------4. HORAS ELEVADOR PARA FUMIGACION DE CAFE PARA FNC EN TERMINAL CTG CON PRESENCIA ICA DE 100 HASTA 200 TON-------------------*/
INSERT INTO data_billing_full (
SELECT  
    NULL AS ID,
    group_concat(rm.id separator ' | ') AS REMESA,
    cts.id AS COD_EXPORTADOR_SISCAFE,
    cts.exporter_code AS CODIGO_EXPORTADOR,
    cts.business_name AS NOMBRE_EXPORTADOR,
    group_concat(cts.exporter_code,'-',rm.lot_caffee, ' x ',rm.quantity_radicated_bag_in separator ' | ') AS LOTE,
    fs.hour_elevator AS CANTIDAD_OPERATIVA,
    NULL AS TIPO_SACOS,
    NULL AS MODALIDAD,
    NULL AS PLACA_VEHICULO,
    DATE(fs.finished_date) AS FECHA_OPERACION,
    NULL AS UNIDAD_CAFE,
    NULL AS UNIDAD_FACTURA,
    NULL AS NUM_MICRO_LOTES,
    NULL AS OIE,
    NULL AS CONTENEDOR,
    NULL AS ISOCTN,
    NULL AS TIPO_PAPEL,
    NULL AS CAPAS_ADICIONALES_KRAFT_20,
    NULL AS CAPAS_ADICIONALES_CARTON_20,
    NULL AS CAPAS_ADICIONALES_KRAFT_40,
    NULL AS CAPAS_ADICIONALES_CARTON_40,
    NULL AS ESTADO,
    NULL AS MUESTRA,
    NULL AS EMPRESA_SUMINISTRO,
    NULL AS TIPO_ENVIO,
    "CFE" AS TIPO_DOC,
    "002" AS CO_DOC,
    "002" AS CO_MOV,
    pds.tipo_invent AS CCOSTO_MOV,
    pds.unidad_medida AS UND_ITEM,
    "002" AS SUCURSAL,
    "00201" AS BODEGA,
    NULL AS TIPO_MUESTRA,
    NULL AS IP_CONEXION,
    cts.nit AS CLIENTE_UNOERP,
    NULL AS CLIENTE_NOMBRE_UNOERP,
    lp.referencia AS ITEM_UNOERP,
    NULL AS NUM_FACTURA,
    lp.id_lista AS LISTA_PRECIO, 
    "HORAS ELEVADOR DE FUMIGACION ICA FNC CTG DESDE 100 HASTA 200 TON" AS DESC_CONSULTA,
    pds.descripcion AS DESCRIPCION_PRODUCTTO_UNOEE,
    lp.precio*hour_elevator AS VALOR_FACTURA_UNOEE,
    lp.precio AS VALOR_UNIDAD,
    concat(' Corte del ',@FECHA_INF ,' al ', @FECHA_SUP) AS FECHA_CORTE,
    NULL AS STATUS_TX_WS,
    NULL AS FECHA_TX_WS
FROM fumigation_services AS fs 
INNER JOIN remittances_caffee_has_fumigation_services AS rfs ON fs.id = rfs.fumigation_services_id
INNER JOIN remittances_caffee AS rm ON rfs.remittances_caffee_id = rm.id
INNER JOIN clients AS cts ON cts.id=rm.client_id
INNER JOIN units_caffee AS uc ON rm.units_cafee_id=uc.id
inner join bill AS b_rm on rm.bill_id=b_rm.id
inner join items_unoee pds on pds.codigo='0000006'
inner join list_prices_unoee lp  on (lp.referencia,lp.id_lista)=('0000006','007')
WHERE (fs.departament_id=2 OR fs.departament_id=4 OR fs.departament_id=5 OR fs.departament_id=6 OR fs.departament_id=9) AND fs.completed=1 AND fs.ica=1 AND rm.client_id=1
      AND (  DATE(fs.finished_date) BETWEEN @FECHA_INF AND @FECHA_SUP  ) AND ( (fs.qta_bags*70/1000) between 100 and 200)
GROUP BY fs.id
ORDER BY cts.exporter_code ASC
);



